#include <solarus/launcher/quests/update/QuestUpdater.h>

#include <QTemporaryDir>
#include <QFile>
#include <QVersionNumber>
#include <QDebug>
#include <QtConcurrent/QtConcurrent>
#include <QLoggingCategory>

#include <oclero/QtFileUtils.hpp>
#include <oclero/QtConnectionUtils.hpp>
#include <oclero/QtUpdater.hpp>

#include <utility>

namespace solarus::launcher::quests {
#define QUESTUPDATER_DEBUG
#ifdef QUESTUPDATER_DEBUG
Q_LOGGING_CATEGORY(LOGCAT_QUESTUPDATER, "solarus.launcher.QuestUpdater")
#endif

QuestUpdater::QuestUpdater(app::Settings& settings, LocalQuestListModel& localQuestListModel,
  OnlineQuestListModel& onlineQuestListModel, QObject* parent)
  : QObject(parent)
  , _settings(settings)
  , _localQuestListModel(localQuestListModel)
  , _onlineQuestListModel(onlineQuestListModel) {
  //  // When a LocalQuest or OnlineQuest has changed, refresh the local updates data.
  //  const auto onDataChanged = [this](const QModelIndex&, const QModelIndex&, const QVector<int>&) {
  //    refreshUpdates();
  //  };

  //  // When the models have fetched their data, refresh the local updates data.
  //  // NB: The handled signals are sent asynchronously.
  //  const auto onStateChanged = [this]() {
  //    if (_onlineQuestListModel.state() == OnlineQuestListModel::State::Idle
  //        && _localQuestListModel.state() == LocalQuestListModel::State::Idle) {
  //      refreshUpdates();
  //    }
  //  };

  // Connect to OnlineQuestListModel.
  //QObject::connect(&_onlineQuestListModel, &OnlineQuestListModel::questCountChanged, this, &QuestUpdater::refreshUpdates);
  //QObject::connect(&_onlineQuestListModel, &OnlineQuestListModel::dataChanged, this, onDataChanged);
  //QObject::connect(&_onlineQuestListModel, &OnlineQuestListModel::stateChanged, this, onStateChanged);

  // Connect to LocalQuestListModel.
  //QObject::connect(&_localQuestListModel, &LocalQuestListModel::questCountChanged, this, &QuestUpdater::refreshUpdates);
  //QObject::connect(&_localQuestListModel, &LocalQuestListModel::dataChanged, this, onStateChanged);
  //QObject::connect(&_localQuestListModel, &LocalQuestListModel::stateChanged, this, onStateChanged);

  // Connect to QFutureWatcher for asynchronous IO.
  QObject::connect(&_ioFutureWatcher, &QFutureWatcher<bool>::finished, this, [this]() {
    const auto installationResult = _ioFutureWatcher.result();
    // Mark quest as correctly updated or not.
    if (_updatingQuest) {
      if (installationResult) {
#ifdef QUESTUPDATER_DEBUG
        qCInfo(LOGCAT_QUESTUPDATER) << QString("Updated '%1' successfully!").arg(_updatingQuest->id());
#endif
        _updatingQuest->setErrorCode(static_cast<int>(oclero::QtDownloader::ErrorCode::NoError));
        removeQuestUpdate(_updatingQuest);
      } else {
#ifdef QUESTUPDATER_DEBUG
        qCInfo(LOGCAT_QUESTUPDATER) << QString("Error when installing '%1'.").arg(_updatingQuest->id());
#endif
        _updatingQuest->setErrorCode(100); // TODO
        _updatingQuest->setState(QuestUpdate::State::Error);
      }
    }

    // Next.
    dequeueAndHandleNext();
  });

  // Start checking for quest updates as soon as the local and online models are ready.
  oclero::singleShotConnect(&_localQuestListModel, &LocalQuestListModel::searchFinished, [this]() {
    checkForUpdates();
  });
  oclero::singleShotConnect(&_onlineQuestListModel, &OnlineQuestListModel::searchFinished, [this]() {
    checkForUpdates();
  });
}

QuestUpdater::~QuestUpdater() {}

const QVector<QuestUpdate*>& QuestUpdater::updates() const {
  return _updates;
}

int QuestUpdater::updateCount() const {
  return _updates.count();
}

QuestUpdater::State QuestUpdater::state() const {
  return _state;
}

QDateTime QuestUpdater::lastCheckTime() const {
  return _settings.questLastUpdateCheck();
}

void QuestUpdater::forceCheckForUpdates() {
  // Don't start checking if there are currently pending installations.
  if (!_pendingUpdates.empty() || _state != State::Idle)
    return;

  // Don't bother checking if the models are not initialized yet.
  if (_onlineQuestListModel.state() == OnlineQuestListModel::State::Loading)
    return;
  if (_localQuestListModel.state() == LocalQuestListModel::State::Loading)
    return;

  setState(State::CheckingForUpdates);

  // Listen to the signal that will be emitted when the quests are fetched asynchronously.
  oclero::singleShotConnect(&_onlineQuestListModel, &OnlineQuestListModel::searchFinished, [this]() {
    if (_onlineQuestListModel.state() == OnlineQuestListModel::State::Idle
        && _localQuestListModel.state() == LocalQuestListModel::State::Idle) {
      refreshUpdates();
    }
  });
  _onlineQuestListModel.startSearch();
}

void QuestUpdater::checkForUpdates() {
  if (shouldCheckForUpdates()) {
    forceCheckForUpdates();
  }
}

void QuestUpdater::refreshUpdates() {
  // Don't start checking if there are currently pending installations.
  if (!_pendingUpdates.empty() || _state == State::Updating || _state == State::WaitingForNextUpdate)
    return;

  // Don't bother checking if the models are not initialized yet.
  if (_onlineQuestListModel.state() != OnlineQuestListModel::State::Idle)
    return;
  if (_localQuestListModel.state() != LocalQuestListModel::State::Idle)
    return;

  setState(State::CheckingForUpdates);

  // Clear current data.
  for (auto* update : _updates) {
    QObject::disconnect(update);
    update->deleteLater();
  }
  _updates.clear();

  // Update settings.
  _settings.setQuestLastUpdateCheck(QDateTime::currentDateTime());

  // Get available updates.
  const auto& localQuests = _localQuestListModel.quests();
  for (auto* localQuest : localQuests) {
    // Find online quests with same id.
    const auto questId = localQuest->id();
    auto* onlineQuest = _onlineQuestListModel.questOfId(questId);
    if (!localQuest || !onlineQuest)
      continue;

    // Compare versions.
    const auto currentVersion = QVersionNumber::fromString(localQuest->version());
    const auto latestVersion = QVersionNumber::fromString(onlineQuest->version());
    const auto updateFound = !currentVersion.isNull() && !latestVersion.isNull() && currentVersion < latestVersion;
#ifdef QUESTUPDATER_DEBUG
    qCInfo(LOGCAT_QUESTUPDATER) << QString("Checking update for '%1'... Current: %2 / Latest: %3. Update: %4.")
                                     .arg(questId)
                                     .arg(currentVersion.toString())
                                     .arg(latestVersion.toString())
                                     .arg(updateFound ? "YES" : "NO");
#endif

    if (!updateFound)
      continue;

    // Add to available updates.
    auto* update = new QuestUpdate(localQuest, onlineQuest, this);
    QObject::connect(update, &QuestUpdate::stateChanged, this, [this, update]() {
      emit updateChanged(update);
    });
    QObject::connect(update, &QuestUpdate::titleChanged, this, [this, update]() {
      emit updateChanged(update);
    });
    _updates.append(update);
  }

  emit updatesChanged();

  setState(State::Idle);
}

void QuestUpdater::updateQuest(QuestUpdate* update) {
  if (!update)
    return;

  const auto questId = update->id();
  if (!_pendingUpdates.contains(questId)) {
    update->setState(QuestUpdate::State::Pending);
    _pendingUpdates.enqueue(questId);
    handleNextPendingUpdate();
  }
}

void QuestUpdater::updateQuest(const QString& questId) {
  updateQuest(updateOfId(questId));
}

void QuestUpdater::cancelQuestUpdate(QuestUpdate* update) {
  if (!update)
    return;

  if (_updatingQuest == update) {
    // Cancel current task.
    if (update->state() == QuestUpdate::State::Downloading) {
      _downloader.cancel();
    } else if (update->state() == QuestUpdate::State::Installing) {
      _ioFutureWatcher.cancel();
    }
    update->setState(QuestUpdate::State::Available);

    // Update next quest.
    dequeueAndHandleNext();
  } else if (update->state() == QuestUpdate::State::Pending) {
    // Remove from pending.
    _pendingUpdates.removeAll(update->id());
    update->setState(QuestUpdate::State::Available);
  }
}

int QuestUpdater::questUpdateIndex(const QuestUpdate* update) const {
  if (!update)
    return -1;

  const auto it = std::find_if(_updates.begin(), _updates.end(), [update](const QuestUpdate* other) {
    return update == other;
  });
  const auto index = it == _updates.end() ? -1 : std::distance(_updates.begin(), it);
  return index;
}

int QuestUpdater::questUpdatePendingIndex(const QuestUpdate* update) const {
  if (!update)
    return -1;

  const auto it = std::find_if(_pendingUpdates.begin(), _pendingUpdates.end(), [update](const QString& questId) {
    return update->id() == questId;
  });
  const auto index = it == _pendingUpdates.end() ? -1 : std::distance(_pendingUpdates.begin(), it);
  return index;
}

QuestUpdate* QuestUpdater::updateOfId(const QString& questId) const {
  const auto it = std::find_if(_updates.begin(), _updates.end(), [this, &questId](const QuestUpdate* update) {
    return update->id() == questId;
    ;
  });
  return it == _updates.end() ? nullptr : *it;
}

void QuestUpdater::setState(State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}

void QuestUpdater::dequeueAndHandleNext() {
  if (_pendingUpdates.empty())
    return;

  // Clear previous state.
  _pendingUpdates.dequeue();
  oclero::clearDirectoryContent(_tempDir.path());
  _updatingQuest.clear();

  // Continue handling the queue.
  setState(State::WaitingForNextUpdate);
  QTimer::singleShot(0, this, [this]() {
    handleNextPendingUpdate();
  });
}

void QuestUpdater::handleNextPendingUpdate() {
  if (_pendingUpdates.empty()) {
    // No more pending updates to handle.
    setState(State::Idle);

  } else if (_state == State::WaitingForNextUpdate || _state == State::Idle) {
    // Get temporary directory to download the file.
    if (!_tempDir.isValid()) {
      qDebug() << "Can't get temporary directory to download quest";
      return;
    }
    oclero::clearDirectoryContent(_tempDir.path());

    // Get next quest to update.
    _updatingQuest = updateOfId(_pendingUpdates.head());
    if (!_updatingQuest) {
      dequeueAndHandleNext();
      return;
    }

    setState(State::Updating);
    _updatingQuest->setState(QuestUpdate::State::Downloading);

    // Start downloading the file.
#ifdef QUESTUPDATER_DEBUG
    qCInfo(LOGCAT_QUESTUPDATER) << QString("Downloading update for '%1'...").arg(_updatingQuest->id());
#endif
    _downloader.downloadFile(
      _updatingQuest->downloadUrl(), _tempDir.path(),

      // onFinished
      [this](const oclero::QtDownloader::ErrorCode code, const QString& downloadedFilePath) {
        if (code == oclero::QtDownloader::ErrorCode::NoError) {
          _updatingQuest->setState(QuestUpdate::State::Installing);

          const auto currentFilePath = _updatingQuest->localPath();
          const auto questId = _updatingQuest->id();

          const auto future = QtConcurrent::run([currentFilePath, downloadedFilePath, questId]() {
#ifdef QUESTUPDATER_DEBUG
            qCInfo(LOGCAT_QUESTUPDATER) << QString("Installing update for '%1'...").arg(questId);
#endif
            QFile downloadedFile(downloadedFilePath);
            if (downloadedFile.exists()) {
              // Ensure it is possible to copy the downloaded file.
              QFileInfo currentFile(currentFilePath);
              if (currentFile.exists()) {
                // Remove current file if it already exists.
                const auto removeResult = QFile(currentFile.absoluteFilePath()).remove();
                if (!removeResult) {
                  return false;
                }
              } else {
                // Create necessary directories.
                const auto pathCreationResult = currentFile.dir().mkpath(QStringLiteral("."));
                if (!pathCreationResult) {
                  return false;
                }
              }

              // Copy new file.
              const auto newFilePath = currentFile.absolutePath() + "/" + questId + ".solarus";
              const auto result = downloadedFile.copy(newFilePath);
              return result;
            }
            return false;
          });
          _ioFutureWatcher.setFuture(future);
        } else if (code == oclero::QtDownloader::ErrorCode::Cancelled) {
          // Just mark update as available again.
          _updatingQuest->setErrorCode(0);
          _updatingQuest->setState(QuestUpdate::State::Available);

          // Next.
          dequeueAndHandleNext();
        } else {
          // Mark update as failure.
          _updatingQuest->setErrorCode(static_cast<int>(code));
          _updatingQuest->setState(QuestUpdate::State::Error);

          // Next.
          dequeueAndHandleNext();
        }
      },

      // onProgress
      [this](const int progress) {
        if (_updatingQuest) {
          _updatingQuest->setProgress(progress);
        }
      });
  }
}

void QuestUpdater::removeQuestUpdate(QuestUpdate* update) {
  if (!update)
    return;

  const auto index = questUpdateIndex(update);
  update->setState(QuestUpdate::State::Updated);
  _updates.removeAll(update);
  update->deleteLater();

  emit updateRemoved(update, index);
}

bool QuestUpdater::shouldCheckForUpdates() const {
  const auto lastCheckTime = _settings.questLastUpdateCheck();
  const auto frequency = _settings.questUpdateCheckFrequency();
  auto shouldCheckForUpdate = true;

  if (lastCheckTime.isValid()) {
    const auto currentTime = QDateTime::currentDateTime();
    QDateTime comparisonTime;
    using Frequency = oclero::QtUpdater::Frequency;
    switch (frequency) {
      case Frequency::EveryStart:
        comparisonTime = lastCheckTime;
        break;
      case Frequency::EveryHour:
        comparisonTime = lastCheckTime.addSecs(3600);
        break;
      case Frequency::EveryDay:
        comparisonTime = lastCheckTime.addDays(1);
        break;
      case Frequency::EveryWeek:
        comparisonTime = lastCheckTime.addDays(7);
        break;
      case Frequency::EveryTwoWeeks:
        comparisonTime = lastCheckTime.addDays(14);
        break;
      case Frequency::EveryMonth:
        comparisonTime = lastCheckTime.addMonths(1);
        break;
      default:
        break;
    }
    shouldCheckForUpdate = comparisonTime < currentTime;
  }

  return shouldCheckForUpdate;
}
} // namespace solarus::launcher::quests
