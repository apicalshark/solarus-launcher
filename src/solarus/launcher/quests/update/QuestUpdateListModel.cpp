/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/update/QuestUpdateListModel.h>

#include <oclero/QtEnumUtils.hpp>

namespace solarus::launcher::quests {

namespace {
QuestUpdateListModel::UpdateSection mapStateToSection(QuestUpdate::State state) {
  switch (state) {
    case QuestUpdate::State::Available:
      return QuestUpdateListModel::UpdateSection::Available;
    case QuestUpdate::State::Downloading:
    case QuestUpdate::State::Installing:
      return QuestUpdateListModel::UpdateSection::Updating;
    case QuestUpdate::State::Pending:
      return QuestUpdateListModel::UpdateSection::Pending;
    case QuestUpdate::State::Error:
      return QuestUpdateListModel::UpdateSection::Error;
    default:
      return QuestUpdateListModel::UpdateSection::NoSection;
  }
}
} // namespace

QuestUpdateListModel::QuestUpdateListModel(QuestUpdater& questUpdater, QObject* parent)
  : QAbstractListModel(parent)
  , _questUpdater(questUpdater) {
  // Connect to QuestUpdater signals.
  QObject::connect(&_questUpdater, &QuestUpdater::stateChanged, this, &QuestUpdateListModel::stateChanged);

  QObject::connect(&_questUpdater, &QuestUpdater::updatesChanged, this, [this]() {
    beginResetModel();
    endResetModel();
    emit updateCountChanged();
  });

  QObject::connect(
    &_questUpdater, &QuestUpdater::updateRemoved, this, [this](const QuestUpdate* questUpdate, const int row) {
      beginRemoveRows({}, row, row);
      endRemoveRows();
      emit updateCountChanged();
    });

  QObject::connect(&_questUpdater, &QuestUpdater::updateChanged, this, [this](const QuestUpdate* questUpdate) {
    const auto row = _questUpdater.questUpdateIndex(questUpdate);
    if (row < 0 || row >= rowCount())
      return;

    const auto modelIndex = index(row);
    if (!modelIndex.isValid())
      return;

    emit dataChanged(modelIndex, modelIndex,
      {
        static_cast<int>(QuestUpdateRoles::QuestUpdate),
        static_cast<int>(QuestUpdateRoles::QuestUpdateState),
        static_cast<int>(QuestUpdateRoles::Title),
        static_cast<int>(QuestUpdateRoles::Section),
        static_cast<int>(QuestUpdateRoles::PendingOrder),
      });
  });
}

int QuestUpdateListModel::updateCount() const {
  return _questUpdater.updateCount();
}

QuestUpdateListModel::State QuestUpdateListModel::state() const {
  switch (_questUpdater.state()) {
    case QuestUpdater::State::CheckingForUpdates:
      return State::Loading;
    case QuestUpdater::State::Updating:
    case QuestUpdater::State::WaitingForNextUpdate:
      return State::UpdatingQuests;
    case QuestUpdater::State::Idle:
      return _questUpdater.updateCount() > 0 ? State::Idle : State::Uninitialized;
    default:
      return State::Uninitialized;
  }
}

int QuestUpdateListModel::rowCount(const QModelIndex& index) const {
  return updateCount();
}

QVariant QuestUpdateListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return {};

  if (index.row() >= rowCount())
    return {};

  const auto row = index.row();
  const auto questUpdates = _questUpdater.updates();

  auto* questUpdate = questUpdates.at(row);
  if (!questUpdate)
    return {};

  switch (role) {
    case static_cast<int>(QuestUpdateRoles::QuestUpdate):
      return QVariant::fromValue(questUpdate);
    case static_cast<int>(QuestUpdateRoles::QuestUpdateState):
      return QVariant::fromValue(questUpdate->state());
    case Qt::DisplayRole:
    case static_cast<int>(QuestUpdateRoles::Title):
      return QVariant::fromValue(questUpdate->title());
    case static_cast<int>(QuestUpdateRoles::PendingOrder):
      return QVariant::fromValue(_questUpdater.questUpdatePendingIndex(questUpdate));
    case static_cast<int>(QuestUpdateRoles::Section):
      return QVariant::fromValue(static_cast<int>(mapStateToSection(questUpdate->state())));
    case static_cast<int>(QuestUpdateRoles::Id):
      return QVariant::fromValue(questUpdate->id());
    default:
      return {};
  }
}

QHash<int, QByteArray> QuestUpdateListModel::roleNames() const {
  static const auto roles = QHash<int, QByteArray>{
    { static_cast<int>(QuestUpdateRoles::QuestUpdate), "questUpdate" },
    { static_cast<int>(QuestUpdateRoles::QuestUpdateState), "questUpdateState" },
    { static_cast<int>(QuestUpdateRoles::Title), "title" },
    { static_cast<int>(QuestUpdateRoles::PendingOrder), "pendingOrder" },
    { static_cast<int>(QuestUpdateRoles::Section), "section" },
  };
  return roles;
}
} // namespace solarus::launcher::quests
