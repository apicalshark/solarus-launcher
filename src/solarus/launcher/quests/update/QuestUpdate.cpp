/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/update/QuestUpdate.h>

namespace solarus::launcher::quests {
QuestUpdate::QuestUpdate(LocalQuest* localQuest, OnlineQuest* onlineQuest, QObject* parent)
  : QObject(parent)
  , _localQuest(localQuest)
  , _onlineQuest(onlineQuest) {
  if (localQuest) {
    QObject::connect(localQuest, &LocalQuest::idChanged, this, [this]() {
      emit idChanged();
      emit thumbnailPathChanged();
    });
    QObject::connect(localQuest, &LocalQuest::titleChanged, this, &QuestUpdate::titleChanged);
    QObject::connect(localQuest, &LocalQuest::versionChanged, this, &QuestUpdate::currentVersionChanged);
    QObject::connect(localQuest, &QObject::destroyed, this, [this]() {
      emit idChanged();
      emit titleChanged();
      emit thumbnailPathChanged();
      emit currentVersionChanged();
    });
  }

  if (onlineQuest) {
    QObject::connect(onlineQuest, &OnlineQuest::idChanged, this, &QuestUpdate::idChanged);
    QObject::connect(onlineQuest, &OnlineQuest::titleChanged, this, &QuestUpdate::titleChanged);
    QObject::connect(onlineQuest, &OnlineQuest::idChanged, this, &QuestUpdate::thumbnailPathChanged);
    QObject::connect(onlineQuest, &OnlineQuest::versionChanged, this, &QuestUpdate::latestVersionChanged);
    QObject::connect(onlineQuest, &OnlineQuest::latestReleaseDateChanged, this, &QuestUpdate::latestReleaseDateChanged);
    QObject::connect(onlineQuest, &QObject::destroyed, this, [this]() {
      emit idChanged();
      emit titleChanged();
      emit thumbnailPathChanged();
      emit latestVersionChanged();
      emit latestReleaseDateChanged();
    });
  }

  setState(State::Available);
}

QuestUpdate::~QuestUpdate() = default;

QuestUpdate::State QuestUpdate::state() const {
  return _state;
}

int QuestUpdate::progress() const {
  return _progress;
}

QString QuestUpdate::id() const {
  return _onlineQuest ? _onlineQuest->id() : (_localQuest ? _localQuest->id() : "");
}

QString QuestUpdate::title() const {
  return _onlineQuest ? _onlineQuest->title() : (_localQuest ? _localQuest->title() : "");
}

QString QuestUpdate::thumbnailPath() const {
  static const auto urlPattern = QString("image://quest/%1/thumbnail");
  return _onlineQuest ? _onlineQuest->thumbnail() : (_localQuest ? urlPattern.arg(_localQuest->id()) : "");
}

QString QuestUpdate::currentVersion() const {
  return _localQuest ? _localQuest->version() : "";
}

QString QuestUpdate::latestVersion() const {
  return _onlineQuest ? _onlineQuest->version() : "";
}

QDate QuestUpdate::latestReleaseDate() const {
  return _onlineQuest ? _onlineQuest->latestReleaseDate() : QDate{};
}

QString QuestUpdate::localPath() const {
  return _localQuest ? _localQuest->path() : "";
}

QUrl QuestUpdate::downloadUrl() const {
  return _onlineQuest ? _onlineQuest->url() : QUrl{};
}

int QuestUpdate::errorCode() const {
  return _errorCode;
}

void QuestUpdate::setState(State state) {
  if (state != _state) {
    _state = state;

    switch (_state) {
      case State::Available:
        if (_localQuest) {
          _localQuest->setHasUpdate(true);
        }
        if (_onlineQuest) {
          _onlineQuest->setHasUpdate(true);
        }
        break;
      case State::Pending:
        if (_localQuest) {
          _localQuest->setHasUpdate(true);
          _localQuest->setState(LocalQuest::State::Downloading);
        }
        if (_onlineQuest) {
          _onlineQuest->setHasUpdate(true);
          _onlineQuest->setState(OnlineQuest::State::Downloading);
        }
        break;
      case State::Downloading:
        if (_localQuest) {
          _localQuest->setState(LocalQuest::State::Downloading);
        }
        if (_onlineQuest) {
          _onlineQuest->setState(OnlineQuest::State::Downloading);
        }
        break;
      case State::Installing:
        if (_localQuest) {
          _localQuest->setState(LocalQuest::State::Downloading);
        }
        if (_onlineQuest) {
          _onlineQuest->setState(OnlineQuest::State::Downloading);
        }
        break;
      case State::Updated:
        if (_localQuest) {
          _localQuest->setHasUpdate(false);
          _localQuest->setState(LocalQuest::State::Idle);
        }
        if (_onlineQuest) {
          _onlineQuest->setHasUpdate(false);
          _onlineQuest->setState(OnlineQuest::State::Downloaded);
        }
        break;
      default:
        break;
    }

    emit stateChanged();
  }
}

void QuestUpdate::setProgress(int progress) {
  if (progress != _progress) {
    _progress = progress;
    if (_localQuest) {
      _localQuest->setDownloadProgress(progress);
    }
    if (_onlineQuest) {
      _onlineQuest->setDownloadProgress(progress);
    }
    emit progressChanged();
  }
}

void QuestUpdate::setErrorCode(int code) {
  if (code != _errorCode) {
    _errorCode = code;
    emit errorCodeChanged();
  }
}
} // namespace solarus::launcher::quests
