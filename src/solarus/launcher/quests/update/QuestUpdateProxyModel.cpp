/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/update/QuestUpdateProxyModel.h>

namespace solarus::launcher::quests {

QuestUpdateProxyModel::QuestUpdateProxyModel(
  QuestUpdateListModel& parentModel, QuestUpdateListModel::UpdateSection section, QObject* parent)
  : QSortFilterProxyModel(parent)
  , _parentModel(parentModel)
  , _section(section) {
  setSourceModel(&_parentModel);
  setDynamicSortFilter(true);
  setSortRole(static_cast<int>(QuestUpdateListModel::QuestUpdateRoles::Section));
  sort(0, Qt::AscendingOrder);
  setFilterRole(static_cast<int>(QuestUpdateListModel::QuestUpdateRoles::Section));
  invalidateFilter();
}

QuestUpdateProxyModel::~QuestUpdateProxyModel() = default;

QuestUpdateListModel::UpdateSection QuestUpdateProxyModel::section() const {
  return _section;
}

bool QuestUpdateProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const {
  using Role = QuestUpdateListModel::QuestUpdateRoles;
  {
    const auto leftSection = _parentModel.data(left, static_cast<int>(Role::Section)).toInt();
    const auto rightSection = _parentModel.data(right, static_cast<int>(Role::Section)).toInt();
    if (leftSection < rightSection)
      return true;
  }
  {
    const auto leftState = _parentModel.data(left, static_cast<int>(Role::QuestUpdateState)).toInt();
    const auto rightState = _parentModel.data(right, static_cast<int>(Role::QuestUpdateState)).toInt();
    if (leftState < rightState)
      return true;
  }
  {
    const auto leftPendingOrder = _parentModel.data(left, static_cast<int>(Role::PendingOrder)).toInt();
    const auto rightPendingOrder = _parentModel.data(right, static_cast<int>(Role::PendingOrder)).toInt();
    if (leftPendingOrder < rightPendingOrder)
      return true;
  }
  {
    const auto leftTitle = _parentModel.data(left, static_cast<int>(Role::Title)).toString();
    const auto rightTitle = _parentModel.data(right, static_cast<int>(Role::Title)).toString();
    if (leftTitle < rightTitle)
      return true;
  }
  {
    const auto leftId = _parentModel.data(left, static_cast<int>(Role::Id)).toString();
    const auto rightId = _parentModel.data(right, static_cast<int>(Role::Id)).toString();
    if (leftId < rightId)
      return true;
  }
  return false;
}

bool QuestUpdateProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
  const auto modelIndex = sourceModel()->index(sourceRow, 0, sourceParent);
  if (!modelIndex.isValid())
    return false;

  const auto section = modelIndex.data(static_cast<int>(QuestUpdateListModel::QuestUpdateRoles::Section))
                         .value<QuestUpdateListModel::UpdateSection>();
  return section == _section;
}
} // namespace solarus::launcher::quests
