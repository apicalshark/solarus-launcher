/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/base/Quest.h>

namespace solarus::launcher::quests {
Quest::Quest(QObject* parent)
  : QObject(parent) {}

Quest::~Quest() = default;

void Quest::notifyAllChanged() {
  emit titleChanged();
  emit authorsChanged();
  emit initialReleaseDateChanged();
  emit latestReleaseDateChanged();
  emit descriptionChanged();
  emit versionChanged();
  emit engineVersionChanged();
  emit licensesChanged();
  emit languagesChanged();
  emit minPlayersChanged();
  emit maxPlayersChanged();
  emit genreChanged();
  emit websiteChanged();
  emit idChanged();
  emit isValidChanged();
}

void Quest::clear() {
  _title.clear();
  _authors.clear();
  _initialReleaseDate = {};
  _latestReleaseDate = {};
  _description.clear();
  _version = {};
  _engineVersion = {};
  _licenses.clear();
  _languages.clear();
  _minPlayers = 0;
  _maxPlayers = 0;
  _genre.clear();
  _website.clear();
  _id.clear();
  _isValid = false;

  notifyAllChanged();
}

const QString& Quest::title() const {
  return _title;
}

const QStringList& Quest::authors() const {
  return _authors;
}

const QDate& Quest::initialReleaseDate() const {
  return _initialReleaseDate;
}

const QDate& Quest::latestReleaseDate() const {
  return _latestReleaseDate;
}

const QString& Quest::description() const {
  return _description;
}

QString Quest::version() const {
  return _version.toString();
}

QString Quest::engineVersion() const {
  return _engineVersion.toString();
}

const QStringList& Quest::licenses() const {
  return _licenses;
}

const QStringList& Quest::languages() const {
  return _languages;
}

uint Quest::minPlayers() const {
  return _minPlayers;
}

uint Quest::maxPlayers() const {
  return _maxPlayers;
}

const QStringList& Quest::genre() const {
  return _genre;
}

const QUrl& Quest::website() const {
  return _website;
}

const QString& Quest::id() const {
  return _id;
}

bool Quest::isValid() const {
  return _isValid;
}
} // namespace solarus::launcher::quests
