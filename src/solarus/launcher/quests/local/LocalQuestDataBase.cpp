/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/local/LocalQuestDataBase.h>

#include <QFileInfo>
#include <QDir>

#include <algorithm>
#include <cmath>

namespace solarus::launcher::quests {

static const auto DIR_EXTENSION_FILTER = QStringLiteral("*.solarus");
static constexpr QDir::Filters DIR_FILTERS =
  QDir::Filter::NoDot | QDir::Filter::NoDotDot | QDir::Filter::Readable | QDir::Filter::Files;
static constexpr QDir::SortFlags DIR_SORTFLAGS = QDir::SortFlag::Name | QDir::SortFlag::IgnoreCase;
static constexpr auto COMPRESS_EVENT_TIMEOUT = 1000; // ms

LocalQuestDataBase::LocalQuestDataBase(QObject* parent)
  : QObject(parent) {
  setupQuestSearcher();
}

LocalQuestDataBase::~LocalQuestDataBase() {
  if (_questSearcher) {
    _questSearcher->stop();
    _questSearcher->deleteLater();
    _questSearcher = nullptr;
  }
  _questSearcherThread->quit();
  _questSearcherThread->wait();
}

void LocalQuestDataBase::setupQuestSearcher() {
  // Configure timer.
  _timer.setSingleShot(true);

  // Configure filesystem watcher.
  _filesystemWatcher = new QFileSystemWatcher(this);

  // Signal emitted when a quest file has changed.
  QObject::connect(_filesystemWatcher, &QFileSystemWatcher::fileChanged, this, [this](const QString& path) {
    if (!path.trimmed().isEmpty() && QFileInfo::exists(path)) {
      // The content of the file has changed: we need to update the quest information.
      emit questRequested(path);
    } else {
      // The quest has been removed.
      auto* quest = questOfPath(path);
      removeQuest(quest);
    }
  });

  // Signal emitted when the content of the directory has changed.
  QObject::connect(_filesystemWatcher, &QFileSystemWatcher::directoryChanged, this, [this](const QString&) {
    _timer.stop();

    // Stop worker immediately.
    if (_questSearcher) {
      _questSearcher->stop();
    }

    // Use a timer as a way to compress signals.
    setState(State::NeedsRefresh);
    _timer.start(COMPRESS_EVENT_TIMEOUT);
  });

  // Search quests after timeout.
  QObject::connect(&_timer, &QTimer::timeout, this, [this]() {
    if (_state == State::NeedsRefresh) {
      emit searchRequested(_directoryInfo.path);
    }
  });

  // Configure worker to search for quests.
  _questSearcherThread = new QThread(this);
  _questSearcher = new LocalQuestSearcher(/*NB: no parent. Will be deleted with deleteLater().*/);
  _questSearcher->moveToThread(_questSearcherThread);

  QObject::connect(
    _questSearcher, &LocalQuestSearcher::questFound, this, &LocalQuestDataBase::addOrUpdateQuest, Qt::QueuedConnection);
  QObject::connect(_questSearcher, &LocalQuestSearcher::questsFound, this, &LocalQuestDataBase::addOrUpdateQuests,
    Qt::QueuedConnection);
  QObject::connect(
    _questSearcher, &LocalQuestSearcher::started, this, &LocalQuestDataBase::onSearchStarted, Qt::QueuedConnection);
  QObject::connect(
    _questSearcher, &LocalQuestSearcher::finished, this, &LocalQuestDataBase::onSearchFinished, Qt::QueuedConnection);
  QObject::connect(
    _questSearcher, &LocalQuestSearcher::stopped, this, &LocalQuestDataBase::onSearchFinished, Qt::QueuedConnection);
  QObject::connect(_questSearcher, &LocalQuestSearcher::progressChanged, this,
    &LocalQuestDataBase::onSearchProgressChanged, Qt::QueuedConnection);

  QObject::connect(
    this, &LocalQuestDataBase::searchRequested, _questSearcher, &LocalQuestSearcher::start, Qt::QueuedConnection);
  QObject::connect(
    this, &LocalQuestDataBase::questRequested, _questSearcher, &LocalQuestSearcher::readQuest, Qt::QueuedConnection);

  _questSearcherThread->start();
}

const QString& LocalQuestDataBase::directory() const {
  return _directoryInfo.path;
}

void LocalQuestDataBase::setDirectory(const QString& path) {
  // Check if we receive a URL parameter from QML, in the form "file:///"
  const QUrl url{ path };
  const auto urlLocalPath = url.toLocalFile();
  const auto directoryPath = urlLocalPath.isEmpty() ? path.trimmed() : urlLocalPath;

  // Get absolute path.
  const QFileInfo fileInfo{ QDir::fromNativeSeparators(directoryPath) };
  const auto absolutePath = fileInfo.absoluteFilePath();

  if (absolutePath != _directoryInfo.path) {
    // Reset data.
    clear();

    // Set new path.
    _directoryInfo.path = absolutePath;
    _directoryInfo.isValid = !absolutePath.isEmpty() && fileInfo.exists();
    if (_directoryInfo.isValid) {
      _directoryInfo.entries = fileInfo.dir().entryList({ DIR_EXTENSION_FILTER }, DIR_FILTERS, DIR_SORTFLAGS);
    } else {
      _directoryInfo.entries.clear();
    }

    // Notify UI.
    emit directoryChanged();
    emit validPathChanged();

    // Search for quests.
    if (_directoryInfo.isValid) {
      _filesystemWatcher->addPath(absolutePath);
      setState(State::NeedsRefresh);

      // Use a timer to compress possible spamming from the user.
      _timer.start(COMPRESS_EVENT_TIMEOUT / 2);
    } else {
      emit progressChanged(100);
      setState(State::UpToDate);
    }
  }
}

bool LocalQuestDataBase::validPath() const {
  return _directoryInfo.isValid;
}

LocalQuestDataBase::State LocalQuestDataBase::state() const {
  return _state;
}

void LocalQuestDataBase::setState(State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged(state);
  }
}

void LocalQuestDataBase::startSearch() {
  _questSearcher->stop();
  setState(State::NeedsRefresh);
  emit searchRequested(_directoryInfo.path);
}

void LocalQuestDataBase::stopSearch() {
  if (state() == State::Searching) {
    _questSearcher->stop();
  }
}

void LocalQuestDataBase::onSearchStarted() {
  setState(State::Searching);
  emit searchStarted();
}

void LocalQuestDataBase::onSearchProgressChanged(int progress, int total) {
  const auto ratio = total > 0 ? static_cast<double>(progress) / static_cast<double>(total) : 1.;
  const auto normalizedProgress = std::min(100, std::max(0, static_cast<int>(std::round(ratio * 100.))));
  emit progressChanged(normalizedProgress);
}

void LocalQuestDataBase::onSearchFinished() {
  setState(State::UpToDate);
  emit searchFinished();
}

void LocalQuestDataBase::reset() {
  setState(State::Uninitialized);
  emit progressChanged(0);

  _timer.stop();

  if (_questSearcher) {
    _questSearcher->stop();
  }

  // Delete and clear quests.
  clear();

  // Reset path.
  _directoryInfo.path.clear();
  _directoryInfo.isValid = false;
  _directoryInfo.entries.clear();

  // Reset fileSystemWatcher.
  const auto watchedDirectories = _filesystemWatcher->directories();
  if (!watchedDirectories.empty()) {
    _filesystemWatcher->removePaths(watchedDirectories);
  }
  const auto watchedFiles = _filesystemWatcher->files();
  if (!watchedFiles.empty()) {
    _filesystemWatcher->removePaths(watchedFiles);
  }
}

const QVector<LocalQuest*>& LocalQuestDataBase::quests() const {
  return _quests;
}

int LocalQuestDataBase::questCount() const {
  return _quests.size();
}

LocalQuest* LocalQuestDataBase::questAt(int index) const {
  if (index >= 0 && index < _quests.size()) {
    return _quests.at(index);
  }
  return nullptr;
}

LocalQuest* LocalQuestDataBase::questOfId(const QString& id) const {
  if (id.isEmpty())
    return nullptr;

  const auto it = std::find_if(_quests.begin(), _quests.end(), [&id](const LocalQuest* quest) {
    return quest->id() == id;
  });

  return it != _quests.end() ? *it : nullptr;
}

LocalQuest* LocalQuestDataBase::questOfPath(const QString& path) const {
  if (path.isEmpty())
    return nullptr;

  const auto it = std::find_if(_quests.begin(), _quests.end(), [&path](const LocalQuest* quest) {
    return quest->path() == path;
  });

  return it != _quests.end() ? *it : nullptr;
}

int LocalQuestDataBase::questIndex(const LocalQuest* quest) const {
  const auto it = std::find_if(_quests.begin(), _quests.end(), [quest](const LocalQuest* otherQuest) {
    return quest == otherQuest;
  });
  const auto index = it == _quests.end() ? -1 : std::distance(_quests.begin(), it);
  return index;
}

void LocalQuestDataBase::addOrUpdateQuest(const LocalQuestData& data) {
  // Look if quest is not already in the list.
  const auto& questPath = data.path;
  const auto it = std::find_if(_quests.begin(), _quests.end(), [&questPath](const LocalQuest* quest) {
    return quest && quest->path() == questPath;
  });
  const auto count = _quests.size();
  const auto row = it == _quests.end() ? count : std::distance(_quests.begin(), it);

  if (row == count) {
    // Add a new LocalQuest.
    auto* newQuest = new LocalQuest(this);
    newQuest->initFromSolarusFile(data);
    _quests.append(newQuest);
    _filesystemWatcher->addPath(questPath);
    emit questAdded(newQuest);
    emit questCountChanged(_quests.count());
  } else {
    // Change the LocalQuest's data.
    auto* existingQuest = *it;
    existingQuest->initFromSolarusFile(data);
    emit questUpdated(existingQuest);
  }
}

void LocalQuestDataBase::addOrUpdateQuests(const QVector<LocalQuestData>& datas) {
  for (const auto& data : datas) {
    addOrUpdateQuest(data);
  }
}

void LocalQuestDataBase::removeQuest(LocalQuest* quest) {
  if (quest && _quests.contains(quest)) {
    // Remove from list.
    _quests.removeAll(quest);
    emit questRemoved(quest);
    emit questCountChanged(_quests.count());

    // Remove from filesystem watcher.
    const auto questPath = quest->path();
    if (!questPath.isEmpty()) {
      _filesystemWatcher->removePath(questPath);
    }

    // Safely delete the quest, QML might still use it.
    quest->deleteLater();

    // Remove quest from disk.
    QFile questFile(questPath);
    if (questFile.exists()) {
      questFile.remove();
    }
  }
}

void LocalQuestDataBase::clear() {
  if (questCount() == 0)
    return;

  // Delete quests from list but not from disk.
  for (auto* quest : qAsConst(_quests)) {
    const auto questPath = quest->path();
    _filesystemWatcher->removePath(questPath);
    quest->deleteLater();
  }
  _quests.clear();
  emit questCountChanged(_quests.count());
  emit cleared();
}
} // namespace solarus::launcher::quests
