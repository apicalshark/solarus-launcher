#include <solarus/launcher/quests/local/LocalQuest.h>

namespace solarus::launcher::quests {
LocalQuest::LocalQuest(QObject* parent)
  : Quest(parent) {}

LocalQuest::~LocalQuest() = default;

void LocalQuest::initFromSolarusFile(const LocalQuestData& data) {
  if (!data.isValid) {
    clear();
    return;
  } else {
    _isValid = true;
  }

  // Basic information.
  _title = data.title;
  _authors = data.authors;
  _initialReleaseDate = data.initialReleaseDate;
  _latestReleaseDate = data.latestReleaseDate;
  _description = data.description;
  _version = data.version;
  _engineVersion = data.engineVersion;
  _licenses = data.licenses;
  _languages = data.languages;
  _minPlayers = data.minPlayers;
  _maxPlayers = data.maxPlayers;
  _genre = data.genre;
  _website = data.website;
  _id = data.id;
  notifyAllChanged();

  // Additionnal information.
  _path = data.path;
  _thumbnail = data.thumbnail;
  emit pathChanged();
  emit thumbnailChanged();
}

void LocalQuest::clear() {
  Quest::clear();

  _thumbnail = {};
  _lastPlayedTime = {};
  _state = State::Idle;
  _hasUpdate = false;
  _downloadProgress = 0;

  emit thumbnailChanged();
  emit lastPlayedTimeChanged();
  emit stateChanged();
  emit hasUpdateChanged();
  emit downloadProgressChanged();
}

const QString& LocalQuest::path() const {
  return _path;
}

const QPixmap& LocalQuest::thumbnail() const {
  return _thumbnail;
}

const QDateTime& LocalQuest::lastPlayedTime() const {
  return _lastPlayedTime;
}

void LocalQuest::setLastPlayedTime(const QDateTime& lastPlayedTime) {
  if (lastPlayedTime != _lastPlayedTime) {
    _lastPlayedTime = lastPlayedTime;
    emit lastPlayedTimeChanged();
  }
}

LocalQuest::State LocalQuest::state() const {
  return _state;
}

bool LocalQuest::hasUpdate() const {
  return _hasUpdate;
}

int LocalQuest::downloadProgress() const {
  return _downloadProgress;
}

void LocalQuest::setPlaying(bool playing) {
  if (playing && _state == State::Idle) {
    setState(State::Playing);
  } else if (!playing && _state == State::Playing) {
    setState(State::Idle);
  }
}

void LocalQuest::setState(LocalQuest::State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}

void LocalQuest::setHasUpdate(bool hasUpdate) {
  if (hasUpdate != _hasUpdate) {
    _hasUpdate = hasUpdate;
    emit hasUpdateChanged();
  }
}

void LocalQuest::setDownloadProgress(int progress) {
  if (progress != _downloadProgress) {
    _downloadProgress = progress;
    emit downloadProgressChanged();
  }
}
} // namespace solarus::launcher::quests
