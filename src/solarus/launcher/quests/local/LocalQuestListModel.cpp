/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/local/LocalQuestListModel.h>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/local/LocalQuestData.h>
#include <solarus/launcher/quests/local/LocalQuestSearcher.h>

#include <QDate>
#include <QDir>
#include <QFile>
#include <QMap>
#include <QStringLiteral>
#include <QTimer>
#include <QVariant>

namespace solarus::launcher::quests {
LocalQuestListModel::LocalQuestListModel(app::Settings& settings, QObject* parent)
  : QAbstractListModel(parent)
  , _settings(settings) {
  // Connect to database and forward changes to the view.
  QObject::connect(&_db, &LocalQuestDataBase::questAdded, this, [this](LocalQuest* quest) {
    const auto row = _db.questIndex(quest);
    beginInsertRows({}, row, row);
    endInsertRows();
    emit questCountChanged();
  });

  QObject::connect(&_db, &LocalQuestDataBase::questRemoved, this, [this](LocalQuest* quest) {
    const auto row = _db.questIndex(quest);
    beginRemoveRows({}, row, row);
    endRemoveRows();
    emit questCountChanged();
  });

  QObject::connect(&_db, &LocalQuestDataBase::questUpdated, this, [this](LocalQuest* quest) {
    const auto row = _db.questIndex(quest);
    const auto modelIndex = index(row);
    emit dataChanged(modelIndex, modelIndex);
  });

  QObject::connect(&_db, &LocalQuestDataBase::progressChanged, this, [this](int progress) {
    emit loadingProgressChanged(progress);
  });

  QObject::connect(&_db, &LocalQuestDataBase::stateChanged, this, [this]() {
    emit stateChanged();
  });

  QObject::connect(&_db, &LocalQuestDataBase::searchStarted, this, [this]() {
    emit searchStarted();
  });

  QObject::connect(&_db, &LocalQuestDataBase::searchFinished, this, [this]() {
    emit searchFinished();
  });

  QObject::connect(&_db, &LocalQuestDataBase::validPathChanged, this, [this]() {
    emit validConfigChanged();
  });

  // Get directory from user settings.
  setDirectory(_settings.questDirectory());

  // Update directory when it changes.
  QObject::connect(&_settings, &app::Settings::questDirectoryChanged, this, [this]() {
    setDirectory(_settings.questDirectory());
  });
}

int LocalQuestListModel::rowCount(const QModelIndex& index) const {
  Q_UNUSED(index)
  return questCount();
}

QVariant LocalQuestListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return {};

  if (index.row() >= rowCount())
    return {};

  const auto row = index.row();
  auto* quest = _db.questAt(row);
  if (!quest)
    return {};

  switch (role) {
    case static_cast<int>(QuestInfoRoles::Quest):
      return QVariant::fromValue(quest);
    case static_cast<int>(QuestInfoRoles::Title):
      return QVariant::fromValue(quest->title());
    case static_cast<int>(QuestInfoRoles::InitialReleaseDate):
      return QVariant::fromValue(quest->initialReleaseDate());
    case static_cast<int>(QuestInfoRoles::LatestUpdateDate):
      return QVariant::fromValue(quest->latestReleaseDate());
    case static_cast<int>(QuestInfoRoles::LastPlayedTime):
      return QVariant::fromValue(quest->lastPlayedTime());
    default:
      return {};
  }
}

QHash<int, QByteArray> LocalQuestListModel::roleNames() const {
  static const auto roles = QHash<int, QByteArray>{
    { static_cast<int>(QuestInfoRoles::Quest), "quest" },
    { static_cast<int>(QuestInfoRoles::Title), "title" },
    { static_cast<int>(QuestInfoRoles::InitialReleaseDate), "initialReleaseDate" },
    { static_cast<int>(QuestInfoRoles::LatestUpdateDate), "latestUpdateDate" },
    { static_cast<int>(QuestInfoRoles::LastPlayedTime), "lastPlayedTime" },
  };
  return roles;
}

int LocalQuestListModel::questCount() const {
  return _db.questCount();
}

LocalQuestListModel::State LocalQuestListModel::state() const {
  switch (_db.state()) {
    case LocalQuestDataBase::State::Uninitialized:
      return State::Uninitialized;
    case LocalQuestDataBase::State::Searching:
      return State::Loading;
    default:
      return State::Idle;
  }
}

const QString& LocalQuestListModel::directory() const {
  return _db.directory();
}

bool LocalQuestListModel::validConfig() const {
  return _db.validPath();
}

void LocalQuestListModel::setDirectory(const QString& path) {
  _db.setDirectory(path);
}

void LocalQuestListModel::startSearch() {
  _db.startSearch();
}

void LocalQuestListModel::stopSearch() {
  _db.stopSearch();
}

void LocalQuestListModel::removeQuest(LocalQuest* quest) {
  _db.removeQuest(quest);
}

LocalQuest* LocalQuestListModel::addQuest(LocalQuestData& questData) {
  _db.addOrUpdateQuest(questData);
  auto* quest = _db.questOfId(questData.id);
  return quest;
}

const QVector<LocalQuest*>& LocalQuestListModel::quests() const {
  return _db.quests();
}

LocalQuest* LocalQuestListModel::questOfId(const QString& id) const {
  return _db.questOfId(id);
}

int LocalQuestListModel::questRow(const LocalQuest* quest) const {
  return _db.questIndex(quest);
}
} // namespace solarus::launcher::quests
