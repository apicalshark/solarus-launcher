/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/local/LocalQuestImageProvider.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>

namespace solarus::launcher::quests {
constexpr auto THUMBNAIL_WIDTH = 700;
constexpr auto THUMBNAIL_HEIGHT = 360;

LocalQuestImageProvider::LocalQuestImageProvider()
  : QQuickImageProvider(QQuickImageProvider::Pixmap, QQmlImageProviderBase::ForceAsynchronousImageLoading) {}

void LocalQuestImageProvider::setModel(LocalQuestListModel* model) {
  _model = model;
}

QPixmap LocalQuestImageProvider::requestPixmap(const QString& id, QSize* size, const QSize& requestedSize) {
  Q_UNUSED(requestedSize)

  if (!_model) {
    return {};
  }

  const auto parts = id.split('/', Qt::SplitBehaviorFlags::SkipEmptyParts);
  if (parts.size() < 2) {
    return {};
  }

  const auto& questId = parts.at(0);
  const auto& imageType = parts.at(1);
  if (imageType == QLatin1String("thumbnail")) {
    return thumbnail(questId, size);
  }

  return {};
}

QPixmap LocalQuestImageProvider::defaultThumbnail(QSize* size) const {
  QPixmap result(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);
  result.fill(Qt::transparent);
  size->setWidth(result.width());
  size->setHeight(result.height());
  return result;
}

QPixmap LocalQuestImageProvider::thumbnail(const QString& questId, QSize* size) const {
  if (const auto* quest = _model->questOfId(questId)) {
    const auto& questThumbnail = quest->thumbnail();
    if (!questThumbnail.isNull()) {
      size->setWidth(questThumbnail.width());
      size->setHeight(questThumbnail.height());
      return questThumbnail;
    }
  }
  return defaultThumbnail(size);
}
} // namespace solarus::launcher::quests
