/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/local/LocalQuestSearcher.h>

#include <QApplication>
#include <QDirIterator>
#include <QDir>
#include <QMutexLocker>
#include <QFile>
#include <QFileInfo>
#include <QRegularExpression>
#include <QTextStream>
#include <QUrl>
#include <QPainter>
#include <QDebug>
#include <QLoggingCategory>

#include <solarus/core/CurrentQuest.h>
#include <solarus/core/QuestFiles.h>
#include <solarus/core/QuestProperties.h>
#include <solarus/core/Debug.h>
#include <solarus/core/SolarusFatal.h>

namespace solarus::launcher::quests {
#define LOCALQUESTSEARCHER_DEBUG
#ifdef LOCALQUESTSEARCHER_DEBUG
Q_LOGGING_CATEGORY(LOGCAT_LOCALQUESTSEARCHER, "solarus.launcher.LocalQuestSearcher")
#endif

const QRegularExpression listSplitRE(QStringLiteral("\\s*,\\s*"));

void initializeFromProperties(LocalQuestData& questData, const Solarus::QuestProperties& properties) {
  questData.title = QString::fromStdString(properties.get_title());
  questData.authors =
    QString::fromStdString(properties.get_author()).split(listSplitRE, Qt::SplitBehaviorFlags::SkipEmptyParts);
  questData.initialReleaseDate =
    QDate::fromString(QString::fromStdString(properties.get_initial_release_date()), QStringLiteral("yyyyMMdd"));
  questData.latestReleaseDate =
    QDate::fromString(QString::fromStdString(properties.get_release_date()), QStringLiteral("yyyyMMdd"));
  questData.description = QString::fromStdString(properties.get_long_description());
  questData.version = QVersionNumber::fromString(QString::fromStdString(properties.get_quest_version()));
  questData.engineVersion = QVersionNumber::fromString(QString::fromStdString(properties.get_quest_version()));
  questData.licenses =
    QString::fromStdString(properties.get_license()).split(listSplitRE, Qt::SplitBehaviorFlags::SkipEmptyParts);
  questData.languages.clear();
  for (const auto& lang : properties.get_languages()) {
    questData.languages << QString::fromStdString(lang);
  }
  questData.minPlayers = properties.get_min_players();
  questData.maxPlayers = properties.get_max_players();
  questData.genre =
    QString::fromStdString(properties.get_genre()).split(listSplitRE, Qt::SplitBehaviorFlags::SkipEmptyParts);
  questData.website = QString::fromStdString(properties.get_website());
  questData.id = QString::fromStdString(properties.get_quest_write_dir());

  questData.isValid = true;
}

QPixmap loadPixmap(const std::string& file_name) {
  QPixmap result;

  if (Solarus::QuestFiles::data_file_exists(file_name) && !Solarus::QuestFiles::data_file_is_dir(file_name)) {
    const auto buffer = Solarus::QuestFiles::data_file_read(file_name);
    result.loadFromData(reinterpret_cast<const uchar*>(buffer.data()), static_cast<uint>(buffer.size()));
  }

  return result;
}

QPixmap getThumnailFromLogo(const QPixmap& logo) {
  if (logo.isNull())
    return {};

  constexpr auto thumbnailW = 700;
  constexpr auto thumbnailH = 360;
  constexpr auto thumbnailPadding = 30;
  constexpr auto logoW = thumbnailW - thumbnailPadding * 2;
  constexpr auto logoH = thumbnailH - thumbnailPadding * 2;

  QPixmap result{ thumbnailW, thumbnailH };
  result.fill(Qt::transparent);

  QPainter p(&result);
  p.setRenderHint(QPainter::Antialiasing, true);

  const auto resizedLogo = logo.scaled(logoW, logoH, Qt::KeepAspectRatio, Qt::SmoothTransformation);
  const auto logoX = (thumbnailW - resizedLogo.width()) / 2;
  const auto logoY = (thumbnailH - resizedLogo.height()) / 2;
  const auto logoRect = QRect{ logoX, logoY, resizedLogo.width(), resizedLogo.height() };
  p.drawPixmap(logoRect, resizedLogo);
  return result;
}

QPixmap getQuestThumbnail() {
  // First, try to get the actual thumbnail.
  static const auto thumbnail_file_name = std::string{ "logos/thumbnail.png" };
  const auto thumbnail = loadPixmap(thumbnail_file_name);
  if (!thumbnail.isNull())
    return thumbnail;

  // Else, try the fallback pictures, ordered by priority.
  static const auto fallback_file_names = std::array<std::string, 3>{
    "logos/logo_2x.png",
    "logos/logo@2x.png",
    "logos/logo.png",
  };
  QPixmap logo;
  for (const auto& file_name : fallback_file_names) {
    logo = loadPixmap(file_name);
    if (!logo.isNull())
      break;
  }

  // We reduce the logo size to make sure it fits the bounds entirely.
  return getThumnailFromLogo(logo);
}

LocalQuestData makeQuestData(const QString& path) {
  LocalQuestData result;
  result.path = path;

  // Open the quest to get its quest.dat file.
  const auto arguments = QApplication::arguments();
  const auto& program_name = arguments.isEmpty() ? QString{} : arguments.first();

  // Prevent Solarus from closing the launcher.
  Solarus::Debug::set_die_on_error(false);
  Solarus::Debug::set_show_popup_on_die(false);
  Solarus::Debug::set_abort_on_die(false);

  try {
    if (Solarus::QuestFiles::open_quest(program_name.toStdString(), path.toStdString())) {
      // Load all properties.
      const auto properties = Solarus::QuestProperties{ Solarus::CurrentQuest::get_properties() };
      initializeFromProperties(result, properties);

      // Load thumbnail.
      result.thumbnail = getQuestThumbnail();

#ifdef LOCALQUESTSEARCHER_DEBUG
      qCInfo(LOGCAT_LOCALQUESTSEARCHER) << QString("Found quest: '%1' (%2)").arg(result.id).arg(path);
#endif
    } else {
#ifdef LOCALQUESTSEARCHER_DEBUG
      qCWarning(LOGCAT_LOCALQUESTSEARCHER) << QString("Error when reading quest: '%1'").arg(path);
#endif
    }
    Solarus::QuestFiles::close_quest();
  } catch (const Solarus::SolarusFatal&) {
    // Solarus always throws an exception when loading a quest isn't successful.
    // Let's just catch it, and ignore this quest.
#ifdef LOCALQUESTSEARCHER_DEBUG
    qCWarning(LOGCAT_LOCALQUESTSEARCHER) << QString("Error when reading quest: '%1'").arg(path);
#endif
  }

  return result;
}

LocalQuestSearcher::LocalQuestSearcher(QObject* parent)
  : QObject(parent) {}

LocalQuestSearcher::~LocalQuestSearcher() = default;

void LocalQuestSearcher::start(const QString& directory) {
  {
    QMutexLocker locker(&_mutex);
    _stopped = false;
  }

  emit started();

  if (directory.trimmed().isEmpty())
    return;

  const auto path = QDir::fromNativeSeparators(directory);
  if (path.trimmed().isEmpty()) {
    return;
  }

  if (!QDir(directory).exists()) {
    return;
  }

  QVector<LocalQuestData> result;

  QDir dir(path);
  const auto files = dir.entryInfoList({ "*.solarus", "*.SOLARUS" }, QDir::Filter::Files | QDir::NoDotAndDotDot);
  const auto total = files.size();
  auto processed = 0;
  emit progressChanged(0, total);

  for (const auto& file : qAsConst(files)) {
    // Create Solarus quest data.
    const auto path = file.absoluteFilePath();
    const auto questData = makeQuestData(path);

    // Don't emit signal if not necessary.
    {
      QMutexLocker locker(&_mutex);
      if (_stopped) {
        // Quit if stopped.
        emit stopped();
        return;
      } else if (questData.isValid) {
        result.append(questData);
      }
    }
    processed++;
    emit progressChanged(processed, total);
  }

  // Send signal to receiver (on other thread).
  if (!result.empty()) {
    emit questsFound(result);
  }

  emit finished();
}

void LocalQuestSearcher::stop() {
  QMutexLocker locker(&_mutex);
  _stopped = true;
}

void LocalQuestSearcher::readQuest(const QString& path) const {
  const auto questData = makeQuestData(path);

  if (questData.isValid) {
    // Send signal to receiver (on other thread).
    emit questFound(questData);
  }
}
} // namespace solarus::launcher::quests
