/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/online/OnlineQuestListModel.h>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/app/Url.h>
#include <solarus/launcher/utils/UrlUtils.h>

#include <QDate>
#include <QDebug>
#include <QFile>
#include <QMap>
#include <QStringLiteral>
#include <QVariant>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

namespace solarus::launcher::quests {
constexpr auto questsApiVersion = "1.0.0";

OnlineQuestListModel::OnlineQuestListModel(app::Settings& settings, QObject* parent)
  : QAbstractListModel(parent)
  , _settings(settings) {
  // Get endpoint URL from user settings.
  setEndpointUrl(_settings.questLibraryEndpointUrl());

  // Update endpoint URL when it changes.
  QObject::connect(&_settings, &app::Settings::questLibraryEndpointUrlChanged, this, [this]() {
    setEndpointUrl(_settings.questLibraryEndpointUrl());
  });
}

int OnlineQuestListModel::rowCount(const QModelIndex& index) const {
  Q_UNUSED(index)
  return questCount();
}

QVariant OnlineQuestListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return {};

  if (index.row() >= rowCount())
    return {};

  const auto row = index.row();
  auto* quest = row >= 0 && row < _quests.count() ? _quests.at(row) : nullptr;
  if (!quest)
    return {};

  switch (role) {
    case static_cast<int>(QuestInfoRoles::Quest):
      return QVariant::fromValue(quest);
    case static_cast<int>(QuestInfoRoles::Title):
      return QVariant::fromValue(quest->title());
    case static_cast<int>(QuestInfoRoles::InitialReleaseDate):
      return QVariant::fromValue(quest->initialReleaseDate());
    case static_cast<int>(QuestInfoRoles::LatestUpdateDate):
      return QVariant::fromValue(quest->latestReleaseDate());
    default:
      return {};
  }
}

QHash<int, QByteArray> OnlineQuestListModel::roleNames() const {
  static const auto roles = QHash<int, QByteArray>{
    { static_cast<int>(QuestInfoRoles::Quest), "quest" },
    { static_cast<int>(QuestInfoRoles::Title), "title" },
    { static_cast<int>(QuestInfoRoles::InitialReleaseDate), "initialReleaseDate" },
    { static_cast<int>(QuestInfoRoles::LatestUpdateDate), "latestUpdateDate" },
  };
  return roles;
}

QString OnlineQuestListModel::endpointUrl() const {
  return _endpointUrl.toString();
}

bool OnlineQuestListModel::validConfig() const {
  return _endpointUrl.isValid();
}

int OnlineQuestListModel::questCount() const {
  return _quests.count();
}

OnlineQuestListModel::State OnlineQuestListModel::state() const {
  return _state;
}

void OnlineQuestListModel::setEndpointUrl(const QString& endpointUrl) {
  const QUrl url(endpointUrl);
  if (url != _endpointUrl) {
    _endpointUrl = url;
    emit endpointUrlChanged();
    emit validConfigChanged();
    clear();
  }
}

void OnlineQuestListModel::startSearch() {
  clear();

  setState(State::Loading);
  emit searchStarted();
  beginResetModel();
  _downloader.downloadData(
    _endpointUrl,
    // OnFinished.
    [this](const oclero::QtDownloader::ErrorCode errorCode, const QByteArray& data) {
      if (errorCode == oclero::QtDownloader::ErrorCode::NoError) {
        QJsonParseError parseError;
        const auto jsonDoc = QJsonDocument::fromJson(data, &parseError);
        if (!jsonDoc.isNull() && parseError.error == QJsonParseError::NoError) {
          populateFromJson(jsonDoc);
        }
      }
      endResetModel();
      setState(State::Idle);
      emit questCountChanged();
      emit searchFinished();
    },
    // OnProgress.
    [this](const int progress) {
      emit loadingProgressChanged(progress);
    });
}

void OnlineQuestListModel::stopSearch() {
  if (state() == State::Loading) {
    _downloader.cancel();
    clear();
  }
}

void OnlineQuestListModel::setState(State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}

OnlineQuest* OnlineQuestListModel::questOfId(const QString& id) const {
  if (id.isEmpty())
    return nullptr;

  const auto it = std::find_if(_quests.begin(), _quests.end(), [&id](const OnlineQuest* quest) {
    return quest->id() == id;
  });

  return it != _quests.end() ? *it : nullptr;
}

int OnlineQuestListModel::questRow(const OnlineQuest* quest) const {
  const auto it = std::find_if(_quests.begin(), _quests.end(), [quest](const OnlineQuest* otherQuest) {
    return quest == otherQuest;
  });
  const auto index = it == _quests.end() ? -1 : std::distance(_quests.begin(), it);
  return index;
}

bool OnlineQuestListModel::populateFromJson(const QJsonDocument& jsonDoc) {
  if (!jsonDoc.isObject())
    return false;

  const auto jsonObject = jsonDoc.object();
  const auto resultApiVersion = jsonObject.value(QStringLiteral("version")).toString();
  if (resultApiVersion != questsApiVersion)
    return false;

  const auto resultArray = jsonObject.value(QStringLiteral("data")).toArray();
  if (resultArray.isEmpty())
    return false;

  // Links are relative so we need the domain.
  const auto urlDomain = utils::getRoot(app::NEWS_API_URL);

  for (const auto& arrayElement : resultArray) {
    if (arrayElement.isObject()) {
      const auto jsonObject = arrayElement.toObject();
      auto* quest = new OnlineQuest(this);
      quest->initFromJson(jsonObject, urlDomain);
      if (quest->isValid()) {
        _quests.append(quest);
      } else {
        delete quest;
      }
    }
  }

  return true;
}

void OnlineQuestListModel::clear() {
  if (questCount() == 0)
    return;

  beginResetModel();
  for (auto* quest : qAsConst(_quests)) {
    quest->deleteLater();
  }
  _quests.clear();
  endResetModel();
  emit questCountChanged();
  setState(State::Uninitialized);
}
} // namespace solarus::launcher::quests
