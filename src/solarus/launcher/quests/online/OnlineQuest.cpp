#include <solarus/launcher/quests/online/OnlineQuest.h>

#include <QJsonObject>
#include <QJsonArray>

namespace solarus::launcher::quests {
OnlineQuest::OnlineQuest(QObject* parent)
  : Quest(parent) {}

OnlineQuest::~OnlineQuest() = default;

void OnlineQuest::initFromJson(const QJsonObject& json, const QString& urlDomain) {
  const auto jsonMap = json.toVariantMap();

  // Title.
  _title = jsonMap.value(QStringLiteral("title")).toString();

  // Developer.
  const auto developerValue = jsonMap.value(QStringLiteral("developer"));
  _authors =
    developerValue.canConvert<QStringList>() ? developerValue.toStringList() : QStringList{ developerValue.toString() };

  // Dates.
  _initialReleaseDate =
    QDate::fromString(jsonMap.value(QStringLiteral("initialReleaseDate")).toString(), QStringLiteral("yyyy-MM-dd"));
  _latestReleaseDate =
    QDate::fromString(jsonMap.value(QStringLiteral("latestUpdateDate")).toString(), QStringLiteral("yyyy-MM-dd"));

  // Description.
  _description = jsonMap.value(QStringLiteral("excerpt")).toString();

  // Version.
  _version = QVersionNumber::fromString(jsonMap.value(QStringLiteral("version")).toString());

  // Solarus version.
  _engineVersion = QVersionNumber::fromString(jsonMap.value(QStringLiteral("solarusVersion")).toString());

  // License.
  const auto licenseValue = jsonMap.value(QStringLiteral("license"));
  _licenses =
    licenseValue.canConvert<QStringList>() ? licenseValue.toStringList() : QStringList{ licenseValue.toString() };

  // Languages.
  const auto languageValue = jsonMap.value(QStringLiteral("languages"));
  const auto jsonLanguages =
    languageValue.canConvert<QStringList>() ? languageValue.toStringList() : QStringList{ languageValue.toString() };
  for (auto& language : jsonLanguages) {
    if (language.isEmpty())
      continue;
    _languages.append(language.toLower());
  }

  // Players.
  _minPlayers = jsonMap.value(QStringLiteral("minPlayers"), 1).toUInt();
  _maxPlayers = jsonMap.value(QStringLiteral("maxPlayers"), 1).toUInt();

  // Genre.
  const auto genreValue = jsonMap.value(QStringLiteral("genre"));
  _genre = genreValue.canConvert<QStringList>() ? genreValue.toStringList() : QStringList{ genreValue.toString() };

  // Website.
  const auto websiteValue = jsonMap.value(QStringLiteral("website")).toString();
  if (websiteValue != QStringLiteral("#")) {
    _website = QUrl{ websiteValue };
  }

  // Id.
  _id = jsonMap.value(QStringLiteral("id")).toString();

  // Download URL.
  _url = QUrl{ jsonMap.value(QStringLiteral("download")).toString() };

  // Thumbnail URL.
  _thumbnail = QUrl{ urlDomain + jsonMap.value(QStringLiteral("thumbnail")).toString() }.toString();

  // Screenshots.
  _screenshots.clear();
  const auto screenshotArray = jsonMap.value(QStringLiteral("screenshots")).toJsonArray();
  for (const auto& screenshot : screenshotArray) {
    const auto screenshotUrl = QUrl{ urlDomain + screenshot.toString() }.toString();
    _screenshots.push_back(screenshotUrl);
  }

  // Validity criterion.
  _isValid = !_id.isEmpty() && !_title.isEmpty() && !_authors.isEmpty() && _initialReleaseDate.isValid()
             && _latestReleaseDate.isValid() && !_description.isEmpty() && !_version.isNull()
             && !_engineVersion.isNull() && !_licenses.isEmpty() && _minPlayers > 0 && _maxPlayers >= _minPlayers
             && !_genre.isEmpty() && _url.isValid();

  if (_isValid) {
    notifyAllChanged();
  } else {
    clear();
  }
}

void OnlineQuest::clear() {
  Quest::clear();

  _thumbnail.clear();
  _screenshots.clear();
  _state = State::Idle;
  _hasUpdate = false;
  _downloadProgress = 0;

  emit thumbnailChanged();
  emit screenshotsChanged();
  emit stateChanged();
  emit hasUpdateChanged();
  emit downloadProgressChanged();
}

const QUrl& OnlineQuest::url() const {
  return _url;
}

const QString& OnlineQuest::thumbnail() const {
  return _thumbnail;
}

const QStringList& OnlineQuest::screenshots() const {
  return _screenshots;
}

OnlineQuest::State OnlineQuest::state() const {
  return _state;
}

bool OnlineQuest::hasUpdate() const {
  return _hasUpdate;
}

int OnlineQuest::downloadProgress() const {
  return _downloadProgress;
}

void OnlineQuest::setHasUpdate(bool hasUpdate) {
  if (hasUpdate != _hasUpdate) {
    _hasUpdate = hasUpdate;
    emit hasUpdateChanged();
  }
}

void OnlineQuest::setDownloadProgress(int progress) {
  if (progress != _downloadProgress) {
    _downloadProgress = progress;
    emit downloadProgressChanged();
  }
}

void OnlineQuest::setState(State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}
} // namespace solarus::launcher::quests
