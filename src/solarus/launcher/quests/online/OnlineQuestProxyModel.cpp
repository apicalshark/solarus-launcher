/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/quests/online/OnlineQuestProxyModel.h>

#include <QModelIndex>
#include <QRegularExpression>

namespace solarus::launcher::quests {
OnlineQuestProxyModel::OnlineQuestProxyModel(OnlineQuestListModel& parentModel, QObject* parent)
  : QSortFilterProxyModel(parent)
  , _parentModel(parentModel) {
  setSourceModel(&_parentModel);
  setDynamicSortFilter(true);
  setSortRole(static_cast<int>(OnlineQuestListModel::QuestInfoRoles::Title));
  sort(0, Qt::AscendingOrder);

  // Subscribe to OnlineQuestListModel's signals to be able to forward them.
  QObject::connect(&_parentModel, &OnlineQuestListModel::stateChanged, this, &OnlineQuestProxyModel::stateChanged);
  QObject::connect(
    &_parentModel, &OnlineQuestListModel::questCountChanged, this, &OnlineQuestProxyModel::questCountChanged);
}

OnlineQuestProxyModel::~OnlineQuestProxyModel() = default;

const QString& OnlineQuestProxyModel::searchText() const {
  return _searchText;
}

void OnlineQuestProxyModel::setSearchText(const QString& text) {
  static const QRegularExpression spaceRegex{ QStringLiteral("\\s+") };

  if (text != _searchText) {
    _searchText = text;
    _searchTags = _searchText.simplified().toLower().split(spaceRegex, Qt::SkipEmptyParts);
    emit searchTextChanged();
    invalidateFilter();
  }
}

OnlineQuestProxyModel::QuestSortMode OnlineQuestProxyModel::questSortMode() const {
  return _questSortMode;
}

void OnlineQuestProxyModel::setQuestSortMode(QuestSortMode questSortMode) {
  if (questSortMode != _questSortMode) {
    _questSortMode = questSortMode;
    switch (_questSortMode) {
      case QuestSortMode::Title:
        setSortRole(static_cast<int>(OnlineQuestListModel::QuestInfoRoles::Title));
        break;
      case QuestSortMode::ReleaseDate:
        setSortRole(static_cast<int>(OnlineQuestListModel::QuestInfoRoles::InitialReleaseDate));
        break;
      default:
        break;
    }
    const auto sortOrder = _questSortOrder == QuestSortOrder::Ascending ? Qt::AscendingOrder : Qt::DescendingOrder;
    sort(0, sortOrder);

    emit questSortModeChanged();
  }
}

OnlineQuestProxyModel::QuestSortOrder OnlineQuestProxyModel::questSortOrder() const {
  return _questSortOrder;
}

void OnlineQuestProxyModel::setQuestSortOrder(QuestSortOrder questSortOrder) {
  if (questSortOrder != _questSortOrder) {
    _questSortOrder = questSortOrder;
    const auto sortOrder = _questSortOrder == QuestSortOrder::Ascending ? Qt::AscendingOrder : Qt::DescendingOrder;
    sort(0, sortOrder);

    emit questSortOrderChanged();
  }
}

OnlineQuestListModel::State OnlineQuestProxyModel::state() const {
  return _parentModel.state();
}

int OnlineQuestProxyModel::questCount() const {
  return _parentModel.questCount();
}

bool OnlineQuestProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const {
  const auto modelIndex = sourceModel()->index(sourceRow, 0, sourceParent);

  if (_searchTags.empty())
    return true;

  const auto title = modelIndex.data(static_cast<int>(OnlineQuestListModel::QuestInfoRoles::Title)).toString();
  const auto titleLowerCase = title.toLower();

  // Possible improvement: compute Levenshtein distance to simulate fuzzy search.
  return std::all_of(_searchTags.begin(), _searchTags.end(), [&titleLowerCase](const QString& tag) {
    return titleLowerCase.contains(tag);
  });
}
} // namespace solarus::launcher::quests
