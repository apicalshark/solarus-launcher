/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/UpdateDialogController.h>

#include <solarus/launcher/app/Url.h>

#include <oclero/QtUpdater.hpp>

#include <QDesktopServices>
#include <QUrl>

namespace solarus::launcher::controllers {
UpdateDialogController::UpdateDialogController(oclero::QtUpdater& updater, QObject* parent)
  : QObject(parent)
  , _updater(updater) {
  using Updater = oclero::QtUpdater;

  const auto updaterState = _updater.state();
  switch (updaterState) {
    case Updater::State::CheckingForUpdate:
      setState(State::Checking);
      break;
    case Updater::State::DownloadingInstaller:
      setState(State::Downloading);
      break;
    case Updater::State::InstallingUpdate:
      setState(State::Installing);
      break;
    default:
      break;
  }

  // Checking.
  QObject::connect(&_updater, &Updater::checkForUpdateForced, this, &UpdateDialogController::manualCheckingRequested);
  QObject::connect(&_updater, &Updater::checkForUpdateStarted, this, [this]() {
    setState(State::Checking);
  });
  QObject::connect(&_updater, &Updater::checkForUpdateProgressChanged, this, [this](int percentage) {
    setDownloadProgress(percentage);
  });
  QObject::connect(&_updater, &Updater::checkForUpdateOnlineFailed, this, [this]() {
    setState(State::CheckingFail);
  });
  QObject::connect(&_updater, &Updater::checkForUpdateFinished, this, [this]() {
    const auto availability = _updater.updateAvailability();
    switch (availability) {
      case oclero::QtUpdater::UpdateAvailability::Available:
        setState(State::CheckingSuccess);
        break;
      case oclero::QtUpdater::UpdateAvailability::UpToDate:
        setState(State::CheckingUpToDate);
        break;
      default:
        setState(State::CheckingFail);
        break;
    }
  });

  // Downloading.
  QObject::connect(&_updater, &Updater::installerDownloadStarted, this, [this]() {
    setState(State::Downloading);
  });
  QObject::connect(&_updater, &Updater::installerDownloadProgressChanged, this, [this](int percentage) {
    setDownloadProgress(percentage);
  });
  QObject::connect(&_updater, &Updater::installerDownloadFailed, this, [this]() {
    setState(State::DownloadingFail);
  });
  QObject::connect(&_updater, &Updater::installerDownloadFinished, this, [this]() {
    const auto available = _updater.installerAvailable();
    setState(available ? State::DownloadingSuccess : State::DownloadingFail);
  });

  // Installing.
  QObject::connect(&_updater, &Updater::installationStarted, this, [this]() {
    setState(State::Installing);
  });
  QObject::connect(&_updater, &Updater::installationFailed, this, [this]() {
    setState(State::InstallingFail);
  });
  QObject::connect(&_updater, &Updater::installationFinished, this, [this]() {
    setState(State::InstallingSuccess);
  });

  // Metadata.
  QObject::connect(&_updater, &Updater::latestVersionChanged, this, &UpdateDialogController::latestVersionChanged);
  QObject::connect(
    &_updater, &Updater::latestVersionDateChanged, this, &UpdateDialogController::latestVersionDateChanged);
}

UpdateDialogController::State UpdateDialogController::state() const {
  return _state;
}

void UpdateDialogController::setState(State state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}

void UpdateDialogController::setDownloadProgress(int value) {
  if (value != _downloadProgress) {
    _downloadProgress = value;
    emit downloadProgressChanged(value);
  }
}

QString UpdateDialogController::currentVersion() const {
  return _updater.currentVersion();
}

QDateTime UpdateDialogController::currentVersionDate() const {
  return _updater.currentVersionDate();
}

QString UpdateDialogController::latestVersion() const {
  return _updater.latestVersion();
}

QDateTime UpdateDialogController::latestVersionDate() const {
  return _updater.latestVersionDate();
}

int UpdateDialogController::downloadProgress() const {
  return _downloadProgress;
}

void UpdateDialogController::cancel() {
  _updater.cancel();
  setState(State::None);
}

void UpdateDialogController::checkForUpdates() {
  _updater.forceCheckForUpdate();
}

void UpdateDialogController::downloadUpdate() {
  if (_updater.updateAvailability() == oclero::QtUpdater::UpdateAvailability::Available) {
#ifdef Q_OS_LINUX
    QDesktopServices::openUrl(QUrl{ app::HOMEPAGE_URL });
    emit closeDialogRequested();
#else
    _updater.downloadInstaller();
#endif
  }
}

void UpdateDialogController::installUpdate() {
#ifdef Q_OS_LINUX
  QDesktopServices::openUrl(QUrl{ app::HOMEPAGE_URL });
  emit closeDialogRequested();
#else
  if (_updater.installerAvailable()) {
    _updater.installUpdate();
  }
#endif
}
} // namespace solarus::launcher::controllers
