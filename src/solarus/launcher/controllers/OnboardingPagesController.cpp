/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/OnboardingPagesController.h>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>

#include <QDir>
#include <QFileInfo>

namespace solarus::launcher::controllers {

OnboardingPagesController::OnboardingPagesController(
  app::Settings& settings, app::Navigation& navigation, quests::LocalQuestListModel& questListModel, QObject* parent)
  : QObject(parent)
  , _settings(settings)
  , _navigation(navigation)
  , _questListModel(questListModel) {
  // Forward signals.
  QObject::connect(
    &_settings, &app::Settings::appOnboardingDoneChanged, this, &OnboardingPagesController::onboardingDoneChanged);
  QObject::connect(&_gamepad, &QGamepad::connectedChanged, this, &OnboardingPagesController::gamepadConnectedChanged);
  QObject::connect(&_questListModel, &quests::LocalQuestListModel::loadingProgressChanged, this, [this](int progress) {
    emit scanProgressChanged(progress);
    if (progress == 100) {
      emit scanFinished();
    }
  });

  // Initialize state.
  setOnboardingDone(_settings.appOnboardingDone());
  _questDirectory = getDefaultQuestDirectory();
}

const QString& OnboardingPagesController::questDirectory() const {
  return _questDirectory;
}

void OnboardingPagesController::setQuestDirectory(const QString& value) {
  if (value != _questDirectory) {
    _questDirectory = value;
    emit questDirectoryChanged();
  }
}

QString OnboardingPagesController::getDefaultQuestDirectory() const {
  return app::Settings::getDefaultQuestDirectory();
}

bool OnboardingPagesController::onboardingDone() const {
  return _settings.appOnboardingDone();
}

void OnboardingPagesController::setOnboardingDone(bool value) {
  _settings.setAppOnboardingDone(value);
  if (value) {
    _navigation.setCurrentWindow(app::Navigation::WindowId::MainWindow);
    _navigation.setCurrentPage(app::Navigation::PageId::Home);
  }
}

bool OnboardingPagesController::gamepadConnected() const {
  return _gamepad.isConnected();
}

void OnboardingPagesController::skipOnboarding() {
  setOnboardingDone(true);
}

void OnboardingPagesController::startScan() {
  // Create directory if it doesn't exist yet.
  const auto fileInfo = QFileInfo{ _questDirectory };
  if (!fileInfo.exists()) {
    const auto dir = QDir{ fileInfo.absoluteFilePath() };
    auto successfulCreation = false;
    if (!dir.exists()) {
      successfulCreation = dir.mkpath(QStringLiteral("."));
    }

    if (!successfulCreation) {
      emit scanFailed(ScanError::DirectoryCreationFailed);
      return;
    }
  } else if (fileInfo.isFile()) {
    emit scanFailed(ScanError::PathIsAlreadyAFile);
    return;
  }

  // Commit directory path to settings.
  emit scanStarted();
  emit scanProgressChanged(0);
  _settings.setQuestDirectory(_questDirectory);

  // Force search.
  _questListModel.startSearch();
}

void OnboardingPagesController::stopScan() {
  _questListModel.stopSearch();
}
} // namespace solarus::launcher::controllers
