/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/AboutPageController.h>

#include <solarus/launcher/app/Version.h>
#include <solarus/launcher/app/Url.h>

#include <QDesktopServices>
#include <QUrl>

namespace solarus::launcher::controllers {

constexpr auto LICENSE_GPL_URL = "https://gnu.org/licenses/gpl-3.0.html";
constexpr auto LICENSE_CC_URL = "https://creativecommons.org/licenses/by-sa/4.0";

AboutPageController::AboutPageController(QObject* parent)
  : QObject(parent) {}

QString AboutPageController::applicationVersion() const {
  return app::APPLICATION_DISPLAY_VERSION;
}

QString AboutPageController::applicationName() const {
  return app::APPLICATION_DISPLAY_NAME;
}

QString AboutPageController::applicationCopyright() const {
  return app::LEGAL_COPYRIGHT;
}

QString AboutPageController::gplLicenseURL() const {
  return LICENSE_GPL_URL;
}

QString AboutPageController::ccLicenseURL() const {
  return LICENSE_CC_URL;
}

QString AboutPageController::sourceCodeURL() const {
  return app::SOURCE_CODE_URL;
}

QString AboutPageController::bugReportURL() const {
  return app::BUG_REPORT_URL;
}

QString AboutPageController::websiteURL() const {
  return app::HOMEPAGE_URL;
}

void AboutPageController::showUserManual() const {}

void AboutPageController::showChangelog() const {}

void AboutPageController::openSourceCodeURL() const {
  QDesktopServices::openUrl(QUrl{ app::SOURCE_CODE_URL });
}

void AboutPageController::openBugReportURL() const {
  QDesktopServices::openUrl(QUrl{ app::BUG_REPORT_URL });
}

void AboutPageController::openWebsiteURL() const {
  QDesktopServices::openUrl(QUrl{ app::HOMEPAGE_URL });
}
} // namespace solarus::launcher::controllers
