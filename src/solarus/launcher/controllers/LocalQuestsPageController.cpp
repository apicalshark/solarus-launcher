/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/LocalQuestsPageController.h>

namespace solarus::launcher::controllers {
LocalQuestsPageController::LocalQuestsPageController(app::Navigation& navigation, runner::QuestRunner& questRunner,
  quests::QuestUpdater& questUpdater, quests::LocalQuestListModel& questListModel, QObject* parent)
  : QObject(parent)
  , _navigation(navigation)
  , _questRunner(questRunner)
  , _questUpdater(questUpdater)
  , _questListModel(questListModel)
  , _questListProxyModel(new quests::LocalQuestProxyModel(questListModel)) {
  _questListProxyModel->setQuestSortMode(quests::LocalQuestProxyModel::QuestSortMode::Title);
  QObject::connect(
    &_navigation, &app::Navigation::selectedQuestChanged, this, &LocalQuestsPageController::selectedQuestChanged);
}

quests::LocalQuestListModel* LocalQuestsPageController::questListModel() const {
  return const_cast<quests::LocalQuestListModel*>(&_questListModel);
}

quests::LocalQuestProxyModel* LocalQuestsPageController::questListProxyModel() const {
  return _questListProxyModel;
}

quests::LocalQuest* LocalQuestsPageController::selectedQuest() const {
  return _navigation.selectedQuest();
}

void LocalQuestsPageController::setSelectedQuest(quests::LocalQuest* quest) {
  _navigation.setSelectedQuest(quest);
}

void LocalQuestsPageController::playQuest(quests::LocalQuest* quest) {
  if (!quest)
    return;

  if (quest->state() != quests::LocalQuest::State::Idle) {
    return;
  }

  // Stop currently playing quest.
  _questRunner.stop();

  // Start new one.
  _questRunner.start(quest);
}

void LocalQuestsPageController::stopQuest(quests::LocalQuest* quest) {
  Q_UNUSED(quest)
  _questRunner.stop();
}

void LocalQuestsPageController::updateQuest(quests::LocalQuest* quest) {
  if (quest) {
    _questUpdater.updateQuest(quest->id());
  }
}

void LocalQuestsPageController::removeQuest(quests::LocalQuest* quest) {
  _questListModel.removeQuest(quest);
}

void LocalQuestsPageController::goToSettings() {
  _navigation.setCurrentPage(app::Navigation::PageId::Settings);
}

void LocalQuestsPageController::goToOnlineQuestList() {
  _navigation.setCurrentPage(app::Navigation::PageId::OnlineQuests);
}

void LocalQuestsPageController::focusSideBar() {
  _navigation.focusSideBar();
}
} // namespace solarus::launcher::controllers
