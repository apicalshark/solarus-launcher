/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/HomePageController.h>
#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/app/Url.h>

#include <QDesktopServices>
#include <QUrl>

namespace solarus::launcher::controllers {
HomePageController::HomePageController(app::Navigation& navigation, quests::LocalQuestListModel& questListModel,
  runner::QuestRunner& questRunner, news::NewsListModel& newsListModel, QObject* parent)
  : QObject(parent)
  , _navigation(navigation)
  , _questRunner(questRunner)
  , _newsListModel(newsListModel)
  , _questListModel(questListModel)
  , _questListProxyModel(new quests::LocalQuestProxyModel(questListModel, this)) {
  _questListProxyModel->setMaxCount(3);
  _questListProxyModel->setQuestSortMode(quests::LocalQuestProxyModel::QuestSortMode::LastPlayedTime);
  _questListProxyModel->setQuestSortOrder(quests::LocalQuestProxyModel::QuestSortOrder::Descending);
}

news::NewsListModel* HomePageController::newsListModel() const {
  return &_newsListModel;
}

quests::LocalQuestProxyModel* HomePageController::questListModel() const {
  return _questListProxyModel;
}

void HomePageController::ensureSearchStarted() {
  if (_questListModel.state() == quests::LocalQuestListModel::State::Uninitialized) {
    _questListModel.startSearch();
  }
}

void HomePageController::goToQuestsPage() {
  _navigation.setCurrentPage(app::Navigation::PageId::LocalQuests);
}

void HomePageController::openNewsWebsiteURL() const {
  QDesktopServices::openUrl(QUrl{ app::NEWS_URL });
}

void HomePageController::startQuest(quests::LocalQuest* quest) {
  if (quest) {
    _questRunner.start(quest);
  }
}
} // namespace solarus::launcher::controllers
