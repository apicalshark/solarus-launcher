/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/SettingsPageController.h>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>
#include <oclero/QtUpdater.hpp>
#include <oclero/QtEnumUtils.hpp>

#include <QAbstractListModel>
#include <QByteArray>
#include <array>

namespace solarus::launcher::controllers {

class CheckFrequencyListModel : public QAbstractListModel {
public:
  enum ItemDataRoles {
    TextRole = Qt::UserRole + 1,
    ValueRole,
  };

  CheckFrequencyListModel(QObject* parent)
    : QAbstractListModel(parent) {}

  int rowCount(const QModelIndex& index = {}) const override {
    Q_UNUSED(index)
    return _frequencies.size();
  }

  QVariant data(const QModelIndex& index, int role) const override {
    const auto row = index.row();
    if (row < 0 || row > static_cast<int>(_frequencies.size()))
      return {};

    switch (role) {
      case Qt::DisplayRole:
      case ItemDataRoles::TextRole:
        return updateCheckFrequencyToString(_frequencies.at(row));
      case ItemDataRoles::ValueRole:
        return QVariant::fromValue(_frequencies.at(row));
      default:
        return {};
    }
  }

  QHash<int, QByteArray> roleNames() const override {
    auto result = QAbstractItemModel::roleNames();
    result[ItemDataRoles::ValueRole] = "value";
    result[ItemDataRoles::TextRole] = "text";
    return result;
  }

private:
  QString updateCheckFrequencyToString(oclero::QtUpdater::Frequency frequency) const {
    switch (frequency) {
      case oclero::QtUpdater::Frequency::EveryStart:
        return tr("Every Start");
      case oclero::QtUpdater::Frequency::EveryDay:
        return tr("Every Day");
      case oclero::QtUpdater::Frequency::EveryWeek:
        return tr("Every Week");
      case oclero::QtUpdater::Frequency::EveryMonth:
        return tr("Every Month");
      default:
        return {};
    }
  }

  const std::array<oclero::QtUpdater::Frequency, 4> _frequencies{
    oclero::QtUpdater::Frequency::EveryStart,
    oclero::QtUpdater::Frequency::EveryDay,
    oclero::QtUpdater::Frequency::EveryWeek,
    oclero::QtUpdater::Frequency::EveryMonth,
  };
};

SettingsPageController::SettingsPageController(app::Settings& settings, oclero::QtUpdater& updater,
  quests::QuestUpdater& questUpdater, quests::LocalQuestListModel& questListModel,
  app::LanguageManager& languageManager, QObject* parent)
  : QObject(parent)
  , _settings(settings)
  , _updater(updater)
  , _questUpdater(questUpdater)
  , _questListModel(questListModel)
  , _languageListModel(languageManager)
  , _checkFrequencyListModel(new CheckFrequencyListModel(this)) {
  QObject::connect(&_questListModel, &quests::LocalQuestListModel::validConfigChanged, this,
    &SettingsPageController::validPathChanged);

  // Connections to app updater.
  QObject::connect(&_updater, &oclero::QtUpdater::lastCheckTimeChanged, this, [this]() {
    _settings.setAppLastUpdateCheck(QDateTime::currentDateTime());
    emit lastAppUpdateCheckTimeChanged();
  });

  QObject::connect(&_updater, &oclero::QtUpdater::stateChanged, this, [this]() {
    emit checkingForAppUpdatesChanged();
  });

  // Connections to quest updater.
  QObject::connect(&_questUpdater, &quests::QuestUpdater::stateChanged, this, [this]() {
    emit checkingForQuestUpdatesChanged();
  });
}

app::Settings* SettingsPageController::settings() const {
  return const_cast<app::Settings*>(&_settings);
}

bool SettingsPageController::validPath() const {
  return _questListModel.validConfig();
}

app::LanguageListModel* SettingsPageController::languageListModel() const {
  return const_cast<app::LanguageListModel*>(&_languageListModel);
}

QAbstractListModel* SettingsPageController::checkFrequencyListModel() const {
  return const_cast<QAbstractListModel*>(_checkFrequencyListModel);
}

QDateTime SettingsPageController::lastAppUpdateCheckTime() const {
  return _updater.lastCheckTime();
}

bool SettingsPageController::checkingforAppUpdates() const {
  return _updater.state() != oclero::QtUpdater::State::Idle;
}

bool SettingsPageController::checkingForQuestUpdates() const {
  return _questUpdater.state() != quests::QuestUpdater::State::Idle;
}

void SettingsPageController::checkForAppUpdates() {
  _updater.forceCheckForUpdate();
}

void SettingsPageController::checkForQuestUpdates() {
  _questUpdater.forceCheckForUpdates();
}
} // namespace solarus::launcher::controllers
