/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/BottomBarController.h>

namespace solarus::launcher::controllers {
BottomBarController::BottomBarController(gamepad::GamepadManager& gamepadManager, QObject* parent)
  : QObject(parent)
  , _gamepadManager(gamepadManager) {
  _timer.setInterval(6000);
  _timer.setSingleShot(true);
  QObject::connect(&_timer, &QTimer::timeout, this, [this]() {
    emit visibleChanged();
  });

  QObject::connect(&_gamepadManager, &gamepad::GamepadManager::connectedChanged, this, [this]() {
    _timer.stop();
    const auto connected = _gamepadManager.connected();
    if (connected) {
      emit visibleChanged();
    } else {
      _timer.start();
    }
  });
}

gamepad::GamepadManager* BottomBarController::gamepadManager() const {
  return &_gamepadManager;
}

bool BottomBarController::visible() const {
  return _timer.isActive() || _gamepadManager.connected();
}
} // namespace solarus::launcher::controllers
