/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/QuestUpdatePageController.h>

namespace solarus::launcher::controllers {
QuestUpdatePageController::QuestUpdatePageController(quests::QuestUpdater& questUpdater, QObject* parent)
  : QObject(parent)
  , _questUpdater(questUpdater)
  , _questUpdateListModel(new quests::QuestUpdateListModel(questUpdater, this))
  , _availableQuestUpdateProxyModel(new quests::QuestUpdateProxyModel(
      *_questUpdateListModel, quests::QuestUpdateListModel::UpdateSection::Available, this))
  , _pendingQuestUpdateProxyModel(new quests::QuestUpdateProxyModel(
      *_questUpdateListModel, quests::QuestUpdateListModel::UpdateSection::Pending, this))
  , _updatingQuestUpdateProxyModel(new quests::QuestUpdateProxyModel(
      *_questUpdateListModel, quests::QuestUpdateListModel::UpdateSection::Updating, this))
  , _errorQuestUpdateProxyModel(new quests::QuestUpdateProxyModel(
      *_questUpdateListModel, quests::QuestUpdateListModel::UpdateSection::Error, this)) {
  QObject::connect(&_questUpdater, &quests::QuestUpdater::stateChanged, this,
    &QuestUpdatePageController::lastQuestUpdateCheckTimeChanged);
}

quests::QuestUpdateListModel* QuestUpdatePageController::questUpdateListModel() const {
  return _questUpdateListModel;
}

quests::QuestUpdateProxyModel* QuestUpdatePageController::availableQuestUpdateProxyModel() const {
  return _availableQuestUpdateProxyModel;
}

quests::QuestUpdateProxyModel* QuestUpdatePageController::pendingQuestUpdateProxyModel() const {
  return _pendingQuestUpdateProxyModel;
}

quests::QuestUpdateProxyModel* QuestUpdatePageController::updatingQuestUpdateProxyModel() const {
  return _updatingQuestUpdateProxyModel;
}

quests::QuestUpdateProxyModel* QuestUpdatePageController::errorQuestUpdateProxyModel() const {
  return _errorQuestUpdateProxyModel;
}

QDateTime QuestUpdatePageController::lastQuestUpdateCheckTime() const {
  return _questUpdater.lastCheckTime();
}

void QuestUpdatePageController::checkForUpdates() {
  _questUpdater.forceCheckForUpdates();
}

void QuestUpdatePageController::updateAllAvailable() {
  for (auto& update : _questUpdater.updates()) {
    if (update->state() == quests::QuestUpdate::State::Available) {
      updateQuest(update);
    }
  }
}

void QuestUpdatePageController::cancelAllPending() {
  for (auto& update : _questUpdater.updates()) {
    if (update->state() == quests::QuestUpdate::State::Pending) {
      cancelQuestUpdate(update);
    }
  }
}

void QuestUpdatePageController::cancelAllUpdating() {
  for (auto& update : _questUpdater.updates()) {
    if (update->state() == quests::QuestUpdate::State::Downloading
        || update->state() == quests::QuestUpdate::State::Installing) {
      cancelQuestUpdate(update);
    }
  }
}

void QuestUpdatePageController::retryAllFailed() {
  for (auto& update : _questUpdater.updates()) {
    if (update->state() == quests::QuestUpdate::State::Error) {
      updateQuest(update);
    }
  }
}

void QuestUpdatePageController::updateQuest(quests::QuestUpdate* update) {
  _questUpdater.updateQuest(update);
}

void QuestUpdatePageController::cancelQuestUpdate(quests::QuestUpdate* update) {
  _questUpdater.cancelQuestUpdate(update);
}
} // namespace solarus::launcher::controllers
