/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/AppMenuController.h>

#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/app/Url.h>
#include <solarus/launcher/app/Settings.h>
#include <oclero/QtUpdater.hpp>
#include <solarus/launcher/runner/QuestRunner.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>

#include <QApplication>
#include <QDesktopServices>
#include <QUrl>

namespace solarus::launcher::controllers {
AppMenuController::AppMenuController(app::Navigation& navigation, app::Settings& settings, oclero::QtUpdater& updater,
  runner::QuestRunner& questRunner, quests::QuestUpdater& questUpdater, QObject* parent)
  : QObject(parent)
  , _navigation(navigation)
  , _settings(settings)
  , _updater(updater)
  , _questRunner(questRunner)
  , _questUpdater(questUpdater) {
  initPreferences();
  initCheckForUpdate();
  initQuit();
  initPlayQuest();
  initStopQuest();
  initCheckForQuestUpdates();
  initFullScreen();
  initFullScreenQuest();
  initChangelog();
  initUserManual();
  initAbout();
}

utils::QmlAction* AppMenuController::preferences() const {
  return _preferences.get();
}

utils::QmlAction* AppMenuController::checkForUpdates() const {
  return _checkForUpdates.get();
}

utils::QmlAction* AppMenuController::quit() const {
  return _quit.get();
}

utils::QmlAction* AppMenuController::playQuest() const {
  return _playQuest.get();
}

utils::QmlAction* AppMenuController::stopQuest() const {
  return _stopQuest.get();
}

utils::QmlAction* AppMenuController::checkForQuestUpdates() const {
  return _checkForQuestUpdates.get();
}

utils::QmlAction* AppMenuController::fullScreen() const {
  return _fullScreen.get();
}

utils::QmlAction* AppMenuController::fullScreenQuest() const {
  return _fullScreenQuest.get();
}

utils::QmlAction* AppMenuController::changelog() const {
  return _changelog.get();
}

utils::QmlAction* AppMenuController::userManual() const {
  return _userManual.get();
}

utils::QmlAction* AppMenuController::about() const {
  return _about.get();
}

void AppMenuController::initPreferences() {
  _preferences = std::make_unique<utils::QmlAction>();
  _preferences->setRole(QAction::MenuRole::PreferencesRole);
#ifdef __APPLE__
  _preferences->setShortcutFromKey(QKeySequence::StandardKey::Preferences);
#else
  _preferences->setShortcutFromKey({ Qt::CTRL + Qt::Key_Comma });
#endif
  _preferences->setCallback([this]() {
    _navigation.setCurrentPage(app::Navigation::PageId::Settings);
  });
  _preferences->setEnabledPredicate([this]() {
    return _navigation.currentWindow() == app::Navigation::WindowId::MainWindow;
  });
  QObject::connect(
    &_navigation, &app::Navigation::currentWindowChanged, _preferences.get(), &utils::QmlAction::updateEnabled);
}

void AppMenuController::initCheckForUpdate() {
  _checkForUpdates = std::make_unique<utils::QmlAction>();
  _checkForUpdates->setRole(QAction::MenuRole::ApplicationSpecificRole);
  _checkForUpdates->setCallback([this]() {
    _updater.checkForUpdate();
  });
  _checkForUpdates->setEnabledPredicate([this]() {
    const auto notCurrentlyChecking = _updater.state() == oclero::QtUpdater::State::Idle;
    return notCurrentlyChecking;
  });
  QObject::connect(
    &_updater, &oclero::QtUpdater::stateChanged, _checkForUpdates.get(), &utils::QmlAction::updateEnabled);
}

void AppMenuController::initQuit() {
  _quit = std::make_unique<utils::QmlAction>();
#ifdef WIN32
  _quit->setShortcutFromKey({ Qt::CTRL + Qt::Key_Q });
#else
  _quit->setShortcutFromKey({ QKeySequence::StandardKey::Quit });
#endif
  _quit->setRole(QAction::MenuRole::QuitRole);
  _quit->setCallback([]() {
    QApplication::quit();
  });
}

void AppMenuController::initPlayQuest() {
  _playQuest = std::make_unique<utils::QmlAction>();
  _playQuest->setShortcutFromKey({ Qt::Key_F5 });
  _playQuest->setCallback([this]() {
    if (auto* selectedQuest = _navigation.selectedQuest()) {
      _questRunner.start(selectedQuest);
    }
  });
  _playQuest->setEnabledPredicate([this]() {
    return _navigation.selectedQuest() != nullptr && _questRunner.state() == runner::QuestRunner::State::Stopped;
  });
  _playQuest->setVisiblePredicate([this]() {
    return _playQuest->enabled() || (_stopQuest && !_stopQuest->enabled());
  });
  QObject::connect(
    &_questRunner, &runner::QuestRunner::stateChanged, _playQuest.get(), &utils::QmlAction::updateEnabled);
  QObject::connect(
    &_navigation, &app::Navigation::selectedQuestStateChanged, _playQuest.get(), &utils::QmlAction::updateEnabled);
  QObject::connect(
    _playQuest.get(), &utils::QmlAction::enabledChanged, _playQuest.get(), &utils::QmlAction::updateVisible);
}

void AppMenuController::initStopQuest() {
  _stopQuest = std::make_unique<utils::QmlAction>();
  _stopQuest->setShortcutFromKey({ Qt::Key_F5 });
  _stopQuest->setCallback([this]() {
    _questRunner.stop();
  });
  _stopQuest->setEnabledPredicate([this]() {
    return _navigation.selectedQuest() != nullptr && _questRunner.state() == runner::QuestRunner::State::Running;
  });
  _stopQuest->setVisiblePredicate([this]() {
    return _stopQuest->enabled();
  });
  QObject::connect(
    &_questRunner, &runner::QuestRunner::stateChanged, _stopQuest.get(), &utils::QmlAction::updateEnabled);
  QObject::connect(
    &_navigation, &app::Navigation::selectedQuestStateChanged, _stopQuest.get(), &utils::QmlAction::updateEnabled);
  QObject::connect(
    _stopQuest.get(), &utils::QmlAction::enabledChanged, _playQuest.get(), &utils::QmlAction::updateVisible);
  _playQuest->updateVisible();
  _stopQuest->updateVisible();
}

void AppMenuController::initCheckForQuestUpdates() {
  _checkForQuestUpdates = std::make_unique<utils::QmlAction>();
  _checkForQuestUpdates->setShortcutFromKey({ Qt::CTRL + Qt::Key_U });
  _checkForQuestUpdates->setCallback([this]() {
    _questUpdater.forceCheckForUpdates();
  });
  _checkForQuestUpdates->setEnabledPredicate([this]() {
    const auto notCurrentlyChecking = _questUpdater.state() == quests::QuestUpdater::State::Idle;
    return notCurrentlyChecking;
  });
  QObject::connect(
    &_questUpdater, &quests::QuestUpdater::stateChanged, _checkForQuestUpdates.get(), &utils::QmlAction::updateEnabled);
}


void AppMenuController::initFullScreen() {
  _fullScreen = std::make_unique<utils::QmlAction>();
  _fullScreen->setCheckable(true);
#ifdef __APPLE__
  // On macOS, the full screen is handled by the OS itself.
  _fullScreen->setVisible(false);
  _fullScreen->setEnabled(false);
#else
  _fullScreen->setShortcutFromKey(QKeySequence::StandardKey::FullScreen);
#endif
  _fullScreen->setCallback([this]() {
    _settings.setAppFullScreen(_fullScreen->checked());
  });
  _fullScreen->setCheckedPredicate([this]() {
    return _settings.appFullScreen();
  });
  QObject::connect(
    &_settings, &app::Settings::appFullScreenChanged, _fullScreen.get(), &utils::QmlAction::updateChecked);
}

void AppMenuController::initFullScreenQuest() {
  _fullScreenQuest = std::make_unique<utils::QmlAction>();
  _fullScreenQuest->setCheckable(true);
  _fullScreenQuest->setShortcutFromKey({ Qt::SHIFT + Qt::Key_F11 });
  _fullScreenQuest->setCallback([this]() {
    _settings.setQuestFullScreen(_fullScreenQuest->checked());
  });
  _fullScreenQuest->setCheckedPredicate([this]() {
    return _settings.questFullScreen();
  });
  QObject::connect(
    &_settings, &app::Settings::questFullScreenChanged, _fullScreenQuest.get(), &utils::QmlAction::updateChecked);
}

void AppMenuController::initChangelog() {
  _changelog = std::make_unique<utils::QmlAction>();
  _changelog->setCallback([]() {
    QDesktopServices::openUrl({ app::CHANGELOG_URL });
  });
}

void AppMenuController::initUserManual() {
  _userManual = std::make_unique<utils::QmlAction>();
  _userManual->setShortcutFromKey(QKeySequence::StandardKey::HelpContents);
  _userManual->setCallback([]() {
    QDesktopServices::openUrl({ app::SUPPORT_URL });
  });
}

void AppMenuController::initAbout() {
  _about = std::make_unique<utils::QmlAction>();
  _about->setRole(QAction::MenuRole::AboutRole);
  _about->setCallback([this]() {
    if (_navigation.currentWindow() == app::Navigation::WindowId::MainWindow) {
      _navigation.setCurrentPage(app::Navigation::PageId::About);
    }
  });
  _about->setEnabledPredicate([this]() {
    return _navigation.currentWindow() == app::Navigation::WindowId::MainWindow;
  });
  QObject::connect(
    &_navigation, &app::Navigation::currentWindowChanged, _about.get(), &utils::QmlAction::updateEnabled);
}
} // namespace solarus::launcher::controllers
