/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/OnlineQuestsPageController.h>
#include <solarus/launcher/app/Url.h>

#include <QDesktopServices>
#include <QUrl>

// #include <solarus/launcher/quests/LocalQuestListModel.h>

namespace solarus::launcher::controllers {
OnlineQuestsPageController::OnlineQuestsPageController(app::Navigation& navigation, quests::QuestUpdater& questUpdater,
  quests::OnlineQuestListModel& questListModel, QObject* parent)
  : QObject(parent)
  , _navigation(navigation)
  , _questListModel(questListModel)
  , _questListProxyModel(new quests::OnlineQuestProxyModel(questListModel))
  , _questUpdater(questUpdater) {
  QObject::connect(
    &_navigation, &app::Navigation::selectedQuestChanged, this, &OnlineQuestsPageController::selectedQuestChanged);
}


quests::OnlineQuestListModel* OnlineQuestsPageController::questListModel() const {
  return const_cast<quests::OnlineQuestListModel*>(&_questListModel);
}

quests::OnlineQuestProxyModel* OnlineQuestsPageController::questListProxyModel() const {
  return _questListProxyModel;
}

quests::Quest* OnlineQuestsPageController::selectedQuest() const {
  return _selectedQuest;
}

void OnlineQuestsPageController::setSelectedQuest(quests::Quest* quest) {
  if (quest != _selectedQuest) {
    _selectedQuest = quest;
    emit selectedQuestChanged();
  }
}

void OnlineQuestsPageController::refresh() {
  _questListModel.startSearch();
}

void OnlineQuestsPageController::focusSideBar() {
  _navigation.focusSideBar();
}

void OnlineQuestsPageController::openGamesOnWebsite() const {
  QDesktopServices::openUrl(QUrl{ app::GAMES_URL });
}

void OnlineQuestsPageController::downloadQuest(quests::Quest* quest) {
  Q_UNUSED(quest)
  //if (quest) {
  // 1. Add a new Quest* to local database.
  // 2. Synchronize local Quest* and remote Quest* state (downloading, progress).
  // 3. When done, mark local and remote Quest* as downloaded.
  //}
}

void OnlineQuestsPageController::updateQuest(quests::Quest* quest) {
  if (quest) {
    //_questUpdater.updateQuest(quest->id());
  }
}
} // namespace solarus::launcher::controllers
