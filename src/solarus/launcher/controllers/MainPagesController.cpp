/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/MainPagesController.h>

namespace solarus::launcher::controllers {
MainPagesController::MainPagesController(runner::QuestRunner& questRunner, app::Navigation& navigation, QObject* parent)
  : QObject(parent)
  , _navigation(navigation)
  , _questRunner(questRunner) {
  QObject::connect(
    &_questRunner, &runner::QuestRunner::errorRaised, this, &MainPagesController::questProcessErrorRaised);

  QObject::connect(&_questRunner, &runner::QuestRunner::aboutToStop, this, [this] {
    if (_navigation.currentPage() == app::Navigation::PageId::PlayingQuest) {
      // Prevent crash...
      QTimer::singleShot(0, [this]() {
        _navigation.setCurrentPage(app::Navigation::PageId::LocalQuests);
      });
    }
  });
}

app::Navigation* MainPagesController::navigation() const {
  return const_cast<app::Navigation*>(&_navigation);
}
} // namespace solarus::launcher::controllers
