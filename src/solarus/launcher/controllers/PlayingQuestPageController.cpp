/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/controllers/PlayingQuestPageController.h>

#include <solarus/launcher/runner/QuestOutputHandler.h>

namespace solarus::launcher::controllers {
PlayingQuestPageController::PlayingQuestPageController(runner::QuestRunner& questRunner, QObject* parent)
  : QObject(parent)
  , _questRunner(questRunner) {
  // Forward quest output.
  auto& handler = _questRunner.outputHandler();
  QObject::connect(
    &handler, &runner::QuestOutputHandler::outputColorsChanged, this, &PlayingQuestPageController::outputColorsChanged);
  QObject::connect(
    &handler, &runner::QuestOutputHandler::outputCleared, this, &PlayingQuestPageController::outputCleared);
  QObject::connect(&handler, &runner::QuestOutputHandler::htmlProduced, this, &PlayingQuestPageController::outputAdded);

  // Forward state changes.
  QObject::connect(
    &_questRunner, &runner::QuestRunner::questChanged, this, &PlayingQuestPageController::playingQuestChanged);
  QObject::connect(&_questRunner, &runner::QuestRunner::errorRaised, this, &PlayingQuestPageController::errorRaised);
}

runner::QuestOutputColors* PlayingQuestPageController::outputColors() const {
  return _questRunner.outputHandler().outputColors();
}

void PlayingQuestPageController::setOutputColors(runner::QuestOutputColors* outputColors) {
  _questRunner.outputHandler().setOutputColors(outputColors);
}

const QString& PlayingQuestPageController::fullOutput() const {
  return _questRunner.fullOutput();
}

quests::LocalQuest* PlayingQuestPageController::playingQuest() const {
  return _questRunner.quest();
}

void PlayingQuestPageController::stopQuest() {
  _questRunner.stop();
}
} // namespace solarus::launcher::controllers
