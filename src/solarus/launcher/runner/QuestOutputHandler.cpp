/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/runner/QuestOutputHandler.h>
#include <solarus/launcher/runner/QuestRunner.h>

#include <QDebug>
#include <QLoggingCategory>

namespace solarus::launcher::runner {
Q_LOGGING_CATEGORY(LOGCAT_QUESTOUTPUTHANDLER, "solarus.launcher.QuestOutputHandler")

namespace {
static const QRegularExpression output_regexp("^\\[Solarus\\] \\[(\\d+)\\] (\\w*): (.+)$");
static const QRegularExpression output_command_result_begin_regexp("^====== Begin Lua command #(\\d+) ======$");
static const QRegularExpression output_command_result_end_regexp("^====== End Lua command #(\\d+): (\\w+) ======$");
static const QRegularExpression output_simplify_console_error_regexp("In Lua command: \\[string \".*\"\\]:\\d+: ");
static const QRegularExpression output_setting_fullscreen_regexp("^Fullscreen: (\\w+)$");

constexpr auto LOGLEVEL_DEBUG = "Debug";
constexpr auto LOGLEVEL_INFO = "Info";
constexpr auto LOGLEVEL_WARNING = "Warning";
constexpr auto LOGLEVEL_ERROR = "Error";
constexpr auto LOGLEVEL_FATAL = "Fatal";
} // namespace

QuestOutputHandler::QuestOutputHandler(QObject* parent)
  : QObject(parent) {}

QuestOutputHandler::~QuestOutputHandler() = default;

QuestOutputColors* QuestOutputHandler::outputColors() const {
  return _outputColors.data();
}

void QuestOutputHandler::setOutputColors(QuestOutputColors* outputColors) {
  if (outputColors != _outputColors) {
    _outputColors = outputColors;
    emit outputColorsChanged();
  }
}

void QuestOutputHandler::onOutputProduced(const QStringList& lines) {
  for (const auto& line : lines) {
    parseAndPrintLine(line);
  }
}

void QuestOutputHandler::onStateChanged(bool playing) {
  if (!playing) {
    emit outputCleared();
  }
}

void QuestOutputHandler::parseAndPrintLine(const QString& line) {
#ifdef QT_DEBUG
  qDebug().noquote() << line;
#endif

  if (line.isEmpty()) {
    return;
  }

  QString logLevel;
  QString message = line;
  const auto matchResult = output_regexp.match(line);

  // 4 captures expected: full line, time, log level, message.
  if (matchResult.hasMatch()) {
    const auto captures = matchResult.capturedTexts();
    if (captures.size() != 4) {
      emit htmlProduced(line.toHtmlEscaped());
      return;
    }

    logLevel = captures[2];
    message = captures[3];
  }

  // Solarus produced an empty message just to flush its stdout.
  if (!logLevel.isEmpty() && message.isEmpty()) {
    return;
  }

  // Detect technical delimiters of commands output but don't show them.
  if (detectCommandResult(logLevel, message)) {
    return;
  }

  // Not a line from Solarus, probably one from the quest or Qt.
  if (logLevel.isEmpty()) {
    emit htmlProduced(line.toHtmlEscaped());
    return;
  }

  // Report system setting changes.
  detectSettingChange(logLevel, message);

  // Clean specific error messages.
  if (logLevel == LOGLEVEL_ERROR) {
    message.remove(output_simplify_console_error_regexp);
  }

  // Add color.
  const auto htmlLine = colorizeOutput(logLevel, message);
  if (htmlLine.isEmpty()) {
    return;
  }

  // Emit signal to tell the GUI that html is ready.
  emit htmlProduced(htmlLine);
}

bool QuestOutputHandler::detectCommandResult(const QString& logLevel, const QString& message) {
  QRegularExpressionMatch matchResult;

  // Detect the beginning of a console command result.
  matchResult = output_command_result_begin_regexp.match(message);
  if (logLevel == LOGLEVEL_INFO && matchResult.lastCapturedIndex() == 1) {
    // Start of a command result.
    if (_outputCommandId != -1) {
      qCWarning(LOGCAT_QUESTOUTPUTHANDLER) << "Beginning of a command result inside another command result";
    }

    _outputCommandId = matchResult.captured(1).toInt();
    _outputCommandResult.clear();

    // Show the command in the log view.
    // We show the command only when receiving its results,
    // to make sure it is displayed just before its results.
    const auto command = _pendingCommands.take(_outputCommandId);
    emit htmlProduced(QStringLiteral("> %1").arg(command).toHtmlEscaped());

    return true;
  }

  // Detect the end of a console command result.
  matchResult = output_command_result_end_regexp.match(message);
  if (logLevel == LOGLEVEL_INFO && matchResult.lastCapturedIndex() == 2) {
    // End of a command result.
    if (_outputCommandId == -1) {
      qCWarning(LOGCAT_QUESTOUTPUTHANDLER) << "End of a command result without beginning";
      return false;
    }
    const auto id = matchResult.captured(1).toInt();
    const auto success = matchResult.captured(2) == "success";
    const auto command = _pendingCommands.take(_outputCommandId);
    const auto result = _outputCommandResult;

    if (id != _outputCommandId) {
      qCWarning(LOGCAT_QUESTOUTPUTHANDLER) << "Unmatched command delimiters";
    }

    _pendingCommands.remove(_outputCommandId);
    _outputCommandId = -1;
    _outputCommandResult.clear();

    emit commandResultReceived(id, command, success, result);

    return true;
  }

  // Process the current result between delimiters.
  if (_outputCommandId != -1) {
    // We are inside the delimiters.
    _outputCommandResult += message;
    // Let the console colorize and show the text normally.
    return false;
  }

  return false;
}

void QuestOutputHandler::detectSettingChange(const QString& logLevel, const QString& message) {
  if (logLevel != LOGLEVEL_INFO) {
    return;
  }

  const auto match_result = output_setting_fullscreen_regexp.match(message);
  if (match_result.lastCapturedIndex() == 1) {
    const auto value = QVariant(match_result.captured(1) == QStringLiteral("yes"));
    emit questSettingChanged(QStringLiteral("quest_fullscreen"), value);
  }
}

QString QuestOutputHandler::colorizeOutput(const QString& log_level, const QString& message) const {
  if (message.isEmpty()) {
    return message;
  }

  // Colorize warnings and errors.
  const auto decorated_line = QStringLiteral("%1: %2").arg(log_level, message);
  if (log_level == LOGLEVEL_DEBUG) {
    return colorize(
      decorated_line, _outputColors ? _outputColors->debugColor().name(QColor::HexArgb) : QStringLiteral(""));
  } else if (log_level == LOGLEVEL_INFO) {
    return colorize(
      decorated_line, _outputColors ? _outputColors->infoColor().name(QColor::HexArgb) : QStringLiteral(""));
  } else if (log_level == LOGLEVEL_WARNING) {
    return colorize(
      decorated_line, _outputColors ? _outputColors->warningColor().name(QColor::HexArgb) : QStringLiteral(""));
  } else if (log_level == LOGLEVEL_ERROR) {
    return colorize(
      decorated_line, _outputColors ? _outputColors->errorColor().name(QColor::HexArgb) : QStringLiteral(""));
  } else if (log_level == LOGLEVEL_FATAL) {
    return colorize(
      decorated_line, _outputColors ? _outputColors->fatalColor().name(QColor::HexArgb) : QStringLiteral(""));
  } else {
    // Unknown log level.
    return message;
  }
}

QString QuestOutputHandler::colorize(const QString& line, const QString& color) const {
  return line.trimmed().isEmpty()
           ? ""
           : QStringLiteral("<span style=\"color: %1\">%2</span>").arg(color, line.toHtmlEscaped());
}
} // namespace solarus::launcher::runner
