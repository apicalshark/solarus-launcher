/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/runner/QuestOutputColors.h>

namespace solarus::launcher::runner {
QuestOutputColors::QuestOutputColors(QObject* parent)
  : QObject(parent) {}

const QColor& QuestOutputColors::debugColor() const {
  return _debugColor;
}

void QuestOutputColors::setDebugColor(const QColor& color) {
  if (color != _debugColor) {
    _debugColor = color;
    emit debugColorChanged();
  }
}

const QColor& QuestOutputColors::infoColor() const {
  return _infoColor;
}

void QuestOutputColors::setInfoColor(const QColor& color) {
  if (color != _infoColor) {
    _infoColor = color;
    emit infoColorChanged();
  }
}

const QColor& QuestOutputColors::warningColor() const {
  return _warningColor;
}

void QuestOutputColors::setWarningColor(const QColor& color) {
  if (color != _warningColor) {
    _warningColor = color;
    emit warningColorChanged();
  }
}

const QColor& QuestOutputColors::errorColor() const {
  return _errorColor;
}

void QuestOutputColors::setErrorColor(const QColor& color) {
  if (color != _errorColor) {
    _errorColor = color;
    emit errorColorChanged();
  }
}

const QColor& QuestOutputColors::fatalColor() const {
  return _fatalColor;
}

void QuestOutputColors::setFatalColor(const QColor& color) {
  if (color != _fatalColor) {
    _fatalColor = color;
    emit fatalColorChanged();
  }
}
} // namespace solarus::launcher::runner
