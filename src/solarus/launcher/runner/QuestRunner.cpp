/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QCoreApplication>

#include <solarus/launcher/runner/QuestRunner.h>
#include <solarus/launcher/app/Settings.h>

namespace solarus::launcher::runner {

QuestRunner::QuestRunner(app::Settings& settings, QObject* parent)
  : QObject(parent)
  , _settings(settings) {
  // Set the process channel mode to merged (stdout + stderr).
  _process.setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);

  // Connect to QProcess signals to know when the quest is running and finished.
  QObject::connect(&_process, &QProcess::errorOccurred, this, &QuestRunner::onProcessError);
  QObject::connect(&_process, &QProcess::readyReadStandardOutput, this, &QuestRunner::onProcessStandardOutputAvailable);
  QObject::connect(&_process, &QProcess::stateChanged, this, &QuestRunner::onProcessStateChanged);
  QObject::connect(
    &_process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, &QuestRunner::onProcessFinished);

  // Workaround to make the quest process close properly instead of hanging while reading on its stdin on Windows.
  constexpr auto timerDelay = 100; // ms
  QObject::connect(&_timer, &QTimer::timeout, this, &QuestRunner::onTimerTimeout);
  _timer.start(timerDelay);

  QObject::connect(&_questOutputHandler, &QuestOutputHandler::htmlProduced, this, [this](const auto& html) {
    _questFullOutput.append(html);
  });
  QObject::connect(&_questOutputHandler, &QuestOutputHandler::outputCleared, this, [this]() {
    _questFullOutput.clear();
  });
}

QuestRunner::~QuestRunner() {
  if (_process.state() != QProcess::ProcessState::NotRunning) {
    // Give a chance to the quest process to finish properly.
    _process.terminate();
    constexpr auto waitDelay = 1000; // ms
    if (!_process.waitForFinished(waitDelay)) {
      // Kill it after a delay.
      _process.kill();
    }
  }
}

QuestRunner::State QuestRunner::state() const {
  switch (_process.state()) {
    case QProcess::ProcessState::NotRunning:
      return State::Stopped;
    case QProcess::ProcessState::Starting:
      return State::Starting;
    case QProcess::ProcessState::Running:
      return State::Running;
    default:
      return State::Stopped;
  }
}

quests::LocalQuest* QuestRunner::quest() const {
  return _quest;
}

void QuestRunner::setQuest(quests::LocalQuest* quest) {
  if (quest != _quest) {
    // Stop previous quest.
    if (_quest) {
      _quest->setPlaying(false);
      _questFullOutput.clear();
    }
    // Start new one.
    _quest = quest;
    if (_quest) {
      _quest->setPlaying(true);
    }
    emit questChanged();
  }
}

QuestOutputHandler& QuestRunner::outputHandler() const {
  return const_cast<QuestRunner*>(this)->_questOutputHandler;
}

const QString& QuestRunner::fullOutput() const {
  return _questFullOutput;
}

void QuestRunner::start(quests::LocalQuest* quest) {
  if (!quest || quest->path().isEmpty()) {
    return;
  }

  if (_process.state() != QProcess::ProcessState::NotRunning) {
    return;
  }

  // Prepare arguments for the command.
  const auto [programName, arguments] = createArguments(quest->path());

  // Run the current executable itself.
  setQuest(quest);
  _process.start(programName, arguments);
}

void QuestRunner::stop() {
  if (_process.state() != QProcess::ProcessState::NotRunning) {
    emit aboutToStop();
    setQuest(nullptr);
    _process.terminate();
  }
}

int QuestRunner::executeCommand(const QString& command) {
  if (_process.state() != QProcess::ProcessState::NotRunning) {
    return -1;
  }

  if (command.trimmed().isEmpty()) {
    return -1;
  }

  auto commandUtf8 = command.toUtf8();
  commandUtf8.append('\n');
  const auto bytesWritten = _process.write(commandUtf8);
  if (bytesWritten != commandUtf8.size()) {
    return -1;
  }

  ++_lastCommandId;
  return _lastCommandId;
}

void QuestRunner::onProcessError(QProcess::ProcessError error) {
  switch (error) {
    case QProcess::ProcessError::FailedToStart:
      emit errorRaised(ErrorCode::ProcessFailedToStart);
      break;
    case QProcess::ProcessError::Crashed:
      emit errorRaised(ErrorCode::ProcessCrashed);
      break;
    case QProcess::ProcessError::Timedout:
      emit errorRaised(ErrorCode::ProcessTimedOut);
      break;
    case QProcess::ProcessError::WriteError:
      emit errorRaised(ErrorCode::ProcessWriteError);
      break;
    case QProcess::ProcessError::ReadError:
      emit errorRaised(ErrorCode::ProcessReadError);
      break;
    case QProcess::ProcessError::UnknownError:
      emit errorRaised(ErrorCode::UnknownError);
      break;
    default:
      break;
  }
}

void QuestRunner::onProcessFinished(int /*exitCode*/, QProcess::ExitStatus /*exitStatus*/) {
  emit aboutToStop();
  _lastCommandId = -1;
  setQuest(nullptr);
  emit stateChanged();
  _questOutputHandler.onStateChanged(false);
}

void QuestRunner::onProcessStandardOutputAvailable() {
  // Read the UTF-8 data available.
  QStringList lines;
  while (_process.canReadLine()) {
    auto line = QString::fromUtf8(_process.readLine());

    // Remove the final '\n'.
    line = line.trimmed();

    if (!line.isEmpty()) {
      lines << line;
    }
  }

  if (!lines.isEmpty()) {
    _questOutputHandler.onOutputProduced(lines);
  }
}

void QuestRunner::onProcessStateChanged(QProcess::ProcessState state) {
  if (state == QProcess::ProcessState::NotRunning) {
    _lastCommandId = -1;
    setQuest(nullptr);
  }
  emit stateChanged();
  _questOutputHandler.onStateChanged(this->state() == State::Running);
}

void QuestRunner::onTimerTimeout() {
  if (_process.state() != QProcess::ProcessState::NotRunning) {
    _process.write(QByteArrayLiteral("\n"));
  }
}

std::pair<QString, QStringList> QuestRunner::createArguments(const QString& questPath) const {
  // Options should be first, then quest path as ultimate arg.
  const auto programName = QCoreApplication::applicationFilePath();
  QStringList arguments;

  // -no-audio
  if (!_settings.questEnableAudio()) {
    arguments << QStringLiteral("-no-audio");
  }

  // -force-software-rendering
  if (_settings.questForceSoftwareRendering()) {
    arguments << QStringLiteral("-force-software-rendering");
  }

  // -fullscreen
  if (_settings.questFullScreen()) {
    arguments << QStringLiteral("-fullscreen");
  }

  // -suspend-unfocused
  if (!_settings.questSuspendWhenUnfocused()) {
    arguments << QStringLiteral("-suspend-unfocused=no");
  }

  // <questPath>
  arguments << questPath;

  return { programName, arguments };
}
} // namespace solarus::launcher::runner
