/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/news/News.h>

#include <QJsonObject>

namespace solarus::launcher::news {
News News::fromJson(const QJsonObject& jsonObject, const QString& urlDomain) {
  const auto urlKey = QStringLiteral("permalink");
  const auto urlValue = jsonObject.contains(urlKey) ? QUrl{ urlDomain + jsonObject.value(urlKey).toString() } : QUrl{};
  if (!urlValue.isValid()) {
    return {};
  }

  const auto titleKey = QStringLiteral("title");
  const auto titleValue = jsonObject.contains(titleKey) ? jsonObject.value(titleKey).toString() : QString{};
  if (titleValue.isEmpty()) {
    return {};
  }

  const auto excerptKey = QStringLiteral("excerpt");
  const auto excerptValue = jsonObject.contains(excerptKey) ? jsonObject.value(excerptKey).toString() : QString{};
  if (excerptValue.isEmpty()) {
    return {};
  }

  const auto dateKey = QStringLiteral("date");
  const auto dateValue = jsonObject.contains(dateKey)
                           ? QDate::fromString(jsonObject.value(dateKey).toString(), QStringLiteral("yyyy-MM-dd"))
                           : QDate{};
  if (!dateValue.isValid()) {
    return {};
  }

  const auto thumbnailUrlKey = QStringLiteral("thumbnail");
  const auto thumbnailUrlValue =
    jsonObject.contains(urlKey) ? QUrl{ urlDomain + jsonObject.value(thumbnailUrlKey).toString() } : QUrl{};
  if (!thumbnailUrlValue.isValid()) {
    return {};
  }

  return { true, urlValue, titleValue, excerptValue, dateValue, thumbnailUrlValue };
}
} // namespace solarus::launcher::news
