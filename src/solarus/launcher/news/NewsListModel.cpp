/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/Url.h>
#include <solarus/launcher/news/NewsListModel.h>
#include <solarus/launcher/utils/UrlUtils.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>

namespace solarus::launcher::news {
constexpr auto newsApiVersion = "1.0.0";

NewsListModel::NewsListModel(QObject* parent)
  : QAbstractListModel(parent) {
  setEndpointUrl(app::NEWS_API_URL);
}

NewsListModel::~NewsListModel() {}

int NewsListModel::rowCount(const QModelIndex& index) const {
  Q_UNUSED(index)

  return _news.size();
}

QVariant NewsListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return {};

  if (index.row() >= _news.size())
    return {};

  const auto row = index.row();
  const auto& newsArticle = _news.at(row);

  if (!newsArticle.isValid)
    return {};

  switch (role) {
    case IsValidRole:
      return newsArticle.isValid;
    case UrlRole:
      return newsArticle.url;
    case TitleRole:
      return newsArticle.title;
    case DateRole:
      return newsArticle.date;
    case ExcerptRole:
      return newsArticle.excerpt;
    case CoverUrlRole:
      return newsArticle.thumbnailUrl;
    default:
      return {};
  }
}

QHash<int, QByteArray> NewsListModel::roleNames() const {
  return QHash<int, QByteArray>{
    { IsValidRole, "isValid" },
    { UrlRole, "url" },
    { TitleRole, "title" },
    { DateRole, "date" },
    { ExcerptRole, "excerpt" },
    { CoverUrlRole, "coverUrl" },
  };
}

int NewsListModel::newsCount() const {
  return _news.count();
}

int NewsListModel::maxNewsCount() const {
  return _maxNewsCount;
}

void NewsListModel::setMaxNewsCount(int maxNewsCount) {
  if (maxNewsCount != _maxNewsCount) {
    _maxNewsCount = maxNewsCount;
    emit maxNewsCountChanged();
    refresh();
  }
}

const QString& NewsListModel::endpointUrl() const {
  return _endpointUrl;
}

void NewsListModel::setEndpointUrl(const QString& url) {
  if (url != _endpointUrl) {
    _endpointUrl = url;
    emit endpointUrlChanged();
  }
}

NewsListModel::State NewsListModel::state() const {
  return _state;
}

void NewsListModel::setState(State const state) {
  if (state != _state) {
    _state = state;
    emit stateChanged();
  }
}

void NewsListModel::reset() {
  setState(State::Loading);
  beginResetModel();
  _news.clear();
  endResetModel();
  setState(State::Idle);
  emit newsCountChanged();
}

bool NewsListModel::updateNewsList(const QByteArray& data) {
  // Parse JSON.
  QJsonParseError parseError;
  const auto jsonDoc = QJsonDocument::fromJson(data, &parseError);
  if (jsonDoc.isNull() || parseError.error != QJsonParseError::NoError || !jsonDoc.isObject())
    return false;

  const auto jsonObject = jsonDoc.object();
  const auto resultApiVersion = jsonObject.value(QStringLiteral("version")).toString();
  if (resultApiVersion != newsApiVersion)
    return false;

  const auto resultArray = jsonObject.value(QStringLiteral("data")).toArray();
  if (resultArray.isEmpty())
    return false;

  // Links are relative so we need the domain.
  const auto urlDomain = utils::getRoot(app::NEWS_API_URL);

  for (const auto& arrayElement : resultArray) {
    if (arrayElement.isObject()) {
      const auto newsJsonObject = arrayElement.toObject();
      const auto newsData = News::fromJson(newsJsonObject, urlDomain);
      if (newsData.isValid) {
        _news.append(newsData);
      }

      // Only treat N first elements.
      if (_news.size() >= _maxNewsCount)
        break;
    }
  }

  return true;
}

void NewsListModel::refresh() {
  setState(State::Loading);
  beginResetModel();
  _news.clear();

  _downloader.downloadData(
    QUrl(_endpointUrl),
    // onFinished
    [this](oclero::QtDownloader::ErrorCode const errorCode, const QByteArray& data) {
      auto jsonError = false;
      const auto networkError = errorCode != oclero::QtDownloader::ErrorCode::NoError;
      if (!networkError) {
        jsonError = !updateNewsList(data);
      }

      // Update current state.
      const auto state = networkError || jsonError ? State::Error : State::Idle;
      setState(state);

      endResetModel();
      emit newsCountChanged();
    },
    // onProgress
    [this](int progress) {
      emit progressChanged(progress);
    });
}
} // namespace solarus::launcher::news
