/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/utils/QmlAction.h>

namespace solarus::launcher::utils {
QmlAction::QmlAction(QObject* parent)
  : QObject(parent) {}

QmlAction::QmlAction(const QString& text, const QString& shortcut, const QAction::MenuRole role, QObject* parent)
  : QObject(parent)
  , _text(text)
  , _shortcut(shortcut)
  , _role(role) {}

const QString& QmlAction::text() const {
  return _text;
}

bool QmlAction::enabled() const {
  return _enabled;
}

bool QmlAction::visible() const {
  return _visible;
}

bool QmlAction::checkable() const {
  return _checkable;
}

bool QmlAction::checked() const {
  return _checked;
}

const QString& QmlAction::shortcut() const {
  return _shortcut;
}

QAction::MenuRole QmlAction::role() const {
  return _role;
}

void QmlAction::setText(const QString& text) {
  if (text != _text) {
    _text = text;
    emit textChanged();
  }
}

void QmlAction::setEnabled(bool enabled) {
  if (enabled != _enabled) {
    _enabled = enabled;
    emit enabledChanged();
  }
}

void QmlAction::setVisible(bool visible) {
  if (visible != _visible) {
    _visible = visible;
    emit visibleChanged();
  }
}

void QmlAction::setCheckable(bool checkable) {
  if (checkable != _checkable) {
    _checkable = checkable;

    if (!checkable) {
      _checked = false;
    }

    emit checkableChanged();

    if (checkable) {
      emit checkedChanged();
    }
  }
}

void QmlAction::setChecked(bool checked) {
  if (!checkable())
    return;

  if (checked != _checked) {
    _checked = checked;
    emit checkedChanged();
  }
}

void QmlAction::setShortcut(const QString& shortcut) {
  if (shortcut != _shortcut) {
    _shortcut = shortcut;
    emit shortcutChanged();
  }
}

void QmlAction::setShortcutFromKey(const QKeySequence& keySeq) {
  const auto shortcutAsString = keySeq.toString();
  setShortcut(shortcutAsString);
}

void QmlAction::setRole(QAction::MenuRole role) {
  if (role != _role) {
    _role = role;
    emit roleChanged();
  }
}

void QmlAction::trigger() {
  if (_enabled) {
    if (_checkable) {
      setChecked(!checked());
    }

    if (_callback) {
      _callback();
    }

    emit triggered();
  }
}

void QmlAction::updateChecked() {
  if (_checkedPredicate) {
    const auto checked = _checkedPredicate();
    setChecked(checked);
  }
}

void QmlAction::updateEnabled() {
  if (_enabledPredicate) {
    const auto enabled = _enabledPredicate();
    setEnabled(enabled);
  }
}

void QmlAction::updateVisible() {
  if (_visiblePredicate) {
    const auto visible = _visiblePredicate();
    setVisible(visible);
  }
}

void QmlAction::setCallback(const std::function<void()>& callback) {
  _callback = callback;
}

void QmlAction::setCheckedPredicate(const std::function<bool()>& predicate) {
  _checkedPredicate = predicate;
  updateChecked();
}

void QmlAction::setEnabledPredicate(const std::function<bool()>& predicate) {
  _enabledPredicate = predicate;
  updateEnabled();
}

void QmlAction::setVisiblePredicate(const std::function<bool()>& predicate) {
  _visiblePredicate = predicate;
  updateVisible();
}
} // namespace solarus::launcher::utils
