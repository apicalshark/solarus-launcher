/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/LanguageListModel.h>

namespace solarus::launcher::app {
LanguageListModel::LanguageListModel(LanguageManager& languageManager, QObject* parent)
  : QAbstractListModel(parent)
  , _languageManager(languageManager) {
  QObject::connect(&_languageManager, &LanguageManager::availableLanguagesChanged, this, [this]() {
    beginResetModel();
    endResetModel();
  });
  QObject::connect(
    &_languageManager, &LanguageManager::languageChanged, this, &LanguageListModel::currentLanguageChanged);
}

QString LanguageListModel::currentLanguage() const {
  return _languageManager.language();
}

void LanguageListModel::setCurrentLanguage(const QString& value) {
  _languageManager.setLanguage(value);
}

int LanguageListModel::rowCount(const QModelIndex& index) const {
  Q_UNUSED(index)
  return _languageManager.availableLanguages().size();
}

Qt::ItemFlags LanguageListModel::flags(const QModelIndex& index) const {
  if (!index.isValid() || index.row() >= rowCount())
    return {};

  return Qt::ItemFlag::ItemIsSelectable | Qt::ItemFlag::ItemIsEnabled | Qt::ItemFlag::ItemNeverHasChildren;
}

QVariant LanguageListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid() || index.row() >= rowCount())
    return {};

  const auto row = index.row();
  const auto lang = _languageManager.availableLanguages().at(row);
  switch (role) {
    case Qt::ItemDataRole::DisplayRole: {
      const auto display = QLocale(lang).nativeLanguageName();
      return display.at(0).toUpper() + display.mid(1);
    }
    case ItemDataRoles::LanguageRole:
      return lang;
    default:
      break;
  }

  return {};
}

QHash<int, QByteArray> LanguageListModel::roleNames() const {
  static const QHash<int, QByteArray> roles{
    { static_cast<int>(ItemDataRoles::LanguageRole), "language" },
  };
  auto result = QAbstractItemModel::roleNames();
  result.insert(roles);
  return result;
}
} // namespace solarus::launcher::app
