/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <solarus/launcher/app/Settings.h>

#include <solarus/launcher/app/Url.h>

#include <oclero/QtSettingsUtils.hpp>

#include <QGuiApplication>
#include <QStandardPaths>
#include <QUrl>

#include <algorithm>

namespace solarus::launcher::app {
constexpr auto audioVolumeMin = 0;
constexpr auto audioVolumeMax = 100;

const inline auto key_appAudioVolume{ QStringLiteral("app/audioVolume") };
const inline auto key_appEnableAudio{ QStringLiteral("app/enableAudio") };
const inline auto key_appFullScreen{ QStringLiteral("app/fullScreen") };
const inline auto key_appEnableUpdates{ QStringLiteral("app/enableUpdates") };
const inline auto key_appUpdateCheckFrequency{ QStringLiteral("app/updateCheckFrequency") };
const inline auto key_appLastUpdateCheck{ QStringLiteral("app/lastUpdateCheck") };
const inline auto key_appOnboardingDone{ QStringLiteral("app/onboardingDone") };
const inline auto key_appLanguage{ QStringLiteral("app/language") };
const inline auto key_appForceSoftwareRendering{ QStringLiteral("app/forceSoftwareRendering") };

const inline auto key_windowX{ QStringLiteral("window/x") };
const inline auto key_windowY{ QStringLiteral("window/y") };
const inline auto key_windowWidth{ QStringLiteral("window/width") };
const inline auto key_windowHeight{ QStringLiteral("window/height") };

const inline auto key_questForceSoftwareRendering{ QStringLiteral("quests/forceSoftwareRendering") };
const inline auto key_questFullScreen{ QStringLiteral("quests/fullScreen") };
const inline auto key_questEnableAudio{ QStringLiteral("quests/enableAudio") };
const inline auto key_questEnableUpdates{ QStringLiteral("quests/enableUpdates") };
const inline auto key_questUpdateCheckFrequency{ QStringLiteral("quests/updateCheckFrequency") };
const inline auto key_questLastUpdateCheck{ QStringLiteral("quests/lastUpdateCheck") };
const inline auto key_questDirectory{ QStringLiteral("quests/directory") };
const inline auto key_questLibraryEndpointUrl{ QStringLiteral("quests/libraryEndpointUrl") };
const inline auto key_questSuspendWhenUnfocused{ QStringLiteral("quests/suspendWhenUnFocused") };

constexpr auto default_appAudioVolume{ 50 };
constexpr auto default_appEnableAudio{ true };
constexpr auto default_appFullScreen{ false };
constexpr auto default_appEnableUpdates{ true };
constexpr auto default_appUpdateCheckFrequency{ oclero::QtUpdater::Frequency::EveryWeek };
constexpr auto default_appOnboardingDone{ false };
constexpr auto default_appLanguage{ "en_US" };
constexpr auto default_appForceSoftwareRendering{ false };

constexpr auto default_windowX{ 0 };
constexpr auto default_windowY{ 0 };
constexpr auto default_windowWidth{ 1280 };
constexpr auto default_windowHeight{ 720 };

constexpr auto default_questForceSoftwareRendering{ false };
constexpr auto default_questFullScreen{ false };
constexpr auto default_questEnableAudio{ true };
constexpr auto default_questEnableUpdates{ true };
constexpr auto default_questUpdateCheckFrequency{ oclero::QtUpdater::Frequency::EveryWeek };
constexpr auto default_questSuspendWhenUnfocused{ true };

Settings::Settings(QObject* parent)
  : QObject(parent)
  , _qSettings(QSettings::UserScope, organizationName(), applicationName()) {}

Settings::~Settings() {
  // Force synchronization before quitting.
  _qSettings.sync();
}

void Settings::resetToDefaults() {
  setAppAudioVolume(default_appAudioVolume);
  setAppEnableAudio(default_appEnableAudio);
  setAppFullScreen(default_appFullScreen);
  setAppEnableUpdates(default_appEnableUpdates);
  setAppOnboardingDone(default_appOnboardingDone);

  setWindowX(default_windowX);
  setWindowY(default_windowY);
  setWindowWidth(default_windowWidth);
  setWindowHeight(default_windowHeight);

  setQuestEnableAudio(default_questEnableAudio);
  setQuestForceSoftwareRendering(default_questForceSoftwareRendering);
  setQuestFullScreen(default_questFullScreen);
  setQuestDirectory(getDefaultQuestDirectory());
}

QString Settings::getDefaultQuestDirectory() {
  return QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation) + '/' + QGuiApplication::organizationName()
         + QStringLiteral("/Quests");
}

QString Settings::applicationName() {
#if defined(Q_OS_WIN)
  return QGuiApplication::applicationDisplayName();
#else
  return QGuiApplication::applicationName().toLower().replace(' ', '-');
#endif
}

QString Settings::organizationName() {
#if defined(Q_OS_WIN)
  return QGuiApplication::organizationName();
#else
  return QGuiApplication::organizationName().toLower().replace(' ', '-');
#endif
}

int Settings::appAudioVolume() const {
  const auto value = _qSettings.value(key_appAudioVolume, default_appAudioVolume).toInt();
  return std::clamp(value, audioVolumeMin, audioVolumeMax);
}

bool Settings::appEnableAudio() const {
  return _qSettings.value(key_appEnableAudio, default_appEnableAudio).toBool();
}

bool Settings::appFullScreen() const {
  return _qSettings.value(key_appFullScreen, default_appFullScreen).toBool();
}

bool Settings::appEnableUpdates() const {
  return _qSettings.value(key_appEnableUpdates, default_appEnableUpdates).toBool();
}

oclero::QtUpdater::Frequency Settings::appUpdateCheckFrequency() const {
  return oclero::loadSetting(_qSettings, key_appUpdateCheckFrequency, default_appUpdateCheckFrequency);
}

QDateTime Settings::appLastUpdateCheck() const {
  return _qSettings.value(key_appLastUpdateCheck, {}).toDateTime();
}

bool Settings::appOnboardingDone() const {
  return _qSettings.value(key_appOnboardingDone, default_appOnboardingDone).toBool();
}

QString Settings::appLanguage() const {
  return _qSettings.value(key_appLanguage, default_appLanguage).toString();
}

bool Settings::appForceSoftwareRendering() const {
  return _qSettings.value(key_appForceSoftwareRendering, default_appForceSoftwareRendering).toBool();
}

int Settings::windowX() const {
  return _qSettings.value(key_windowX, default_windowX).toInt();
}

int Settings::windowY() const {
  return _qSettings.value(key_windowY, default_windowY).toInt();
}

int Settings::windowWidth() const {
  return std::max(200, _qSettings.value(key_windowWidth, default_windowWidth).toInt());
}

int Settings::windowHeight() const {
  return std::max(200, _qSettings.value(key_windowHeight, default_windowHeight).toInt());
}

bool Settings::questEnableAudio() const {
  return _qSettings.value(key_questEnableAudio, default_questEnableAudio).toBool();
}

bool Settings::questForceSoftwareRendering() const {
  return _qSettings.value(key_questForceSoftwareRendering, default_questForceSoftwareRendering).toBool();
}

bool Settings::questFullScreen() const {
  return _qSettings.value(key_questFullScreen, default_questFullScreen).toBool();
}

bool Settings::questEnableUpdates() const {
  return _qSettings.value(key_questEnableUpdates, default_questEnableUpdates).toBool();
}

QString Settings::questDirectory() const {
  return _qSettings.value(key_questDirectory, getDefaultQuestDirectory()).toString();
}

QString Settings::questLibraryEndpointUrl() const {
  return _qSettings.value(key_questLibraryEndpointUrl, app::GAMES_API_URL).toString();
}

oclero::QtUpdater::Frequency Settings::questUpdateCheckFrequency() const {
  return oclero::loadSetting(_qSettings, key_questUpdateCheckFrequency, default_questUpdateCheckFrequency);
}

QDateTime Settings::questLastUpdateCheck() const {
  return _qSettings.value(key_questLastUpdateCheck, {}).toDateTime();
}

bool Settings::questSuspendWhenUnfocused() const {
  return _qSettings.value(key_questSuspendWhenUnfocused, default_questSuspendWhenUnfocused).toBool();
}

void Settings::setAppAudioVolume(int value) {
  value = std::clamp(value, audioVolumeMin, audioVolumeMax);
  if (value != appAudioVolume()) {
    _qSettings.setValue(key_appAudioVolume, value);
    emit appAudioVolumeChanged();
  }
}

void Settings::setAppEnableAudio(bool value) {
  if (value != appEnableAudio()) {
    _qSettings.setValue(key_appEnableAudio, value);
    emit appEnableAudioChanged();
  }
}

void Settings::setAppFullScreen(bool value) {
  if (value != appFullScreen()) {
    _qSettings.setValue(key_appFullScreen, value);
    emit appFullScreenChanged();
  }
}

void Settings::setAppEnableUpdates(bool value) {
  if (value != appEnableUpdates()) {
    _qSettings.setValue(key_appEnableUpdates, value);
    emit appEnableUpdatesChanged();
  }
}

void Settings::setAppUpdateCheckFrequency(oclero::QtUpdater::Frequency value) {
  if (value != appUpdateCheckFrequency()) {
    oclero::saveSetting(_qSettings, key_appUpdateCheckFrequency, value);
    emit appUpdateCheckFrequencyChanged();
  }
}

void Settings::setAppLastUpdateCheck(const QDateTime& value) {
  if (value != appLastUpdateCheck()) {
    _qSettings.setValue(key_appLastUpdateCheck, value);
    emit appLastUpdateCheckChanged();
  }
}

void Settings::setAppOnboardingDone(bool value) {
  if (value != appOnboardingDone()) {
    _qSettings.setValue(key_appOnboardingDone, value);
    emit appOnboardingDoneChanged();
  }
}

void Settings::setAppLanguage(const QString& value) {
  if (value != appLanguage()) {
    _qSettings.setValue(key_appLanguage, value);
    emit appLanguageChanged();
  }
}

void Settings::setAppForceSoftwareRendering(bool value) {
  if (value != appForceSoftwareRendering()) {
    _qSettings.setValue(key_appForceSoftwareRendering, value);
    emit appForceSoftwareRenderingChanged();
  }
}

void Settings::setWindowX(int value) {
  if (value != windowX()) {
    _qSettings.setValue(key_windowX, value);
    emit windowXChanged();
  }
}

void Settings::setWindowY(int value) {
  if (value != windowY()) {
    _qSettings.setValue(key_windowY, value);
    emit windowYChanged();
  }
}

void Settings::setWindowWidth(int value) {
  if (value != windowWidth()) {
    _qSettings.setValue(key_windowWidth, value);
    emit windowWidthChanged();
  }
}

void Settings::setWindowHeight(int value) {
  if (value != windowHeight()) {
    _qSettings.setValue(key_windowHeight, value);
    emit windowHeightChanged();
  }
}

void Settings::setQuestEnableAudio(bool value) {
  if (value != questEnableAudio()) {
    _qSettings.setValue(key_questEnableAudio, value);
    emit questEnableAudioChanged();
  }
}

void Settings::setQuestForceSoftwareRendering(bool value) {
  if (value != questForceSoftwareRendering()) {
    _qSettings.setValue(key_questForceSoftwareRendering, value);
    emit questForceSoftwareRenderingChanged();
  }
}

void Settings::setQuestFullScreen(bool value) {
  if (value != questFullScreen()) {
    _qSettings.setValue(key_questFullScreen, value);
    emit questFullScreenChanged();
  }
}

void Settings::setQuestEnableUpdates(bool value) {
  if (value != questEnableUpdates()) {
    _qSettings.setValue(key_questEnableUpdates, value);
    emit questEnableUpdatesChanged();
  }
}

void Settings::setQuestDirectory(const QString& value) {
  if (value != questDirectory()) {
    _qSettings.setValue(key_questDirectory, value);
    emit questDirectoryChanged();
  }
}

void Settings::setQuestLibraryEndpointUrl(const QString& value) {
  if (value != questLibraryEndpointUrl()) {
    _qSettings.setValue(key_questLibraryEndpointUrl, value);
    emit questLibraryEndpointUrlChanged();
  }
}

void Settings::setQuestUpdateCheckFrequency(oclero::QtUpdater::Frequency value) {
  if (value != questUpdateCheckFrequency()) {
    oclero::saveSetting(_qSettings, key_questUpdateCheckFrequency, value);
    emit questUpdateCheckFrequencyChanged();
  }
}

void Settings::setQuestLastUpdateCheck(const QDateTime& value) {
  if (value != questLastUpdateCheck()) {
    oclero::saveSetting(_qSettings, key_questLastUpdateCheck, value);
    emit questLastUpdateCheckChanged();
  }
}

void Settings::setQuestSuspendWhenUnfocused(bool value) {
  if (value != questSuspendWhenUnfocused()) {
    oclero::saveSetting(_qSettings, key_questSuspendWhenUnfocused, value);
    emit questSuspendWhenUnfocusedChanged();
  }
}
} // namespace solarus::launcher::app
