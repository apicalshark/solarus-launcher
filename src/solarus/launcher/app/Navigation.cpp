/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/Navigation.h>

namespace solarus::launcher::app {
Navigation::Navigation(QObject* parent)
  : QObject(parent) {}

Navigation::~Navigation() = default;

Navigation::WindowId Navigation::currentWindow() const {
  return _currentWindow;
}

Navigation::PageId Navigation::currentPage() const {
  return _currentPage;
}

void Navigation::setCurrentWindow(WindowId windowId) {
  if (windowId != _currentWindow) {
    _currentWindow = windowId;
    emit currentWindowChanged();
  }
}

void Navigation::setCurrentPage(PageId pageId) {
  if (pageId != _currentPage) {
    _currentPage = pageId;
    emit currentPageChanged();
  }
}

quests::LocalQuest* Navigation::selectedQuest() const {
  return _selectedQuest;
}

void Navigation::setSelectedQuest(quests::LocalQuest* quest) {
  if (quest != _selectedQuest) {
    _selectedQuestConnection.disconnect();

    _selectedQuest = quest;
    if (_selectedQuest) {
      _selectedQuestConnection = QObject::connect(
        _selectedQuest.data(), &quests::LocalQuest::stateChanged, this, &Navigation::selectedQuestStateChanged);
    }

    emit selectedQuestChanged();
    emit selectedQuestStateChanged();
  }
}

void Navigation::focusSideBar() {
  emit sideBarFocusRequested();
}

void Navigation::focusPageContent() {
  emit pageContentFocusRequested();
}
} // namespace solarus::launcher::app
