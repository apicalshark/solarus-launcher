/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/Application.h>
#include <solarus/launcher/app/Version.h>
#include <solarus/launcher/app/Url.h>

#include <solarus/launcher/runner/QuestOutputColors.h>
#include <solarus/launcher/gamepad/GamepadManager.h>

#include <array>

#include <QByteArray>
#include <QDir>
#include <QIcon>
#include <QQmlContext>
#include <QString>

namespace solarus::launcher::app {
namespace {
void setAppFlags() {
  QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
  // Removes the flickering when the window is resized, but lots of error in the
  // console.
  // QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
}

void setAppInfo() {
  QGuiApplication::setApplicationName(APPLICATION_NAME);
  QGuiApplication::setApplicationDisplayName(APPLICATION_DISPLAY_NAME);
  QGuiApplication::setOrganizationName(ORGANIZATION_NAME);
  QGuiApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
  QGuiApplication::setApplicationVersion(APPLICATION_DISPLAY_VERSION);
  QGuiApplication::setDesktopFileName(APPLICATION_NAME);
}

void setAppIcon() {
  static constexpr std::array<QIcon::Mode, 4> modes{ QIcon::Mode::Normal, QIcon::Mode::Disabled, QIcon::Mode::Active,
    QIcon::Mode::Selected };
  static constexpr std::array<int, 12> dimensions{ 16, 20, 24, 32, 40, 48, 64, 96, 128, 256, 512, 1024 };
  static constexpr const char* path_base(":/solarus/launcher/resources/icon/solarus_launcher_icon_");
  static constexpr const char* path_ext(".png");

  QIcon app_icon;
  for (const auto dimension : dimensions) {
    const auto path = path_base + QString::number(dimension) + path_ext;
    QPixmap pixmap(path);
    for (const auto mode : modes) {
      app_icon.addPixmap(pixmap, mode, QIcon::State::On);
      app_icon.addPixmap(pixmap, mode, QIcon::State::Off);
    }
  }
  QGuiApplication::setWindowIcon(app_icon);
}
} // namespace

Application::Application(int& argc, char* argv[])
  : QApplication(argc, argv)
  , _updater(UPDATE_API_URL,
      {
        QSettings::Format::NativeFormat,
        QSettings::Scope::UserScope,
        Settings::organizationName(),
        Settings::applicationName(),
      })
  , _localQuestListModel(_settings)
  , _onlineQuestListModel(_settings)
  , _questUpdater(_settings, _localQuestListModel, _onlineQuestListModel)
  , _questRunner(_settings)
  , _aboutPageController()
  , _appMenuController(_navigation, _settings, _updater, _questRunner, _questUpdater)
  , _bottomBarController(_gamepadManager)
  , _mainPagesController(_questRunner, _navigation)
  , _homePageController(_navigation, _localQuestListModel, _questRunner, _newsListModel)
  , _localQuestsPageController(_navigation, _questRunner, _questUpdater, _localQuestListModel)
  , _onlineQuestsPageController(_navigation, _questUpdater, _onlineQuestListModel)
  , _onboardingPagesController(_settings, _navigation, _localQuestListModel)
  , _playingQuestPageController(_questRunner)
  , _questUpdatePageController(_questUpdater)
  , _settingsPageController(_settings, _updater, _questUpdater, _localQuestListModel, _languageManager)
  , _updateDialogController(_updater) {
  // Use software rendering if specified by the user.
  if (_settings.appForceSoftwareRendering()) {
    QQuickWindow::setSceneGraphBackend(QSGRendererInterface::Software);
  }

  setupSingleInstance();
  setupQuests();
  setupNews();
  setupUpdater();
  setupQmlEngine();
  setupLanguages();

  //  QObject::connect(this, &QGuiApplication::focusObjectChanged, this, [](QObject* focusObject) {
  //    qDebug() << focusObject;
  //  });

  // Try to load view.
  if (loadView()) {
    _window = getWindow();
  } else {
    std::exit(EXIT_FAILURE);
  }
}

Application::~Application() = default;

int Application::start(int& argc, char* argv[]) {
  // Needs to be done before creating the QApplication.
  setAppFlags();
  setAppInfo();

  Application app(argc, argv);

  // Needs to be done after creating the QApplication.
  setAppIcon();

  return QApplication::exec();
}

void Application::setupSingleInstance() {
  _instanceManager.setForceSingleInstance(true);
  QObject::connect(&_instanceManager, &oclero::QtAppInstanceManager::secondaryInstanceMessageReceived, this,
    [this](const unsigned int, const QByteArray& /*message*/) {
      // Since the GUI app starts only when no arguments are given, we just bring main window to front.
      if (_window) {
        _window->raise();
        _window->requestActivate();
      }
    });
}

void Application::setupLanguages() {
  _languageManager.setQmlEngine(&_qmlEngine);
  _languageManager.setLanguage(_settings.appLanguage());
  _settings.setAppLanguage(_languageManager.language());
  QObject::connect(&_languageManager, &LanguageManager::languageChanged, this, [this]() {
    _settings.setAppLanguage(_languageManager.language());
  });
  QObject::connect(&_settings, &Settings::appLanguageChanged, this, [this]() {
    _languageManager.setLanguage(_settings.appLanguage());
  });
}

void Application::setupQuests() {
  // Used by QML to get images for Solarus quests.
  // The owner of this pointer will be taken by the QML engine.
  _localQuestImageProvider = new quests::LocalQuestImageProvider();
  _localQuestImageProvider->setModel(&_localQuestListModel);

  // Check for quests updates.
  QTimer::singleShot(0, [this]() {
    //_questUpdater.c
  });
}

void Application::setupUpdater() {
  _updater.setInstallMode(oclero::QtUpdater::InstallMode::ExecuteFile);

  // Check for updates.
  QTimer::singleShot(0, [this]() {
    _updater.checkForUpdate();
  });
}

void Application::setupNews() {
  _newsListModel.refresh();
}

void Application::setupQmlEngine() {
  const auto errMsg = QStringLiteral("Not allowed to instantiate this class in QML.");

  // Register C++ types to use in QML.
  {
    // For convenience, all the C++ namespace are grouped under the same QML module.
    constexpr auto qmlModule = "Solarus.Launcher.Model";
    using namespace solarus::launcher::quests;
    using namespace solarus::launcher::runner;
    using namespace solarus::launcher::app;
    using namespace solarus::launcher::controllers;
    using namespace solarus::launcher::news;
    using namespace solarus::launcher::gamepad;

    qmlRegisterUncreatableType<Navigation>(qmlModule, 1, 0, "Navigation", errMsg);
    qmlRegisterUncreatableType<NewsListModel>(qmlModule, 1, 0, "NewsListModel", errMsg);

    qmlRegisterUncreatableType<LocalQuest>(qmlModule, 1, 0, "LocalQuest", errMsg);
    qmlRegisterUncreatableType<LocalQuestListModel>(qmlModule, 1, 0, "LocalQuestListModel", errMsg);
    qmlRegisterUncreatableType<LocalQuestProxyModel>(qmlModule, 1, 0, "LocalQuestProxyModel", errMsg);

    qmlRegisterUncreatableType<OnlineQuest>(qmlModule, 1, 0, "OnlineQuest", errMsg);
    qmlRegisterUncreatableType<OnlineQuestListModel>(qmlModule, 1, 0, "OnlineQuestListModel", errMsg);
    qmlRegisterUncreatableType<OnlineQuestProxyModel>(qmlModule, 1, 0, "OnlineQuestProxyModel", errMsg);

    qmlRegisterUncreatableType<QuestUpdate>(qmlModule, 1, 0, "QuestUpdate", errMsg);
    qmlRegisterUncreatableType<QuestUpdateListModel>(qmlModule, 1, 0, "QuestUpdateListModel", errMsg);
    //qmlRegisterUncreatableType<QuestUpdateProxyModel>(qmlModule, 1, 0, "QuestUpdateProxyModel", errMsg);

    qmlRegisterUncreatableType<QuestOutputHandler>(qmlModule, 1, 0, "QuestOutputHandler", errMsg);
    qmlRegisterUncreatableType<QuestRunner>(qmlModule, 1, 0, "QuestRunner", errMsg);
    qmlRegisterType<QuestOutputColors>(qmlModule, 1, 0, "QuestOutputColors");

    qmlRegisterUncreatableType<Settings>(qmlModule, 1, 0, "Settings", errMsg);
    qmlRegisterUncreatableType<oclero::QtUpdater>(qmlModule, 1, 0, "Updater", errMsg);

    qmlRegisterUncreatableType<GamepadManager>(qmlModule, 1, 0, "GamepadManager", errMsg);
    qmlRegisterUncreatableType<GamepadListModel>(qmlModule, 1, 0, "GamepadListModel", errMsg);
    qmlRegisterType<Gamepad>(qmlModule, 1, 0, "Gamepad");
    qmlRegisterType<GamepadAction>(qmlModule, 1, 0, "GamepadAction");
  }

  // Register C++ controllers to QML engine.
  {
    constexpr auto qmlModule = "Solarus.Launcher.Controllers";
    using namespace solarus::launcher::controllers;

    qmlRegisterUncreatableType<AboutPageController>(qmlModule, 1, 0, "AboutPageController", errMsg);
    qmlRegisterUncreatableType<AppMenuController>(qmlModule, 1, 0, "AppMenuController", errMsg);
    qmlRegisterUncreatableType<BottomBarController>(qmlModule, 1, 0, "BottomBarController", errMsg);
    qmlRegisterUncreatableType<HomePageController>(qmlModule, 1, 0, "HomePageController", errMsg);
    qmlRegisterUncreatableType<LocalQuestsPageController>(qmlModule, 1, 0, "LocalQuestsPageController", errMsg);
    qmlRegisterUncreatableType<MainPagesController>(qmlModule, 1, 0, "MainPagesController", errMsg);
    qmlRegisterUncreatableType<OnboardingPagesController>(qmlModule, 1, 0, "OnboardingPagesController", errMsg);
    qmlRegisterUncreatableType<PlayingQuestPageController>(qmlModule, 1, 0, "PlayingQuestPageController", errMsg);
    qmlRegisterUncreatableType<SettingsPageController>(qmlModule, 1, 0, "SettingsPageController", errMsg);
    qmlRegisterUncreatableType<UpdateDialogController>(qmlModule, 1, 0, "UpdateDialogController", errMsg);
    qmlRegisterUncreatableType<QuestUpdatePageController>(qmlModule, 1, 0, "QuestUpdatePageController", errMsg);
  }

  // Add import path to look for QML modules.
  _qmlEngine.addImportPath(QStringLiteral("qrc:/"));

  // Make C++ objects accessible from QML context (must be done before loading the view).
  // Expose only controllers.
  auto* qmlContext = _qmlEngine.rootContext();
  qmlContext->setContextProperty(QStringLiteral("$aboutPageController"), &_aboutPageController);
  qmlContext->setContextProperty(QStringLiteral("$appMenuController"), &_appMenuController);
  qmlContext->setContextProperty(QStringLiteral("$bottomBarController"), &_bottomBarController);
  qmlContext->setContextProperty(QStringLiteral("$homePageController"), &_homePageController);
  qmlContext->setContextProperty(QStringLiteral("$localQuestsPageController"), &_localQuestsPageController);
  qmlContext->setContextProperty(QStringLiteral("$onlineQuestsPageController"), &_onlineQuestsPageController);
  qmlContext->setContextProperty(QStringLiteral("$mainPagesController"), &_mainPagesController);
  qmlContext->setContextProperty(QStringLiteral("$onboardingPagesController"), &_onboardingPagesController);
  qmlContext->setContextProperty(QStringLiteral("$playingQuestPageController"), &_playingQuestPageController);
  qmlContext->setContextProperty(QStringLiteral("$settingsPageController"), &_settingsPageController);
  qmlContext->setContextProperty(QStringLiteral("$updateDialogController"), &_updateDialogController);
  qmlContext->setContextProperty(QStringLiteral("$questUpdatePageController"), &_questUpdatePageController);

  // Add ImageProvider to QML context, to get Pixmaps from the model.
  // Will be accessible from QML with "image://quest/<resource_id>"
  // NB: QML engine takes ownership of this pointer.
  _qmlEngine.addImageProvider(QStringLiteral("quest"), _localQuestImageProvider);

  // Forward signals from QML context to QApplication.
  QObject::connect(&_qmlEngine, &QQmlApplicationEngine::quit, this, &QCoreApplication::quit);
}

bool Application::loadView() {
  _qmlEngine.load(QUrl(QStringLiteral("qrc:/main.qml")));
  return !_qmlEngine.rootObjects().empty();
}

QQuickWindow* Application::getWindow() const {
  const auto rootObjects = _qmlEngine.rootObjects();
  if (!rootObjects.empty()) {
    const auto rootObject = rootObjects.first();
    return qobject_cast<QQuickWindow*>(rootObject);
  }
  return nullptr;
}
} // namespace solarus::launcher::app
