/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/LanguageManager.h>

#include <QCoreApplication>
#include <QDir>
#include <QTranslator>
#include <QQmlEngine>

namespace solarus::launcher::app {
LanguageManager::LanguageManager(QObject* parent)
  : QObject(parent)
  , _translator(new QTranslator(this))
  , _qtTranslator(new QTranslator(this)) {
  setSourceDirectory(defaultTranslationsDirectory());
}

LanguageManager::~LanguageManager() = default;

void LanguageManager::setQmlEngine(QQmlEngine* engine) {
  if (engine == _qmlEngine)
    return;

  _qmlEngine = engine;

  // Force language update.
  if (_qmlEngine) {
    _qmlEngine->retranslate();
  }
}

const QVector<QString>& LanguageManager::availableLanguages() const {
  return _languages;
}

QString LanguageManager::defaultTranslationsDirectory() {
  return QDir{ QCoreApplication::applicationDirPath() }.absolutePath() + QStringLiteral("/translations");
}

QString LanguageManager::sourceDirectory() const {
  return _sourceDirectory.absolutePath();
}

void LanguageManager::setSourceDirectory(const QString& sourceDirectory) {
  const auto newSourceDirectory = QDir{ sourceDirectory };
  if (newSourceDirectory != _sourceDirectory) {
    _sourceDirectory = newSourceDirectory;
    emit sourceDirectoryChanged();

    // Look for translations and choose the best one.
    synchronize();
  }
}

const QString& LanguageManager::language() const {
  return _currentLanguage;
}

void LanguageManager::setLanguage(const QString& value) {
  if (value == _currentLanguage) {
    return;
  }

  _desiredLanguage = value;

  auto appTranslationSuccess = false;
  auto qtTranslationSuccess = false;

  // Change app-specific translations.
  // Load file "<filename><filePrefix><locale>.qm" (extension added automatically).
  const auto translationsDir = _sourceDirectory.absolutePath();
  const auto appTranslationBaseName = _filePrefix + value;
  if (_translator && _translator->load(appTranslationBaseName, translationsDir)) {
    QCoreApplication::removeTranslator(_translator);
    if (QCoreApplication::installTranslator(_translator)) {
      appTranslationSuccess = true;
    }
  }

  // Change Qt-specific translations.
  const auto qtTranslationBaseName = QStringLiteral("qtbase_") + value;
  if (_qtTranslator && _qtTranslator->load(qtTranslationBaseName, translationsDir)) {
    QCoreApplication::removeTranslator(_qtTranslator);
    if (QCoreApplication::installTranslator(_translator)) {
      qtTranslationSuccess = true;
    }
  }

  if (!appTranslationSuccess && !qtTranslationSuccess)
    return;

  // Trigger re-translating for QML scene.
  if (_qmlEngine) {
    _qmlEngine->retranslate();
  }

  // Save to user settings.
  _currentLanguage = value;

  emit languageChanged();
}

void LanguageManager::updateLanguages() {
  _languages.clear();

  constexpr auto filterFlags = QDir::Filter::NoDotAndDotDot | QDir::Filter::Files | QDir::Filter::Readable;
  constexpr auto sortFlags = QDir::SortFlag::Name;
  const auto entries = _sourceDirectory.entryInfoList({ QStringLiteral("*.qm") }, filterFlags, sortFlags);
  for (const auto& entry : qAsConst(entries)) {
    if (!entry.baseName().startsWith(QStringLiteral("qt"))) {
      const auto baseName = entry.baseName();
      const auto hasPrefix = !_filePrefix.isEmpty() && baseName.startsWith(_filePrefix);
      const auto& languageName = hasPrefix ? baseName.mid(_filePrefix.length()) : baseName;
      _languages.append(languageName);
    }
  }

  emit availableLanguagesChanged();
}

void LanguageManager::synchronize() {
  updateLanguages();

  // Set new localization, among what is available.
  const auto newValue = findBestLanguage(_desiredLanguage);
  setLanguage(newValue);
}

QString LanguageManager::findBestLanguage(const QString& desiredLanguage) const {
  if (_languages.size() > 0) {
    const auto begin = _languages.begin();
    const auto end = _languages.end();

    // Try to find the desired language.
    const auto itDesired = std::find_if(begin, end, [&desiredLanguage](const QString& item) {
      return item == desiredLanguage;
    });
    if (itDesired != end) {
      return *itDesired;
    }

    // If not found, try to find the user's system language.
    const auto systemLanguage = QLocale().name();
    const auto itSystem = std::find_if(begin, end, [&systemLanguage](const QString& item) {
      return item == systemLanguage;
    });
    if (itSystem != end) {
      return *itSystem;
    }

    // If not found, try to find English.
    const auto defaultLanguage = QLocale(QLocale::English, QLocale::UnitedStates).name();
    const auto itEnglish = std::find_if(begin, end, [&defaultLanguage](const QString& item) {
      return item == defaultLanguage;
    });
    if (itEnglish != end) {
      return *itEnglish;
    }

    // Otherwise, use first available language.
    return _languages.front();
  }

  return QString{};
}
} // namespace solarus::launcher::app
