/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/gamepad/GamepadAction.h>

namespace solarus::launcher::gamepad {
GamepadAction::GamepadAction(QObject* parent)
  : QObject(parent) {}

GamepadAction::GamepadAction(const QString& text, const Button button, QObject* parent)
  : QObject(parent)
  , _text(text)
  , _button(button) {}

GamepadAction::~GamepadAction() = default;

const QString& GamepadAction::text() const {
  return _text;
}

void GamepadAction::setText(const QString& text) {
  if (text != _text) {
    _text = text;
    emit textChanged();
  }
}

GamepadAction::Button GamepadAction::button() const {
  return _button;
}

void GamepadAction::setButton(Button button) {
  if (button != _button) {
    _button = button;
    emit buttonChanged();
  }
}
} // namespace solarus::launcher::gamepad
