/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/gamepad/GamepadListModel.h>

#include <QGamepadManager>

namespace solarus::launcher::gamepad {

GamepadListModel::GamepadListModel(QObject* parent)
  : QAbstractListModel(parent) {
  refresh();
}

GamepadListModel::~GamepadListModel() = default;

int GamepadListModel::rowCount(const QModelIndex& index) const {
  Q_UNUSED(index)

  return count();
}

QVariant GamepadListModel::data(const QModelIndex& index, int role) const {
  if (!index.isValid())
    return {};

  if (index.row() >= _gamepads.size())
    return {};

  const auto row = index.row();
  const auto& item = _gamepads.at(row);

  switch (role) {
    case DeviceIdRole:
      return item.deviceId;
    case Qt::DisplayRole:
    case DeviceNameRole:
      return item.deviceName;
    default:
      return {};
  }
}

QHash<int, QByteArray> GamepadListModel::roleNames() const {
  return QHash<int, QByteArray>{
    { DeviceIdRole, "deviceId" },
    { DeviceNameRole, "deviceName" },
  };
}

int GamepadListModel::count() const {
  return _gamepads.count();
}

void GamepadListModel::refresh() {
  beginResetModel();
  _gamepads.clear();
  const auto* manager = QGamepadManager::instance();
  const auto deviceIds = manager->connectedGamepads();
  for (const auto deviceId : deviceIds) {
    const auto deviceName = manager->gamepadName(deviceId);
    _gamepads.append(GamepadInfo{
      deviceId,
      deviceName,
    });
  }
  std::sort(_gamepads.begin(), _gamepads.end(), [](const auto& a, const auto& b) {
    return a.deviceName < b.deviceName;
  });

  endResetModel();
  emit countChanged();
}
} // namespace solarus::launcher::gamepad
