/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/gamepad/Gamepad.h>

namespace solarus::launcher::gamepad {
Gamepad::Gamepad(QObject* parent)
  : QObject(parent) {}

Gamepad::~Gamepad() = default;

QQmlListProperty<GamepadAction> Gamepad::actions() {
  return QQmlListProperty<GamepadAction>{
    this,
    this,
    &Gamepad::appendItem,
    &Gamepad::itemCount,
    &Gamepad::itemAt,
    &Gamepad::clearItems,
    &Gamepad::replaceItem,
    &Gamepad::removeLastItem,
  };
}

void Gamepad::appendItem(GamepadAction* item) {
  _actions.append(item);
}

int Gamepad::itemCount() const {
  return _actions.size();
}

GamepadAction* Gamepad::itemAt(int index) const {
  return _actions.at(index);
}

void Gamepad::clearItems() {
  _actions.clear();
}

void Gamepad::removeLastItem() {
  _actions.removeLast();
}

void Gamepad::replaceItem(int index, GamepadAction* item) {
  _actions[index] = item;
}

void Gamepad::appendItem(QQmlListProperty<GamepadAction>* list, GamepadAction* item) {
  reinterpret_cast<Gamepad*>(list->data)->appendItem(item);
}

int Gamepad::itemCount(QQmlListProperty<GamepadAction>* list) {
  return reinterpret_cast<Gamepad*>(list->data)->itemCount();
}

GamepadAction* Gamepad::itemAt(QQmlListProperty<GamepadAction>* list, int index) {
  return reinterpret_cast<Gamepad*>(list->data)->itemAt(index);
}

void Gamepad::clearItems(QQmlListProperty<GamepadAction>* list) {
  reinterpret_cast<Gamepad*>(list->data)->clearItems();
}

void Gamepad::removeLastItem(QQmlListProperty<GamepadAction>* list) {
  reinterpret_cast<Gamepad*>(list->data)->removeLastItem();
}

void Gamepad::replaceItem(QQmlListProperty<GamepadAction>* list, int index, GamepadAction* item) {
  reinterpret_cast<Gamepad*>(list->data)->replaceItem(index, item);
}
} // namespace solarus::launcher::gamepad
