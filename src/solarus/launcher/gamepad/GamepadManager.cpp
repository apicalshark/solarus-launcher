/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/gamepad/GamepadManager.h>

#include <QGamepadKeyNavigation>

namespace solarus::launcher::gamepad {
GamepadManager::GamepadManager(QObject* parent)
  : QObject(parent) {
  QObject::connect(&_qGamepad, &QGamepad::connectedChanged, this, [this]() {
    emit currentDeviceIdChanged();
    emit currentDeviceNameChanged();
    emit connectedChanged();
  });

  QObject::connect(QGamepadManager::instance(), &QGamepadManager::connectedGamepadsChanged, this, [this]() {
    const auto* manager = QGamepadManager::instance();
    const auto deviceIds = manager->connectedGamepads();

    // When the previously connected gamepad can't be found in the gamepad list,
    // fallback to the first of the list, or nothing.
    if (!deviceIds.contains(_qGamepad.deviceId())) {
      const auto defaultId = deviceIds.size() > 0 ? deviceIds.at(0) : -1;
      _qGamepad.setDeviceId(defaultId);
      emit currentDeviceIdChanged();
      emit currentDeviceNameChanged();
    }

    emit connectedChanged();
  });

  // Configure mapping from gamepad's buttons to keyboard's keys.
  _qGamepadKeyNavigation.setActive(true);
  _qGamepadKeyNavigation.setGamepad(&_qGamepad);
  _qGamepadKeyNavigation.setButtonAKey(Qt::Key_Space);
  _qGamepadKeyNavigation.setButtonBKey(Qt::Key_Escape);
  _qGamepadKeyNavigation.setButtonStartKey(Qt::Key_F5);
  _qGamepadKeyNavigation.setLeftKey(Qt::Key_Left);
  _qGamepadKeyNavigation.setUpKey(Qt::Key_Up);
  _qGamepadKeyNavigation.setDownKey(Qt::Key_Down);
  _qGamepadKeyNavigation.setRightKey(Qt::Key_Right);
}

GamepadManager::~GamepadManager() = default;

Gamepad* GamepadManager::gamepad() const {
  return _gamepad;
}

void GamepadManager::setGamepad(Gamepad* gamepad) {
  if (gamepad != _gamepad) {
    _gamepad = gamepad;
    emit gamepadChanged();
  }
}

bool GamepadManager::connected() const {
  const auto gamepads = QGamepadManager::instance()->connectedGamepads();
  return gamepads.size() > 0;
}

int GamepadManager::currentDeviceId() const {
  return _qGamepad.deviceId();
}

void GamepadManager::setCurrentDeviceId(int id) {
  if (id != _qGamepad.deviceId()) {
    _qGamepad.setDeviceId(id);
    emit currentDeviceIdChanged();
    emit currentDeviceNameChanged();
  }
}

QString GamepadManager::currentDeviceName() const {
  return _qGamepad.name();
}

GamepadListModel* GamepadManager::deviceListModel() const {
  return &(const_cast<GamepadManager*>(this)->_deviceListModel);
}
} // namespace solarus::launcher::gamepad
