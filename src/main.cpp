/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <solarus/launcher/app/Application.h>
#include <solarus/launcher/app/Version.h>

#include <solarus/core/Arguments.h>
#include <solarus/core/Debug.h>
#include <solarus/core/MainLoop.h>

#include <string>
#include <iostream>

/**
 * \def SOLARUS_DEFAULT_QUEST
 * \brief Path of the quest to run is none is specified at runtime.
 */
#ifndef SOLARUS_DEFAULT_QUEST
// if no default quest was specified at compilation time,
// use the current directory.
#  define SOLARUS_DEFAULT_QUEST "."
#endif

/**
 * @brief Runs the Solarus Launcher GUI.
 * @param argc Number of arguments of the command line.
 * @param argv Command-line arguments.
 * @return 0 in case of success.
 */
int runLauncher(int argc, char** argv) {
#ifdef WIN32
#  ifdef _DEBUG
  // Temporary fix for "CoInitialize has not been called" spam warnings.
  CoInitializeEx(nullptr, COINIT_MULTITHREADED);
#  endif
#endif

  return solarus::launcher::app::Application::start(argc, argv);
}

/**
 * @brief Runs a quest like the solarus-run executable does.
 * @param argc Number of arguments of the command line.
 * @param argv Command-line arguments.
 * @return 0 in case of success.
 */
int runQuest(int argc, char* argv[]) {
  // Abort the process in case of fatal error.
  Solarus::Debug::set_abort_on_die(true);

  // Don't show a MessageBox on fatal error.
  // The launcher will handle this by itself.
  Solarus::Debug::set_show_popup_on_die(false);

  // Run the Solarus main loop.
  const Solarus::Arguments args(argc, argv);
  Solarus::MainLoop(args).run();

  return EXIT_SUCCESS;
}

/**
 * @brief Prints the command-line usage.
 * @param argc Number of arguments of the command line.
 * @param argv Command-line arguments.
 * @return 0 in case of success.
 */
int printHelp(int argc, char* argv[]) {
  const auto binary_name = std::string{ (argc > 0) ? argv[0] : solarus::launcher::app::APPLICATION_NAME };
  std::cout << "Usage:\n" << binary_name << " [--version] [--help] [<path/to/quest.solarus>]" << std::endl;
  return EXIT_SUCCESS;
}

/**
 * @brief Entry point of Solarus Launcher.
 *
 * To run the Solarus Launcher GUI:
 *   solarus-launcher
 * To directly run a quest (no GUI, similar to solarus-run):
 *   solarus-launcher -run /path/to/quest
 *
 * @param argc Number of arguments of the command line.
 * @param argv Command-line arguments.
 * @return 0 in case of success.
 */
int main(int argc, char** argv) {
  if (argc > 1) {
    const auto arg1 = std::string{ argv[1] };
    if (arg1 == "--help" || arg1 == "-h") {
      // Print help.
      return printHelp(argc, argv);
    } else if (arg1 == "--version" || arg1 == "-v") {
      // Print version.
      std::cout << solarus::launcher::app::APPLICATION_DISPLAY_VERSION << std::endl;
      return EXIT_SUCCESS;
    } else {
      // Run the quest directly, without GUI.
      return runQuest(argc, argv);
    }
  } else {
    // Opens the launcher window
    return runLauncher(argc, argv);
  }
}
