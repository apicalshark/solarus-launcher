# License

The source code of Solarus Launcher is freely available [on GitLab](https://gitlab.com/oclero/solarus-launcher), and is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html) (GPL v3).

Images (by [Olivier Cléro](https://www.olivierclero.com)) and sounds (by [Louis Couka](https://www.louiscouka.com/)) are licensed under [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).
