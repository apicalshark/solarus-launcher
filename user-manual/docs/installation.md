# Installation

## Windows

On Windows, you may install Solarus Launcher alternatively with these methods:

### With the installer

1. Download the latest [Solarus Launcher Windows installer](https://gitlab.com/solarus-games/solarus-launcher/-/package_files/latest/download).
2. Run the executable file.
3. Follow the instructions.

![Windows installer](assets/images/windows-installer-screenshot.png)

### With a Package Manager

With [`winget`](https://www.microsoft.com/en-us/p/app-installer/9nblggh4nns1#activetab=pivot:overviewtab) (Windows' official package manager):

```powershell
winget install solarus-launcher
```

With [`choco`](https://chocolatey.org/install):

```powershell
choco install solarus-launcher
```

With [`scoop`](https://scoop.sh/):

```powershell
scoop install solarus-launcher
```

## MacOS

On macOS, you may install Solarus Launcher alternatively with these methods:

### With the DMG

1. Download the latest [Solarus Launcher MacOS DMG](https://gitlab.com/solarus-games/solarus-launcher/-/package_files/latest/download).
2. Open the file.
3. Drag and drop the Solarus Launcher icon to the Applications icon.

### With a Package Manager

```zsh
brew install solarus-launcher
```

## Linux

### With Snap

```bash
sudo snap install solarus-launcher
```
