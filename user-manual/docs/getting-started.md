# Getting started

## First steps

The first time you start Solarus Launcher, the app will guide you to setup the initial configuration.

![Welcome page](assets/images/onboarding-screenshot-home.png)

Solarus Launcher needs a directory where it can store Solarus-made games, called **_quests_**. It will also use this directory to download new quests from Solarus' website.

![Directory page](assets/images/onboarding-screenshot-directory.png)

By default, this directory is inside your user directory (but you may change it to another directory):

- Windows: `%USERPROFILE%\Documents\Solarus\Quests`
- MacOS: `$HOME/solarus/quests`
- Linux: `$HOME/solarus/quests`

Solarus Launcher will then procede a first scan of this directory to look for Solarus quests, and will inform you about its findings.

Once this step done, you will be welcomed by the [Home Screen](home-screen.md).
