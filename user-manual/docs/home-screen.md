# Home Screen

The Home Page is the page that welcomes you when you open Solarus Launcher.

It shows the last Solarus Quests you have played, sorted by most recent. Thus it is very convenient to resume a game without having to look for it in the library.

It also displays the latests news articles from Solarus' official website to keep you informed about the latest updates and released quests.
