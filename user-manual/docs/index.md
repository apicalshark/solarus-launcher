![Solarus Launcher logo](assets/images/solarus_launcher_logo.png)

# Welcome

Welcome to the Solarus Launcher User Manual.

**Solarus Launcher** is a game launcher and browser for [Solarus](https://gitlab.com/solarus-games/solarus), a 2D game engine. Its goals are:

- Providing more user-friendly interface to Solarus for players.
- Providing an installer and better OS integration for `.solarus` files.
- Allowing people to browse and download games directly from the launcher thanks to the website API.
- Being designed to both desktop and TV usage with clear and big interface.
- Being usable with mouse, keyboard or gamepad.
- Being multiplatform for desktop (Windows, MacOS, Linux).
