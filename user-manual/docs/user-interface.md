# User Interface

TODO Screenshot

| Number | Element    | Description                                     |
| :----: | :--------- | :---------------------------------------------- |
|   1    | Side bar   | Access the different pages of Solarus Launcher. |
|   2    | Content    | Current page content.                           |
|   3    | Bottom bar | Get Gamepad contextual button actions.          |
