# Contributing Guidelines

Any contribution and help is welcome. You can bring new features and ideas, new translations, signal or fix a bug. You can contact the team on [Discord](https://discord.gg/yYHjJHt).

## Bugs

If you have found a bug or problem, please create an [issue](https://gitlab.com/solarus-games/solarus-launcher/issues) for it on GitLab, if the bug hasn't already been listed. Please avoid creating a duplicate.

Be sure to include as much information about the bug as possible: detailed bug reproduction scenario, screenshots, log dumps, etc.

You can also submit a pull request that fixes the bug. Make sure you follow the pull requests guidelines.

## Pull Requests

Your branch should be based on the `dev` branch, not `master`. This one points to the latest release, and the `dev` one contains the in-development changes and might differ a lot.

To find your path in the project, [documentation about the code](docs/DOCUMENTATION.md) may help you know where to begin, and where to find files.

The repository owners will have the final word about if your pull request will be merged or not. To maximize its chances to be merged, please ensure that:

- Your code compiles on all supported platforms, without warnings.
- Your code follows the coding style guidelines.

## Coding Style Guidelines

All the code in this repository should be consistent in style and naming scheme. An **EditorConfig** file is there to ensure at least correct indentation and encoding, if your code editor supports it. The root **Clang-Format** file must be used to format C++ code. You may use the `scripts/format.sh` script to format files.

Here is a quick recap:

- Indentation: **2** spaces, always use **spaces**.
- Curly braces on same line.
- C++ `class`, `struct` and `typedef` names should use `PascalCase`.
- Function names and variables should use `camelCase`.
- One class/type per header, one implementation per source file (except for inner classes and other tightly coupled types).
- Private member variables must be prefixed with `_`.
- Try to avoid macros, use `constexpr` functions when possible instead.
- Write some comments about what the code does, but be succint.
- C-style casts should not be used.
