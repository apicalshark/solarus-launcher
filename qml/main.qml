/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import Solarus.Launcher.App 1.0 as App
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Model 1.0 as Model

import Solarus.Launcher.Components.Updates 1.0 as Updates

// Main window
Window {
  id: _window
  minimumHeight: 450
  minimumWidth: 500
  width: 1280
  height: 720
  visible: true
  color: Styles.Colors.windowColor
  onXChanged: $settingsPageController.settings.windowX = x
  onYChanged: $settingsPageController.settings.windowY = y
  onWidthChanged: $settingsPageController.settings.windowWidth = width
  onHeightChanged: $settingsPageController.settings.windowHeight = height

   App.AppWindow {
     id: _windowContent
     anchors.fill: parent
   }

   Component.onCompleted: {
     // Avoid binding.
     width = $settingsPageController.settings.windowWidth
     height = $settingsPageController.settings.windowHeight
     x = $settingsPageController.settings.windowX
     y = $settingsPageController.settings.windowY

     // Synchronize window state.
     if ($settingsPageController.settings.appFullScreen) {
       showFullScreen()
     }
     $settingsPageController.settings.appFullScreen = visibility === Window.FullScreen
     _connections.enabled = true
   }

   QtObject {
     property int windowStateBackup: -1
     id: _private
   }

   Connections {
     id: _connections
     target: $settingsPageController.settings
     enabled: false

     function onAppFullScreenChanged() {
       const fullScreen = $settingsPageController.settings.appFullScreen
       if (fullScreen) {
         _private.windowStateBackup = _window.visibility
         _window.showFullScreen()
       } else {
         if (_private.windowStateBackup === -1) {
           _private.windowStateBackup = Window.Windowed
         }
         _window.visibility = _private.windowStateBackup
       }
     }
   }
}
