/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  signal backRequested

  id: _root

  ColumnLayout {
    id: _layout
    spacing: Styles.Hints.spacing
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: Styles.Hints.spacing * 2
    anchors.rightMargin: Styles.Hints.spacing * 2
    anchors.verticalCenter: parent.verticalCenter

    Controls.Text {
      id: _title
      text: qsTr("Quests Folder")
      role: Controls.Text.Role.H1
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing * 2
    }

    Controls.Text {
      id: _text
      text: qsTr("This will be the the folder where Solarus Launcher looks for existing Solarus Quests (i.e. games), and downloads them.")
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing
    }

    RowLayout {
      id: _rowFolder
      spacing: Styles.Hints.spacing / 2
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing

      Controls.TextField {
        id: _textFieldFolder
        Layout.fillWidth: true
        placeholderText: qsTr("Path to the Solarus quests directory")
        focus: true
        text: $onboardingPagesController.questDirectory
        onEditingFinished: $onboardingPagesController.questDirectory = text

        Keys.onLeftPressed: {
          event.accepted = false
          if (cursorPosition === 0) {
            Utils.EventUtils.tryFocusNextItem(this, false)
          }
        }

        Keys.onRightPressed: {
          event.accepted = false
          if (cursorPosition === text.length) {
            Utils.EventUtils.tryFocusNextItem(this, true)
          }
        }
      }

      Controls.Button {
        id: _buttonFolderEdit
        iconId: Styles.Icons.folder_16
        onClicked: _folderDialog.open()
        KeyNavigation.left: nextItemInFocusChain(false)
        KeyNavigation.right: nextItemInFocusChain(true)

        FolderDialog {
          id: _folderDialog
          currentFolder: `file:///${ $onboardingPagesController.questDirectory }`
          onFolderChanged: {
            let newFolder = folder.toString()
            const fileUrlScheme = "file:///"
            if (newFolder.startsWith(fileUrlScheme)) {
              newFolder = newFolder.substring(fileUrlScheme.length)
            }
            $onboardingPagesController.questDirectory = newFolder
          }
        }
      }
    }

    RowLayout {
      id: _rowNotExistingFolder
      spacing: Styles.Hints.spacing
      Layout.leftMargin: Styles.Hints.spacing / 2
      Layout.fillWidth: true

      Controls.SvgIcon {
        id: _iconNotExistingFolder
        color: Styles.Colors.textColorCaption
        iconId: Styles.Icons.help_16
      }

      Controls.Text {
        id: _captionNotExistingFolder
        text: qsTr("If the folder doesn't exist yet, it will be automatically created.")
        role: Controls.Text.Role.Caption
        Layout.fillWidth: true
      }
    }

    RowLayout {
      id: _rowChangeInPreferences
      spacing: Styles.Hints.spacing
      Layout.leftMargin: Styles.Hints.spacing / 2
      Layout.fillWidth: true

      Controls.SvgIcon {
        id: _iconChangeInPreferences
        color: _captionNotExistingFolder.color
        iconId: Styles.Icons.settings_16
      }

      Controls.Text {
        id: _captionChangeInPreferences
        text: qsTr("It can still be changed later in the app preferences.")
        role: Controls.Text.Role.Caption
        Layout.fillWidth: true
      }
    }
  }
}
