/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Components.Onboarding 1.0 as Onboarding
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  id: _root

  ColumnLayout {
    id: _layout
    spacing: Styles.Hints.spacing
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: Styles.Hints.spacing * 2
    anchors.rightMargin: Styles.Hints.spacing * 2
    anchors.verticalCenter: parent.verticalCenter
    height: Math.min(parent.height, implicitHeight)

    Controls.Text {
      id: _title
      text: qsTr("Controls")
      role: Controls.Text.Role.H1
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing * 2
    }

    Controls.Text {
      id: _text
      text: qsTr("You can control Solarus Launcher with these devices:")
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing
    }

    Onboarding.OnboardingDevice {
      id: _deviceMouse
      iconSource: Styles.Icons.mouse_64
      text: qsTr("Mouse")
      isConnected: true
      Layout.fillWidth: true
    }

    Onboarding.OnboardingDevice {
      id: _deviceKeyboard
      iconSource: Styles.Icons.keyboard_64
      text: qsTr("Keyboard")
      isConnected: true
      Layout.fillWidth: true
    }

    Onboarding.OnboardingDevice {
      id: _deviceGamepad
      iconSource: Styles.Icons.gamepad_64
      text: qsTr("Gamepad")
      isConnected: $onboardingPagesController.gamepadConnected
      Layout.fillWidth: true
    }
  }
}
