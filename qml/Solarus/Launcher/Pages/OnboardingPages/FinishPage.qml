/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.Onboarding 1.0 as Onboarding
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  id: _root

  ColumnLayout {
    id: _layout
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: Styles.Hints.spacing * 2
    anchors.rightMargin: Styles.Hints.spacing * 2
    spacing: Styles.Hints.spacing * 2
    anchors.verticalCenter: parent.verticalCenter
    height: Math.min(parent.height, implicitHeight)

    Onboarding.OnboardingCheckIcon {
      id: _checkIcon
      Layout.alignment: Qt.AlignHCenter
      Component.onCompleted: startAnimation()
    }

    Controls.Text {
      id: _title
      text: qsTr("Ready!")
      role: Controls.Text.Role.H1
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
    }

    Controls.Text {
      id: _text
      text: qsTr("Have fun!")
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
    }
  }
}
