/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Controllers 1.0 as Controllers

FocusScope {
  signal backRequested
  signal finished

  id: _root

  StackView.onStatusChanged: {
    if (StackView.status === StackView.Active) {
      _progressBar.value = 0
      $onboardingPagesController.startScan()
    } else if (StackView.status === StackView.Inactive) {
      $onboardingPagesController.stopScan()
    }
  }

  QtObject {
    id: _private

    function getScanErrorTitle(error) {
      switch (error) {
      case Controllers.OnboardingPagesController.ScanError.DirectoryCreationFailed:
        return qsTr("Directory creation failed")
      case Controllers.OnboardingPagesController.ScanError.PathIsAlreadyAFile:
        return qsTr("Path is not a directory")
      default:
        return qsTr("Error")
      }
    }

    function getScanErrorText(error) {
      switch (error) {
      case Controllers.OnboardingPagesController.ScanError.DirectoryCreationFailed:
        return qsTr("It is impossible to create such a directory.\nCheck that you have correct access rights.")
      case Controllers.OnboardingPagesController.ScanError.PathIsAlreadyAFile:
        return qsTr("This path seems to already exists, but points to a file instead of a directory.")
      default:
        return qsTr("Error code: %1").arg(error)
      }
    }
  }

  Controls.Dialog {
    id: _errorDialog
    title: qsTr("Error!")
    text: ""
    iconId: Styles.Icons.warning_color_16
    standardButtons: Dialog.Ok
    onAccepted: _root.backRequested()
  }

  Connections {
    target: $onboardingPagesController

    function onScanFailed(error) {
      _errorDialog.title = _private.getScanErrorTitle(error)
      _errorDialog.text = _private.getScanErrorText(error)
      _errorDialog.open()
    }
  }

  ColumnLayout {
    id: _layout
    spacing: Styles.Hints.spacing
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: Styles.Hints.spacing * 2
    anchors.rightMargin: Styles.Hints.spacing * 2
    anchors.verticalCenter: parent.verticalCenter

    Controls.Text {
      id: _title
      text: qsTr("Loading Quests")
      role: Controls.Text.Role.H1
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing * 2
    }

    Controls.Text {
      id: _text
      text: qsTr("Solarus Launcher is loading your Solarus Quest library…")
      Layout.fillWidth: true
      Layout.bottomMargin: Styles.Hints.spacing * 2
    }

    Controls.ProgressBar {
      id: _progressBar
      Layout.fillWidth: true
      value: 0
      from: 0
      to: 100

      Connections {
        target: $onboardingPagesController

        function onScanProgressChanged(progress) {
          _progressBar.value = progress

          if (progress === 100) {
            _root.finished()
          }
        }
      }
    }

    Controls.Text {
      id: _progressText
      horizontalAlignment: Text.AlignHCenter
      text: `${ Number(_progressBar.value).toFixed(0) }%`
      Layout.fillWidth: true
    }
  }
}
