/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.News 1.0 as News
import Solarus.Launcher.Components.RecentQuests 1.0 as RecentQuests
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils
import Solarus.Launcher.Model 1.0 as Model

FocusScope {
  readonly property Item focusStart: _questsLoader.item
  property real contentMaxWidth: Styles.Hints.homeContentMaxWidth
  signal backRequested

  id: _root

  onBackRequested: $localQuestsPageController.focusSideBar()
  Keys.onEscapePressed: _root.backRequested()

  QtObject {
    id: _private

    function getNewsComponent(state) {
      switch (state) {
      case Model.NewsListModel.State.Idle: return _newsListComponent
      case Model.NewsListModel.State.Loading: return _newsLoadingComponent
      case Model.NewsListModel.State.Error: return _newsErrorComponent
      default: return null
      }
    }

    function getQuestsComponent(state, questCount) {
      switch (state) {
      case Model.LocalQuestListModel.State.Idle: return questCount > 0 ? _questsListComponent : _questsErrorComponent
      case Model.LocalQuestListModel.State.Loading: return _questsLoadingComponent
      default: return null
      }
    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Component.onCompleted: {
    $homePageController.ensureSearchStarted()
    $bottomBarController.gamepadManager.gamepad = _gamepad
  }

  Controls.ScrollView {
    id: _scrollview
    contentWidth: -1
    contentHeight: _content.implicitHeight
    anchors.fill: parent
    anchors.leftMargin: Styles.Hints.spacing / 2
    anchors.rightMargin: Styles.Hints.spacing / 2
    verticalScrollBarVMargin: Math.round(Styles.Hints.spacing / 2)
    clip: false

    Item {
      id: _content
      x: Math.round((_scrollview.width - width) / 2)
      width: Math.min(_scrollview.availableWidth, _root.contentMaxWidth)
      height: implicitHeight
      implicitHeight: _layoutContent.implicitHeight

      ColumnLayout {
        id: _layoutContent
        anchors.fill: _content
        anchors.leftMargin: Styles.Hints.spacing * 2
        anchors.rightMargin: Styles.Hints.spacing * 2
        anchors.topMargin: Styles.Hints.spacing
        anchors.bottomMargin: Styles.Hints.spacing
        spacing: 0

        RowLayout {
          id: _pageTitleLayout
          spacing: Styles.Hints.spacing
          Layout.preferredWidth: Math.min(implicitWidth, parent.width)
          Layout.preferredHeight: implicitHeight
          Layout.fillWidth: false
          Layout.fillHeight: false
          Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
          Layout.bottomMargin: Styles.Hints.spacing * 2

          Controls.SvgImage {
            id: _icon
            source: Styles.Brand.icon
            Layout.preferredHeight: implicitHeight
            Layout.preferredWidth: Math.round(Styles.Hints.controlHeight * 2.5)
          }

          ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: false
            spacing: Math.round(Styles.Hints.spacing / 2)

            Controls.Text {
              id: _textTitle
              role: Controls.Text.Role.H1
              text: qsTr("Welcome to Solarus Launcher")
              horizontalAlignment: Text.AlignLeft
              Layout.fillWidth: true
            }

            Controls.Text {
              id: _textTitleCaption
              role: Controls.Text.Role.Body
              text: qsTr("Browse, download and play Solarus Quests on desktop.")
              horizontalAlignment: Text.AlignLeft
              Layout.fillWidth: true
            }
          }
        }

        Controls.Text {
          id: _questsTitle
          role: Controls.Text.Role.H2
          text: qsTr("Recently played")
          horizontalAlignment: Text.AlignLeft
          Layout.alignment: Qt.AlignLeft | Qt.AlignTop
          Layout.fillWidth: true
          Layout.fillHeight: false
          Layout.bottomMargin: Styles.Hints.spacing
        }

        Loader {
          id: _questsLoader
          focus: true
          Layout.fillWidth: true
          Layout.preferredHeight: implicitHeight
          Layout.bottomMargin: Styles.Hints.spacing * 4
          Layout.rightMargin: -Styles.Hints.spacing
          sourceComponent: _private.getQuestsComponent($homePageController.questListModel.state,
                                                       $homePageController.questListModel.questCount)
          Component {
            id: _questsListComponent

            RecentQuests.RecentQuestListView {
              id: _questsListView
              model: $homePageController.questListModel
              onQuestClicked: (quest) => $homePageController.startQuest(quest)
              onBackRequested: _root.backRequested()
              KeyNavigation.tab: _buttonMoreQuests
              KeyNavigation.down: _buttonMoreQuests
              Keys.onBacktabPressed: _root.backRequested()
            }
          }

          Component {
            id: _questsErrorComponent

            RowLayout {
              spacing: Styles.Hints.spacing

              Controls.SvgIcon {
                iconId: Styles.Icons.info_32
                color: Styles.Colors.textColorCaption
                width: Styles.Hints.iconSize * 2
                height: width
                Layout.alignment: Qt.AlignTop
                Layout.topMargin: -height / 4
              }

              ColumnLayout {
                Layout.fillWidth: true
                spacing: Styles.Hints.spacing

                Controls.Text {
                  text: qsTr("Once you've played some Solarus quests, the most recent ones will appear here.")
                  Layout.fillWidth: true
                }

                Controls.Button {
                  id: _buttonGoToQuestList
                  text: qsTr("Go to Quest list")
                  onClicked: $homePageController.goToQuestsPage()
                  iconId: Styles.Icons.next_16
                  KeyNavigation.down: _newsLoader.item
                  //KeyNavigation.right: _buttonRefreshNews

                  Keys.onLeftPressed: _root.backRequested()
                }
              }
            }
          }

          Component {
            id: _questsLoadingComponent

            ColumnLayout {
              spacing: Styles.Hints.spacing

              Controls.BusyIndicator {
                Layout.alignment: Qt.AlignHCenter
              }

              Controls.Text {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Loading…")
              }
            }
          }
        }

        Controls.Button {
          id: _buttonMoreQuests
          text: qsTr("See all quests")
          visible: $homePageController.questListModel.questCount > 0
          Layout.alignment: Qt.AlignHCenter
          onClicked: $homePageController.goToQuestsPage()
          Layout.bottomMargin: Styles.Hints.spacing * 3
          // KeyNavigation is buggy and doesn't work here.
          // Keys.onUpPressed: {
          //   const questListView = _questsLoader.item
          //   if (questListView) {
          //     questListView.forceActiveFocus()
          //   }
          // }
          //KeyNavigation.up: _questsLoader.item
          //KeyNavigation.down:
          //Keys.onDownPressed: Utils.EventUtils.tryFocusNextItem(this, true)
          Keys.onLeftPressed: _root.backRequested()
          Keys.onBacktabPressed: {
            const questListView = _questsLoader.item
            if (questListView) {
              questListView.forceActiveFocus()
            }
          }
        }

        Controls.Text {
          id: _titleLatestNews
          role: Controls.Text.Role.H2
          text: qsTr("Latest News")
          horizontalAlignment: Text.AlignLeft
          verticalAlignment: Text.AlignVCenter
          Layout.alignment: Qt.AlignVCenter
          Layout.fillWidth: true
          Layout.fillHeight: true
          Layout.bottomMargin: Styles.Hints.spacing
        }

        Loader {
          id: _newsLoader
          Layout.fillWidth: true
          Layout.preferredHeight: implicitHeight
          Layout.bottomMargin: Styles.Hints.spacing * 2
          Layout.rightMargin: -Styles.Hints.spacing
          sourceComponent: _private.getNewsComponent($homePageController.newsListModel.state)

          Component {
            id: _newsListComponent

            News.NewsListView {
              id: _newsListView
              model: $homePageController.newsListModel
              //KeyNavigation.up: _buttonRefreshNews
              KeyNavigation.tab: _buttonMoreNews
              KeyNavigation.backtab:_buttonRefreshNews
              onBackRequested: _root.backRequested()
            }
          }

          Component {
            id: _newsErrorComponent

            RowLayout {
              Layout.topMargin: Styles.Hints.spacing
              spacing: Styles.Hints.spacing

              Controls.SvgIcon {
                iconId: Styles.Icons.info_32
                color: Styles.Colors.textColorCaption
                width: Styles.Hints.iconSize * 2
                height: width
                Layout.alignment: Qt.AlignTop
                Layout.topMargin: -height / 4
              }

              ColumnLayout {
                spacing: Styles.Hints.spacing
                Layout.fillWidth: true
                Layout.fillHeight: true

                Controls.Text {
                  text: qsTr("Can't fetch latest news.")
                  Layout.fillWidth: true
                }

                Controls.Button {
                  text: qsTr("Refresh")
                  tooltip: qsTr("Refresh news articles")
                  iconId: Styles.Icons.refresh_16
                  onClicked: $homePageController.newsListModel.refresh()
                }
              }

            }
          }

          Component {
            id: _newsLoadingComponent

            ColumnLayout {
              spacing: Styles.Hints.spacing

              Controls.BusyIndicator {
                Layout.alignment: Qt.AlignHCenter
              }

              Controls.Text {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Loading…")
              }
            }
          }
        }

        RowLayout {
          spacing: Styles.Hints.spacing
          Layout.fillWidth: true

          Controls.Button {
            id: _buttonMoreNews
            text: qsTr("More news")
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: $homePageController.openNewsWebsiteURL()
            visible: $homePageController.newsListModel.state === Model.NewsListModel.State.Idle
            Layout.bottomMargin: Styles.Hints.spacing * 3
            KeyNavigation.backtab: _newsLoader.item
            KeyNavigation.up: _newsLoader.item
            Keys.onTabPressed: function(event) {
              // Prevent focus to go out of the page.
              event.accepted = true
            }
            Keys.onLeftPressed: _root.backRequested()
            KeyNavigation.right: _buttonRefreshNews
          }

          Controls.Button {
            id: _buttonRefreshNews
            tooltip: qsTr("Refresh news articles")
            iconId: Styles.Icons.refresh_16
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: $homePageController.newsListModel.refresh()
            enabled: $homePageController.newsListModel.state === Model.NewsListModel.State.Idle
            opacity: enabled ? 1 : 0
            // KeyNavigation is buggy and doesn't work here.
            //Keys.onUpPressed: Utils.EventUtils.tryFocusNextItem(this, false)
            //Keys.onRightPressed: Utils.EventUtils.tryFocusNextItem(this, false)
            // Keys.onDownPressed: {
            //   const newsListView = _newsLoader.item
            //   if (newsListView) {
            //     newsListView.forceActiveFocus()
            //   }
            // }
            KeyNavigation.up: _newsLoader.item
            KeyNavigation.left: _buttonMoreNews
            //KeyNavigation.down: _newsLoader.item
          }
        }
      }
    }
  }
}
