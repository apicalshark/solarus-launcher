/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Components.QuestUpdates 1.0 as QuestUpdates
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  property real bottomBarHeight: 0
  signal backRequested

  id: _root
  onBackRequested: $mainPagesController.navigation.focusSideBar()

  QtObject {
    readonly property Item scrollView: _loaderListView.item ? _loaderListView.item.scrollView : null
    readonly property bool showListView: $questUpdatePageController.questUpdateListModel.updateCount > 0
    readonly property bool showTopBarBackground: scrollView && !scrollView.atYBeginning
    property bool showScrollBar: false

    id: _private

    function onQuestUpdateActionTriggered(questUpdate, action) {
      if (!questUpdate)
        return

      switch (action) {
      case QuestUpdates.QuestUpdateUtils.Action.Update:
        $questUpdatePageController.updateQuest(questUpdate)
        break;
      case QuestUpdates.QuestUpdateUtils.Action.Cancel:
        $questUpdatePageController.cancelQuestUpdate(questUpdate)
        break;
      default:
        break
      }
    }

    function onSectionActionTriggered(action) {
      switch (action) {
      case QuestUpdates.QuestUpdateUtils.Action.Update:
        $questUpdatePageController.updateAll()
        break;
      case QuestUpdates.QuestUpdateUtils.Action.Cancel:
        $questUpdatePageController.cancelAll()
        break;
      default:
        break
      }
    }

    function formatDate(date) {
      return date ? Qt.formatDateTime(date, "dd/MM/yyyy hh:mm") : ""
    }

    function onLastItemDownRequested() {
      const currentFocus = _root.Window.activeFocusItem
      if (currentFocus) {
        const nextItem = currentFocus.nextItemInFocusChain(true)
        if (nextItem && nextItem !== _buttonCheckForUpdates) {
          nextItem.forceActiveFocus()
        }
      }
    }

    function focusFirstElement() {

    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Rectangle {
    id: _pageContent
    anchors.fill: _root
    color: Styles.Colors.windowColor

    Loader {
      id: _loaderListView
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.topMargin: _topBar.height + Styles.Hints.spacing / 2
      anchors.bottomMargin: _root.bottomBarHeight + Styles.Hints.spacing / 2
      anchors.leftMargin: Styles.Hints.spacing / 2
      anchors.rightMargin: Styles.Hints.spacing / 2
      active: _private.showListView

      sourceComponent: Component {
        Rectangle {
          readonly property Item scrollView: _scrollView
          id: _scrollViewBg
          color: Styles.Colors.windowColor

          QtObject {
            id: _private2

            function onSectionTopRequested(section) {
              const items = _layoutLists.children
              let sectionIndex = -1
              for (let i = 0; i < items.length; i++) {
                if (items[i] === section) {
                  sectionIndex = i
                  break
                }
              }

              let found = false
              for (let j = sectionIndex - 1; j >= 0; j--) {
                const previousItem = items[j]
                if (previousItem.visible) {
                  previousItem.forceActiveFocus()
                  found = true
                  break
                }
              }

              if (!found) {
                _topBar.nextItemInFocusChain(true).forceActiveFocus()
              }
            }
          }

          Controls.ScrollView {
            id: _scrollView
            anchors.fill: parent
            clip: false

            ColumnLayout {
              id: _layoutLists
              width: Math.min(_scrollView.width - Styles.Hints.spacing * 2, Styles.Hints.contentMaxWidth)
              height: implicitHeight
              x: Math.round((_scrollView.width - width) / 2)
              spacing: 0

              QuestUpdates.QuestUpdateListSection {
                id: _sectionUpdating
                section: Model.QuestUpdateListModel.UpdateSection.Updating
                visible: _listViewUpdating.visible
                onBackRequested: _root.backRequested()
                onTopRequested: _private2.onSectionTopRequested(this)
                Layout.fillWidth: true
              }

              QuestUpdates.QuestUpdateListView {
                id: _listViewUpdating
                visible: count > 0
                Layout.fillWidth: true
                model: $questUpdatePageController.updatingQuestUpdateProxyModel
                onBackRequested: _root.backRequested()
                onQuestUpdateActionTriggered: function (questUpdate, action) {
                  _private.onQuestUpdateActionTriggered(questUpdate, action)
                }
              }

              QuestUpdates.QuestUpdateListSection {
                id: _sectionPending
                section: Model.QuestUpdateListModel.UpdateSection.Pending
                visible: _listViewPending.visible
                onBackRequested: _root.backRequested()
                onTopRequested: _private2.onSectionTopRequested(this)
                Layout.fillWidth: true
                onSectionActionTriggered: function (action) {
                  switch (action) {
                  case QuestUpdates.QuestUpdateUtils.Action.Cancel:
                    $questUpdatePageController.cancelAllPending()
                    break;
                  default:
                    break
                  }
                }
              }

              QuestUpdates.QuestUpdateListView {
                id: _listViewPending
                visible: count > 0
                Layout.fillWidth: true
                model: $questUpdatePageController.pendingQuestUpdateProxyModel
                onBackRequested: _root.backRequested()
                onQuestUpdateActionTriggered: function (questUpdate, action) {
                  _private.onQuestUpdateActionTriggered(questUpdate, action)
                }
              }

              QuestUpdates.QuestUpdateListSection {
                id: _sectionAvailable
                section: Model.QuestUpdateListModel.UpdateSection.Available
                visible: _listViewAvailable.visible
                onBackRequested: _root.backRequested()
                onTopRequested: _private2.onSectionTopRequested(this)
                Layout.fillWidth: true
                onSectionActionTriggered: function (action) {
                  switch (action) {
                  case QuestUpdates.QuestUpdateUtils.Action.Update:
                    $questUpdatePageController.updateAllAvailable()
                    break;
                  default:
                    break
                  }
                }
              }

              QuestUpdates.QuestUpdateListView {
                id: _listViewAvailable
                visible: count > 0
                Layout.fillWidth: true
                model: $questUpdatePageController.availableQuestUpdateProxyModel
                onBackRequested: _root.backRequested()
                onQuestUpdateActionTriggered: function (questUpdate, action) {
                  _private.onQuestUpdateActionTriggered(questUpdate, action)
                }
                onLastItemDownRequested: _private.onLastItemDownRequested()
              }

              QuestUpdates.QuestUpdateListSection {
                section: Model.QuestUpdateListModel.UpdateSection.Error
                visible: _listViewError.visible
                onBackRequested: _root.backRequested()
                onTopRequested: _private2.onSectionTopRequested(this)
                Layout.fillWidth: true
                onSectionActionTriggered: function (action) {
                  switch (action) {
                  case QuestUpdates.QuestUpdateUtils.Action.Update:
                    $questUpdatePageController.retryAllFailed()
                    break;
                  default:
                    break
                  }
                }
              }

              QuestUpdates.QuestUpdateListView {
                id: _listViewError
                Layout.fillWidth: true
                visible: count > 0
                model: $questUpdatePageController.errorQuestUpdateProxyModel
                onBackRequested: _root.backRequested()
                onQuestUpdateActionTriggered: function (questUpdate, action) {
                  _private.onQuestUpdateActionTriggered(questUpdate, action)
                }
              }

              Controls.LayoutSpacer { Layout.bottomMargin: Styles.Hints.spacing }
            }
          }
        }
      }
    }

    Loader {
     id: _loaderPlaceholder
     width: Math.min(parent.width, Styles.Hints.contentMaxWidth)
     anchors.verticalCenter: parent.verticalCenter
     anchors.horizontalCenter: parent.horizontalCenter
     active: !_private.showListView
     sourceComponent: Component {
       ColumnLayout {
         spacing: Styles.Hints.spacing

         Controls.Text {
           role: Controls.Text.Role.H2
           text: qsTr("No quest updates")
           horizontalAlignment: Text.AlignHCenter
           Layout.fillWidth: true
         }

         Controls.Text {
           text: qsTr("No quest updates have been found.")
           horizontalAlignment: Text.AlignHCenter
           Layout.fillWidth: true
         }
       }
      }
    }
  }

  Item {
    readonly property Item blurSource: _pageContent
    readonly property real backgroundOpacity: _private.showTopBarBackground ? 1 : 0
    readonly property bool blurEnabled: true

    id: _topBar
    anchors.left: _root.left
    anchors.top: _root.top
    anchors.right: _root.right
    height: _topBarLayout.implicitHeight + _topBarLayout.anchors.topMargin + _topBarLayout.anchors.bottomMargin

    Rectangle {
      id: _topBarOpaqueBackground
      anchors.fill: parent
      color: Styles.Colors.windowColor
    }

    ShaderEffectSource {
      id: _effectSource
      anchors.fill: parent
      sourceItem: _topBar.blurSource
      sourceRect: sourceItem ? Qt.rect(sourceItem.x, sourceItem.y, sourceItem.width, _topBar.height)
                             : Qt.rect(0, 0, 0, 0)
      recursive: false
      live: true
      visible: _topBar.blurEnabled
    }

    FastBlur {
      id: _blur
      anchors.fill: parent
      source: _effectSource
      radius: Styles.Hints.blurRadius
      transparentBorder: false
      visible: _topBar.blurEnabled
      opacity: _topBar.backgroundOpacity

      Behavior on opacity {
        NumberAnimation {
          easing.type: Easing.OutCubic
          duration: Styles.Hints.animationDuration
        }
      }
    }

    Rectangle {
      id: _topBarBackground
      anchors.fill: parent
      color: Styles.Colors.overlayColor
      opacity: _topBar.backgroundOpacity

      Behavior on opacity {
        NumberAnimation {
          easing.type: Easing.OutCubic
          duration: Styles.Hints.animationDuration
        }
      }
    }

    RowLayout {
      id: _topBarLayout
      spacing: Styles.Hints.spacing
      width: Math.min(parent.width - Styles.Hints.spacing * 2, Styles.Hints.contentMaxWidth)
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: parent.verticalCenter
      anchors.topMargin: Styles.Hints.spacing
      anchors.bottomMargin: Styles.Hints.spacing

      Controls.Text {
        id: _textTitle
        text: qsTr("Quest Updates")
        role: Controls.Text.Role.H1
        wrapMode: Text.NoWrap
        elide: Text.ElideNone
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignVCenter
      }

      RowLayout {
        spacing: Styles.Hints.spacing / 4
        opacity: x > _textTitle.implicitWidth + _topBarLayout.spacing && $questUpdatePageController.lastQuestUpdateCheckTime !== undefined ? 1 : 0

        Controls.SvgIcon {
          iconId: Styles.Icons.calendar_16
          color: _textLastTimeCheck.color
          Layout.alignment: Qt.AlignVCenter
        }

        Controls.Text {
          id: _textLastTimeCheck
          text: _private.formatDate($questUpdatePageController.lastQuestUpdateCheckTime)
          role: Controls.Text.Role.Caption
          Layout.alignment: Qt.AlignVCenter
        }

        Behavior on opacity {
          NumberAnimation {
            easing.type: Easing.OutCubic
            duration: Styles.Hints.animationDuration
          }
        }
      }

      Controls.Button {
        id: _buttonCheckForUpdates
        iconId: Styles.Icons.refresh_16
        tooltip: qsTr("Check For Updates")
        onClicked: $questUpdatePageController.checkForUpdates()
        enabled: $questUpdatePageController.questUpdateListModel.state === Model.QuestUpdateListModel.State.Idle
        Layout.alignment: Qt.AlignVCenter
        Layout.rightMargin: Styles.Hints.spacing
        Keys.onUpPressed: (event) => { event.accepted = true }
        Keys.onLeftPressed: _root.backRequested()
        Keys.onBacktabPressed: function(event) {
          event.accepted = true
          _root.backRequested()
        }
        Keys.onTabPressed: function(event) {
          event.accepted = true
          _private.focusFirstElement()
        }
        Keys.onDownPressed: _private.focusFirstElement()
      }
    }
  }
}
