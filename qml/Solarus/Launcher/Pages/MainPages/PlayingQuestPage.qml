/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.Console 1.0 as Console
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Model 1.0 as Model

FocusScope {
  property real bottomBarHeight: 0
  property Model.LocalQuest quest: $playingQuestPageController.playingQuest

  id: _root

  QtObject {
    property int lastProcessError: Model.QuestRunner.ErrorCode.NoError
  }

  Component.onCompleted: {
    // Define HTML colors used in C++ from QML.
    $playingQuestPageController.outputColors = _outputColors

    $bottomBarController.gamepadManager.gamepad = _gamepad

    // Add quest output that we potentially missed because
    // the page wasn't loaded by the user.
    _console.append($playingQuestPageController.fullOutput());
  }

  Model.QuestOutputColors {
    id: _outputColors
    debugColor: Styles.Colors.textColorDebug
    infoColor: Styles.Colors.textColorInfo
    warningColor: Styles.Colors.textColorWarning
    errorColor: Styles.Colors.textColorError
    fatalColor: Styles.Colors.textColorFatal
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  ColumnLayout {
    id: _rootLayout
    spacing: Styles.Hints.spacing
    anchors.fill: parent
    anchors.topMargin: Styles.Hints.spacing * 2
    anchors.leftMargin: Styles.Hints.spacing * 2
    anchors.rightMargin: Styles.Hints.spacing * 2
    anchors.bottomMargin: Styles.Hints.spacing * 2 + _root.bottomBarHeight

    GridLayout {
      id: _layout
      rowSpacing: 0
      columnSpacing: Styles.Hints.spacing * 2
      Layout.fillWidth: true

      Controls.Text {
        id: _title
        text: _root.quest ? _root.quest.title : qsTr("Unknown title")
        role: Controls.Text.Role.H1
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        Layout.row: 0
        Layout.column: 0
      }

      Controls.Text {
        id: _titleAuthor
        text: _root.quest ? _root.quest.authors.join(", ") : qsTr("Unknown author")
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        Layout.row: 1
        Layout.column: 0
      }

      Controls.Button {
        id: _buttonStop
        iconId: Styles.Icons.stop_16
        text: qsTr("Stop")
        Layout.row: 0
        Layout.column: 1
        Layout.rowSpan: 2
        Layout.alignment: Qt.AlignTop | Qt.AlignRight
        enabled: $playingQuestPageController.playingQuest !== null
        onClicked: $playingQuestPageController.stopQuest()
      }
    }

    Console.Console {
      id: _console
      Layout.fillWidth: true
      Layout.fillHeight: true
    }
  }

  Connections {
    target: $playingQuestPageController

    function onOutputProduced() {
      _console.append(text)
      if (!_console.userHasInteracted) {
        _console.scrollToEnd()
      }
    }

    function onOutputCleared() {
      _console.clear()
      _console.scrollToStart()
    }

    function onErrorRaised(errorCode) {
      if (errorCode !== Model.QuestRunner.ErrorCode.NoError) {
        _private.lastProcessError = errorCode
        // TODO display error on the page
      }
    }
  }
}
