/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.LocalQuests 1.0 as LocalQuests
import Solarus.Launcher.Components.Quests 1.0 as Quests
import Solarus.Launcher.Components.SearchBar 1.0 as SearchBar
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Controllers 1.0 as Controllers
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property real bottomBarHeight: 0 //Styles.Hints.bottomBarHeight
  property real contentMaxWidth: Styles.Hints.contentMaxWidth
  property real searchBarHeight: Styles.Hints.searchBarHeight

  id: _root
  Keys.onEscapePressed: $localQuestsPageController.focusSideBar()
  onActiveFocusChanged: if (activeFocus) _loaderQuestListView.forceActiveFocus()

  QtObject {
    readonly property bool showQuestList: $localQuestsPageController.questListModel.questCount > 0
    property bool showScrollBar: false
    readonly property Item listView: _loaderQuestListView.item ? _loaderQuestListView.item.listView : null

    id: _private

    function getTitleText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("Configuration error")
      } else if (questCount === 0 && state === Model.LocalQuestListModel.State.Idle) {
        return qsTr("No quests found")
      } else {
        return qsTr("Looking for Solarus quests…")
      }
    }

    function getContentText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("You need to add a valid directory that contains Solarus quests.\nPlease change this configuration in the app settings.")
      } else if (questCount === 0 && state === Model.LocalQuestListModel.State.Idle) {
        return qsTr("The app cannot find any Solarus quest in the directory path you provided.\nHave you tried downloading some quests?")
      } else {
        return qsTr("Please wait while the app is looking for Solarus quests.")
      }
    }

    function getButtonText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("Go to Settings page")
      } else if (questCount === 0 && state === Model.LocalQuestListModel.State.Idle) {
        return qsTr("Browse and download quests")
      } else {
        return ""
      }
    }

    function getButtonIconId(state, validConfig, questCount) {
      if (!validConfig) {
        return Styles.Icons.settings_16
      } else if (questCount === 0 && state === Model.LocalQuestListModel.State.Idle) {
        return Styles.Icons.shop_16
      } else {
        return Styles.Icons.none
      }
    }

    function buttonCallback(state, validConfig, questCount) {
      if (!validConfig) {
        $localQuestsPageController.goToSettings()
      } else if (questCount === 0 && state === Model.LocalQuestListModel.State.Idle) {
        $localQuestsPageController.goToOnlineQuestList()
      }
    }

    function setScrollBarVisible(show) {
      _timer.stop()

      if (show) {
        _private.showScrollBar = true
      } else {
        _timer.start()
      }
    }

    function openQuestDialog() {
      _questDialog.quest = $localQuestsPageController.selectedQuest
      _questDialog.open()
    }

    function onQuestActionTriggered(quest, action) {
      if (!quest)
        return

      switch (action) {
      case LocalQuests.LocalQuestUtils.Action.Select:
        $localQuestsPageController.selectedQuest = quest
        break
      case LocalQuests.LocalQuestUtils.Action.ShowInfo:
        _private.openQuestDialog(quest)
        break
      case LocalQuests.LocalQuestUtils.Action.Play:
        $localQuestsPageController.playQuest(quest)
        break
      case LocalQuests.LocalQuestUtils.Action.Stop:
        $localQuestsPageController.stopQuest(quest)
        break
      case LocalQuests.LocalQuestUtils.Action.Update:
        $localQuestsPageController.updateQuest(quest)
        break
      case LocalQuests.LocalQuestUtils.Action.Remove:
        _removeQuestDialog.open()
        break
      default:
        break
      }
    }
  }

  // Timer used to delay the hiding of the scrollbar.
  Timer {
    id: _timer
    interval: Styles.Hints.scrollBarDisappearingDelay
    repeat: false
    onTriggered: _private.showScrollBar = false
  }

  Loader {
    id: _loaderPlaceholder
    active: !_private.showQuestList
    anchors.fill: parent
    sourceComponent: Component {
      Quests.QuestListPlaceholder {
        focus: true
        imageSource: Styles.Brand.questIcon
        showBusyIndicator: !$localQuestsPageController.questListModel.state
        textTitle: _private.getTitleText($localQuestsPageController.questListModel.state,
                                         $localQuestsPageController.questListModel.validConfig,
                                         $localQuestsPageController.questListModel.questCount)
        textContent: _private.getContentText($localQuestsPageController.questListModel.state,
                                             $localQuestsPageController.questListModel.validConfig,
                                             $localQuestsPageController.questListModel.questCount)
        buttonText: _private.getButtonText($localQuestsPageController.questListModel.state,
                                           $localQuestsPageController.questListModel.validConfig,
                                           $localQuestsPageController.questListModel.questCount)
        buttonIconId: _private.getButtonIconId($localQuestsPageController.questListModel.state,
                                               $localQuestsPageController.questListModel.validConfig,
                                               $localQuestsPageController.questListModel.questCount)
        onButtonClicked: _private.buttonCallback($localQuestsPageController.questListModel.state,
                                                 $localQuestsPageController.questListModel.validConfig,
                                                 $localQuestsPageController.questListModel.questCount)
      }
    }
  }

  Loader {
    id: _loaderQuestListView
    anchors.fill: parent
    active: _private.showQuestList
    sourceComponent: Component {
      // This rect is necessary if we want the blur to not have alpha.
      Rectangle {
        readonly property Item listView: _questListView
        color: Styles.Colors.windowColor

        LocalQuests.LocalQuestListView {
          id: _questListView
          anchors.top: parent.top
          anchors.topMargin: _root.searchBarHeight + Styles.Hints.spacing
          anchors.bottom: parent.bottom
          anchors.bottomMargin: _root.bottomBarHeight
          anchors.left: parent.left
          anchors.leftMargin: horizontalMargin + Styles.Hints.spacing // Shift to the right.
          anchors.right: parent.right
          anchors.rightMargin: horizontalMargin - Styles.Hints.spacing // Shift to the right.
          focus: true
          model: $localQuestsPageController.questListProxyModel
          onBackRequested: $localQuestsPageController.focusSideBar()
          onTopRequested: _loaderSearchBar.item.forceActiveFocus()
          selectedQuest: $localQuestsPageController.selectedQuest
          onSelectedQuestChanged: $localQuestsPageController.selectedQuest = selectedQuest
          ScrollBar.vertical: _verticalScrollBar
          KeyNavigation.tab: _loaderSearchBar.item
          KeyNavigation.backtab: _loaderSearchBar.item
          onQuestActionTriggered: _private.onQuestActionTriggered(quest, action)

          Keys.onPressed: function(event) {
            event.accepted = false
            if (event.key === Qt.Key_Up || event.key === Qt.Key_Down) {
              _private.setScrollBarVisible(true)
            }
          }
          Keys.onReleased: function(event) {
            event.accepted = false
            if (event.key === Qt.Key_Up || event.key === Qt.Key_Down) {
              _private.setScrollBarVisible(false)
            }
          }
          onMovingChanged: _private.setScrollBarVisible(moving)
        }

        Connections {
          target: $localQuestsPageController

          function onSelectedQuestChanged() {
            _questListView.selectedQuest = $localQuestsPageController.selectedQuest
          }
        }

        Controls.ScrollBar {
          id: _verticalScrollBar
          orientation: Qt.Vertical
          policy: ScrollBar.AsNeeded
          anchors.right: parent.right
          anchors.rightMargin: Styles.Hints.scrollBarMargin
          anchors.top: parent.top
          anchors.topMargin: _root.searchBarHeight + Styles.Hints.scrollBarMargin
          anchors.bottom: parent.bottom
          anchors.bottomMargin: Styles.Hints.scrollBarMargin
          needed: (_hoverHandler.hovered || _private.showScrollBar || _questListView.moving)
                  && _questListView.visibleArea.heightRatio < 1
        }
      }
    }
  }

  HoverHandler {
    id: _hoverHandler
    onHoveredChanged: _private.setScrollBarVisible(hovered)
  }

  Loader {
    id: _loaderSearchBar
    z: 1
    active: _private.showQuestList
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    sourceComponent: Component {
      SearchBar.SearchBar {
        id: _searchBar
        searchText: $localQuestsPageController.questListProxyModel.searchText
        onSearchTextEdited: $localQuestsPageController.questListProxyModel.searchText = searchText
        currentOptionButton: $localQuestsPageController.questListProxyModel.questSortMode
        backgroundVisible: _loaderQuestListView.item && !_loaderQuestListView.item.children[0].atYBeginning
        blurSource: _loaderQuestListView.item
        refreshButtonVisible: false
        onBackRequested: $localQuestsPageController.focusSideBar()
        onDownRequested: _loaderQuestListView.item.children[0].forceActiveFocus()

        // Since QML doesn't accept dynamic values for declarative QML models,
        // we have to create the items dynamically.
        Component.onCompleted: {
          _searchBar.optionModel.append({
            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.Title,
            "tooltip": qsTr("Sort by title"),
            "icon": Styles.Icons.sortAlphabetically_16,
          })
          _searchBar.optionModel.append({
            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.ReleaseDate,
            "tooltip": qsTr("Sort by release date"),
            "icon": Styles.Icons.sortDate_16,
          })
          _searchBar.optionModel.append({
            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.LastPlayedTime,
            "tooltip": qsTr("Sort by last played time"),
            "icon": Styles.Icons.sortTime_16,
          })

          // Update icons according to current order.
          const proxyModel = $localQuestsPageController.questListProxyModel
          const ascending = proxyModel.questSortOrder === Model.LocalQuestProxyModel.Ascending
          _searchBar.updateSearchBarIcons(ascending)
        }

        onOptionButtonClicked: function(index) {
          const optionModel = _searchBar.optionModel
          const proxyModel = $localQuestsPageController.questListProxyModel

          // Change sort mode in C++ model.
          proxyModel.questSortMode = optionModel.get(index).sortMode

          // Inverse sort order in C++ model if clicked again.
          if (index === _searchBar.currentOptionButton) {
            if (proxyModel.questSortOrder === Model.LocalQuestProxyModel.Ascending) {
              proxyModel.questSortOrder = Model.LocalQuestProxyModel.Descending
            } else {
              proxyModel.questSortOrder = Model.LocalQuestProxyModel.Ascending
            }

            // Update icons according to new order.
            _searchBar.updateSearchBarIcons(proxyModel.questSortOrder === Model.LocalQuestProxyModel.Ascending)
          }
        }

        function updateSearchBarIcons(ascending) {
          const optionModel = _searchBar.optionModel
          optionModel.setProperty(0, "icon",
            ascending ? Styles.Icons.sortAlphabetically_16 : Styles.Icons.sortAlphabeticallyInverted_16)
          optionModel.setProperty(1, "icon",
            ascending ? Styles.Icons.sortDate_16 : Styles.Icons.sortDateInverted_16)
          optionModel.setProperty(2, "icon",
            ascending ? Styles.Icons.sortTime_16 : Styles.Icons.sortTimeInverted_16)
        }
      }
    }
  }

  LocalQuests.LocalQuestDialog {
    id: _questDialog
    onQuestActionTriggered: _private.onQuestActionTriggered(quest, action)
    onAboutToHide: {
      if (_private.listView) {
        _private.listView.forceActiveFocus()
      }
    }
  }

  Controls.Dialog {
    id: _removeQuestDialog
    title: qsTr("Are you sure?")
    iconId: Styles.Icons.warning_color_16
    text: qsTr("Do you really want to remove '<b>%1</b>' from disk?").arg(
            $localQuestsPageController.selectedQuest ? $localQuestsPageController.selectedQuest.title : "?")
    standardButtons: Dialog.Yes | Dialog.Cancel
    onAccepted: {
      _questDialog.close()
      const quest = $localQuestsPageController.selectedQuest
      if (quest) {
        $localQuestsPageController.removeQuest(quest)
      }
    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Component.onCompleted: {
    $bottomBarController.gamepadManager.gamepad = _gamepad
  }
}
