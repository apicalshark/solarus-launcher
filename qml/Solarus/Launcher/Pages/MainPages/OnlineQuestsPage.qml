/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.OnlineQuests 1.0 as OnlineQuests
import Solarus.Launcher.Components.Quests 1.0 as Quests
import Solarus.Launcher.Components.SearchBar 1.0 as SearchBar
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Controllers 1.0 as Controllers
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property real bottomBarHeight: 0 //Styles.Hints.bottomBarHeight
  property real contentMaxWidth: Styles.Hints.contentMaxWidth
  property real searchBarHeight: Styles.Hints.searchBarHeight

  id: _root
  Keys.onEscapePressed: $onlineQuestsPageController.focusSideBar()
  onActiveFocusChanged: if (activeFocus) _loaderQuestListView.forceActiveFocus()

  Component.onCompleted: function() {
    $bottomBarController.gamepadManager.gamepad = _gamepad

    if ($onlineQuestsPageController.questListModel.state === Model.OnlineQuestListModel.State.Uninitialized) {
      $onlineQuestsPageController.refresh()
    }
  }

  QtObject {
    readonly property bool showQuestList: $onlineQuestsPageController.questListModel.questCount > 0
    property bool showScrollBar: false
    readonly property Item listView: _loaderQuestListView.item ? _loaderQuestListView.item.listView : null

    id: _private

    function getTitleText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("Configuration error")
      } else if (questCount === 0 && state === Model.OnlineQuestListModel.State.Idle) {
        return qsTr("No quests found")
      } else {
        return qsTr("Asking the server for Solarus quests…")
      }
    }

    function getContentText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("You need to set a valid URL to retrieve Solarus quests.\nPlease change this configuration in the app settings.")
      } else if (questCount === 0 && state === Model.OnlineQuestListModel.State.Idle) {
        return qsTr("There are no quests or the server isn't responding.")
      } else {
        return qsTr("Please wait while the app is downloading the Solarus quests list.")
      }
    }

    function getButtonText(state, validConfig, questCount) {
      if (!validConfig) {
        return qsTr("Go to Settings page")
      } else if (questCount === 0 && state === Model.OnlineQuestListModel.State.Idle) {
        return qsTr("Refresh")
      } else {
        return ""
      }
    }

    function getButtonIconId(state, validConfig, questCount) {
      if (!validConfig) {
        return Styles.Icons.settings_16
      } else if (questCount === 0 && state === Model.OnlineQuestListModel.State.Idle) {
        return Styles.Icons.refresh_16
      } else {
        return Styles.Icons.none
      }
    }

    function buttonCallback(state, validConfig, questCount) {
      if (!validConfig) {
        $onlineQuestsPageController.goToSettings()
      } else if (questCount === 0 && state === Model.OnlineQuestListModel.State.Idle) {
        $onlineQuestsPageController.refresh()
      }
    }

    function setScrollBarVisible(show) {
      _timer.stop()

      if (show) {
        _private.showScrollBar = true
      } else {
        _timer.start()
      }
    }

    function openQuestDialog() {
      _questDialog.quest = $onlineQuestsPageController.selectedQuest
      _questDialog.open()
    }

    function onQuestActionTriggered(quest, action) {
      if (!quest)
        return

      switch (action) {
      case OnlineQuests.OnlineQuestUtils.Action.Select:
        $onlineQuestsPageController.selectedQuest = quest
        break
      case OnlineQuests.OnlineQuestUtils.Action.ShowInfo:
        _private.openQuestDialog(quest)
        break
      case OnlineQuests.OnlineQuestUtils.Action.Download:
        $onlineQuestsPageController.downloadQuest(quest)
        break
      default:
        break
      }
    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  // Timer used to delay the hiding of the scrollbar.
  Timer {
    id: _timer
    interval: Styles.Hints.scrollBarDisappearingDelay
    repeat: false
    onTriggered: _private.showScrollBar = false
  }

  Loader {
    id: _loaderPlaceholder
    active: !_private.showQuestList
    anchors.fill: parent
    sourceComponent: Component {
      Quests.QuestListPlaceholder {
        focus: true
        imageSource: showBusyIndicator ? "" : Styles.Brand.libraryIcon
        showBusyIndicator: $onlineQuestsPageController.questListModel.state === Model.OnlineQuestListModel.State.Loading
        textTitle: _private.getTitleText($onlineQuestsPageController.questListModel.state,
                                         $onlineQuestsPageController.questListModel.validConfig,
                                         $onlineQuestsPageController.questListModel.questCount)
        textContent: _private.getContentText($onlineQuestsPageController.questListModel.state,
                                             $onlineQuestsPageController.questListModel.validConfig,
                                             $onlineQuestsPageController.questListModel.questCount)
        buttonText: _private.getButtonText($onlineQuestsPageController.questListModel.state,
                                           $onlineQuestsPageController.questListModel.validConfig,
                                           $onlineQuestsPageController.questListModel.questCount)
        buttonIconId: _private.getButtonIconId($onlineQuestsPageController.questListModel.state,
                                               $onlineQuestsPageController.questListModel.validConfig,
                                               $onlineQuestsPageController.questListModel.questCount)
        onButtonClicked: _private.buttonCallback($onlineQuestsPageController.questListModel.state,
                                                 $onlineQuestsPageController.questListModel.validConfig,
                                                 $onlineQuestsPageController.questListModel.questCount)
      }
    }
  }

  Loader {
    id: _loaderQuestListView
    anchors.fill: parent
    active: _private.showQuestList
    sourceComponent: Component {
      // This rect is necessary if we want the blur to not have alpha.
      Rectangle {
        readonly property Item listView: _questListView

        color: Styles.Colors.windowColor

        OnlineQuests.OnlineQuestListView {
          id: _questListView
          anchors.top: parent.top
          anchors.topMargin: _root.searchBarHeight + Styles.Hints.spacing
          anchors.bottom: parent.bottom
          anchors.bottomMargin: _root.bottomBarHeight
          anchors.left: parent.left
          anchors.leftMargin: horizontalMargin + Styles.Hints.spacing // Shift to the right.
          anchors.right: parent.right
          anchors.rightMargin: horizontalMargin - Styles.Hints.spacing // Shift to the right.
          focus: true
          model: $onlineQuestsPageController.questListProxyModel
          onBackRequested: $onlineQuestsPageController.focusSideBar()
          onTopRequested: _loaderSearchBar.item.forceActiveFocus()
          selectedQuest: $onlineQuestsPageController.selectedQuest
          onSelectedQuestChanged: $onlineQuestsPageController.selectedQuest = selectedQuest
          ScrollBar.vertical: _verticalScrollBar
          KeyNavigation.tab: _loaderSearchBar.item
          KeyNavigation.backtab: _loaderSearchBar.item
          clip: false

          onQuestActionTriggered: _private.onQuestActionTriggered(quest, action)

          Keys.onPressed: function(event) {
            event.accepted = false
            if (event.key === Qt.Key_Up || event.key === Qt.Key_Down) {
              _private.setScrollBarVisible(true)
            }
          }
          Keys.onReleased: function(event) {
            event.accepted = false
            if (event.key === Qt.Key_Up || event.key === Qt.Key_Down) {
              _private.setScrollBarVisible(false)
            }
          }
          onMovingChanged: _private.setScrollBarVisible(moving)
        }

        Connections {
          target: $onlineQuestsPageController

          function onSelectedQuestChanged() {
            _questListView.selectedQuest = $onlineQuestsPageController.selectedQuest
          }
        }

        Controls.ScrollBar {
          id: _verticalScrollBar
          orientation: Qt.Vertical
          policy: ScrollBar.AsNeeded
          anchors.right: parent.right
          anchors.rightMargin: Styles.Hints.scrollBarMargin
          anchors.top: parent.top
          anchors.topMargin: _root.searchBarHeight + Styles.Hints.scrollBarMargin
          anchors.bottom: parent.bottom
          anchors.bottomMargin: Styles.Hints.scrollBarMargin
          needed: (_hoverHandler.hovered || _private.showScrollBar || _questListView.moving)
                  && _questListView.visibleArea.heightRatio < 1
        }
      }
    }
  }

  HoverHandler {
    id: _hoverHandler
    onHoveredChanged: _private.setScrollBarVisible(hovered)
  }

  Loader {
    id: _loaderSearchBar
    z: 1
    active: _private.showQuestList
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.right: parent.right
    sourceComponent: Component {
      SearchBar.SearchBar {
        id: _searchBar
        searchText: $onlineQuestsPageController.questListProxyModel.searchText
        onSearchTextEdited: $onlineQuestsPageController.questListProxyModel.searchText = searchText
        currentOptionButton: $onlineQuestsPageController.questListProxyModel.questSortMode
        backgroundVisible: _loaderQuestListView.item && !_loaderQuestListView.item.children[0].atYBeginning
        blurSource: _loaderQuestListView.item
        onBackRequested: $onlineQuestsPageController.focusSideBar()
        onDownRequested: _loaderQuestListView.item.children[0].forceActiveFocus()
        onRefreshClicked: $onlineQuestsPageController.refresh()

        // Since QML doesn't accept dynamic values for declarative QML models,
        // we have to create the items dynamically.
        Component.onCompleted: {
          _searchBar.optionModel.append({
            "sortMode": Model.OnlineQuestProxyModel.QuestSortMode.Title,
            "tooltip": qsTr("Sort by title"),
            "icon": Styles.Icons.sortAlphabetically_16,
          })
          _searchBar.optionModel.append({
            "sortMode": Model.OnlineQuestProxyModel.QuestSortMode.ReleaseDate,
            "tooltip": qsTr("Sort by release date"),
            "icon": Styles.Icons.sortDate_16,
          })

          // Update icons according to current order.
          const proxyModel = $onlineQuestsPageController.questListProxyModel
          const ascending = proxyModel.questSortOrder === Model.OnlineQuestProxyModel.Ascending
          _searchBar.updateSearchBarIcons(ascending)
        }

        onOptionButtonClicked: function(index) {
          const optionModel = _searchBar.optionModel
          const proxyModel = $onlineQuestsPageController.questListProxyModel

          // Change sort mode in C++ model.
          proxyModel.questSortMode = optionModel.get(index).sortMode

          // Inverse sort order in C++ model if clicked again.
          if (index === _searchBar.currentOptionButton) {
            if (proxyModel.questSortOrder === Model.OnlineQuestProxyModel.Ascending) {
              proxyModel.questSortOrder = Model.OnlineQuestProxyModel.Descending
            } else {
              proxyModel.questSortOrder = Model.OnlineQuestProxyModel.Ascending
            }

            // Update icons according to new order.
            _searchBar.updateSearchBarIcons(proxyModel.questSortOrder === Model.OnlineQuestProxyModel.Ascending)
          }
        }

        function updateSearchBarIcons(ascending) {
          const optionModel = _searchBar.optionModel
          optionModel.setProperty(0, "icon",
            ascending ? Styles.Icons.sortAlphabetically_16 : Styles.Icons.sortAlphabeticallyInverted_16)
          optionModel.setProperty(1, "icon",
            ascending ? Styles.Icons.sortDate_16 : Styles.Icons.sortDateInverted_16)
        }
      }
    }
  }

  OnlineQuests.OnlineQuestDialog {
    id: _questDialog
    onClosed: quest = null
    onQuestActionTriggered: _private.onQuestActionTriggered(quest, action)
    onAboutToHide: {
      if (_private.listView) {
        _private.listView.forceActiveFocus()
      }
    }
  }
}
