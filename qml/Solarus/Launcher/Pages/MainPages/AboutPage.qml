/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Model 1.0 as Model

FocusScope {
  property real contentMaxWidth: Styles.Hints.contentMaxWidth
  signal backRequested

  id: _root
  onBackRequested: $mainPagesController.navigation.focusSideBar()

  QtObject {
    readonly property int contentMaxWidth: Math.ceil(_root.contentMaxWidth / 1.125)
    id: _private
  }

  Controls.ScrollView {
    id: _scrollview
    contentWidth: -1
    contentHeight: _layoutContent.implicitHeight
    verticalScrollBarVMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.fill: parent
    anchors.leftMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.rightMargin: Math.round(Styles.Hints.spacing / 2)
    clip: false

    Keys.onLeftPressed: _root.backRequested()
    Keys.onEscapePressed: _root.backRequested()

    Item {
      id: _content
      width: Math.floor(Math.min(_scrollview.availableWidth, _layoutContent.implicitWidth))
      height: _layoutContent.implicitHeight
      x: Math.round((_scrollview.availableWidth - width) / 2)
      y:  Math.round(Math.max(0, (_scrollview.availableHeight - height) / 2))

      ColumnLayout {
        id: _layoutContent
        spacing: Styles.Hints.spacing * 2
        anchors.fill: parent

        Controls.SvgImage {
          id: _logoIcon
          source: Styles.Brand.icon
          Layout.preferredHeight: implicitHeight
          Layout.preferredWidth: Math.round(Styles.Hints.controlHeight * 3)
          Layout.topMargin: Styles.Hints.spacing * 2
          Layout.bottomMargin: -Styles.Hints.spacing
          Layout.alignment: Qt.AlignHCenter
        }

        ColumnLayout {
          id: _layoutTitle
          spacing: Styles.Fonts.bodyLineHeight
          Layout.fillWidth: true
          Layout.leftMargin: Styles.Hints.spacing * 2
          Layout.rightMargin: Styles.Hints.spacing * 2
          Layout.alignment: Qt.AlignHCenter
          Layout.maximumWidth: _private.contentMaxWidth

          Controls.Text {
            id: _textApplicationName
            text: $aboutPageController.applicationName
            role: Controls.Text.Role.H1
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }

          Controls.Text {
            id: _textVersionNumber
            text: qsTr("Version %1").arg($aboutPageController.applicationVersion)
            role: Controls.Text.Role.UppercaseLabel
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }

          Controls.Text {
            id: _textDescription
            text: qsTr("A game launcher and browser for Solarus, a free and open-source Action-RPG/Adventure 2D game engine.")
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }
        }

        ColumnLayout {
          id: _layoutCopyrights
          spacing: Styles.Fonts.bodyLineSpacing
          Layout.fillWidth: true
          Layout.leftMargin: Styles.Hints.spacing * 2
          Layout.rightMargin: Styles.Hints.spacing * 2
          Layout.maximumWidth: _private.contentMaxWidth
          Layout.alignment: Qt.AlignHCenter

          Controls.Text {
            id: _textCopyright
            text: $aboutPageController.applicationCopyright
            bold: true
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }

          // Due to a bug in QML Text for highlighted links, we have to separate this into 2 elements.
          Controls.Text {
            id: _textLicense
            text: qsTr("This program is licensed under the <a href='%1'>GNU Public License, version 3</a>.").arg($aboutPageController.gplLicenseURL)
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }

          Controls.Text {
            id: _textLicense2
            text: qsTr("Included assets are licensed under <a href='%2'>CC-BY-SA 4.0</a>.").arg($aboutPageController.ccLicenseURL)
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
          }
        }

        RowLayout {
          id: _layoutThirdPartyLogos
          Layout.fillWidth: true
          spacing: Styles.Hints.spacing
          Layout.alignment: Qt.AlignHCenter

          Controls.SvgImage {
            id: _imageGplLogo
            source: Styles.ThirdParty.gplLogo
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Styles.Hints.controlHeight
            Layout.preferredWidth: height * ratio
          }

          Controls.SvgImage {
            id: _imageCCLogo
            source: Styles.ThirdParty.creativeCommonsLogo
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredHeight: Styles.Hints.controlHeight
            Layout.preferredWidth: height * ratio
          }
        }

        ColumnLayout {
          id: _layoutButtons
          spacing: Styles.Hints.spacing
          Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
          Layout.preferredHeight: implicitHeight
          Layout.minimumWidth: implicitWidth
          Layout.bottomMargin: Styles.Hints.spacing * 2
          Layout.leftMargin: Styles.Hints.spacing * 2
          Layout.rightMargin: Styles.Hints.spacing * 2
          Layout.maximumWidth: Math.ceil(_root.contentMaxWidth / 2)

          Controls.Button {
            id: _buttonUserManual
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("User Manual")
            tooltip: qsTr("Show User Manual")
            iconId: Styles.Icons.help_16
            Layout.fillWidth: true
            focus: true
            KeyNavigation.down: _buttonChangelog
            KeyNavigation.tab: _buttonChangelog
            onClicked: $aboutPageController.showUserManual()
          }

          Controls.Button {
            id: _buttonChangelog
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Changelog")
            tooltip: qsTr("Show Changelog")
            iconId: Styles.Icons.calendar_16
            Layout.fillWidth: true
            focus: true
            KeyNavigation.down: _buttonSourceCode
            KeyNavigation.tab: _buttonSourceCode
            onClicked: $aboutPageController.showChangelog()
          }

          Controls.Button {
            id: _buttonSourceCode
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Source code")
            tooltip: $aboutPageController.sourceCodeURL
            iconId: Styles.Icons.sourceCode_16
            Layout.fillWidth: true
            focus: true
            KeyNavigation.down: _buttonReportBug
            KeyNavigation.tab: _buttonReportBug
            onClicked: $aboutPageController.openSourceCodeURL()
          }

          Controls.Button {
            id: _buttonReportBug
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Report a bug")
            tooltip: $aboutPageController.bugReportURL
            iconId: Styles.Icons.bug_16
            Layout.fillWidth: true
            focus: true
            KeyNavigation.down: _buttonViewWebsite
            KeyNavigation.tab: _buttonViewWebsite
            onClicked: $aboutPageController.openBugReportURL()
          }

          Controls.Button {
            id: _buttonViewWebsite
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("View website")
            tooltip: $aboutPageController.websiteURL
            iconId: Styles.Icons.network_16
            Layout.fillWidth: true
            KeyNavigation.up: _buttonReportBug
            onClicked: $aboutPageController.openWebsiteURL()
            Keys.onTabPressed: function(event) {
              event.accepted = true
            }
          }
        }
      }
    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Component.onCompleted: {
    $bottomBarController.gamepadManager.gamepad = _gamepad
  }
}
