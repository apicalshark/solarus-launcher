/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QtControls
//import QtGamepad 1.15
import QtQuick.Window 2.15
import Qt.labs.platform 1.1

import Solarus.Launcher.Components.Settings 1.0 as SC
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  // property Gamepad gamepad
  property real bottomBarHeight: 0
  property real contentMaxWidth: Styles.Hints.contentMaxWidth
  signal backRequested

  id: _root

  onBackRequested: {
    $mainPagesController.navigation.focusSideBar()
  }

  QtObject {
    id: _private

    function formatDate(date) {
      return date ? Qt.formatDateTime(date, "dd/MM/yyyy hh:mm") : ""
    }
  }

  Controls.ScrollView {
    id: _scrollview
    contentWidth: -1
    contentHeight: _layoutContent.implicitHeight
    verticalScrollBarVMargin: Math.round(Styles.Hints.spacing / 2)
    leftPadding: Styles.Hints.spacing * 2
    rightPadding: Styles.Hints.spacing * 2
    topPadding: Styles.Hints.spacing * 2
    bottomPadding: Styles.Hints.spacing * 2
    anchors.fill: parent
    anchors.leftMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.rightMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.bottomMargin: _root.bottomBarHeight
    Keys.onLeftPressed: _root.backRequested()
    Keys.onEscapePressed: _root.backRequested()
    clip: false

    ColumnLayout {
      id: _layoutContent
      spacing: Styles.Hints.spacing * 4
      width: Math.floor(Math.min(_scrollview.availableWidth, _root.contentMaxWidth))
      x: Math.floor((_scrollview.availableWidth - width) / 2)

      SC.SettingsGroup {
        id: _groupApp
        title: qsTr("Application Settings")
        titleRole: Controls.Text.Role.H1

        SC.SettingsGroup {
          id: _groupAppGeneral
          title: qsTr("General")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowAppLanguage
            label: qsTr("Language")
            description: qsTr("Choose the language this application is displayed in.")
            Controls.ComboBox {
              id: _comboboxAppLanguage
              model: $settingsPageController.languageListModel
              textRole: "display"
              valueRole: "language"
              onActivated: $settingsPageController.languageListModel.currentLanguage = currentValue
              Component.onCompleted: currentIndex = Qt.binding(() => indexOfValue($settingsPageController.languageListModel.currentLanguage))
              Keys.onBacktabPressed: event.accepted = true // Block backtab
              KeyNavigation.down: _rowQuestDirectory.item
              // Dirty hack to make it scroll to the top.
              onUpDownKeyPressed: Utils.ScrollUtils.startScroll(_scrollview.QtControls.ScrollBar.vertical, 0, true)
            }
          }

          SC.SettingsRow {
            id: _rowQuestDirectory
            label: qsTr("Quests directory")
            description: qsTr("This application will look for Solarus quests in this directory.")
            below: true
            SC.DirectoryChooser {
              id: _questDirectoryChooser
              placeholderText: qsTr("Path to the Solarus quests directory")
              validPath: $settingsPageController.validPath
              directory: $settingsPageController.settings.questDirectory
              onDirectoryEdited: $settingsPageController.settings.questDirectory = directory
              KeyNavigation.down: _rowAppForceSoftwareRendering.item
            }
          }
        }

        SC.SettingsGroup {
          id: _groupAppVideo
          title: qsTr("Video")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowAppForceSoftwareRendering
            label: qsTr("Force software rendering for app")
            description: qsTr("Enable this option if your GPU doesn't support OpenGL. Restart required.")
            Controls.Switch {
              id: _switchForceSoftwareRenderingApp
              checked: $settingsPageController.settings.appForceSoftwareRendering
              onToggled: $settingsPageController.settings.appForceSoftwareRendering = checked
              KeyNavigation.up: _rowQuestDirectory.item
              KeyNavigation.down: _rowAppFullScreen.item
            }
          }

          SC.SettingsRow {
            id: _rowAppFullScreen
            label: qsTr("Full screen app")
            description: qsTr("Display this application in full screen.")
            Controls.Switch {
              id: _switchFullScreenApp
              checked: $settingsPageController.settings.appFullScreen
              onToggled: $settingsPageController.settings.appFullScreen = checked
              KeyNavigation.down: _rowAppEnableAudio.item
            }
          }
        }

        SC.SettingsGroup {
          id: _groupAppAudio
          title: qsTr("Audio")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowAppEnableAudio
            label: qsTr("Enable app audio")
            description: qsTr("Play sound effects in this application.")
            Controls.Switch {
              id: _switchEnableAppAudio
              checked: $settingsPageController.settings.appEnableAudio
              onToggled: $settingsPageController.settings.appEnableAudio = checked
              KeyNavigation.down: _rowAppAudioVolume.item
            }
          }

          SC.SettingsRow {
            id: _rowAppAudioVolume
            label: qsTr("App audio volume")
            description: qsTr("Adjust audio volume for this application's sound effects.")
            below: width < 480
            enabled: $settingsPageController.settings.appEnableAudio
            Controls.Slider {
              id: _sliderAppAudioVolume
              Layout.fillWidth: _rowAppAudioVolume.below
              from: 0
              to: 100
              value: $settingsPageController.settings.appAudioVolume
              onMoved: $settingsPageController.settings.appAudioVolume = value
              KeyNavigation.down: _rowAppEnableUpdates.item
            }
          }
        }

        SC.SettingsGroup {
          id: _groupAppUpdates
          title: qsTr("Updates")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowAppEnableUpdates
            label: qsTr("Enable auto-updates")
            description: qsTr("Automatically check and download updates for this application.")
            Controls.Switch {
              id: _switchEnableAppUpdates
              checked: $settingsPageController.settings.appEnableUpdates
              onToggled: $settingsPageController.settings.appEnableUpdates = checked
              KeyNavigation.down: _rowAppUpdatesFrequency.item
            }
          }
          SC.SettingsRow {
            id: _rowAppUpdatesFrequency
            label: qsTr("Checking frequency")
            description: qsTr("Define how frequently this application should look for updates.")
            enabled: $settingsPageController.settings.appEnableUpdates
            Controls.ComboBox {
              id: _comboboxAppUpdatesFrequency
              KeyNavigation.down: _rowAppCheckForUpdates.item
              KeyNavigation.priority: KeyNavigation.BeforeItem
              model: $settingsPageController.checkFrequencyListModel
              textRole: "text"
              valueRole: "value"
              Component.onCompleted: {
                // Strange bug: the binding only seem to work when created like this.
                _comboboxAppUpdatesFrequency.currentIndex = Qt.binding(() => _comboboxAppUpdatesFrequency.indexOfValue($settingsPageController.settings.appUpdateCheckFrequency))
                _connections1.enabled = true
              }
              Connections {
                id: _connections1
                target: _comboboxAppUpdatesFrequency
                enabled: false
                function onCurrentValueChanged() {
                  $settingsPageController.settings.appUpdateCheckFrequency = _comboboxAppUpdatesFrequency.currentValue
                }
              }
            }
          }
          SC.SettingsRow {
            id: _rowAppCheckForUpdates
            label: qsTr("Check For Updates")
            description: qsTr("Last checked: %1").arg(_private.formatDate($settingsPageController.lastAppUpdateCheckTime))
            enabled: $settingsPageController.settings.appEnableUpdates && !$settingsPageController.checkingforAppUpdates
            Controls.Button {
              id: _buttonCheckForAppUpdates
              KeyNavigation.down: _rowQuestsForceSoftwareRendering.item
              text: qsTr("Check")
              iconId: Styles.Icons.refresh_16
              onClicked: $settingsPageController.checkForAppUpdates()
              Layout.alignment: Qt.AlignRight
            }
          }
        }
      }

      SC.SettingsGroup {
        id: _groupQuests
        title: qsTr("Quest Settings")
        titleRole: Controls.Text.Role.H1

        SC.SettingsGroup {
          id: _groupQuestsVideo
          title: qsTr("Video")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowQuestsForceSoftwareRendering
            label: qsTr("Force software rendering for games")
            description: qsTr("Enable this option if your GPU doesn't support OpenGL.")
            Controls.Switch {
              checked: $settingsPageController.settings.questForceSoftwareRendering
              onToggled: $settingsPageController.settings.questForceSoftwareRendering = checked
              KeyNavigation.down: _rowQuestsFullScreen.item
            }
          }

          SC.SettingsRow {
            id: _rowQuestsFullScreen
            label: qsTr("Full screen games")
            description: qsTr("Display games in full screen.")
            Controls.Switch {
              id: _switchFullScreenGame
              checked: $settingsPageController.settings.questFullScreen
              onToggled: $settingsPageController.settings.questFullScreen = checked
              KeyNavigation.down: _rowQuestsEnableAudio.item
            }
          }
        }

        SC.SettingsGroup {
          id: _groupQuestsAudio
          title: qsTr("Audio")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowQuestsEnableAudio
            label: qsTr("Enable quests audio")
            description: qsTr("Play the games audio.")
            Controls.Switch {
              id: _switchEnableQuestsAudio
              checked: $settingsPageController.settings.questEnableAudio
              onToggled: $settingsPageController.settings.questEnableAudio = checked
              KeyNavigation.down: _rowQuestsEnableUpdates.item
            }
          }
        }

        SC.SettingsGroup {
          id: _groupQuestsUpdates
          title: qsTr("Updates")
          titleRole: Controls.Text.Role.H2

          SC.SettingsRow {
            id: _rowQuestsEnableUpdates
            label: qsTr("Enable auto-updates")
            description: qsTr("Automatically check and download updates for quests.")
            Controls.Switch {
              id: _switchEnableQuestsUpdates
              checked: $settingsPageController.settings.questEnableUpdates
              onToggled: $settingsPageController.settings.questEnableUpdates = checked
              KeyNavigation.down: _rowQuestsUpdatesFrequency.item
            }
          }
          SC.SettingsRow {
            id: _rowQuestsUpdatesFrequency
            label: qsTr("Checking frequency")
            description: qsTr("Define how frequently this application should look for quest updates.")
            enabled: $settingsPageController.settings.questEnableUpdates
            Controls.ComboBox {
              id: _comboboxQuestsUpdatesFrequency
              KeyNavigation.down: _rowQuestsCheckForUpdates.item
              KeyNavigation.priority: KeyNavigation.BeforeItem
              model: $settingsPageController.checkFrequencyListModel
              textRole: "text"
              valueRole: "value"
              Component.onCompleted: {
                // Strange bug: the binding only seem to work when created like this.
                _comboboxQuestsUpdatesFrequency.currentIndex = Qt.binding(() => _comboboxQuestsUpdatesFrequency.indexOfValue($settingsPageController.settings.questUpdateCheckFrequency))
                _connections2.enabled = true
              }
              Connections {
                id: _connections2
                target: _comboboxQuestsUpdatesFrequency
                enabled: false
                function onCurrentValueChanged() {
                  $settingsPageController.settings.questUpdateCheckFrequency = _comboboxQuestsUpdatesFrequency.currentValue
                }
              }
            }
          }
          SC.SettingsRow {
            id: _rowQuestsCheckForUpdates
            label: qsTr("Check For Updates")
            description: qsTr("Last checked: %1").arg(_private.formatDate($settingsPageController.settings.questLastUpdateCheck))
            enabled: $settingsPageController.settings.questEnableUpdates && !$settingsPageController.checkingforQuestsUpdates
            Controls.Button {
              id: _buttonCheckForQuestsUpdates
              text: qsTr("Check")
              iconId: Styles.Icons.refresh_16
              onClicked: $settingsPageController.checkForQuestUpdates()
              Layout.alignment: Qt.AlignRight
              KeyNavigation.down: _rowReset.item
            }
          }
        }
      }

      SC.SettingsGroup {
        id: _groupReset
        title: qsTr("Reset")
        titleRole: Controls.Text.Role.H1

        SC.SettingsRow {
          id: _rowReset
          label: qsTr("Reset settings")
          description: qsTr("Reset the application settings to defaults. Doesn't remove quests and saves.")
          Controls.Button {
            id: _buttonResetSettings
            text: qsTr("Reset")
            iconId: Styles.Icons.undo_16
            onClicked: _dialogConfirmResetSettings.open()
         }
        }
      }
    }
  }

  Controls.Dialog {
    id: _dialogConfirmResetSettings
    title: qsTr("Reset Settings to defaults")
    iconId: Styles.Icons.warning_color_16
    text: qsTr("Do you really want to reset settings to defaults?
All your current settings will be erased.\n
Note: Your Solarus quests and their save files will be kept.")
    standardButtons: QtControls.Dialog.Yes | QtControls.Dialog.Cancel
    onAccepted: $settingsPageController.settings.resetToDefaults()
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Component.onCompleted: {
    $bottomBarController.gamepadManager.gamepad = _gamepad
  }
}
