/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property bool down: false
  property bool hovered: false
  property bool hasFocus: false
  property int radius: Styles.Hints.radius
  property bool shadowEnabled: true

  id: _root
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: Styles.Hints.controlHeight

  Rectangle {
    id: _background
    anchors.fill: parent
    color: Styles.Colors.getTextFieldBackground(_root.enabled)
    radius: _root.radius
    layer.enabled: _root.enabled && _root.shadowEnabled
    layer.effect: InnerShadow {
      visible: _root.shadowEnabled
      horizontalOffset: 0
      verticalOffset: Styles.Hints.borderWidth * 2
      radius: Styles.Hints.borderWidth
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColorLighter
    }

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  Rectangle {
    anchors.fill: parent
    border.width: 1
    border.color: Styles.Colors.getTextFieldBorder(_root.hasFocus, _root.hovered, _root.down, _root.enabled)
    color: "transparent"
    radius: _root.radius

    Behavior on border.color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }
}
