/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15
//import QtQuick.Layouts 1.15

QtObject {
  id: _root

  function getMinimumWidth(listView, count, minWidth) {
    // Force the ListView to create the items.
    listView.forceLayout()

    let maxWidth = minWidth
    for (let i = 0; i < count; i++) {
      const item = listView.itemAtIndex(i)
      if (item) {
        const itemWidth = item.implicitWidth
        maxWidth = Math.max(maxWidth, itemWidth)
      }
    }
    return maxWidth
  }

  function getColumnLayoutMinimumWidth(layout, minWidth) {
    let maxWidth = minWidth ?? 0
    const children = layout.children
    for (let i = 0; i < children.length; i++) {
      const child = children[i]
      const childWidth = child.implicitWidth
      maxWidth = Math.max(maxWidth, childWidth)
    }
    return maxWidth
  }

  function getHorizontalWidth(item) {
    let result = 0
    const children = item.children
    for (let i = 0; i < children.length; i++) {
      const child = children[i]
      result += Math.ceil(Math.max(child.implicitWidth, child.width) + child.anchors.leftMargin + child.anchors.rightMargin)
    }
    return result
  }

  function getGridViewHeight(width, cellWidth, cellHeight, count) {
    const columnCount = cellWidth > 0 ? Math.floor(width / cellWidth) : 0
    const rowCount = columnCount > 0 ? Math.ceil(count / columnCount) : 0
    const height = cellHeight * rowCount
    return height
  }

  function getGridViewNecessaryWidth(availableWidth, cellWidth) {
    return cellWidth > 0 ? Math.floor(availableWidth / cellWidth) * cellWidth : 0
  }

  function getProgressLabelWidth(from, to, fontMetrics) {
    const fromText = Number(from).toFixed(0) + "%"
    const toText = Number(to).toFixed(0) + "%"
    const fromWidth = Math.ceil(fontMetrics.boundingRect(fromText).width)
    const toWidth = Math.ceil(fontMetrics.boundingRect(toText).width)
    return Math.max(fromWidth, toWidth)
  }
}
