/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  id: _root

  function getTranslatedLanguage(languageCode) {
    switch (languageCode) {
    case "de":
      return qsTr("German")
    case "en":
      return qsTr("English")
    case "es":
      return qsTr("Spanish")
    case "fr":
      return qsTr("French")
    case "it":
      return qsTr("Italian")
    case "pt":
      return qsTr("Portuguese")
    case "ru":
      return qsTr("Russian")
    case "zh":
      return qsTr("Chinese")
    default:
      return languageCode
    }
  }

  function processErrorToString(errorCode) {
    switch (errorCode) {
    case Model.QuestRunner.ErrorCode.ProcessFailedToStart:
      return qsTr("The process failed to start.")
    case Model.QuestRunner.ErrorCode.ProcessCrashed:
      return qsTr("The process crashed some time after starting successfully.")
    case Model.QuestRunner.ErrorCode.ProcessWriteError:
      return qsTr("An error occurred when attempting to write to the process.")
    case Model.QuestRunner.ErrorCode.ProcessReadError:
      return qsTr("An error occurred when attempting to read from the process.")
    case Model.QuestRunner.ErrorCode.ProcessTimedOut:
      return qsTr("The last wait-for function on the process timed out.")
    case Model.QuestRunner.ErrorCode.UnknownError:
      return qsTr("Unknown process error.")
    default:
      return ""
    }
  }
}
