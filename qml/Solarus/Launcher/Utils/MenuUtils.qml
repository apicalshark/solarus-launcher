/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  readonly property bool showNativeMenuIcons: Qt.platform.os != "osx" && Qt.platform.os != "ios" && Qt.platform.os != "tvos"

  id: _root

  // Checks the right MenuItem according to currentData.
  // MenuItemGroup must be exclusive, and MenuItems must have a 'data' property.
  function updateCheckedMenuItem(menuItemGroup, currentData, dataOperation) {
    const items = menuItemGroup.items

    // Transform currentData if necessary.
    if (dataOperation && currentData) {
      currentData = dataOperation(currentData)
    }

    // Iterate over menu items.
    for (var i = 0; i < items.length; i++) {
      const item = items[i]

      // Transform item's data if necessary.
      let data = item.data
      if (dataOperation && data) {
        data = dataOperation(data)
      }

      // Check the menu item.
      // We don't need to iterate over all items since the group is mutually exclusive.
      if (data === currentData) {
        item.checked = true
        break
      }
    }
  }
}
