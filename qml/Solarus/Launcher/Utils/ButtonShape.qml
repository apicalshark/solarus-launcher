/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property int radius: Styles.Hints.radius
  property int role: Styles.Colors.Role.Primary
  property bool down: false
  property bool hovered: false
  property bool checkable: false
  property bool checked: false
  property bool shadowEnabled: true
  property bool showBackgroundWhenIdle: true

  id: _root
  implicitHeight: Styles.Hints.controlHeight
  implicitWidth: Styles.Hints.controlDefaultWidth
  layer.enabled: _root.visible && _root.enabled && _root.shadowEnabled
  layer.effect: DropShadow {
    visible: _root.enabled && _root.shadowEnabled
    transparentBorder: true
    horizontalOffset: 0
    verticalOffset: _root.down ? 0 : Styles.Hints.borderWidth
    radius: _root.down ? Styles.Hints.borderWidth : Styles.Hints.borderWidth * 2
    samples: (radius * 2) + 1
    color: (!_root.checkable || _root.checkable && _root.checked) ? Styles.Colors.shadowColor
                                                                  : Styles.Colors.shadowColorLighter
    Behavior on verticalOffset {
      enabled: _root.enabled
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on radius {
      enabled: _root.enabled
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  Behavior on radius {
    NumberAnimation {
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }

  Behavior on anchors.margins {
    NumberAnimation {
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }

  Rectangle {
    id: _fill
    anchors.fill: parent
    radius: _root.radius
    opacity: !showBackgroundWhenIdle && !_root.hovered && !_root.down && !_root.checked ? 0 : 1
    color: Styles.Colors.getButtonBackground(_root.role, _root.hovered, _root.down, _root.enabled)
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.getButtonBorder(_root.role, _root.hovered, _root.down, _root.enabled)

    Behavior on anchors.margins {
      enabled: _root.enabled
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on color {
      enabled: _root.enabled
      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on border.color {
      enabled: _root.enabled
      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on opacity {
      enabled: _root.enabled
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }
}
