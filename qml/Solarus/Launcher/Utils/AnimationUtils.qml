/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

pragma Singleton

QtObject {
  id: _root

  readonly property Component componentNumberAnimation: Component {
    NumberAnimation {
      onRunningChanged: {
        if (!running)
          destroy()
      }
    }
  }

  // Starts an animation that will be destroyed when finished.
  function startNumberAnimation(target, property, from, to, duration, easing) {
    const anim = _root.componentNumberAnimation.createObject(target, {
      "target": target,
      "running": false
    })
    anim.property = property
    anim.from = from
    anim.to = to
    anim.duration = duration ?? 150
    anim.easing.type = easing ?? Easing.InOutQuad
    anim.running = true
  }
}
