/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  enum OS {
    Windows,
    MacOS,
    Linux,
    Unix,
    Unsupported
  }

  readonly property int os: getOS(Qt.platform.os)

  function getOS(qtPlatformOS) {
    if (qtPlatformOS === "osx") {
      return Platform.OS.MacOS
    } else if (qtPlatformOS === "windows") {
      return Platform.OS.Windows
    } else if (qtPlatformOS === "linux") {
      return Platform.OS.Linux
    } else if (qtPlatformOS === "unix") {
      return Platform.OS.Unix
    } else {
      return Platform.OS.Unsupported
    }
  }
}
