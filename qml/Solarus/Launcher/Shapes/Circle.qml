/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

Item {
  property alias color: _rectangle.color
  property alias borderColor: _rectangle.border.color
  property alias borderWidth: _rectangle.border.width

  id: _root

  enabled: false

  Rectangle {
    id: _rectangle
    anchors.centerIn: parent
    width: Math.min(_root.width, _root.height)
    height: width
    color: "white"
    border.color: "black"
    border.width: 0
    radius: width / 2
  }
}
