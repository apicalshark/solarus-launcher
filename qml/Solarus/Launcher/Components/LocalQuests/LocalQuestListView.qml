/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

GridView {
  // Used to center horizontally the view.
  readonly property int horizontalMargin: parent ? (parent.width - Utils.GeometryUtils.getGridViewNecessaryWidth(parent.width, cellWidth)) / 2 : 0
  property Model.LocalQuest selectedQuest: null

  signal topRequested
  signal backRequested
  signal questActionTriggered(Model.LocalQuest quest, int action)

  id: _root
  cellWidth: Styles.Hints.questThumbnailWidth + Styles.Hints.spacing * 2
  cellHeight: Styles.Hints.questThumbnailHeight + Styles.Hints.spacing * 2
  pixelAligned: false
  displayMarginBeginning: cellHeight
  displayMarginEnd: cellHeight
  keyNavigationWraps: false
  keyNavigationEnabled: true
  currentIndex: 0
  highlightFollowsCurrentItem: true
  highlight: null
  clip: false

  Component.onCompleted: {
    const index = currentIndex && currentIndex >= 0 ? currentIndex : 0
    positionViewAtIndex(index, GridView.Beginning)
  }

  onCurrentItemChanged: {
    if (currentItem) {
      if (_root.activeFocus)
        currentItem.forceActiveFocus()
    } else {
      _root.selectedQuest = null
    }
  }

  onActiveFocusChanged: {
    if (currentItem && _root.activeFocus) {
      const index = currentIndex && currentIndex >= 0 ? currentIndex : 0
      positionViewAtIndex(index, GridView.Contain)
    }
  }

  Keys.onPressed: function (event) {
    const columnCount = Math.floor(_root.width / cellWidth)
    switch (event.key) {
    case Qt.Key_Left:
      // First column.
      if (currentIndex % columnCount == 0) {
        event.accepted = true
        _root.backRequested()
      }
      break
    case Qt.Key_Right:
      // List item.
      if (currentIndex === _root.count - 1) {
        Services.SoundManager.limit.stop()
        Services.SoundManager.limit.play()
      }
      break
    case Qt.Key_Up:
      // First row.
      if (currentIndex < columnCount) {
        event.accepted = true
        _root.topRequested()
      }
      break
    case Qt.Key_Down:
      // Last row.
      if (currentIndex >= count - columnCount) {
        Services.SoundManager.limit.stop()
        Services.SoundManager.limit.play()
      }
      break
    default:
      break
    }
  }

  delegate: LocalQuestListItem {
    id: _item
    quest: model.quest ?? null
    selected: _item.GridView.isCurrentItem
    onQuestActionTriggered: function (action) {
      _root.currentIndex = index
      _root.currentItem.forceActiveFocus()
      _root.selectedQuest = model.quest
      _root.questActionTriggered(model.quest, action)
    }

//    onSelectedChanged: {
//      if (selected) {
//        _root.selectedQuest = model.quest
//      }
//    }
  }

  populate: Transition {
    NumberAnimation {
      property: "opacity"
      from: 0
      to: 1
      duration: Styles.Hints.animationDuration
      easing.type: Easing.InCubic
    }
  }
  add: Transition {
    NumberAnimation {
      property: "opacity"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  remove: Transition {
    NumberAnimation {
      property: "opacity"
      to: 0
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  displaced: Transition {
    NumberAnimation {
      properties: "x,y"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  move: displaced
  moveDisplaced: displaced
}
