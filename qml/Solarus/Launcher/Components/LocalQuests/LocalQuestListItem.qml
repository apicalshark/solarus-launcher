/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Components.Quests 1.0 as Quests
import Solarus.Launcher.Components.LocalQuests 1.0 as LocalQuests
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  readonly property bool down: _mouseArea.pressed || _private.spacePressed || _private.enterPressed
  readonly property bool hovered: _mouseArea.containsMouse
  property bool selected: true
  property Model.LocalQuest quest: null
  property bool contextMenuEnabled: true
  signal questActionTriggered(int action)

  id: _root
  implicitHeight: Styles.Hints.questThumbnailHeight
  implicitWidth: Styles.Hints.questThumbnailWidth

  onQuestChanged: _private.updateOverlayState()

  Keys.onPressed: function(event) {
    if (event.isAutoRepeat) return

    if (event.key === Qt.Key_Space)
      _private.spacePressed = true
    else if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter)
      _private.enterPressed = true
  }

  Keys.onReleased: function(event) {
    if (event.isAutoRepeat) return

    if (event.key === Qt.Key_Space) {
      _private.spacePressed = false
      if (_root.quest) {
        _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.ShowInfo)
      }
    } else if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
      _private.enterPressed = false
      if (_root.quest) {
        _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.ShowInfo)
      }
    }
  }

  onHoveredChanged: {
    if (!hovered) {
      _private.spacePressed = false
      _private.enterPressed = false
    }
  }

  //    onActiveFocusChanged: function () {
  //      if (activeFocus) {
  //        Services.SoundManager.focus.play()
  //      }
  //    }

  QtObject {
    property bool spacePressed: false
    property bool enterPressed: false

    id: _private

    function getOverlayState(questState) {
      switch (questState) {
        case Model.LocalQuest.State.Playing: return Quests.QuestThumbnailOverlay.State.Playing
        case Model.LocalQuest.State.Downloading: return Quests.QuestThumbnailOverlay.State.Downloading
        default: return Quests.QuestThumbnailOverlay.State.Default
      }
    }

    function updateOverlayState() {
      if (!_root.quest) { _overlay.downloadProgress = 0 }
      _thumbnail.overlayState = _private.getOverlayState(_root.quest ? _root.quest.state : Model.LocalQuest.State.Idle)
    }
  }

  Connections {
    id: _questConnections
    target: _root.quest
    enabled: _root.quest != null

    function onStateChanged() {
      _private.updateOverlayState()
    }

    function onDownloadProgressChanged(progress) {
      _overlay.downloadProgress = progress
    }
  }

  Quests.QuestThumbnail {
    id: _thumbnail
    anchors.fill: parent
    questTitle: _root.quest ? _root.quest.title : ""
    questThumbnailPath: _root.quest && _root.quest.id ? `image://quest/${ _root.quest.id }/thumbnail` : ""
    pressed: _root.down
    hovered: _root.hovered
    selected: _root.selected
    showFocus: _contextMenu.opened || (_root.selected && _root.activeFocus)
  }

  MouseArea {
    id: _mouseArea
    anchors.fill: parent
    hoverEnabled: true
    cursorShape: Qt.PointingHandCursor
    acceptedButtons: Qt.RightButton | Qt.LeftButton
    onClicked: function(mouse) {
      if (_contextMenu.opened) {
        _contextMenu.close()
      } else if (_root.quest) {
        const button = mouse.button
        if (button === Qt.RightButton) {
          _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.Select)
          if (_root.contextMenuEnabled) {
            _contextMenu.popup(mouse.x, mouse.y)
          }
        } else if (button === Qt.LeftButton) {
           _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.ShowInfo)
        }
      }
    }
  }

  Controls.Tooltip {
    id: _tooltip
    text: _root.quest ? _root.quest.title : ""
    showTooltip: _root.selected && _root.activeFocus
  }

  Controls.Menu {
    id: _contextMenu
    enabled: _root.quest != null

    Controls.MenuItem {
      id: _menuItemPlay
      text: qsTr("Play")
      iconId: Styles.Icons.play_16
      enabled: _root.quest != null && _root.quest.state === Model.LocalQuest.State.Idle
      visible: enabled
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.Play)
    }

    Controls.MenuItem {
      id: _menuItemStop
      text: qsTr("Stop")
      iconId: Styles.Icons.stop_16
      enabled: _root.quest != null && _root.quest.state === Model.LocalQuest.State.Playing
      visible: enabled
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.Stop)
    }

    Controls.MenuSeparator {
      visible: _root.quest && (_menuItemPlay.visible || _menuItemStop.visible)
    }

    Controls.MenuItem {
      id: _menuItemShowInformation
      text: qsTr("Show information")
      iconId: Styles.Icons.info_16
      enabled: _root.quest != null
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.ShowInfo)
    }

    Controls.MenuItem {
      id: _menuItemUpdate
      text: qsTr("Update")
      iconId: Styles.Icons.refresh_16
      enabled: _root.quest != null && _root.quest.state === Model.LocalQuest.State.Idle && _root.quest.hasUpdate
      visible: enabled
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.CheckForUpdates)
    }

    Controls.MenuItem {
      id: _menuItemCheckForUpdates
      text: qsTr("Check For Updates")
      iconId: Styles.Icons.refresh_16
      enabled: _root.quest != null && _root.quest.state === Model.LocalQuest.State.Idle
      visible: _root.quest != null && !_root.quest.hasUpdate
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.CheckForUpdates)
    }

    Controls.MenuSeparator {
      visible: _root.quest && _menuItemRemove.visible
    }

    Controls.MenuItem {
      id: _menuItemRemove
      text: qsTr("Remove")
      iconId: Styles.Icons.close_16
      enabled: _root.quest != null && _root.quest.state !== Model.LocalQuest.State.Idle
      onTriggered: _root.questActionTriggered(LocalQuests.LocalQuestUtils.Action.Remove)
    }
  }
}
