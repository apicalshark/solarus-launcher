/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property alias placeholderText: _textfield.placeholderText
  property bool validPath: false
  property alias directory: _textfield.text
  signal directoryEdited()

  id: _root
  implicitWidth: _layout.implicitWidth
  implicitHeight: _layout.implicitHeight

  QtObject {
    id: _private

    function getValidityIcon(valid) {
       return valid ? Styles.Icons.success_color_16 : Styles.Icons.warning_color_16
    }
  }

  RowLayout {
    id: _layout
    anchors.fill: _root
    spacing: Styles.Hints.spacing

    Controls.TextField {
      id: _textfield
      placeholderText: qsTr("Path to the Solarus quests directory")
      focus: true
      onEditingFinished: _root.directoryEdited()
      iconId: _private.getValidityIcon(_root.validPath)
      iconAutoColor: false
      Layout.fillWidth: true
      Layout.minimumWidth: 300
      KeyNavigation.right: _dialogButton
      KeyNavigation.tab: _dialogButton
      KeyNavigation.down: _root.KeyNavigation.down

      Keys.onPressed: {
        if (event.key === Qt.Key_Right) {
          if (_textfield.cursorPosition === _textfield.text.length) {
            event.accepted = true
            _dialogButton.forceActiveFocus()
          }
        }
      }
    }

    Controls.Button {
      id: _dialogButton
      tooltip: qsTr("Open file dialog")
      iconId: Styles.Icons.folder_16
      onClicked: {
        _dialogButton.forceActiveFocus()
        _folderDialog.open()
      }
      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
      KeyNavigation.left: _textfield
      KeyNavigation.backtab: _textfield
      KeyNavigation.down: _root.KeyNavigation.down

      FolderDialog {
        id: _folderDialog
        currentFolder: _root.directory.length > 0 ? "file:///" + Qt.resolvedUrl(_root.directory) : ""
        onAccepted: {
          let folderString = _folderDialog.folder.toString()
          if (folderString && folderString.startsWith("file:///")) {
            folderString = folderString.substring(8)
          }
          _textfield.text = folderString
          _root.directoryEdited()
        }
      }
    }
  }
}
