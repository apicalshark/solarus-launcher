/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property alias label: _label.text
  property alias description: _description.text
  default property alias content: _loader.sourceComponent
  property int contentMinimumWidth: 0
  property bool below: false
  readonly property Item item: _loader.active ? _loader.item : (_loader2.active ? _loader2.item : null)

  id: _root
  implicitWidth: _layout.implicitWidth + 2 * Styles.Hints.spacing
  implicitHeight: _layout.implicitHeight + 2 * Styles.Hints.spacing
  Layout.fillWidth: true
  Layout.fillHeight: false
  Layout.alignment: Qt.AlignTop

  Rectangle {
    radius: Styles.Hints.radius
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.cardBorderColor
    color: Styles.Colors.cardColor
    anchors.fill: _root
    opacity: enabled ? 1.0 : 0.3
  }

  GridLayout {
    id: _layout
    columnSpacing: Styles.Hints.spacing * 2
    rowSpacing: Styles.Hints.spacing
    flow: GridLayout.LeftToRight
    anchors.margins: Styles.Hints.spacing
    anchors.fill: _root

    ColumnLayout {
      id: _leftLayout
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.row: 0
      Layout.column: 0
      Layout.columnSpan: _root.below ? 2 : 1

      Controls.Text {
        id: _label
        text: "Label"
        Layout.fillWidth: true
        Layout.fillHeight: false
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
      }

      Controls.Text {
        id: _description
        text: "Description"
        role: Controls.Text.Role.Caption
        verticalAlignment: Text.AlignVCenter
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
      }
    }

    Loader {
      id: _loader
      focus: true //!_root.below
      active: !_root.below
      visible: active
      Layout.fillWidth: false
      Layout.fillHeight: false
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      Layout.minimumWidth: _root.contentMinimumWidth
      Layout.column: 1
      Layout.row: 0
    }

    Loader {
      id: _loader2
      sourceComponent: _loader.sourceComponent
      //focus: _root.below
      active: _root.below
      visible: active
      Layout.fillWidth: true
      Layout.fillHeight: false
      Layout.alignment: Qt.AlignCenter
      Layout.column: 0
      Layout.row: 1
      Layout.columnSpan: 2
    }
  }
}
