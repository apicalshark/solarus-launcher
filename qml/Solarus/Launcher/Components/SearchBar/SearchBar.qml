/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property Item blurSource: parent
  property bool blurEnabled: true
  property bool backgroundVisible: false
  property alias searchText: _searchField.text
  property alias optionModel: _searchOptions.model
  property alias currentOptionButton: _searchOptions.currentIndex
  property alias refreshButtonVisible: _refreshButton.visible
  signal searchTextEdited
  signal optionButtonClicked(int index)
  signal backRequested
  signal downRequested
  signal refreshClicked

  id: _root
  height: Styles.Hints.searchBarHeight

  Keys.onEscapePressed: function () {
    _root.backRequested()
  }
  Keys.onDownPressed: function () {
    _root.downRequested()
  }
  Keys.onLeftPressed: _root.backRequested()
  Keys.onRightPressed: function (event) {
    event.accepted = true
  }
  KeyNavigation.right: null

  Rectangle {
    id: _rectOpaqueBackground
    color: Styles.Colors.windowColor
    anchors.fill: parent
    opacity: _root.backgroundVisible ? 1 : 0

    Behavior on opacity {
      NumberAnimation {
        easing.type: Easing.OutCubic
        duration: Styles.Hints.animationDuration
      }
    }
  }

  ShaderEffectSource {
    id: _effectSource
    sourceItem: _root.blurSource
    anchors.fill: parent
    sourceRect: sourceItem ? Qt.rect(sourceItem.x, sourceItem.y, sourceItem.width, _root.height)
                           : Qt.rect(0, 0, 0, 0)
    recursive: false
    live: true
    visible: _root.blurEnabled
  }

  FastBlur {
    id: _blur
    anchors.fill: parent
    source: _effectSource
    radius: Styles.Hints.blurRadius
    transparentBorder: false
    visible: _root.blurEnabled
    opacity: _root.backgroundVisible ? 1 : 0

    Behavior on opacity {
      NumberAnimation {
        easing.type: Easing.OutCubic
        duration: Styles.Hints.animationDuration
      }
    }
  }

  Rectangle {
    id: _rectBackground
    anchors.fill: parent
    color: _root.backgroundVisible ? Styles.Colors.overlayColor
                                   : Styles.Colors.colorWithAlpha(Styles.Colors.overlayColor, 0)

    MouseArea {
      id: _mouseArea
      anchors.fill: parent
    }

    Behavior on color {
      ColorAnimation {
        easing.type: Easing.OutCubic
        duration: Styles.Hints.animationDuration
      }
    }
  }

  RowLayout {
    id: _layout
    anchors.fill: parent
    anchors.leftMargin: Styles.Hints.spacing
    anchors.rightMargin: Styles.Hints.spacing
    spacing: 0

    Controls.LayoutSpacer { id: _itemDummyLeft }

    Controls.TextField {
      id: _searchField
      placeholderText: qsTr("Search")
      Layout.fillWidth: true
      Layout.maximumWidth: 600
      Layout.alignment: Qt.AlignHCenter
      Layout.leftMargin: -2 // Compensate
      Layout.rightMargin: Styles.Hints.spacing
      focus: true
      iconId: Styles.Icons.search_16
      onTextEdited: _root.searchTextEdited()
      Keys.onRightPressed: _searchField.nextItemInFocusChain().forceActiveFocus()
    }

    Controls.ButtonSet {
      id: _searchOptions
      onLeftRequested: _searchField.forceActiveFocus()
      onRightRequested: if (_refreshButton.visible) _refreshButton.forceActiveFocus()
      onButtonClicked: _root.optionButtonClicked(index)
      onDownRequested: _root.downRequested()
      Layout.rightMargin: _refreshButton.visible ? Styles.Hints.spacing : - 2
      Keys.onTabPressed: function(event) {
        if (!_refreshButton.visible) {
          event.accepted = true
          _root.downRequested()
        }
      }
      KeyNavigation.right: _refreshButton.visible ? _refreshButton : null
    }

    Controls.Button {
      id: _refreshButton
      iconId: Styles.Icons.refresh_16
      tooltip: qsTr("Refresh")
      onClicked: _root.refreshClicked()
      Layout.rightMargin: -2 // Compensate
      Keys.onTabPressed: _root.downRequested()
    }

    Controls.LayoutSpacer { id: _itemDummyRight }
  }
}
