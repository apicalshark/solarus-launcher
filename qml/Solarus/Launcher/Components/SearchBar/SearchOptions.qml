/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  property alias model: _repeater.model
  property int currentIndex: undefined
  signal leftRequested
  signal downRequested
  signal buttonClicked(int index)

  id: _root
  implicitWidth: _layout.implicitWidth
  implicitHeight: _layout.implicitHeight
  height: implicitHeight

  QtObject {
    readonly property int currentSortMode: $localQuestsPageController.questListProxyModel.questSortMode
    readonly property int currentSortOrder: $localQuestsPageController.questListProxyModel.questSortOrder

    id: _private

    function invertQuestSortOrder() {
      const model = $localQuestsPageController.questListProxyModel
      if (model.questSortOrder === Model.LocalQuestProxyModel.Ascending) {
        model.questSortOrder = Model.LocalQuestProxyModel.Descending
      } else {
        model.questSortOrder = Model.LocalQuestProxyModel.Ascending
      }
    }

    function getIcon(itemIconArray, sortOrder) {
      const ascending = sortOrder === Model.LocalQuestProxyModel.Ascending
      return ascending ? itemIconArray.get(0) : itemIconArray.get(1)
    }
  }

  Rectangle {
    id: _background
    anchors.fill: parent
    color: Styles.Colors.secondaryColorDarker
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.secondaryColorPressed
    radius: Styles.Hints.radius
  }

  ButtonGroup {
    id: _buttonGroup
    exclusive: true
  }

  RowLayout {
    id: _layout
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    spacing: 3

    Repeater {
      id: _repeater
      model: ListModel {
        id: _listModel
      }

      delegate: Controls.Button {
        id: _checkableButton
        tooltip: model.tooltip
        checkable: true
        shadowEnabled: checked
        showBackgroundWhenIdle: false
        role: checked ? Styles.Colors.Role.Primary : Styles.Colors.Role.Secondary
        checked: model.index === _root.currentIndex
        iconId: model.icon
        Component.onCompleted: _buttonGroup.buttons.push(this)
        onClicked: _root.buttonClicked(model.index)

//        onClicked: {
//          const questModel = $localQuestsPageController.questListProxyModel
//          const alreadyChecked = model.sortMode === _private.currentSortMode
//          questModel.questSortMode = model.sortMode
//          if (alreadyChecked) {
//            _private.invertQuestSortOrder()
//          }

//          // Keep focus within button group if already there.
//          if (_root.activeFocus) {
//            forceActiveFocus()
//          }
//        }

        Keys.onRightPressed: focusNextItem(index, _repeater.count, true)
        Keys.onTabPressed: focusNextItem(index, _repeater.count, true, true)
        Keys.onLeftPressed: focusNextItem(index, _repeater.count, false)
        Keys.onBacktabPressed: focusNextItem(index, _repeater.count, false)

        function focusNextItem(index, count, forward, tab) {
          const next = nextItemInFocusChain(forward)
          const isLast = forward && index >= count - 1

          if (tab && forward && isLast) {
            _root.downRequested()
            return
          }

          if (next && !isLast) {
            next.forceActiveFocus()
          }
        }

        Connections {
          target: _root

          function onCurrentIndexChanged() {
            _checkableButton.checked = model.index === _root.currentIndex
          }
        }
      }

      Component.onCompleted: {
        _listModel.append({
                            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.Title,
                            "tooltip": qsTr("Sort by title"),
                            "icon": [ Styles.Icons.sortAlphabetically_16, Styles.Icons.sortAlphabeticallyInverted_16 ],
                          })
        _listModel.append({
                            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.ReleaseDate,
                            "tooltip": qsTr("Sort by release date"),
                            "icon": [ Styles.Icons.sortDate_16, Styles.Icons.sortDateInverted_16 ],
                          })
        _listModel.append({
                            "sortMode": Model.LocalQuestProxyModel.QuestSortMode.LastPlayedTime,
                            "tooltip": qsTr("Sort by last played time"),
                            "icon": [ Styles.Icons.sortTime_16, Styles.Icons.sortTimeInverted_16 ],
                          })
      }
    }
  }
}
