/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Components.Updates 1.0 as Updates
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Controllers 1.0 as Controllers
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Utils 1.0 as Utils

Controls.Dialog {
  id: _root
  bottomPadding: 0
  topPadding: 0
  leftPadding: 0
  rightPadding: 0
  implicitWidth: Styles.Hints.questDialogWidth
  standardButtons: QtControls.Dialog.NoButton
  footer: null

  QtObject {
    property int maxHeight: Styles.Hints.windowDefaultHeight / 3
    property int currentState: Controllers.UpdateDialogController.State.None
    property bool routingEnabled: true

    id: _private

    function getComponent(state) {
      switch (state) {
      case Controllers.UpdateDialogController.State.Checking: return _componentCheckingPage
      case Controllers.UpdateDialogController.State.CheckingFail: return _componentCheckingFailPage
      case Controllers.UpdateDialogController.State.CheckingSuccess: return _componentCheckingSuccessPage
      case Controllers.UpdateDialogController.State.CheckingUpToDate: return _componentCheckingUpToDatePage
      case Controllers.UpdateDialogController.State.Downloading: return _componentDownloadingPage
      case Controllers.UpdateDialogController.State.DownloadingFail: return _componentDownloadingFailPage
      case Controllers.UpdateDialogController.State.DownloadingSuccess: return _componentDownloadingSuccessPage
      case Controllers.UpdateDialogController.State.Installing: return _componentInstallingPage
      case Controllers.UpdateDialogController.State.InstallingFail: return _componentInstallingFailPage
      case Controllers.UpdateDialogController.State.InstallingSuccess: return _componentInstallingSuccessPage
      default: return _componentStartPage
      }
    }

    function route(state) {
      if (state !== currentState && routingEnabled) {
        currentState = state
        const comp = getComponent(currentState)
        _stackView.replace(comp)
      }
    }

    function updateMaxHeight(item) {
      _private.maxHeight = Math.max(_private.maxHeight, item.implicitHeight)
    }

    function cancelAndCloseDialog() {
      _root.close()
      $updateDialogController.cancel()
    }
  }

  Connections {
    id: _connections
    target: $updateDialogController

    function onStateChanged() {
      if (_root.visible) {
        _private.route($updateDialogController.state)
      } else if ($updateDialogController.state >= Controllers.UpdateDialogController.State.CheckingSuccess) {
        _private.route($updateDialogController.state)
        _root.open()
      }
    }

    function onManualCheckingRequested() {
      _root.open()
    }

    function onCloseDialogRequested() {
      _root.close()
    }
  }

  onDiscarded: $updateDialogController.cancel()
  onRejected: $updateDialogController.cancel()

  onAboutToShow: {
    _private.routingEnabled = true
    _private.route($updateDialogController.state)
  }
  onAboutToHide: _private.routingEnabled = false

  contentItem: QtControls.StackView {
    id: _stackView
    clip: true
    initialItem: _componentStartPage
    implicitHeight: _private.maxHeight

    Component {
      id: _componentStartPage
      Updates.StartPage {
        id: _startPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onCancelClicked: _private.cancelAndCloseDialog()
        onCheckForUpdatesClicked: $updateDialogController.checkForUpdates()
      }
    }
    Component {
      id: _componentCheckingPage
      Updates.CheckingPage {
        id: _checkingPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onCancelClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentCheckingFailPage
      Updates.CheckingFailPage {
        id: _checkingFailPage
        onOkClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentCheckingSuccessPage
      Updates.CheckingSuccessPage {
        id: _checkingSuccessPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        currentVersion: $updateDialogController.currentVersion
        currentVersionDate: $updateDialogController.currentVersionDate
        latestVersion: $updateDialogController.latestVersion
        latestVersionDate: $updateDialogController.latestVersionDate
        onCancelClicked: _private.cancelAndCloseDialog()
        onDownloadClicked: $updateDialogController.downloadUpdate()
      }
    }
    Component {
      id: _componentCheckingUpToDatePage
      Updates.CheckingUpToDatePage {
        id: _checkingUpToDatePage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onOkClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentDownloadingPage
      Updates.DownloadingPage {
        id: _downloadingPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onCancelClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentDownloadingFailPage
      Updates.DownloadingFailPage {
        id: _downloadingFailPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onOkClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentDownloadingSuccessPage
      Updates.DownloadingSuccessPage {
        id: _downloadingPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onInstallClicked: $updateDialogController.installUpdate()
        onCancelClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentInstallingPage
      Updates.InstallingPage {
        id: _installingPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onCancelClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentInstallingFailPage
      Updates.InstallingFailPage {
        id: _installingFailPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onOkClicked: _private.cancelAndCloseDialog()
      }
    }
    Component {
      id: _componentInstallingSuccessPage
      Updates.InstallingSuccessPage {
        id: _installingSuccessPage
        QtControls.StackView.onActivating: _private.updateMaxHeight(this)
        onOkClicked: _private.cancelAndCloseDialog()
      }
    }
  }
}
