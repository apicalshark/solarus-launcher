/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  signal okClicked

  id: _root
  implicitHeight: _layout.implicitHeight + Styles.Hints.spacing * 2
  implicitWidth: _layout.implicitWidth + Styles.Hints.spacing * 2

  ColumnLayout {
   id: _layout
   spacing: Styles.Hints.spacing
   anchors.fill: _root
   anchors.margins: Styles.Hints.spacing * 2

   Controls.SvgImage {
     id: _icon
     source: Styles.Icons.success_color_16
     Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
     height: Styles.Hints.iconSize * 3
     width: height
   }

   Controls.Text {
     id: _title
     role: Controls.Text.Role.H1
     text: qsTr("Installation successful")
     horizontalAlignment: Text.AlignHCenter
     Layout.fillWidth: true
     Layout.alignment: Qt.AlignTop
    }

    Controls.Text {
      id: _text
      role: Controls.Text.Role.Body
      text: qsTr("You're now running the latest version available.")
      horizontalAlignment: Text.AlignHCenter
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignTop
    }

    Controls.LayoutSpacer { }

    RowLayout {
      id: _layoutButtons
      spacing: Styles.Hints.spacing
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom

      Controls.Button {
        id: _okButton
        focus: true
        text: qsTr("OK")
        role: Styles.Colors.Role.Primary
        Layout.alignment: Qt.AlignHCenter
        onClicked: _root.okClicked()
      }
    }
  }

  Component.onCompleted: Qt.callLater(()=> _okButton.forceActiveFocus())
}
