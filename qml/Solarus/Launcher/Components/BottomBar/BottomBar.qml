/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtGraphicalEffects 1.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Components.BottomBar 1.0 as BottomBar

Item {
  property Item blurSource: parent
  property bool blurEnabled: true

  id: _root
  anchors.bottom: parent.bottom
  anchors.bottomMargin: $bottomBarController.visible ? 0 : -Styles.Hints.bottomBarHeight
  anchors.left: parent.left
  anchors.right: parent.right
  height: Styles.Hints.bottomBarHeight

  Behavior on anchors.bottomMargin {
    NumberAnimation {
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }

  ShaderEffectSource {
    id: _effectSource
    enabled: _root.blurEnabled
    sourceItem: _root.blurSource
    anchors.fill: parent
    sourceRect: Qt.rect(_root.blurSource.x,
                        _root.blurSource.height - _root.height,
                        _root.blurSource.width, _root.height)
    recursive: false
    live: true
    visible: _root.blurEnabled
  }

  FastBlur {
    id: _blur
    anchors.fill: _effectSource
    source: _effectSource
    radius: Styles.Hints.blurRadius
    transparentBorder: false
    enabled: _root.blurEnabled
    visible: _root.blurEnabled
  }

  Rectangle {
    id: rect
    anchors.fill: parent
    color: Styles.Colors.bottomBarColor
  }

  MouseArea {
    id: _mouseArea
    anchors.fill: parent
    hoverEnabled: true
  }

  Component {
    id: _componentGamepadConnected

    Item {
      implicitHeight: _gamepadConnectedLayout.implicitHeight
      implicitWidth: _gamepadConnectedLayout.implicitWidth

      RowLayout {
        id: _gamepadConnectedLayout
        spacing: Styles.Hints.spacing / 2
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

        Controls.SvgIcon {
          iconId: Styles.Icons.gamepad_16
          width: Styles.Hints.iconSize * 2
          height: width
          color: Styles.Colors.textColorBody

          SequentialAnimation on color {
            loops: Animation.Infinite
            running: true

            ColorAnimation {
              to: Styles.Colors.textColorError
              duration: Styles.Hints.animationDuration * 2
              easing.type: Easing.InExpo
            }

            ColorAnimation {
              to: Styles.Colors.textColorBody
              duration: Styles.Hints.animationDuration * 4
              easing.type: Easing.InExpo
            }
          }
        }

        Controls.Text {
          text: qsTr("Gamepad not detected.")
          Layout.fillWidth: true
        }
      }
    }
  }

  RowLayout {
    id: _layout
    spacing: 0
    anchors.fill: parent
    anchors.topMargin: 0
    anchors.bottomMargin: 0
    anchors.leftMargin: Styles.Hints.spacing
    anchors.rightMargin: Styles.Hints.spacing

    BottomBar.GamepadStatus {
      id: _gamepadStatus
    }

    // Layout for buttons list.
    ListView {
      id: _listViewButtons
      orientation: ListView.Horizontal
      spacing: Styles.Hints.spacing
      model: $bottomBarController.gamepadManager.gamepad ? $bottomBarController.gamepadManager.gamepad.actions : null
      flickableDirection: Flickable.AutoFlickIfNeeded
      boundsMovement: Flickable.StopAtBounds
      boundsBehavior: Flickable.StopAtBounds
      interactive: false // Prevent scrolling
      Layout.preferredWidth: _listViewButtons.contentWidth
      Layout.preferredHeight: Styles.Hints.controlHeight
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      delegate: BottomBarButton {
        gamepadButton: model.button
        text: model.text
      }
    }
  }
}
