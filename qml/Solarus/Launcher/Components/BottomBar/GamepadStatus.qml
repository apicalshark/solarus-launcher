/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles

Loader {
  id: _root

  Connections {
    target: $bottomBarController.gamepadManager

    function onConnectedChanged() {
      _timer.stop()
      _root.updateComponent()
    }
  }

  Component.onCompleted: {
    _root.updateComponent()
  }

  function updateComponent() {
    const connected = $bottomBarController.gamepadManager.connected
    _root.sourceComponent = connected ? _componentConnected : _componentUnconnected
    _root.item.opacity = 1
    if (connected) {
      // Start timer to hide the component after a delay.
      _timer.start()
    }
  }

  Timer {
    id: _timer
    interval: 5000
    repeat: false
    onTriggered: _root.item.opacity = 0
  }

  Component {
    id: _componentUnconnected

    Item {
      id: _itemUnconnected
      opacity: 0
      implicitHeight: _unconnectedLayout.implicitHeight
      implicitWidth: _unconnectedLayout.implicitWidth

      Behavior on opacity {
        NumberAnimation {
          duration: Styles.Hints.animationDuration * 2
          easing.type: Easing.OutCubic
          onFinished: {
            if (_itemUnconnected.opacity == 0) {
              _root.sourceComponent = null
            }
          }
        }
      }

      RowLayout {
        id: _unconnectedLayout
        spacing: Styles.Hints.spacing / 2
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

        Controls.SvgIcon {
          iconId: Styles.Icons.gamepad_16
          width: Styles.Hints.iconSize * 2
          height: width
          color: Styles.Colors.textColorBody

          SequentialAnimation on color {
            loops: Animation.Infinite
            running: true

            ColorAnimation {
              to: Styles.Colors.textColorError
              duration: Styles.Hints.animationDuration * 2
              easing.type: Easing.InExpo
            }

            ColorAnimation {
              to: Styles.Colors.textColorBody
              duration: Styles.Hints.animationDuration * 4
              easing.type: Easing.InExpo
            }
          }
        }

        Controls.Text {
          text: qsTr("Gamepad connection lost.")
          Layout.fillWidth: true
        }
      }
    }
  }

  Component {
    id: _componentConnected

    Item {
      opacity: 0
      implicitHeight: _connectedLayout.implicitHeight
      implicitWidth: _connectedLayout.implicitWidth

      Behavior on opacity {
        NumberAnimation {
          duration: Styles.Hints.animationDuration * 2
          easing.type: Easing.OutCubic
          onFinished: {
            if (_itemUnconnected.opacity == 0) {
              _root.sourceComponent = null
            }
          }
        }
      }

      RowLayout {
        id: _connectedLayout
        spacing: Styles.Hints.spacing / 2
        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

        Controls.SvgIcon {
          iconId: Styles.Icons.gamepad_16
          width: Styles.Hints.iconSize * 2
          height: width
          color: Styles.Colors.textColorSuccess
        }

        Controls.Text {
          text: qsTr("Gamepad connected!")
          Layout.fillWidth: true
        }
      }
    }
  }
}
