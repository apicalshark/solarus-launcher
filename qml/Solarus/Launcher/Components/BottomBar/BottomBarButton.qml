/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Model 1.0 as Model

QtControls.Button {
  property int gamepadButton: Model.GamepadAction.Button.None

  id: _root
  text: ""
  font: Styles.Fonts.primaryFont
  height: implicitHeight
  implicitHeight: Styles.Hints.controlHeight
  implicitWidth: _root.text ? _contentLayout.implicitWidth + leftPadding + rightPadding
                            : Math.round(Styles.Hints.controlHeight * 1.25)
  bottomPadding: 0
  topPadding: 0
  leftPadding: Styles.Hints.spacing
  rightPadding: Styles.Hints.spacing
  spacing: Styles.Hints.spacing / 2
  hoverEnabled: false
  focusPolicy: Qt.NoFocus
  autoRepeat: false

  QtObject {
    id: _private;

    function getIconId(gamepadButton) {
      switch (gamepadButton) {
      case Model.GamepadAction.Button.A:
        return Styles.Icons.gamepadButtonBottom_16
      case Model.GamepadAction.Button.B:
        return Styles.Icons.gamepadButtonRight_16
      case Model.GamepadAction.Button.X:
        return Styles.Icons.gamepadButtonLeft_16
      case Model.GamepadAction.Button.Y:
        return Styles.Icons.gamepadButtonTop_16
      case Model.GamepadAction.Button.R1:
        return Styles.Icons.gamepadButtonShoulderRight_16
      case Model.GamepadAction.Button.L1:
        return Styles.Icons.gamepadButtonShoulderLeft_16
      case Model.GamepadAction.Button.Left:
      case Model.GamepadAction.Button.Top:
      case Model.GamepadAction.Button.Right:
      case Model.GamepadAction.Button.Bottom:
        return Styles.Icons.gamepadButtonDPad_16
      case Model.GamepadAction.Button.Start:
        return Styles.Icons.gamepadButtonDPad_16 // TODO
      default:
        return ""
      }
    }
  }

  background: Rectangle {
    id: _background
    anchors.fill: _root
    color: Styles.Colors.bottomBarItemColor
    border.width: 0
    radius: Styles.Hints.radius
  }

  contentItem: Item {
    id: _contentItem
    anchors.fill: _root
    anchors.leftMargin: _root.text ? _root.leftPadding : 0
    anchors.rightMargin: _root.text ? _root.rightPadding : 0
    anchors.topMargin: _root.topPadding
    anchors.bottomMargin: _root.bottomPadding

    RowLayout {
      id: _contentLayout
      anchors.centerIn: parent
      spacing: _root.spacing

      Controls.SvgIcon {
        id: _icon
        iconId: _private.getIconId(_root.gamepadButton)
        color: Styles.Colors.bottomBarItemForegroundColor
        width: Styles.Hints.iconSize
        height: Styles.Hints.iconSize
        Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
        visible: status != Image.Null
      }

      Text {
        id: _text
        color: Styles.Colors.bottomBarItemForegroundColor
        elide: Text.ElideRight
        font: _root.font
        horizontalAlignment: Text.AlignHCenter
        renderType: Styles.Fonts.getRenderType(font.pixelSize)
        text: _root.text
        verticalAlignment: Text.AlignVCenter
        visible: _root.text.length > 0
        Layout.alignment: Qt.AlignCenter
        Layout.fillWidth: true
        Layout.minimumWidth: Styles.Hints.controlHeight * 1.25
      }
    }
  }
}
