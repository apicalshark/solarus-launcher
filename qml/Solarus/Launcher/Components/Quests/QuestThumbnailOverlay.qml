/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  enum State {
    Default,
    Downloaded, // Displayed only in the online store.
    Downloading,
    Playing
  }

  property int state: QuestThumbnailOverlay.State.Default
  property string title: ""
  property int downloadProgress: 0

  id: _root
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: Styles.Hints.controlDefaultWidth
  onStateChanged: _private.updateCurrentItem()

  QtObject {
    id: _private

    function getComponent(state) {
      switch (state) {
        case QuestThumbnailOverlay.State.Downloading: return _componentDownloading
        case QuestThumbnailOverlay.State.Downloaded: return _componentDownloaded
        case QuestThumbnailOverlay.State.Playing: return _componentPlaying
        default: return _componentDefault
      }
    }

    function updateCurrentItem() {
      const nextItem = getComponent(_root.state)
      if (nextItem) {
        _stackView.replace(nextItem)
      } else {
        _stackView.clear(QtControls.StackView.ReplaceTransition)
      }
    }
  }

  QtControls.StackView {
    id: _stackView
    anchors.fill: parent
    anchors.margins: Styles.Hints.spacing
    clip: true
    initialItem: _componentDefault
    pushEnter: null
    pushExit: null
    popEnter: null
    popExit: null

    replaceEnter: Transition {
      NumberAnimation {
        property: "opacity"
        from: 0.0
        to: 1.0
        duration: Styles.Hints.animationDuration * 4
        easing.type: Easing.InOutQuart
      }
    }

    replaceExit: Transition {
      NumberAnimation {
        property: "opacity"
        from: 1.0
        to: 0.0
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.InOutQuart
      }
    }

    Component {
      id: _componentDefault

      Item {
        visible: false
        enabled: false
      }
    }

    Component {
      id: _componentDownloading

      ColumnLayout {
        spacing: Styles.Hints.spacing / 2

        Controls.LayoutSpacer {}

        Controls.Text {
          role: Controls.Text.Role.UppercaseLabel
          text: qsTr("Downloading")
          horizontalAlignment: Text.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        Controls.ProgressBar {
          alternateBackground: true
          from: 0
          to: 100
          value: _root.downloadProgress
          Layout.fillWidth: true
          Layout.leftMargin: Styles.Hints.spacing
          Layout.rightMargin: Styles.Hints.spacing
        }

        Controls.Text {
          role: Controls.Text.Role.Body
          text: `${_root.downloadProgress}%`
          horizontalAlignment: Text.AlignHCenter
          Layout.alignment: Qt.AlignHCenter
        }

        Controls.LayoutSpacer {}
      }
    }

    Component {
      id: _componentDownloaded

      ColumnLayout {
        spacing: Styles.Hints.spacing

        Controls.LayoutSpacer {}

        Controls.SvgIcon {
          iconId: Styles.Icons.check_16
          height: Styles.Hints.spacing * 4
          width: height
          Layout.alignment: Qt.AlignHCenter
        }

        Controls.Text {
          role: Controls.Text.Role.UppercaseLabel
          text: qsTr("Downloaded")
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Text.AlignHCenter
        }

        Controls.LayoutSpacer {}
      }
    }

    Component {
      id: _componentPlaying

      ColumnLayout {
        spacing: Styles.Hints.spacing

        Controls.LayoutSpacer {}

        QuestPlayingIcon {
          Layout.alignment: Qt.AlignHCenter
        }

        Controls.Text {
          role: Controls.Text.Role.UppercaseLabel
          text: qsTr("Playing")
          Layout.alignment: Qt.AlignHCenter
          horizontalAlignment: Text.AlignHCenter
          wrapMode: Text.NoWrap
          elide: Text.ElideRight
        }

        Controls.Text {
          text: _root.title
          Layout.alignment: Qt.AlignHCenter
          Layout.fillWidth: true
          horizontalAlignment: Text.AlignHCenter
          role: Controls.Text.Role.Body
          wrapMode: Text.NoWrap
          elide: Text.ElideRight
        }

        Controls.LayoutSpacer {}
      }
    }
  }
}
