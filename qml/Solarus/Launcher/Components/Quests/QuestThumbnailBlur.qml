/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

Item {
  property Item blurSource: null
  property bool blurEnabled: true
  property real blurValue: 1.0
  property real radius: 0.0

  id: _root
  implicitWidth: 100
  implicitHeight: 100

  // Needed to get radius on angles.
  layer.enabled: true
  layer.effect: OpacityMask {
    maskSource: Rectangle {
      width: _root.width
      height: _root.height
      radius: _root.radius
      antialiasing: true
    }
  }

  ShaderEffectSource {
    id: _effectSource
    visible: _root.blurSource != null && _root.blurEnabled
    anchors.fill: _root
    recursive: false
    live: true
    sourceItem: _root.blurSource
    sourceRect: Qt.rect(_root.x, _root.y, _root.width, _root.height)
  }

  FastBlur {
    id: _blur
    visible: _root.blurSource != null && _root.blurEnabled
    anchors.fill: _root
    source: _effectSource
    transparentBorder: false
    radius: (Styles.Hints.blurRadius / 2) * _root.blurValue
  }

  Rectangle {
    id: _colorRect
    anchors.fill: _root
    antialiasing: true
    radius: _root.radius
    color: Styles.Colors.imageOverlayColor
  }
}
