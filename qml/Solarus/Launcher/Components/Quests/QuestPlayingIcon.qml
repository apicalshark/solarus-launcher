/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Shapes 1.0 as Shapes

Item {
  property bool animate: true
  property int iconSize: Styles.Hints.controlHeight

  id: _root
  implicitWidth: Styles.Hints.controlHeight * 1.5
  implicitHeight: implicitWidth
  enabled: false

  Shapes.Circle {
    id: _background
    anchors.fill: _root
    anchors.margins: 0
    color: Styles.Colors.primaryColorLighter
    borderWidth: 0
    transformOrigin: Item.Center
    //layer.enabled: true
    antialiasing: true
    smooth: true

    SequentialAnimation on anchors.margins {
      loops: Animation.Infinite
      running: _root.animate

      NumberAnimation {
        to: -Styles.Hints.pressDelta
        duration: Styles.Hints.animationDuration * 5
        easing.type: Easing.OutCubic
      }
      NumberAnimation {
        to: Styles.Hints.pressDelta
        duration: Styles.Hints.animationDuration * 7
        easing.type: Easing.InCubic
      }
    }

    SequentialAnimation on color {
      loops: Animation.Infinite
      running: _root.animate

      ColorAnimation {
        to: Styles.Colors.primaryColor
        duration: Styles.Hints.animationDuration * 5
      }
      ColorAnimation {
        to: Styles.Colors.primaryColorLighter
        duration: Styles.Hints.animationDuration * 7
      }
    }
  }

  Controls.SvgIcon {
    color: Styles.Colors.foregroundColor
    iconId: Styles.Icons.stop_24
    anchors.centerIn: parent
    width: _root.iconSize
    height: width
  }
}
