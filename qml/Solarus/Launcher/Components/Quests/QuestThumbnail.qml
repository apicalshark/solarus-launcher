/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property alias questTitle: _overlay.title
  property alias questThumbnailPath: _image.source
  property alias overlayState: _overlay.state
  property alias downloadProgress: _overlay.downloadProgress
  property real radius: Styles.Hints.radius
  property bool selected: false
  property bool pressed: false
  property bool hovered: false
  property bool showFocus: false
  property bool showShadow: true

  id: _root
  implicitHeight: Styles.Hints.questThumbnailHeight
  implicitWidth: Styles.Hints.questThumbnailWidth
  height: (implicitHeight / implicitWidth) * width

  QtObject {
    readonly property real pressDelta: _root.pressed ? -2 * Styles.Hints.borderWidth : 0
    property bool shadowEnabled: false

    id: _private

    function getBackgroundColor(isHovered, isPressed, isEnabled) {
      if (!isEnabled) return Styles.Colors.colorWithAlpha(Styles.Colors.secondaryColorPressed, 0)
      else if (isPressed) return Styles.Colors.secondaryColorLightest
      else if (isHovered) return Styles.Colors.secondaryColorLighter
      else return Styles.Colors.secondaryColorPressed
    }

    function getForegroundColor(isHovered, isPressed, isEnabled) {
      if (!isEnabled) return Styles.Colors.colorWithAlpha(Styles.Colors.tertiaryColorHover, 0)
      else if (isPressed) return Styles.Colors.tertiaryColorLighter
      else if (isHovered) return Styles.Colors.tertiaryColorPressed
      else return Styles.Colors.tertiaryColorHover
    }
  }

  // Defer activation of the shadow because there is a bug in QtQuick
  // that makes the Item looks larger than its normal size.
  Component.onCompleted: {
    _timer.start()
  }

  Timer {
    id: _timer
    repeat: false
    interval: 1
    onTriggered: {
      _private.shadowEnabled = true
    }
  }

  Rectangle {
    id: _background
    anchors.fill: _root
    anchors.margins: _private.pressDelta
    color: _private.getBackgroundColor(_root.hovered, _root.pressed, _root.enabled)
    radius: _root.pressed ? Styles.Hints.radius + Styles.Hints.borderWidth : Styles.Hints.radius

    Behavior on anchors.margins {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on radius {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    layer.enabled: _root.visible && _root.enabled && _private.shadowEnabled && _root.showShadow
    layer.effect: DropShadow {
      visible: _root.enabled & _root.showShadow
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: _root.pressed ? 0 : Styles.Hints.borderWidth * 2
      radius: _root.pressed ? Styles.Hints.borderWidth : Styles.Hints.borderWidth * 8
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColor

      Behavior on verticalOffset {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
      Behavior on radius {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    Image {
      id: _image
      anchors.fill: _background
      anchors.centerIn: _background
      fillMode: Image.PreserveAspectCrop
      verticalAlignment: Image.AlignVCenter
      horizontalAlignment: Image.AlignHCenter
      asynchronous: true
      mipmap: true
      antialiasing: true
      smooth: true
      opacity: enabled ? 1.0 : 0.5

      // Needed to get radius on angles.
      layer.enabled: _image.visible && _image.enabled
      layer.effect: OpacityMask {
        maskSource: Rectangle {
          width: _image.width
          height: _image.height
          radius: _background.radius
        }
      }
    }
  }

  Loader {
    id: _loadingIndicator
    anchors.centerIn: _background
    active: _image.status === Image.Loading
    sourceComponent: Component {
      Controls.BusyIndicator { }
    }
  }

  Controls.Text {
    id: _imageFallback
    visible:  _image.status === Image.Error
    anchors.fill: _background
    text: _root.questTitle
    verticalAlignment: Qt.AlignCenter
    horizontalAlignment: Qt.AlignCenter
    wrapMode: Text.WordWrap
  }

  QuestThumbnailBlur {
    id: _blur
    anchors.fill: parent
    anchors.margins: 0
    radius: _background.radius
    blurSource: _background
    opacity: _overlay.opacity
    visible: opacity > 0
    blurValue: opacity

    Behavior on blurValue {
      NumberAnimation {
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.OutCubic
      }
    }
  }

  QuestThumbnailOverlay {
    id: _overlay
    anchors.fill: parent
    visible: opacity > 0
    opacity: state === QuestThumbnailOverlay.State.Default ? 0.0 : 1.0

    Behavior on opacity {
      NumberAnimation {
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.OutCubic
      }
    }
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: _background
    animateBorder: !_root.pressed
    showFocus: _root.showFocus
    borderWidth: 2 * Styles.Hints.focusBorderWidth
    radius: _background.radius + borderWidth
  }
}
