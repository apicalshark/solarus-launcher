/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property int contentMaxWidth: Styles.Hints.contentMaxWidth
  property alias imageSource: _image.source
  property alias textTitle: _titleText.text
  property alias textContent: _contentText.text
  property alias buttonText: _button.text
  property alias buttonIconId: _button.iconId
  property bool showBusyIndicator: false
  signal buttonClicked

  id: _root

  Controls.ScrollView {
    id: _scrollview
    contentWidth: _content.width
    contentHeight: _content.height
    verticalScrollBarVMargin: Math.round(Styles.Hints.spacing / 2)
    bottomPadding: Math.round(Styles.Hints.spacing * 2)
    topPadding: Math.round(Styles.Hints.spacing * 2)
    leftPadding: Math.round(Styles.Hints.spacing * 2)
    rightPadding: Math.round(Styles.Hints.spacing * 2)
    anchors.fill: parent
    anchors.leftMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.rightMargin: Math.round(Styles.Hints.spacing / 2)

    Item {
      id: _content
      width: Math.floor(Math.min(_scrollview.availableWidth, _root.contentMaxWidth))
      height: _layout.implicitHeight
      x: Math.round((_scrollview.availableWidth - width) / 2)
      y: Math.round(Math.max(0, (_scrollview.availableHeight - height) / 2))

      ColumnLayout {
        id: _layout
        spacing: Styles.Hints.spacing
        anchors.fill: parent

        Controls.SvgImage {
          id: _image
          visible: _image.status != Image.Null
          fillMode: Image.PreserveAspectFit
          width: Math.round(Styles.Hints.spacing * 4)
          Layout.alignment: Qt.AlignHCenter
          Layout.bottomMargin: Styles.Hints.spacing
        }

        Controls.BusyIndicator {
          id: _busyIndicator
          visible: _root.showBusyIndicator && !_image.visible
          Layout.alignment: Qt.AlignHCenter
          Layout.bottomMargin: Styles.Hints.spacing
        }

        Controls.Text {
          id: _titleText
          role: Controls.Text.Role.H1
          visible: text
          horizontalAlignment: Text.AlignHCenter
          Layout.fillWidth: true
        }

        Controls.Text {
          id: _contentText
          role: Controls.Text.Role.Body
          visible: text
          horizontalAlignment: Text.AlignHCenter
          Layout.fillWidth: true
        }

        Controls.Button {
          id: _button
          visible: text
          onClicked: _root.buttonClicked()
          focus: true
          iconId: _root.buttonIconId
          Layout.topMargin: Styles.Hints.spacing
          Layout.alignment: Qt.AlignHCenter
        }
      }
    }
  }
}
