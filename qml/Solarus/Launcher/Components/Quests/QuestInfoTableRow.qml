/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

Item {
  property bool evenRow: false
  property string label: ""
  property string value: ""
  property int labelColumnWidth: Math.round(Math.min(Styles.Hints.controlDefaultWidth / 1.5, _layout.width * 0.5))
  property bool monosSpaceValue: false
  readonly property alias labelImplicitWidth: _label.implicitWidth

  id: _root
  implicitHeight: Math.max(_layout.implicitHeight + Styles.Hints.spacing, Styles.Hints.controlHeight)
  implicitWidth: Styles.Hints.controlDefaultWidth

  Rectangle {
    id: _background
    anchors.fill: parent
    radius: Math.round(Styles.Hints.radius / 1.5)
    color: _root.evenRow ? Styles.Colors.tableRowEven : Styles.Colors.tableRowOdd
  }

  RowLayout {
    id: _layout
    anchors.fill: parent
    anchors.leftMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.rightMargin: Math.round(Styles.Hints.spacing / 2)
    anchors.topMargin: Math.round(Styles.Hints.spacing / 4)
    anchors.bottomMargin: Math.round(Styles.Hints.spacing / 4)

    Controls.Text {
      id: _label
      text: _root.label
      role: Controls.Text.Role.Caption
      Layout.alignment: Qt.AlignCenter
      Layout.preferredWidth: _root.labelColumnWidth
      wrapMode: Text.NoWrap
      elide: Text.ElideRight
      color: Styles.Colors.tertiaryColorHover
    }

    Controls.Text {
      id: _value
      text: _root.value
      role: _root.monosSpaceValue ? Controls.Text.Role.Console : Controls.Text.Role.Body
      Layout.alignment: Qt.AlignCenter
      Layout.fillWidth: true
      Layout.preferredHeight: implicitHeight
    }
  }
}
