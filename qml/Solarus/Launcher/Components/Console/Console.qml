/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls

//import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Controls 1.0 as Controls

FocusScope {
  readonly property bool userHasInteracted: _private.userHasInteracted

  id: _root

  function append(text) {
    _textEdit.append(text)
  }

  function clear() {
    _textEdit.clear()
    _private.userHasInteracted = false
  }

  function scrollToStart() {
    _scrollView.scrollToTop()
  }

  function scrollToEnd() {
    _scrollView.scrollToBottom()
  }

  QtObject {
    property bool userHasInteracted: false
    id: _private
  }

  Controls.ScrollView {
    id: _scrollView
    clip: true
    anchors.fill: parent
    contentHeight: _textEdit.implicitHeight
    verticalScrollBarVMargin: Math.round(Styles.Hints.spacing / 2)
    verticalScrollBarHMargin: Math.round(Styles.Hints.spacing / 2)
    contentWidth: -1

    onScrolled: _private.userHasInteracted = true

    background: Rectangle {
      id: _rectBackground
      color: Styles.Colors.getTextFieldBackground(_root.enabled)
      radius: Styles.Hints.radius
      border.width: Styles.Hints.borderWidth
      border.color: Styles.Colors.getTextFieldBorder(_root.hasFocus, _root.hovered, _root.down, _root.enabled)

      Behavior on color {
        enabled: _root.enabled

        ColorAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }

      Behavior on border.color {
        enabled: _root.enabled

        ColorAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    TextEdit {
      id: _textEdit
      padding: Styles.Hints.spacing
      wrapMode: TextEdit.WordWrap
      width: _scrollView.availableWidth
      readOnly: true
      selectByMouse: true
      selectByKeyboard: true
      selectionColor: Styles.Colors.primaryColor
      color: Styles.Colors.textColorBody
      font.pixelSize: Styles.Fonts.consoleFontSize
      font.family: Styles.Fonts.consoleFontFamily
      textFormat: TextEdit.RichText

      MouseArea {
        id: _mouseArea
        anchors.fill: _textEdit
        acceptedButtons: Qt.RightButton
        onReleased: (event) => _menu.popup(event.x, event.y)
      }

      Controls.ContextMenu {
        id: _menu
        canCopy: _textEdit.selectedText.length > 0
        canPaste: _textEdit.canPaste
        canCut: _textEdit.selectedText.length > 0
        canSelectAll: _textEdit.text.length > 0
        onCopyTriggered: {
          _textEdit.forceActiveFocus()
          _textEdit.copy()
        }
        onPasteTriggered: {
          _textEdit.forceActiveFocus()
          _textEdit.paste()
        }
        onCutTriggered: {
          _textEdit.forceActiveFocus()
          _textEdit.cut()
        }
        onSelectAllTriggered: {
          _textEdit.forceActiveFocus()
          _textEdit.selectAll()
        }
      }
    }
  }
}
