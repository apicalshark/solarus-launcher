/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15

StackView {
  property int animationDuration: 350
  id: _root
  clip: true

  pushEnter: Transition {
    ParallelAnimation {
      PropertyAnimation {
        property: "x"
        from: _root.width
        to: 0
        duration: _root.animationDuration
        easing.type: Easing.InOutQuart
      }
      PropertyAnimation {
        property: "opacity"
        from: 0
        to: 1
        duration: _root.animationDuration
        easing.type: Easing.InOutQuart
      }
    }
  }
  pushExit: Transition {
    ParallelAnimation {
      PropertyAnimation {
        property: "x"
        from: 0
        to: -_root.width / 4
        duration: _root.animationDuration
        easing.type: Easing.InOutQuart
      }
      PropertyAnimation {
        property: "opacity"
        from: 1
        to: 0
        duration: _root.animationDuration
        easing.type: Easing.InOutQuart
      }
    }
  }
  popEnter: Transition {
    ParallelAnimation {
      PropertyAnimation {
        property: "x"
        from: -_root.width
        to: 0
        duration: _root.animationDuration
        easing.type: Easing.InCubic
      }
      PropertyAnimation {
        property: "opacity"
        from: 0
        to: 1
        duration: _root.animationDuration
        easing.type: Easing.InCubic
      }
    }
  }
  popExit: Transition {
    ParallelAnimation {
      PropertyAnimation {
        property: "x"
        from: 0
        to: _root.width / 4
        duration: _root.animationDuration
        easing.type: Easing.InCubic
      }
      PropertyAnimation {
        property: "opacity"
        from: 1
        to: 0
        duration: _root.animationDuration
        easing.type: Easing.InCubic
      }
    }
  }
}
