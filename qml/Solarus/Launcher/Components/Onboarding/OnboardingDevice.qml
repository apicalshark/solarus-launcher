/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property alias iconSource: _logoIcon.source
  property alias text: _text.text
  property bool isConnected: false
  id: _root
  implicitHeight: 80
  implicitWidth: _layout.implicitWidth

  Rectangle {
    id: _background
    anchors.fill: parent
    color: Styles.Colors.secondaryColorLightest
    radius: 8
    opacity: 0.25
  }

  RowLayout {
    id: _layout
    spacing: 16
    anchors.fill: parent
    anchors.leftMargin: 16
    anchors.rightMargin: 16
    anchors.topMargin: 8
    anchors.bottomMargin: 8

    Image {
      id: _logoIcon
      source: ""
      fillMode: Image.PreserveAspectFit
      smooth: true
      antialiasing: true
      height: 64
      width: 64
      sourceSize.width: width
      sourceSize.height: height
      Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
    }

    Controls.Text {
      id: _text
      text: ""
      role: Controls.Text.Role.H2
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      Layout.fillWidth: true
      color: Styles.Colors.textColorBody
      elide: Text.ElideRight
      wrapMode: Text.NoWrap
    }

    Controls.Text {
      id: _connectedCaption
      text: qsTr("Not connected")
      role: Controls.Text.Role.Caption
      verticalAlignment: Text.AlignVCenter
      horizontalAlignment: Text.AlignRight
      visible: !_root.isConnected
      Layout.fillHeight: true
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      elide: Text.ElideRight
      wrapMode: Text.NoWrap
    }

    Rectangle {
      id: _connectedRect
      height: 32
      width: 32
      radius: 16
      color: _root.isConnected ? Styles.Colors.primaryColor : Styles.Colors.secondaryColorLightest
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      Layout.rightMargin: 16

      Image {
        id: _connectedIcon
        source: _root.isConnected ? Styles.Icons.check_16 : Styles.Icons.close_16
        fillMode: Image.PreserveAspectFit
        smooth: true
        antialiasing: true
        height: 32
        width: 32
        sourceSize.width: width
        sourceSize.height: height
        anchors.centerIn: parent
      }
    }
  }
}
