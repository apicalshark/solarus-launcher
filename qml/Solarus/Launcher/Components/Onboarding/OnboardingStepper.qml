/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property int steps: 3
  property int step: 0
  id: _root
  enabled: false
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: Styles.Hints.iconSize

  Row {
    anchors.fill: parent
    spacing: _root.steps > 1 ? (_root.width - _root.steps * Styles.Hints.iconSize) / (_root.steps - 1) : 0

    Repeater {
      model: _root.steps
      delegate: Item {
        id: _item
        width: Styles.Hints.iconSize
        height: Styles.Hints.iconSize

        Rectangle {
          id: _circle
          anchors.centerIn: parent
          width: index == _root.step ? Math.floor(Styles.Hints.iconSize * 0.75) : Math.floor(Styles.Hints.iconSize / 2)
          height: width
          radius: height / 2
          color: index == _root.step ? Styles.Colors.primaryColorLighter : Styles.Colors.secondaryColorLighter

          Behavior on width {
            NumberAnimation {
              duration: Styles.Hints.animationDuration * 2
              easing.type: Easing.OutCubic
            }
          }

          Behavior on color {
            ColorAnimation {
              duration: Styles.Hints.animationDuration * 2
              easing.type: Easing.OutCubic
            }
          }
        }
      }
    }
  }
}
