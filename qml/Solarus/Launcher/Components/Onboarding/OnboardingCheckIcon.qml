/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles

Item {
  signal animationFinished
  property color circleColor: Styles.Colors.primaryColor
  property color checkColor: Styles.Colors.primaryColorLighter
  id: _root
  enabled: false
  width: implicitWidth
  height: implicitHeight
  implicitWidth: Styles.Hints.iconSize * 4
  implicitHeight: Styles.Hints.iconSize * 4
  onCircleColorChanged: _canvas.requestPaint()
  onCheckColorChanged: _canvas.requestPaint()

  Canvas {
    property real circleProgress: 0
    property real checkProgress: 0

    id: _canvas
    anchors.centerIn: parent
    width: Math.min(parent.width, parent.height)
    height: width
    renderTarget: Canvas.FramebufferObject
    renderStrategy: Canvas.Cooperative
    antialiasing: true
    onCircleProgressChanged: _canvas.requestPaint()

    onCheckProgressChanged: {
      _canvas.requestPaint()
      // Strangely, the animations signals are not emitted, so we handle
      // the emission of the root signal here.
      if (checkProgress == 1) {
        _root.animationFinished()
      }
    }

    onPaint: {
      var context = getContext("2d")
      context.reset()
      fixSize(context)
      drawCircle(context)
      drawCheck(context)
    }

    states: [
      State {
        name: "animationStart"
        PropertyChanges {
          target: _canvas
          circleProgress: 0
          checkProgress: 0
        }
      },
      State {
        name: "animationEnd"
        PropertyChanges {
          target: _canvas
          circleProgress: 1
          checkProgress: 1
        }
      }
    ]

    transitions: Transition {
      to: "animationEnd"
      SequentialAnimation {
        NumberAnimation {
          property: "circleProgress"
          easing.type: Easing.InOutQuart
          duration: 1000
        }
        NumberAnimation {
          property: "checkProgress"
          easing.type: Easing.InOutCubic
          duration: 1000
        }
      }
    }

    // Hotfix https://bugreports.qt.io/browse/QTBUG-65073
    function fixSize(context) {
      if (width < 64 || height < 64) {
        const xFixRatio = width > 0 && width < 64 ? 64 / width : 1
        const yFixRatio = height > 0 && height < 64 ? 64 / height : 1
        context.scale(xFixRatio, yFixRatio)
      }
    }

    function interpolate(start, end, ratio) {
      return start + (end - start) * ratio
    }

    function drawCircle(context) {
      context.save()
      const circleStrokeWidth = Number(Math.max(1, 0.05 * width)).toFixed(0)
      const r = width / 2 - circleStrokeWidth / 2
      const endAngle = interpolate(-Math.PI / 2, 3 * Math.PI / 2,
                                   circleProgress)
      context.beginPath()
      context.arc(width / 2, height / 2, r, -Math.PI / 2, endAngle, false)
      context.lineWidth = circleStrokeWidth
      context.lineCap = "round"
      context.lineJoin = "round"
      context.strokeStyle = _root.circleColor
      context.stroke()
      context.restore()
    }

    function drawCheck(context) {
      context.save()
      const checkSize = 0.5 * width
      const checkX = (width - checkSize) / 2
      const checkY = (height - checkSize) / 2
      const p0 = Qt.point(checkX, checkY + 0.4 * checkSize)
      const p1 = Qt.point(checkX + 0.4 * checkSize, checkY + 0.8 * checkSize)
      const p2 = Qt.point(checkX + checkSize, checkY + 0.2 * checkSize)
      context.beginPath()
      context.moveTo(p0.x, p0.y)
      context.lineTo(p1.x, p1.y)
      context.lineTo(p2.x, p2.y)
      const l1 = Math.sqrt(Math.pow(p1.x - p0.x, 2) + Math.pow(p1.y - p0.y, 2))
      const l2 = Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2))
      const totalLength = l1 + l2
      const drawnLength = interpolate(0, totalLength, checkProgress)
      context.lineWidth = Number(Math.max(2, 0.1 * width)).toFixed(0)
      context.setLineDash([drawnLength, totalLength])
      context.lineCap = "round"
      context.lineJoin = "mitter"
      context.strokeStyle = _root.checkColor
      context.stroke()
      context.restore()
    }
  }

  function startAnimation() {
    _canvas.state = "animationStart"
    _canvas.state = "animationEnd"
  }
}
