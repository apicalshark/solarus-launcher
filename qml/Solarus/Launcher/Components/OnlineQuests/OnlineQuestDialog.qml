/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Components.OnlineQuests 1.0 as OnlineQuests
import Solarus.Launcher.Components.Quests 1.0 as Quests
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Utils 1.0 as Utils

Controls.Dialog {
  property Model.OnlineQuest quest: null
  property int questThumbnailWidth: 350
  signal questActionTriggered(Model.OnlineQuest quest, int action)

  id: _root
  standardButtons: QtControls.Dialog.NoButton
  implicitWidth: Styles.Hints.questDialogWidth
  footer: null
  rightPadding: Styles.Hints.spacing
  leftPadding: Styles.Hints.spacing
  topPadding: Styles.Hints.borderWidth
  bottomPadding: Styles.Hints.borderWidth

  onQuestChanged: function() {
    _private.updateOverlayState()
  }

  onAboutToShow: Qt.callLater(function() {
    // Focus the first enabled item among the list.
    const focusOrder = []
    const buttons = _layoutButtons.children
    for (let i in buttons) {
      const child = _layoutButtons.children[i]
      focusOrder.push(child)
    }
    focusOrder.push(_closeButton)
    for (let j in focusOrder) {
      const item = focusOrder[j]
      if (item.enabled) {
        item.forceActiveFocus()
        break;
      }
    }
  })

  QtObject {
    id: _private

    function getPlayersValue(minPlayers, maxPlayers) {
      return minPlayers === maxPlayers ? minPlayers
                                       : "%1 - %2".arg(minPlayers).arg(maxPlayers.toString())
    }

    function getLicenses(licenseList) {
      return licenseList.join(", ")
    }

    function getLanguages(languageList) {
      return languageList.map(Utils.LanguageUtils.getTranslatedLanguage).join(", ")
    }

    function getOverlayState(questState) {
      switch (questState) {
        case Model.OnlineQuest.State.Downloading: return Quests.QuestThumbnailOverlay.State.Downloading
        case Model.OnlineQuest.State.Downloaded: return Quests.QuestThumbnailOverlay.State.Downloaded
        default: return Quests.QuestThumbnailOverlay.State.Default
      }
    }

    function updateOverlayState() {
      if (!_root.quest) { _thumbnail.downloadProgress = 0 }
      _thumbnail.overlayState = _private.getOverlayState(_root.quest ? _root.quest.state : Model.OnlineQuest.State.Idle)
    }
  }

  Connections {
    id: _questConnections
    target: _root.quest
    enabled: _root.quest != null

    function onStateChanged() {
      _private.updateOverlayState()
    }

    function onDownloadProgressChanged(progress) {
      _thumbnail.downloadProgress = progress
    }
  }

  contentItem: FocusScope {
    id: _content

    Controls.ScrollView {
      id: _scrollView
      anchors.fill: parent
      contentWidth: availableWidth
      verticalScrollBarVMargin: Styles.Hints.spacing

      ColumnLayout {
        id: _rootLayout
        anchors.fill: parent
        anchors.topMargin: Math.round(Styles.Hints.spacing * 2)
        anchors.bottomMargin: Math.round(Styles.Hints.spacing * 2)
        anchors.leftMargin: Styles.Hints.spacing
        anchors.rightMargin: Styles.Hints.spacing
        spacing: Styles.Hints.spacing * 2

        GridLayout {
          id: _titleLayout
          Layout.fillWidth: true
          Layout.alignment: Qt.AlignTop
          Layout.bottomMargin: -Styles.Hints.spacing
          rowSpacing: Styles.Fonts.bodyLineSpacing
          columnSpacing: Styles.Hints.spacing

          Controls.Text {
            id: _title
            role: Controls.Text.Role.H2
            wrapMode: Text.NoWrap
            elide: Text.ElideRight
            text: _root.quest ? _root.quest.title : qsTr("Unknown Title")
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            Layout.row: 0
            Layout.column: 0
         }
          Controls.Text {
            id: _author
            wrapMode: Text.NoWrap
            elide: Text.ElideRight
            text: _root.quest ? _root.quest.authors.join(", ") : qsTr("Unknown Author")
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            Layout.row: 1
            Layout.column: 0
          }

          Controls.CircleButton {
            id: _closeButton
            implicitHeight: Styles.Hints.controlHeight
            implicitWidth: Styles.Hints.controlHeight
            iconId: Styles.Icons.close_16
            iconWidth: Styles.Hints.iconSize
            iconHeight: Styles.Hints.iconSize
            tooltip: qsTr("Close this popup")
            Layout.alignment: Qt.AlignTop
            Layout.row: 0
            Layout.column: 1
            Layout.rowSpan: 2
            KeyNavigation.down: _playButton
            onClicked: _root.close()
          }
        }

        GridLayout {
          readonly property int breakPoint: _thumbnail.Layout.minimumWidth + _layoutButtons.Layout.minimumWidth + columnSpacing
          readonly property bool shouldBreak: width < breakPoint

          id: _gridLayout
          columnSpacing: Styles.Hints.spacing * 2
          rowSpacing: Styles.Hints.spacing
          flow: shouldBreak ? GridLayout.TopToBottom : GridLayout.LeftToRight
          Layout.fillWidth: true
          Layout.fillHeight: false
          Layout.alignment: Qt.AlignTop
          Layout.minimumWidth: 0
          Layout.preferredWidth: 0
          Layout.minimumHeight: shouldBreak ? _thumbnail.height + _layoutButtons.height + rowSpacing
                                              : Math.max(_thumbnail.implicitHeight, _layoutButtons.implicitHeight)

          Quests.QuestThumbnail {
            id: _thumbnail
            questTitle: _root.quest ? _root.quest.title : ""
            questThumbnailPath: _root.quest ? Qt.resolvedUrl(_root.quest.thumbnail) : ""
            Layout.minimumWidth: _root.questThumbnailWidth
            Layout.minimumHeight: Math.round(this.Layout.minimumWidth / Styles.Hints.questThumbnailAspectRatio)
            Layout.fillWidth: _gridLayout.shouldBreak
            Layout.preferredHeight: width / Styles.Hints.questThumbnailAspectRatio
          }

          ColumnLayout {
            id: _layoutButtons
            spacing: Styles.Hints.spacing
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignTop
            Layout.minimumWidth: Utils.GeometryUtils.getColumnLayoutMinimumWidth(this)

            Controls.Button {
              id: _playButton
              text: qsTr("Download")
              iconId: Styles.Icons.download_16
              visible: _root.quest
              enabled: _root.quest && _root.quest.state === Model.OnlineQuest.State.Idle
              role: Styles.Colors.Role.Primary
              Layout.minimumWidth: implicitWidth
              Layout.fillWidth: true
              Layout.alignment: Qt.AlignHCenter | Qt.AligntTop
              onClicked: _root.questActionTriggered(_root.quest, OnlineQuests.OnlineQuestUtils.Action.Download)
            }
          }
        }

        Controls.Text {
          id: _description
          wrapMode: Text.WordWrap
          verticalAlignment: Text.AlignTop
          text: _root.quest ? _root.quest.description : qsTr("Unknown description.")
          Layout.fillWidth: true
          Layout.minimumHeight: implicitHeight
          Layout.alignment: Qt.AlignTop
        }

        ColumnLayout {
          id: _tableLayout
          spacing: 0
          Layout.fillWidth: true
          Layout.fillHeight: true
          Layout.alignment: Qt.AlignTop
          Layout.bottomMargin: Math.round(Styles.Hints.spacing * 4)

          Quests.QuestInfoTableRow {
            label: qsTr("Version")
            value: _root.quest ? _root.quest.version : qsTr("Unknown Value")
            monosSpaceValue: true
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Solarus Version")
            value: _root.quest ? _root.quest.engineVersion : qsTr("Unknown Value")
            evenRow: true
            monosSpaceValue: true
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("License")
            value: _root.quest ? _private.getLicenses(_root.quest.licenses) : qsTr("Unknown Value")
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Languages")
            value: _root.quest ? _private.getLanguages(_root.quest.languages) : qsTr("Unknown Value")
            evenRow: true
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Players")
            value: _root.quest ? _private.getPlayersValue(_root.quest.minPlayers, _root.quest.maxPlayers) : qsTr("Unknown Value")
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Genre")
            value: _root.quest ? _root.quest.genre.join(", ") : qsTr("Unknown Value")
            evenRow: true
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Release Date")
            value: _root.quest ? Qt.formatDateTime(_root.quest.initialReleaseDate, "dd/MM/yyyy") : qsTr("Unknown Value")
            Layout.fillWidth: true
          }
          Quests.QuestInfoTableRow {
            label: qsTr("Website")
            value: _root.quest ? "<a href='%2'>%2</a>".arg(_root.quest.website) : qsTr("Unknown Value")
            evenRow: true
            Layout.fillWidth: true
          }
        }
      }
    }
  }
}
