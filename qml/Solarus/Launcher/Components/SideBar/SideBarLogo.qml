/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  property bool isExpanded: true
  property int animationDuration: Styles.Hints.animationDuration
  readonly property int iconSize: 48
  readonly property int logoWidth: 124
  readonly property int logoHeight: 34
  readonly property int logoSpacing: Styles.Hints.spacing
  property int widthExpanded: Styles.Hints.sideBarExpandedWidth
  property int widthContracted: Styles.Hints.sideBarContractedWidth

  id: _root
  clip: true
  enabled: false
  implicitHeight: iconSize
  implicitWidth: _root.iconSize + _root.logoWidth + _root.logoSpacing

  Controls.SvgImage {
    id: _appLogoIcon
    source: Styles.Brand.icon
    anchors.verticalCenter: parent.verticalCenter
    height: _root.iconSize
    width: _root.iconSize
    x: _root.isExpanded ? (_root.widthExpanded - _root.implicitWidth) / 2
                        : (_root.widthContracted - _root.iconSize) / 2
    Behavior on x {
      NumberAnimation {
        duration: _root.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  Controls.SvgImage {
    id: _appLogoText
    source: Styles.Brand.logoText
    height: _root.logoHeight
    width: _root.logoWidth
    anchors.verticalCenter: _appLogoIcon.verticalCenter
    anchors.left: _appLogoIcon.right
    anchors.leftMargin: _root.logoSpacing
    opacity: _root.isExpanded ? 1.0 : 0.0

    Behavior on opacity {
      NumberAnimation {
        duration:  _root.animationDuration / 2
        easing.type: Easing.OutCubic
      }
    }
  }
}
