/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

T.Button {
  property alias iconId: _icon.iconId
  property int pageId: -1
  property bool isExpanded: true
  property int animationDuration: Styles.Hints.animationDuration
  property string tooltip: ""
  property int widthExpanded: Styles.Hints.sideBarExpandedWidth
  property int widthContracted: Styles.Hints.sideBarContractedWidth
  readonly property int iconSize: Math.round(Styles.Hints.iconSize * 1.5)
  property bool selected: false

  id: _root
  implicitHeight: Math.round(Styles.Hints.controlHeight * 1.25)
  implicitWidth: _root.iconSize + _text.implicitWidth + _root.spacing + _root.leftPadding + _root.rightPadding
  checkable: false
  checked: false
  bottomPadding: 0
  topPadding: 0
  leftPadding: Styles.Hints.spacing
  rightPadding: Styles.Hints.spacing
  spacing: Styles.Hints.spacing
  hoverEnabled: Qt.styleHints.useHoverEffects
  focusPolicy: Qt.StrongFocus
  autoRepeat: false
  font: Styles.Fonts.sideBarItemFont
  height: implicitHeight

  Keys.onPressed: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)
  Keys.onReleased: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)

  background: Rectangle {
    id: _background
    anchors.fill: _root
    anchors.margins: _root.down ? -Styles.Hints.pressDelta : 0
    color: Styles.Colors.getSideBarItemBackground(_root.hovered, _root.down, _root.enabled, _root.selected)
    radius: _root.down ? Styles.Hints.radius + Styles.Hints.pressDelta / 2 : Styles.Hints.radius

    Behavior on color {
      enabled: _root.enabled
      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on anchors.margins {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  contentItem: Item {
    id: _contentItem
    anchors.fill: _root
    clip: true

    Controls.SvgIcon {
      id: _icon
      anchors.verticalCenter: parent.verticalCenter
      color: _root.selected ? Styles.Colors.primaryColorLighter : Styles.Colors.primaryColorPressed
      width: _root.iconSize
      height: _root.iconSize
      visible: status !== Image.Null
      x: _root.isExpanded ? _root.leftPadding
                          : (_root.widthContracted - _root.iconSize) / 2

      Behavior on color {
        enabled: _root.enabled
        ColorAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }

      Behavior on x {
        NumberAnimation {
          duration: _root.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    Text {
      id: _text
      color: Styles.Colors.getControlForeground(_root.enabled)
      elide: Text.ElideRight
      font: _root.font
      horizontalAlignment: Text.AlignLeft
      verticalAlignment: Text.AlignVCenter
      renderType: Styles.Fonts.getRenderType(font.pixelSize)
      text: _root.text
      opacity: _root.isExpanded ? 1.0 : 0.0
      //anchors.verticalCenter: parent.verticalCenter
      anchors.left: _icon.right
      anchors.leftMargin: _root.spacing
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.topMargin: Math.floor(_root.font.pixelSize * 0.2)

      Behavior on opacity {
        NumberAnimation {
          duration:  _root.animationDuration / 2
          easing.type: Easing.OutCubic
        }
      }
    }
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: _background
    showFocus: _root.visualFocus || _root.activeFocus
    animateBorder: !_root.down
    z: 1
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  Controls.Tooltip {
    id: _tooltip
    text: _root.isExpanded ? "" : _root.tooltip
    x: Math.round(_root.width + Styles.Hints.spacing)
    y: Math.round((_root.height - height) / 2)
  }
}
