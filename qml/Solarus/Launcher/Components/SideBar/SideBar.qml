/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtGraphicalEffects 1.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.SideBar 1.0 as SideBar
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  signal rightRequested

  property int bottomBarHeight: 0
  property bool isExpanded: true
  property int animationDuration: Styles.Hints.animationDuration * 2
  property int page: -1
  property int shadowWidth: Math.floor(Styles.Hints.spacing / 4)
  property int widthExpanded: Styles.Hints.sideBarExpandedWidth + Styles.Hints.spacing + shadowWidth + Styles.Hints.borderWidth
  property int widthContracted: Styles.Hints.sideBarContractedWidth + Styles.Hints.spacing + shadowWidth + Styles.Hints.borderWidth
  property Model.LocalQuest playingQuest: null
  property int questUpdatesState: Model.QuestUpdateListModel.State.Uninitialized

  id: _root
  width: isExpanded ? widthExpanded : widthContracted
  Keys.onRightPressed: _root.rightRequested()
  Keys.onTabPressed: _root.rightRequested()
  Keys.onBacktabPressed: event.accepted = true

  Behavior on width {
    NumberAnimation {
      duration: animationDuration
      easing.type: Easing.OutCubic
    }
  }

  Rectangle {
    id: _backgroundRect
    color: Styles.Colors.sideBarColor
    anchors.fill: parent
    anchors.rightMargin: _root.shadowWidth

    // Optimization: use a Rectangle instead of a DropShadow effect.
    Rectangle {
      id: _backgroundShadow
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.right: _backgroundRect.right
      anchors.rightMargin: -_root.shadowWidth
      width: _root.shadowWidth
      gradient: Gradient {
        orientation: Qt.Horizontal
        GradientStop {
          position: 0.0
          color: Styles.Colors.colorWithAlpha(Styles.Colors.shadowColor, 0.5)
        }
        GradientStop {
          position: 1.0
          color: Styles.Colors.colorWithAlpha(Styles.Colors.shadowColor, 0)
        }
      }
    }

    Rectangle {
      id: _backgroundBorder
      anchors.left: _backgroundShadow.left
      anchors.leftMargin: -width
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      width: Styles.Hints.borderWidth
      color: Styles.Colors.sideBarSeparatorColor
    }
  }

  Utils.BaseObject {
    readonly property Item listViewBottom: _listViewTop.footerItem ? _listViewTop.footerItem.listViewBottom : null

    id: _private
    Component.onCompleted: {
      _private.synchronizeListViewsWithPage()
    }

    // Synchronizes the listView's currentIndex with the page id.
    function getListViewCurrentIndex(listView, pageId) {
      let currentIndex = -1
      for (let i = 0; i < listView.count; i++) {
        const item = listView.model.get(i)
        if (item.pageId === pageId) {
          currentIndex = i
          break
        }
      }
      return currentIndex
    }

    function onListViewCurrentItemChanged(listView, currentItem) {
      if (currentItem) {
        if (listView.activeFocus) {
          currentItem.item.forceActiveFocus(Qt.MouseFocusReason)
        }
        _root.page = currentItem.modelPageId
      }
    }

    function synchronizeListViewsWithPage() {
      _listViewTop.currentIndex = _private.getListViewCurrentIndex(_listViewTop, _root.page)

      //const listViewBottom = _listViewTop.footerItem.listViewBottom
      if (_private.listViewBottom) {
        _private.listViewBottom.currentIndex = _private.getListViewCurrentIndex(listViewBottom, _root.page)
      }
    }

    function getSourceComponent(pageId) {
      switch (pageId) {
      case Model.Navigation.PageId.PlayingQuest: return _componentSideBarPlayingItem
      case Model.Navigation.PageId.QuestUpdates: return _componentSideBarQuestUpdatesItem
      default: return _componentSideBarItem
      }
    }

    function getIcon(pageId) {
      switch(pageId) {
      case Model.Navigation.PageId.Home: return Styles.Icons.home_24
      case Model.Navigation.PageId.LocalQuests: return Styles.Icons.library_24
      case Model.Navigation.PageId.OnlineQuests: return Styles.Icons.shop_24
      case Model.Navigation.PageId.Settings: return Styles.Icons.settings_24
      case Model.Navigation.PageId.About: return Styles.Icons.about_24
      default: return ""
      }
    }

    function getText(pageId) {
      switch(pageId) {
      case Model.Navigation.PageId.Home: return qsTr("Home")
      case Model.Navigation.PageId.LocalQuests: return qsTr("Local Quests")
      case Model.Navigation.PageId.OnlineQuests: return qsTr("Online Quests")
      case Model.Navigation.PageId.PlayingQuest: return qsTr("Playing")
      case Model.Navigation.PageId.QuestUpdates: return qsTr("Updates")
      case Model.Navigation.PageId.Settings: return qsTr("Settings")
      case Model.Navigation.PageId.About: return qsTr("About")
      default: return ""
      }
    }

    // Adds or remove item in the model. Animation is automatic.
    function setItemVisible(visible, pageId, preferredPosition) {
      const listModel = _listViewTop.model

      // Check if already present in model.
      let index = -1
      for (let i = 0; i < listModel.count; i++) {
        if (listModel.get(i).pageId === pageId) {
          index = i
          break;
        }
      }

      console.log("SideBar::setItemVisible", pageId, index)

      if (visible) {
        if (index > 0) {

        } else {
          listModel.insert(Math.min(listModel.count, preferredPosition), { "pageId": pageId })
        }

//        // Ensure it is always in preferred position.
//        if (listModel.count > preferredPosition + 1 && index === listModel.count - 1) {
//          listModel.move(index, preferredPosition, 1)
//        } else {
//          listModel.insert(Math.min(listModel.count, preferredPosition), {"pageId": pageId})
//        }
      } else if (index > 0) {
        listModel.remove(index)
      }
    }

    function setPlayingQuestVisible(visible) {
      setItemVisible(visible, Model.Navigation.PageId.PlayingQuest, 3)
    }

    function setQuestUpdatesVisible(visible) {
      setItemVisible(visible, Model.Navigation.PageId.QuestUpdates, 4)
    }

    Connections {
      target: _root

      function onPageChanged() {
        _private.synchronizeListViewsWithPage()
      }
    }

    Component {
      id: _componentSideBarItem

      SideBar.SideBarItem {
        id: _sideBarItem
        selected: modelIndex === modelListView.currentIndex
        isExpanded: _root.isExpanded
        text: modelPageName
        tooltip: modelPageName
        iconId: modelPageIcon
        onClicked: modelListView.currentIndex = modelIndex
        autoExclusive: true

      }
    }

    Component {
      id: _componentSideBarPlayingItem

      SideBar.SideBarPlayingItem {
        id: _sideBarPlayingItem
        selected: modelIndex === modelListView.currentIndex
        isExpanded: _root.isExpanded
        text: modelPageName
        tooltip: qsTr("Playing: %1").arg(secondaryText)
        secondaryText: _root.playingQuest ? _root.playingQuest.title : ""
        onClicked: modelListView.currentIndex = modelIndex

      }
    }

    Component {
      id: _componentSideBarQuestUpdatesItem

      SideBar.SideBarQuestUpdatesItem {
        id: _sideBarQuestUpdatesItem
        selected: modelIndex === modelListView.currentIndex
        isExpanded: _root.isExpanded
        //text: modelPageName
        tooltip: qsTr("Quest updates are available")
        //secondaryText: _root.playingQuest ? _root.playingQuest.title : ""
        onClicked: modelListView.currentIndex = modelIndex
      }
    }
  }

  onPlayingQuestChanged: function() {
    // First, enable animation only after that initialization has been done.
    if (playingQuest) _addTransition.enabled = true

    _private.setPlayingQuestVisible(playingQuest != null)
  }

  onQuestUpdatesStateChanged: function() {
    const showQuestUpdates = questUpdatesState !== Model.QuestUpdateListModel.State.Uninitialized

    // First, enable animation only after that initialization has been done.
    if (showQuestUpdates) _addTransition.enabled = true

    _private.setQuestUpdatesVisible(showQuestUpdates)

    // Go back to a fallback page if we were on that page.
    if (!showQuestUpdates && _root.page === Model.Navigation.PageId.QuestUpdates) {
      _root.page = Model.Navigation.PageId.LocalQuests
    }
  }

  ListView {
    id: _listViewTop
    highlight: null
    highlightFollowsCurrentItem: false
    spacing: Styles.Hints.spacing
    interactive: false
    keyNavigationEnabled: true
    keyNavigationWraps: false
    anchors.fill: parent
    anchors.topMargin: Styles.Hints.spacing
    anchors.bottomMargin: Styles.Hints.spacing + _root.bottomBarHeight
    anchors.rightMargin: Math.round(Styles.Hints.spacing / 2) + _root.shadowWidth
    anchors.leftMargin: Math.round(Styles.Hints.spacing / 2)
    currentIndex: -1
    onCurrentItemChanged: _private.onListViewCurrentItemChanged(this, currentItem)

    Keys.onUpPressed: {
      // Block if the current item is the first one.
      event.accepted = currentIndex === 0
    }

    Keys.onDownPressed: {
      // Move to bottom list if the current item is the last one.
      if (currentIndex === count - 1) {
        _private.listViewBottom.forceActiveFocus()
        _private.listViewBottom.currentIndex = 0
        event.accepted = true
      } else {
        event.accepted = false
      }
    }

    Keys.onRightPressed: _root.rightRequested()
    Keys.onTabPressed: _root.rightRequested()
    Keys.onBacktabPressed: event.accepted = true

    model: ListModel {
      id: _model
      Component.onCompleted: {
        append({"pageId": Model.Navigation.PageId.Home})
        append({"pageId": Model.Navigation.PageId.LocalQuests})
        append({"pageId": Model.Navigation.PageId.OnlineQuests})
      }
    }

    delegate: Loader {
      readonly property int modelPageId: model.pageId ?? -1
      readonly property int modelIndex: model.index
      readonly property Item modelListView: ListView.view
      readonly property string modelPageName: _private.getText(modelPageId)
      readonly property string modelPageIcon: _private.getIcon(modelPageId)

      sourceComponent: _private.getSourceComponent(modelPageId)
      width: ListView.view.width
    }

    add: Transition {
      id: _addTransition
      enabled: false

      ParallelAnimation {
        NumberAnimation {
          property: "opacity"
          from: 0
          to: 1
          duration: _root.animationDuration
          easing.type: Easing.InCubic
        }
        NumberAnimation {
          property: "x"
          from: -_listViewTop.width
          duration: _root.animationDuration
          easing.type: Easing.InCubic
        }
      }
    }
    remove: Transition {
      ParallelAnimation {
        NumberAnimation {
          property: "opacity"
          from: 1
          to: 0
          duration: _root.animationDuration / 2
          easing.type: Easing.InCubic
        }
        NumberAnimation {
          property: "x"
          to: -_listViewTop.width
          duration: _root.animationDuration / 2
          easing.type: Easing.InCubic
        }
      }
    }

    header: ColumnLayout {
      id: _header
      height: implicitHeight
      width: _listViewTop.width
      spacing: Styles.Hints.spacing


      SideBar.SideBarLogo {
        id: _logo
        Layout.preferredHeight: implicitHeight
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
        Layout.bottomMargin: Styles.Hints.spacing
        isExpanded: _root.isExpanded
      }
    }

    footer: ColumnLayout {
      readonly property alias listViewBottom: _listViewBottom

      id: _footer
      height: implicitHeight
      width: _listViewTop.width
      spacing: Styles.Hints.spacing

      SideBar.SideBarSeparator {
        Layout.topMargin: Styles.Hints.spacing
        Layout.fillWidth: true
      }

      ListView {
        id: _listViewBottom
        highlight: null
        highlightFollowsCurrentItem: false
        spacing: Styles.Hints.spacing
        interactive: false
        keyNavigationEnabled: true
        keyNavigationWraps: false
        Layout.preferredHeight: contentHeight
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignBottom
        currentIndex: -1
        onCurrentItemChanged: _private.onListViewCurrentItemChanged(this, currentItem)

        Keys.onUpPressed: {
          // Move to top list if the current item is the first one.
          if (currentIndex === 0) {
            _listViewTop.forceActiveFocus()
            _listViewTop.currentIndex = _listViewTop.count - 1
            event.accepted = true
          } else {
            event.accepted = false
          }
        }

        Keys.onDownPressed: {
          // Block if the current item is the last one.
          event.accepted = currentIndex === count - 1
        }

        model: ListModel {
          Component.onCompleted: {
            append({"pageId": Model.Navigation.PageId.Settings})
            append({"pageId": Model.Navigation.PageId.About})
          }
        }

        delegate: Loader {
          readonly property int modelPageId: model.pageId ?? -1
          readonly property int modelIndex: model.index
          readonly property Item modelListView: ListView.view
          readonly property string modelPageName: _private.getText(modelPageId)
          readonly property string modelPageIcon: _private.getIcon(modelPageId)

          sourceComponent: _private.getSourceComponent(modelPageId)
          width: ListView.view.width
        }
      }
    }
  }
}
