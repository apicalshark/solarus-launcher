/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles

Item {
  readonly property bool pressed: _mouseArea.pressed || _private.spacePressed || _private.enterPressed
  readonly property bool hovered: _mouseArea.containsMouse
  property alias thumbnailSource: _image.source
  property alias title: _textTitle.text
  property alias excerpt: _textExcerpt.text
  property date date
  property bool showFocus: false
  signal clicked

  id: _root
  implicitWidth: Styles.Hints.newsThumbnailWidth
  implicitHeight: Styles.Hints.newsThumbnailHeight

  Keys.onPressed: function(event) {
    if (event.isAutoRepeat) return

    if (event.key === Qt.Key_Space)
      _private.spacePressed = true
    else if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter)
      _private.enterPressed = true
  }

  Keys.onReleased: function(event) {
    if (event.isAutoRepeat) return

    if (event.key === Qt.Key_Space) {
      _private.spacePressed = false
      _root.clicked()
    } else if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
      _private.enterPressed = false
      _root.clicked()
    }
  }

  onHoveredChanged: {
    if (!hovered) {
      _private.spacePressed = false
      _private.enterPressed = false
    }
  }

  onPressedChanged: if (pressed) Services.SoundManager.focus.play()
  onClicked: Services.SoundManager.click.play()

  QtObject {
    property bool spacePressed: false
    property bool enterPressed: false

    id: _private

    function getBackgroundColor(isHovered, isPressed, isEnabled) {
      if (!isEnabled) return Styles.Colors.colorWithAlpha(Styles.Colors.secondaryColorPressed, 0)
      else if (isPressed) return Styles.Colors.secondaryColorLightest
      else if (isHovered) return Styles.Colors.secondaryColorLighter
      else return Styles.Colors.secondaryColorPressed
    }

    function getForegroundColor(isHovered, isPressed, isEnabled) {
      if (!isEnabled) return Styles.Colors.colorWithAlpha(Styles.Colors.tertiaryColorHover, 0)
      else if (isPressed) return Styles.Colors.tertiaryColorLighter
      else if (isHovered) return Styles.Colors.tertiaryColorPressed
      else return Styles.Colors.tertiaryColorHover
    }
  }

  Rectangle {
    id: _background
    anchors.fill: parent
    anchors.margins: pressed ? -Styles.Hints.borderWidth : 0
    color: _private.getBackgroundColor(hovered, pressed, enabled)
    radius: Styles.Hints.radius

    Behavior on anchors.margins {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    layer.enabled: _root.visible && _root.enabled
    layer.effect: DropShadow {
      visible: _root.enabled
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: _root.pressed ? 0 : Styles.Hints.borderWidth
      radius: _root.pressed ? Styles.Hints.borderWidth : Styles.Hints.borderWidth * 2
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColor

      Behavior on verticalOffset {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
      Behavior on radius {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }
  }

  Rectangle {
    id: _imageBackground
    color: Styles.Colors.secondaryColor
    radius: Styles.Hints.radius
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    height: 84

    Rectangle {
      color: _imageBackground.color
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      height: _imageBackground.radius * 2
    }
  }

  Loader {
    id: _loadingIndicator
    anchors.centerIn: _imageBackground
    active: _image.status === Image.Loading
    sourceComponent: Component {
      Controls.BusyIndicator { }
    }
  }

  Image {
    id: _image
    width: parent.width
    height: (implicitHeight / implicitWidth) * width
    fillMode: Image.PreserveAspectCrop
    verticalAlignment: Image.AlignTop
    antialiasing: true
    smooth: true
    opacity: enabled ? 1.0 : 0.5
    asynchronous: true
    mipmap: true

    // Needed to get radius on angles.
    layer.enabled: true
    layer.effect: OpacityMask {
      maskSource: Rectangle {
        width: _image.width
        height: _image.height
        radius: Styles.Hints.radius
      }
    }
  }

  ColumnLayout {
    id: _layoutContent
    spacing: Math.round(Styles.Hints.spacing / 4)
    anchors.fill: parent
    anchors.topMargin: _imageBackground.height + Styles.Hints.spacing
    anchors.leftMargin: Styles.Hints.spacing
    anchors.rightMargin: Styles.Hints.spacing
    anchors.bottomMargin: Math.round(Styles.Hints.spacing / 2)

    Controls.Text {
      id: _textTitle
      role: Controls.Text.Role.H4
      bold: true
      horizontalAlignment: Text.AlignLeft
      Layout.alignment: Qt.AlignLeft | Qt.AlignTop
      Layout.fillWidth: true
    }

    RowLayout {
      id: _layoutDate
      spacing: Math.round(Styles.Hints.spacing / 2)
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignHCenter
      Layout.fillHeight: false
      Layout.preferredHeight: implicitHeight

      Controls.SvgIcon {
        id: _iconDate
        iconId: Styles.Icons.calendar_16
        color: _private.getForegroundColor(_root.hovered, _root.pressed, _root.enabled)

        Behavior on color {
          enabled: _root.enabled
          ColorAnimation {
            duration: Styles.Hints.animationDuration
            easing.type: Easing.OutCubic
          }
        }
      }

      Controls.Text {
        id: _textDate
        role: Controls.Text.Role.Caption
        color: _private.getForegroundColor(_root.hovered, _root.pressed, _root.enabled)
        font.pixelSize: Styles.Fonts.captionFontSize * 0.85
        verticalAlignment: Text.AlignVCenter
        text: Qt.formatDateTime(_root.date, "dd/MM/yyyy")
        Layout.fillWidth: true

        Behavior on color {
          enabled: _root.enabled
          ColorAnimation {
            duration: Styles.Hints.animationDuration
            easing.type: Easing.OutCubic
          }
        }
      }
    }

    Controls.Text {
      id: _textExcerpt
      role: Controls.Text.Role.Caption
      horizontalAlignment: Text.AlignLeft
      wrapMode: Text.WordWrap
      elide: Text.ElideRight
      color: _private.getForegroundColor(_root.hovered, _root.pressed, _root.enabled)
      Layout.fillWidth: true
      Layout.fillHeight: true

      Behavior on color {
        enabled: _root.enabled

        ColorAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }
  }

  Rectangle {
    id: _border
    color: "transparent"
    border.color: Styles.Colors.getTertiaryBorder(_root.hovered, _root.pressed, _root.enabled)
    opacity: 0.25
    anchors.fill: _background
    radius: Styles.Hints.radius
    enabled: false

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  // Keep this above everything else.
  MouseArea {
    id: _mouseArea
    anchors.fill: parent
    hoverEnabled: true
    cursorShape: Qt.PointingHandCursor
    onClicked: _root.clicked()
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: _background
    animateBorder: !_root.pressed
    showFocus: _root.showFocus
    borderWidth: 2 * Styles.Hints.focusBorderWidth
    radius: _background.radius + borderWidth
  }
}

