/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Components.News 1.0 as News
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

GridView {
  readonly property int horizontalMargin: parent ? (parent.width - Utils.GeometryUtils.getGridViewNecessaryWidth(parent.width, cellWidth)) / 2 : 0
  signal backRequested

  id: _root
  interactive: false
  cellWidth: Styles.Hints.newsThumbnailWidth + Styles.Hints.spacing
  cellHeight: Styles.Hints.newsThumbnailHeight + Styles.Hints.spacing
  implicitHeight: Utils.GeometryUtils.getGridViewHeight(width, cellWidth, cellHeight, count)
  keyNavigationWraps: false
  keyNavigationEnabled: true
  displayMarginBeginning: cellHeight
  displayMarginEnd: cellHeight
  currentIndex: 0
  highlightFollowsCurrentItem: true
  highlight: null

  Component.onCompleted: {
    const index = currentIndex && currentIndex >= 0 ? currentIndex : 0
    positionViewAtIndex(index, GridView.Beginning)
  }

  onActiveFocusChanged: {
    if (currentItem && _root.activeFocus) {
      const index = currentIndex && currentIndex >= 0 ? currentIndex : 0
      positionViewAtIndex(index, GridView.Contain)
    }
  }

  delegate: News.NewsListItem {
    id: _item
    title: model.title
    excerpt: model.excerpt
    date: model.date
    thumbnailSource: model.coverUrl
    onClicked: Qt.openUrlExternally(model.url)
    showFocus: _root.activeFocus && _item.GridView.isCurrentItem
  }

  populate: Transition {
    NumberAnimation {
      property: "opacity"
      from: 0
      to: 1
      duration: Styles.Hints.animationDuration
      easing.type: Easing.InCubic
    }
  }
  add: Transition {
    NumberAnimation {
      property: "opacity"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  remove: Transition {
    NumberAnimation {
      property: "opacity"
      to: 0
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  displaced: Transition {
    NumberAnimation {
      properties: "x,y"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  move: displaced
  moveDisplaced: displaced

  Keys.onPressed: function (event) {
    const columnCount = Math.floor(_root.width / cellWidth)
    switch (event.key) {
    case Qt.Key_Left:
      // First column.
      if (currentIndex % columnCount == 0) {
        event.accepted = true
        _root.backRequested()
      }
      break
    case Qt.Key_Right:
      // Last column.
      if (currentIndex % columnCount == columnCount - 1) {
        event.accepted = true
        //Services.SoundManager.limit.play()
      }
      break
    default:
      break
    }
  }
}
