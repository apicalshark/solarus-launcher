/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.QuestUpdates 1.0 as QuestUpdates
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles

ListView {
  signal questUpdateActionTriggered(Model.QuestUpdate questUpdate, int action)
  signal backRequested
  signal lastItemDownRequested

  id: _root
  keyNavigationWraps: false
  keyNavigationEnabled: true
  currentIndex: 0
  highlightFollowsCurrentItem: true
  highlight: null
  spacing: Math.round(Styles.Hints.spacing / 2)
  height: count * Styles.Hints.questUpdateHeight + Math.max(count - 1, 0) * spacing
  Layout.preferredHeight: height
  interactive: false

  delegate: QuestUpdates.QuestUpdateListItem {
    questUpdate: model.questUpdate
    width: ListView.view.width
    isLastElement: index === ListView.view.count - 1
    onQuestUpdateActionTriggered: function (action) {
      _root.questUpdateActionTriggered(model.questUpdate, action)
    }
    onBackRequested: _root.backRequested()
    onLastItemDownRequested: _root.lastItemDownRequested()
  }

  populate: Transition {
    NumberAnimation {
      property: "opacity"
      from: 0
      to: 1
      duration: Styles.Hints.animationDuration
      easing.type: Easing.InCubic
    }
  }
  add: Transition {
    NumberAnimation {
      property: "opacity"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  remove: Transition {
    NumberAnimation {
      property: "opacity"
      to: 0
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  displaced: Transition {
    NumberAnimation {
      properties: "x,y"
      duration: Styles.Hints.animationDuration
      easing.type: Easing.OutCubic
    }
  }
  move: displaced
  moveDisplaced: displaced
}
