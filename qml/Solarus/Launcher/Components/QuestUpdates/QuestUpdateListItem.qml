/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Components.Quests 1.0 as Quests
import Solarus.Launcher.Components.QuestUpdates 1.0 as QuestUpdates
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Shapes 1.0 as Shapes
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

Item {
  property Model.QuestUpdate questUpdate: null
  property bool isLastElement: false
  signal questUpdateActionTriggered(int action)
  signal backRequested
  signal lastItemDownRequested

  id: _root
  enabled: questUpdate != null
  implicitWidth: _layoutContent.implicitWidth
  implicitHeight: Styles.Hints.questUpdateHeight
  height: implicitHeight
  Layout.fillWidth: true
  Layout.fillHeight: false
  Layout.alignment: Qt.AlignTop

  onQuestUpdateChanged: _private.updateStackPage()

  QtObject {
    id: _private
    readonly property int currentState: _root.questUpdate
                                         ? _root.questUpdate.state
                                         : Model.QuestUpdate.State.Uninitialized

    function getComponent(state) {
      switch (state) {
        case Model.QuestUpdate.State.Available:
          return _componentDefault
        case Model.QuestUpdate.State.Pending:
          return _componentPending
        case Model.QuestUpdate.State.Downloading:
        case Model.QuestUpdate.State.Installing:
          return _componentDownloading
        case Model.QuestUpdate.State.Error:
          return _componentError
        default:
          return null
      }
    }

    function getButtonTooltip(state) {
      switch (state) {
        case Model.QuestUpdate.State.Available:
          return qsTr("Update this quest")
        case Model.QuestUpdate.State.Pending:
        case Model.QuestUpdate.State.Downloading:
        case Model.QuestUpdate.State.Installing:
          return qsTr("Cancel the update for this quest")
        case Model.QuestUpdate.State.Error:
          return qsTr("Retry updating this quest")
        default:
          return ""
      }
    }

    function getButtonIcon(state) {
      switch (state) {
        case Model.QuestUpdate.State.Available:
        case Model.QuestUpdate.State.Error:
          return Styles.Icons.refresh_16
        case Model.QuestUpdate.State.Pending:
        case Model.QuestUpdate.State.Downloading:
        case Model.QuestUpdate.State.Installing:
          return Styles.Icons.close_16
        default:
          return Styles.Icons.none
      }
    }

    function getButtonAction(state) {
      switch (state) {
        case Model.QuestUpdate.State.Available:
        case Model.QuestUpdate.State.Error:
          return QuestUpdates.QuestUpdateUtils.Action.Update
        case Model.QuestUpdate.State.Pending:
        case Model.QuestUpdate.State.Downloading:
          return QuestUpdates.QuestUpdateUtils.Action.Cancel
        default:
          return QuestUpdates.QuestUpdateUtils.Action.None
      }
    }

    function updateStackPage() {
      const nextItem = getComponent(currentState)
      if (nextItem) {
        _stackView.replace(nextItem)
      } else {
        _stackView.clear()
      }
    }
  }

  Connections {
    id: _questUpdateConnections
    target: _root.questUpdate
    enabled: _root.questUpdate != null

    function onStateChanged() {
      _private.updateStackPage()
    }
  }

  Rectangle {
    id: _background
    anchors.fill: _root
    radius: Styles.Hints.radius
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.cardBorderColor
    color: Styles.Colors.cardColor
    opacity: enabled ? 1.0 : 0.3

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  RowLayout {
    id: _layoutContent
    spacing: Styles.Hints.spacing
    anchors.fill: _root
    anchors.topMargin: Styles.Hints.spacing
    anchors.leftMargin: Styles.Hints.spacing
    anchors.rightMargin: Styles.Hints.spacing
    anchors.bottomMargin: Styles.Hints.spacing

    Quests.QuestThumbnail {
      id: _thumbnail
      questThumbnailPath: _root.questUpdate ? _root.questUpdate.thumbnailPath : ""
      showShadow: false
      implicitHeight: Styles.Hints.questUpdateThumbnailHeight
      implicitWidth: Styles.Hints.questUpdateThumbnailWidth
      height: implicitHeight
    }

    ColumnLayout {
      id: _columnLayout
      Layout.fillHeight: false
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignVCenter

      Controls.Text {
        id: _textTitle
        text: _root.questUpdate ? _root.questUpdate.title : ""
        role: Controls.Text.Role.H3
        wrapMode: Text.NoWrap
        elide: Text.ElideRight
        hoverEnabled: true
        Layout.fillWidth: true
        Layout.fillHeight: false
        Layout.alignment: Qt.AlignTop

        Controls.Tooltip {
          id: _tooltip
          text: _textTitle.text
          displayMode: Controls.Tooltip.DisplayMode.Manual
          showTooltip: _textTitle.hovered && _textTitle.width < _textTitle.implicitWidth
        }
      }

      StackView {
        id: _stackView
        initialItem: _private.getComponent(Model.QuestUpdate.State.Uninitialized)
        height: currentItem ? currentItem.implicitHeight : 0
        Layout.fillWidth: true
        Layout.fillHeight: false
        Layout.minimumHeight: height
        clip: true
        pushEnter: null
        pushExit: null
        popEnter: null
        popExit: null

        replaceEnter: Transition {
          NumberAnimation {
            property: "opacity"
            from: 0.0
            to: 1.0
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.InOutQuart
          }
        }

        replaceExit: Transition {
          NumberAnimation {
            property: "opacity"
            from: 1.0
            to: 0.0
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.InOutQuart
          }
        }

        Component {
          id: _componentDefault

          Item {
            id: _defaultLayout
            width: _stackView.width
            implicitHeight: _layoutReleaseDate.implicitHeight

            RowLayout {
              id: _layoutReleaseDate
              width: implicitWidth
              visible: _defaultLayout.width > _layoutReleaseDate.width + _layoutVersion.width
              anchors.left: _defaultLayout.left
              anchors.verticalCenter: _defaultLayout.verticalCenter

              Controls.SvgIcon {
                id: _iconCalendar
                iconId: Styles.Icons.calendar_16
                color: Styles.Colors.textColorCaption
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
              }

              Controls.Text {
                id: _textLatestReleaseDate
                text: _root.questUpdate
                        ? Qt.formatDateTime(_root.questUpdate.latestReleaseDate, "dd/MM/yyyy")
                        : qsTr("Unknown")
                wrapMode: Text.NoWrap
                Layout.minimumWidth: Math.max(_fontMetrics.advanceWidth("00/00/0000"), 100)

                FontMetrics {
                  id: _fontMetrics
                  font: parent.font
                }
              }
            }

            RowLayout {
              id: _layoutVersion
              spacing: Math.round(Styles.Hints.spacing / 4)
              width: Math.max(implicitWidth, _fontMetrics2.advanceWidth("X.XX.XX") * 2 + Math.round(spacing * 3) + Styles.Hints.iconSize * 2)
              anchors.left: _layoutReleaseDate.visible ? _layoutReleaseDate.right : _defaultLayout.left

              FontMetrics {
                id: _fontMetrics2
                font: _textCurrentVersion.font
              }

              Controls.SvgIcon {
                id: _iconVersion
                iconId: Styles.Icons.info_16
                color: Styles.Colors.textColorCaption
                Layout.rightMargin: Math.round(Styles.Hints.spacing / 4)
              }

              Controls.Text {
                id: _textCurrentVersion
                role: Controls.Text.Role.Console
                wrapMode: Text.NoWrap
                text: _root.questUpdate ? _root.questUpdate.currentVersion : qsTr("Unknown")
              }

              Controls.SvgIcon {
                id: _iconArrow
                iconId: Styles.Icons.next_16
                color: _textCurrentVersion.color
                width: Styles.Hints.iconSize * 0.8
                height: width
              }

              Controls.Text {
                id: _textLatestVersion
                role: Controls.Text.Role.Console
                wrapMode: Text.NoWrap
                text: _root.questUpdate ? _root.questUpdate.latestVersion : qsTr("Unknown")
                bold: true
              }

              Controls.LayoutSpacer { }
            }
          }
        }

        Component {
          id: _componentPending

          RowLayout {
            id: _defaultLayout
            spacing: 0
            width: _stackView.width

            Controls.Text {
              id: _textPending
              role: Controls.Text.Role.Body
              wrapMode: Text.NoWrap
              elide: Text.ElideRight
              text: qsTr("Installation pending…")
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            }
          }
        }

        Component {
          id: _componentDownloading

          RowLayout {
            id: _layoutDownloading
            width: _stackView.width
            spacing: Styles.Hints.spacing

            Controls.ProgressBar {
              id: _progressBar
              from: 0
              to: 100
              value: _root.questUpdate ? _root.questUpdate.progress : 0
              Layout.alignment: Qt.AlignVCenter
              Layout.fillWidth: true
              Layout.minimumWidth: Styles.Hints.controlWidthSmall
            }

            Controls.Text {
              id: _progressText
              text: "%1%".arg(_progressBar.value)
              horizontalAlignment: Text.AlignRight
              Layout.minimumWidth: Utils.GeometryUtils.getProgressLabelWidth(_progressBar.from,
                                                                             _progressBar.to,
                                                                             _progressFontMetrics)
              Layout.fillWidth: false
              Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

              FontMetrics {
                id: _progressFontMetrics
                font: _progressText.font
              }
            }
          }
        }

        Component {
          id: _componentError

          RowLayout {
            id: _defaultLayout
            spacing: Math.round(Styles.Hints.spacing / 2)
            width: _stackView.width

            Controls.SvgIcon {
              id: _iconError
              iconId: Styles.Icons.warning_color_16
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            }

            Controls.Text {
              id: _textErrorLabel
              role: Controls.Text.Role.Body
              wrapMode: Text.NoWrap
              text: qsTr("Error:")
              bold: true
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            }

            Controls.Text {
              id: _textErrorValue
              role: Controls.Text.Role.Body
              wrapMode: Text.NoWrap
              elide: Text.ElideRight
              text: _root.questUpdate ? String(_root.questUpdate.errorCode) : ""
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
              Layout.fillWidth: true
            }
          }
        }
      }
    }

    Controls.Button {
      id: _button
      iconId: _private.getButtonIcon(_private.currentState)
      tooltip: _private.getButtonTooltip(_private.currentState)
      visible: _private.currentState !== Model.QuestUpdate.State.Updated && _private.currentState !== Model.QuestUpdate.State.Uninitialized
      onClicked: _root.questUpdateActionTriggered(_private.getButtonAction(_private.currentState))
      Layout.leftMargin: Styles.Hints.spacing
      Keys.onLeftPressed: _root.backRequested()
      Keys.onUpPressed: Utils.EventUtils.tryFocusNextItem(_button, false)
      Keys.onDownPressed: function(event) {
        if (_root.isLastElement) {
          event.accepted = true
          _root.lastItemDownRequested()
        } else {
          Utils.EventUtils.tryFocusNextItem(_button, true)
        }
      }
    }
  }
}

