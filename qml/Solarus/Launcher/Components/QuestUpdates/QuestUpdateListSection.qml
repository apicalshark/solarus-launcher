/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.QuestUpdates 1.0 as QuestUpdates
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

Item {
  required property int section // Model.QuestUpdateListModel.UpdateSection
  signal sectionActionTriggered(int action)
  signal backRequested
  signal topRequested

  id: _root
  implicitHeight: _sectionLayout.implicitHeight + _sectionLayout.anchors.topMargin + _sectionLayout.anchors.bottomMargin
  implicitWidth: _sectionLayout.implicitWidth + _sectionLayout.anchors.leftMargin + _sectionLayout.anchors.rightMargin
  height: implicitHeight

  QtObject {
    id: _private

    function getSectionTitle(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Error:
          return qsTr("Errors")
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return qsTr("Available")
        case Model.QuestUpdateListModel.UpdateSection.Pending:
          return qsTr("Pending")
        case Model.QuestUpdateListModel.UpdateSection.Updating:
          return qsTr("Updating")
        default:
          return ""
      }
    }

    function getButtonText(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Error:
          return qsTr("Retry all")
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return qsTr("Update all")
        case Model.QuestUpdateListModel.UpdateSection.Pending:
          return qsTr("Cancel all")
        default:
          return ""
      }
    }

    function getButtonTooltip(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Error:
          return qsTr("Retry all failed Solarus quest updates")
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return qsTr("Update all Solarus quests")
        case Model.QuestUpdateListModel.UpdateSection.Pending:
          return qsTr("Cancel all pending Solarus quest updates")
        default:
          return ""
      }
    }

    function getButtonIcon(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Error:
          return Styles.Icons.refresh_16
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return Styles.Icons.refresh_16
        case Model.QuestUpdateListModel.UpdateSection.Pending:
          return Styles.Icons.close_16
        default:
          return Styles.Icons.none
      }
    }

    function getButtonRole(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return Styles.Colors.Role.Primary
        default:
          return Styles.Colors.Role.Secondary
      }
    }

    function getButtonAction(section) {
      switch (section) {
        case Model.QuestUpdateListModel.UpdateSection.Error:
          return QuestUpdates.QuestUpdateUtils.Action.Update
        case Model.QuestUpdateListModel.UpdateSection.Available:
          return QuestUpdates.QuestUpdateUtils.Action.Update
        case Model.QuestUpdateListModel.UpdateSection.Pending:
          return QuestUpdates.QuestUpdateUtils.Action.Cancel
        default:
          return QuestUpdates.QuestUpdateUtils.Action.None
      }
    }

    function getButtonVisible(section) {
      return section === Model.QuestUpdateListModel.UpdateSection.Available
        || section === Model.QuestUpdateListModel.UpdateSection.Pending
        || section === Model.QuestUpdateListModel.UpdateSection.Error
    }
  }

  RowLayout {
    id: _sectionLayout
    anchors.fill: _root
    anchors.topMargin: Styles.Hints.spacing
    anchors.bottomMargin: Styles.Hints.spacing
    anchors.rightMargin: Styles.Hints.spacing

    Controls.Text {
      id: _text
      text: _private.getSectionTitle(_root.section)
      elide: Text.ElideRight
      wrapMode: Text.NoWrap
      role: Controls.Text.Role.H2
      Layout.fillWidth: true
    }

    Controls.Button {
      id: _button
      text: _private.getButtonText(_root.section)
      role: _private.getButtonRole(_root.section)
      iconId: _private.getButtonIcon(_root.section)
      visible: _private.getButtonVisible(_root.section)
      enabled: visible
      onClicked: _root.sectionActionTriggered(_private.getButtonAction(_root.section))
      Keys.onLeftPressed: _root.backRequested()
      Keys.onDownPressed: Utils.EventUtils.tryFocusNextItem(_button, true)
      Keys.onUpPressed: function(event) {
        event.accepted = true
        _root.topRequested()
      }
      Keys.onBacktabPressed: function(event) {
        event.accepted = true
        _root.topRequested()
      }
    }
  }
}
