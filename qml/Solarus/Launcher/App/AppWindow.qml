/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Components.BottomBar 1.0 as BottomBar
import Solarus.Launcher.Components.Updates 1.0 as Updates

import Solarus.Launcher.Pages.MainPages 1.0 as MainPages

FocusScope {
  id: _root
  implicitWidth: Styles.Hints.windowDefaultWidth
  implicitHeight: Styles.Hints.windowDefaultHeight

  AppMenu {
    id: _appMenu
  }

  AppWindowStack {
    id: _stack
    anchors.fill: parent
  }

  BottomBar.BottomBar {
    id: _bottomBar
    blurSource: _stack
    z: 1
  }

  Updates.UpdateDialog {
    id: _updateDialog
  }
}
