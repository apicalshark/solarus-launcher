/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15

import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Windows 1.0 as Windows

StackView {
  readonly property bool ready: _componentMain.status == Component.Ready
                       || _componentOnboarding.status == Component.Ready
  id: _root
  initialItem: _private.getComponent($mainPagesController.navigation.currentWindow)

  QtObject {
    property int bottomBarHeight: $bottomBarController.visible ? Styles.Hints.bottomBarHeight : 0

    id: _private

    Behavior on bottomBarHeight {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }

    function getComponent(windowId) {
      switch(windowId) {
      case Model.Navigation.WindowId.OnboardingWindow: return _componentOnboarding
      case Model.Navigation.WindowId.MainWindow: return _componentMain
      default: return null
      }
    }
  }

  Connections {
    target: $mainPagesController.navigation

    function onCurrentWindowChanged() {
      const windowId = $mainPagesController.navigation.currentWindow
      const comp = _private.getComponent(windowId)
      _root.replace(comp)
    }
  }

  Component {
    id: _componentOnboarding

    Windows.OnboardingWindow {
      bottomBarHeight: _private.bottomBarHeight
    }
  }

  Component {
    id: _componentMain

    Windows.MainWindow {
      bottomBarHeight: _private.bottomBarHeight
    }
  }

  replaceExit: Transition {
    NumberAnimation {
      property: "opacity"
      from: 1
      to: 0
      duration: Styles.Hints.animationDuration
      easing.type: Easing.InOutQuart
    }

    NumberAnimation {
      property: "scale"
      from: 1
      to: 1.5
      duration: Styles.Hints.animationDuration * 2
      easing.type: Easing.InOutQuart
    }
  }

  replaceEnter: Transition {
    NumberAnimation {
      property: "opacity"
      from: 0
      to: 1
      duration: Styles.Hints.animationDuration * 4
      easing.type: Easing.InOutQuart
    }

    NumberAnimation {
      property: "scale"
      from: 0.9
      to: 1
      duration: Styles.Hints.animationDuration * 4
      easing.type: Easing.InOutQuart
    }
  }
}
