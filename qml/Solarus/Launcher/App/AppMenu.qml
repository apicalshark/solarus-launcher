/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import Qt.labs.platform 1.1 as Platform

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

Platform.MenuBar {
  id: _root

  Platform.Menu {
    id: _fileMenu
    title: qsTr("File")

    Platform.MenuItem {
      id: _checkForUpdates
      enabled: $appMenuController.checkForUpdates.enabled
      visible: $appMenuController.checkForUpdates.visible
      text: qsTr("Check For Updates")
      shortcut: $appMenuController.checkForUpdates.shortcut
      role: $appMenuController.checkForUpdates.role
      onTriggered: $appMenuController.checkForUpdates.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.update : ""
      icon.name: "system-software-update"
    }

    Platform.MenuItem {
      id: _preferences
      enabled: $appMenuController.preferences.enabled
      visible: $appMenuController.preferences.visible
      text: qsTr("Preferences…")
      shortcut: $appMenuController.preferences.shortcut
      role: $appMenuController.preferences.role
      onTriggered: $appMenuController.preferences.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.preferences : ""
      icon.name: "preferences-system"
    }

    Platform.MenuSeparator { }

    Platform.MenuItem {
      id: _quit
      enabled: $appMenuController.quit.enabled
      visible: $appMenuController.quit.visible
      text: qsTr("Quit")
      shortcut: $appMenuController.quit.shortcut
      role: $appMenuController.quit.role
      onTriggered: $appMenuController.quit.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.quit : ""
      icon.name: "application-exit"
    }
  }

  Platform.Menu {
    id: _questMenu
    title: qsTr("Quest")

    Platform.MenuItem {
      id: _playQuestMenuItem
      enabled: $appMenuController.playQuest.enabled
      visible: $appMenuController.playQuest.visible
      text: qsTr("Play")
      shortcut: $appMenuController.playQuest.shortcut
      role: $appMenuController.playQuest.role
      onTriggered: $appMenuController.playQuest.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.play : ""
      icon.name: "media-playback-start"
    }

    Platform.MenuItem {
      id: _stopQuest
      enabled: $appMenuController.stopQuest.enabled
      visible: $appMenuController.stopQuest.visible
      text: qsTr("Stop")
      shortcut: $appMenuController.stopQuest.shortcut
      role: $appMenuController.stopQuest.role
      onTriggered: $appMenuController.stopQuest.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.stop : ""
      icon.name: "media-playback-stop"
    }

    Platform.MenuSeparator { }

    Platform.MenuItem {
      id: _checkForQuestUpdates
      enabled: $appMenuController.checkForQuestUpdates.enabled
      visible: $appMenuController.checkForQuestUpdates.visible
      text: qsTr("Check for Quest Updates")
      shortcut: $appMenuController.checkForQuestUpdates.shortcut
      role: $appMenuController.checkForQuestUpdates.role
      onTriggered: $appMenuController.checkForQuestUpdates.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.update : ""
      icon.name: "system-software-update"
    }
  }

  Platform.Menu {
    id: _viewMenu
    title: qsTr("View")

    Platform.MenuItem {
      id: _fullScreen
      enabled: $appMenuController.fullScreen.enabled
      visible: $appMenuController.fullScreen.visible
      text: qsTr("Full Screen")
      shortcut: $appMenuController.fullScreen.shortcut
      role: $appMenuController.fullScreen.role
      onTriggered: $appMenuController.fullScreen.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.fullscreen : ""
      icon.name: "view-fullscreen"
    }

    Platform.MenuItem {
      id: _fullScreenQuest
      enabled: $appMenuController.fullScreenQuest.enabled
      visible: $appMenuController.fullScreenQuest.visible
      checkable: $appMenuController.fullScreenQuest.checkable
      checked: $appMenuController.fullScreenQuest.checked
      text: qsTr("Show Quest as Full Screen")
      shortcut: $appMenuController.fullScreenQuest.shortcut
      role: $appMenuController.fullScreenQuest.role
      onTriggered: $appMenuController.fullScreenQuest.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.fullscreenQuest : ""
      icon.name: "view-fullscreen"
    }
  }

  Platform.Menu {
    id: _helpMenu
    title: qsTr("Help")

    Platform.MenuItem {
      id: _changelog
      enabled: $appMenuController.changelog.enabled
      visible: $appMenuController.changelog.visible
      text: qsTr("Changelog…")
      shortcut: $appMenuController.changelog.shortcut
      role: $appMenuController.changelog.role
      onTriggered: $appMenuController.changelog.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.changelog : ""
      icon.name: "help-faq"
    }

    Platform.MenuItem {
      id: _userManual
      enabled: $appMenuController.userManual.enabled
      visible: $appMenuController.userManual.visible
      text: qsTr("User Manual…")
      shortcut: $appMenuController.userManual.shortcut
      role: $appMenuController.userManual.role
      onTriggered: $appMenuController.userManual.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.help : ""
      icon.name: "help-contents"
    }

    Platform.MenuItem {
      id: _about
      enabled: $appMenuController.about.enabled
      visible: $appMenuController.about.visible
      text: qsTr("About")
      shortcut: $appMenuController.about.shortcut
      role: $appMenuController.about.role
      onTriggered: $appMenuController.about.trigger()
      icon.source: Utils.MenuUtils.showNativeMenuIcons ? Styles.NativeMenuIcons.about : ""
      icon.name: "help-about"
    }
  }
}
