/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
//import QtGamepad 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Components.SideBar 1.0 as SideBar
import Solarus.Launcher.Controllers 1.0 as Controllers
import Solarus.Launcher.Model 1.0 as Model
import Solarus.Launcher.Pages.MainPages 1.0 as MainPages
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Translations 1.0 as Translations

FocusScope {
  property int bottomBarHeight: 0

  id: _root
  implicitWidth: Styles.Hints.windowDefaultWidth
  implicitHeight: Styles.Hints.windowDefaultHeight

  Component.onCompleted: function () {
    _private.synchronizeFromService()
  }

  Connections {
    target: $mainPagesController

    function onQuestProcessErrorRaised(errorCode) {
      if (errorCode !== Model.QuestRunner.ErrorCode.NoError) {
        _private.lastProcessError = errorCode
        _questProcessErrorDialog.open()
      }
    }
  }

  Connections {
    target: $mainPagesController.navigation

    function onCurrentPageChanged() {
      _private.synchronizeFromService()
    }

    function onSideBarFocusRequested() {
      _sideBar.focus = true
    }

    function onPageContentFocusRequested() {
      _stackView.currentItem.focus = true
      _stackView.currentItem.forceActiveFocus()
    }
  }

  QtObject {
    property int lastProcessError: Model.QuestRunner.ErrorCode.NoError

    id: _private

    function getComponent(page) {
      switch (page) {
      case Model.Navigation.PageId.Home: return _componentHome
      case Model.Navigation.PageId.LocalQuests: return _componentLocalQuests
      case Model.Navigation.PageId.OnlineQuests: return _componentOnlineQuests
      case Model.Navigation.PageId.PlayingQuest: return _componentPlayingQuest
      case Model.Navigation.PageId.QuestUpdates: return _componentQuestUpdates
      case Model.Navigation.PageId.Settings: return _componentSettings
      case Model.Navigation.PageId.About: return _componentAbout
      default: return null
      }
    }

    function route(page) {
      const comp = getComponent(page)
      _stackView.replace(comp)
    }

    function synchronizeFromService() {
      _sideBar.page = $mainPagesController.navigation.currentPage
    }

    function synchronizeToService() {
      $mainPagesController.navigation.currentPage = _sideBar.page
    }
  }

  Rectangle {
    id: _background
    anchors.fill: parent
    color: Styles.Colors.windowColor
  }

  FocusScope {
    id: _windowContent
    anchors.fill: parent

    // SideBar to navigate between pages.
    SideBar.SideBar {
      id: _sideBar
      z: 1
      bottomBarHeight: _root.bottomBarHeight
      isExpanded: _root.width > Styles.Hints.sideBarBreakpoint
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      playingQuest: $playingQuestPageController.playingQuest
      questUpdatesState: $questUpdatePageController.questUpdateListModel.state

      onPageChanged: function () {
        // Synchronize page with controller.
        _private.synchronizeToService()

        // Update stackView content.
        _private.route(_sideBar.page)
      }

      onRightRequested: function () {
        const item = _stackView.currentItem
        if (item) {
          let nextFocusItem = null
          if (item.contentItem && item.contentItem.focusStart) {
            nextFocusItem = item.contentItem.focusStart
          } else {
            nextFocusItem = _stackView.currentItem.nextItemInFocusChain(true)
          }
          if (!nextFocusItem) {
            nextFocusItem = _stackView.currentItem
          }
          nextFocusItem.forceActiveFocus()
        }
      }
    }

    StackView {
      id: _stackView
      visible: true
      anchors.left: _sideBar.right
      anchors.leftMargin: -_sideBar.shadowWidth // Compensate shadow of sidebar.
      anchors.right: parent.right
      anchors.top: parent.top
      anchors.bottom: parent.bottom
      anchors.bottomMargin: _root.bottomBarHeight
      focus: true

      onActiveFocusChanged: function () {
        if (activeFocus && currentItem) {
          currentItem.forceActiveFocus()
        }
      }

      replaceExit: Transition {
        ScriptAction {
          script: {
            Services.SoundManager.page.stop()
            Services.SoundManager.page.play()
          }
        }
        PropertyAnimation {
          property: "opacity"
          from: 1
          to: 0
          duration: Styles.Hints.animationDuration * 3
          easing.type: Easing.InOutQuart
        }

        NumberAnimation {
          property: "y"
          from: 0
          to: -_stackView.height / 4
          duration: Styles.Hints.animationDuration * 5
          easing.type: Easing.InOutQuart
        }
      }

      replaceEnter: Transition {
        PropertyAnimation {
          property: "opacity"
          from: 0
          to: 1
          duration: Styles.Hints.animationDuration * 5
          easing.type: Easing.InOutQuart
        }

        NumberAnimation {
          property: "y"
          from: _stackView.height / 2
          to: 0
          duration: Styles.Hints.animationDuration * 5
          easing.type: Easing.InOutQuart
        }
      }

      // Pages: declared but loaded only when needed.
      Component {
        id: _componentHome
        Controls.ContentLoader {
          Component {
            MainPages.HomePage {
              id: _pageHome
            }
          }
        }
      }
      Component {
        id: _componentLocalQuests
        Controls.ContentLoader {
          Component {
            MainPages.LocalQuestsPage {
              id: _pageLocalQuests
            }
          }
        }
      }
      Component {
        id: _componentOnlineQuests
        Controls.ContentLoader {
          Component {
            MainPages.OnlineQuestsPage {
              id: _pageOnlineQuests
            }
          }
        }
      }
      Component {
        id: _componentPlayingQuest
        Controls.ContentLoader {
          Component {
            MainPages.PlayingQuestPage {
              id: _pagePlayingQuest
            }
          }
        }
      }
      Component {
        id: _componentQuestUpdates
        Controls.ContentLoader {
          Component {
            MainPages.QuestUpdatePage {
              id: _pageQuestUpdates
            }
          }
        }
      }
      Component {
        id: _componentSettings
        Controls.ContentLoader {
          Component {
            MainPages.SettingsPage {
              id: _pageSettings
            }
          }
        }
      }
      Component {
        id: _componentAbout
        Controls.ContentLoader {
          Component {
            MainPages.AboutPage {
              id: _pageAbout
            }
          }
        }
      }
    }
  }

  Controls.Dialog {
    id: _questProcessErrorDialog
    title: qsTr("An Error Happened")
    iconId: Styles.Icons.warning_color_16
    text: Translations.Common.processErrorToString(_private.lastProcessError)
    standardButtons: Dialog.Ok
    onClosed: _private.lastProcessError = Model.QuestRunner.ErrorCode.NoError
  }
}
