/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import Solarus.Launcher.Components.Onboarding 1.0 as Onboarding
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Pages.OnboardingPages 1.0 as OnboardingPages
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Model 1.0 as Model

FocusScope {
  signal onboardingFinished
  property int bottomBarHeight: 0
  readonly property alias page: _private.page
  readonly property int pages: _private.components.length
  id: _root

  QtObject {
    property var components: [_componentWelcomePage, _componentControlsPage, _componentFolderChoicePage, _componentFolderLoadingPage, _componentFinishPage]
    property int page: 0
    property int previousPage: 0
    property bool blockNextNavigation: false
    id: _private

    onPageChanged: function () {
      // Update stackView content.
      if (page == _private.previousPage + 1) {
        _private.previousPage = page
        _private.blockNextNavigation = false // Reset
        var component = page < _private.components.length ? _private.components[page] : null
        if (component) {
          _stackView.push(component)
        }
      } else if (page == _private.previousPage - 1) {
        _private.previousPage = page
        _private.blockNextNavigation = false // Reset
        _stackView.pop()
      }
    }

    function goToPreviousPage() {
      if (_root.pages <= 1) return
      _private.page = Math.max(0, _root.page - 1)
    }

    function goToNextPage() {
      if (_root.pages <= 1) return
      _private.page = (_root.page + 1) % _root.pages
    }
  }

  Keys.onEscapePressed: {
    _private.goToPreviousPage()
  }

  Onboarding.OnboardingBackground {
    id: _background
    anchors.fill: parent
  }

  GridLayout {
    id: _layout
    width: Math.min(_root.width, 900) - Styles.Hints.spacing * 4
    height: _root.height - Styles.Hints.spacing * 4
    anchors.horizontalCenter: _root.horizontalCenter
    anchors.verticalCenter: _root.verticalCenter
    anchors.bottomMargin: _root.bottomBarHeight
    columns: 3
    rows: 2
    columnSpacing: Styles.Hints.spacing * 2

    // Indicates which page we are on.
    Onboarding.OnboardingStepper {
      id: _stepper
      steps: _root.pages
      step: _root.page
      Layout.row: 0
      Layout.column: 1
      Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
      Layout.maximumWidth: Styles.Hints.controlDefaultWidth * 2
      Layout.minimumWidth: Styles.Hints.spacing * 3
    }

    // Go to previous page.
    Controls.CircleButton {
      id: _buttonPrevious
      Layout.column: 0
      Layout.row: 1
      iconId: Styles.Icons.previous_32
      enabled: _root.page > 0 && _root.page < _root.pages - 1
      hidden: !enabled
      onClicked: _private.goToPreviousPage()
      // KeyNavigation doesn't seem to work, maybe because of scope issues.
      Keys.onRightPressed: {
        const nextFocus = nextItemInFocusChain(true)
        if (nextFocus) {
          nextFocus.forceActiveFocus()
        }
      }
    }

    // Pages.
    Onboarding.OnboardingPageStack {
      id: _stackView
      clip: true
      initialItem: _componentWelcomePage
      Layout.row: 1
      Layout.column: 1
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.bottomMargin: _root.bottomBarHeight

      Component {
        id: _componentWelcomePage
        OnboardingPages.WelcomePage {
          id: _pageWelcome
          StackView.onStatusChanged: {
            if (StackView.status === StackView.Active) {
              _buttonNext.forceActiveFocus()
            }
          }
        }
      }
      Component {
        id: _componentControlsPage
        OnboardingPages.ControlsPage {
          id: _pageControls
        }
      }
      Component {
        id: _componentFolderChoicePage
        OnboardingPages.FolderChoicePage {
          id: _pageFolderChoice
        }
      }
      Component {
        id: _componentFolderLoadingPage
        OnboardingPages.FolderLoadingPage {
          id: _pageFolderLoading
          onFinished: {
            _private.blockNextNavigation = false
          }
          onBackRequested: {
            _private.page = Math.max(0, _root.page - 1)
          }
          StackView.onStatusChanged: {
            if (StackView.status === StackView.Activating) {
              _private.blockNextNavigation = true
            } else if (StackView.status === StackView.Inactive) {
              _private.blockNextNavigation = false
            }
          }
        }
      }
      Component {
        id: _componentFinishPage
        OnboardingPages.FinishPage {
          id: _pageFinish
        }
      }
    }

    // Go to next page.
    Controls.CircleButton {
      id: _buttonNext
      Layout.column: 2
      Layout.row: 1
      iconId: Styles.Icons.next_32
      enabled: _root.page < _root.pages && !_private.blockNextNavigation
      hidden: !enabled && !_private.blockNextNavigation
      focus: true
      onClicked: {
        if (_root.page >= _root.pages - 1) {
          $onboardingPagesController.onboardingDone = true
          _root.onboardingFinished()
        } else {
          _private.goToNextPage()
        }
      }
      // KeyNavigation doesn't seem to work, maybe because of scope issues.
      Keys.onLeftPressed: {
        const nextFocus = nextItemInFocusChain(false)
        if (nextFocus) {
          nextFocus.forceActiveFocus()
        }
      }
    }
  }

  Model.Gamepad {
    id: _gamepad
    actions: [
      Model.GamepadAction {
        text: qsTr("Select")
        button: Model.GamepadAction.Button.A
      },
      Model.GamepadAction {
        text: qsTr("Cancel")
        button: Model.GamepadAction.Button.B
      }
    ]
  }

  Component.onCompleted: {
    $bottomBarController.gamepadManager.gamepad = _gamepad
  }
}
