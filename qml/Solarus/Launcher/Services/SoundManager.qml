/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtMultimedia 5.15
import QtQuick.Window 2.15

//import Solarus.Launcher.Model 1.0 as Model

QtObject {
  id: _root
  readonly property int volume: $settingsPageController.settings.appAudioVolume
  readonly property bool enabled: $settingsPageController.settings.appEnableAudio
  readonly property real linearVolume: enabled ? getLinearVolume(_root.volume) : 0.0

  readonly property SoundEffect click: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/click.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect close: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/close.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect focus: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/focus.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect limit: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/limit.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect open: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/open.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect page: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/page.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }
  readonly property SoundEffect slider: SoundEffect {
    source: "qrc:/solarus/launcher/resources/sounds/slider.wav"
    volume: _root.linearVolume
    muted: !_root.enabled
  }

  // Gets the linear volume (in the range [0.0;1.0])
  // from the logarithmic volume (in the range [0;100]).
  function getLinearVolume(volume) {
    const normalizedVol = Math.max(0, Math.min(100, volume)) / 100.
    return QtMultimedia.convertVolume(normalizedVol,
                                      QtMultimedia.LogarithmicVolumeScale,
                                      QtMultimedia.LinearVolumeScale)
  }
}
