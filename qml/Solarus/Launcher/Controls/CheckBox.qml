/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQml 2.15
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

// @disable-check M129
T.CheckBox {
  property int radius: Styles.Hints.radius

  id: _root
  font: Styles.Fonts.primaryFont
  spacing: Styles.Hints.spacing / 2
  height: implicitHeight
  width: implicitWidth
  implicitHeight: Styles.Hints.controlHeight
  implicitWidth: text.length > 0 ? _text.implicitWidth : _indicatorBox.implicitWidth
  bottomPadding: 0
  topPadding: 0
  leftPadding: 0
  rightPadding: 0
  hoverEnabled: Styles.Hints.hoverEnabled
  focusPolicy: Qt.StrongFocus
  autoRepeat: false
  Keys.onPressed: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)
  Keys.onReleased: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)
  onPressed: Services.SoundManager.focus.play()
  onClicked: Services.SoundManager.click.play()

  indicator: Item {
    id: _indicatorBox
    implicitWidth: Math.floor(Styles.Hints.controlHeight - Styles.Hints.radius - 4)
    implicitHeight: implicitWidth
    x: _root.leftPadding
    y: Math.floor((parent.height - height) / 2)

    Utils.ButtonShape {
      id: _indicator
      anchors.fill: parent
      anchors.margins: _root.down ? -Styles.Hints.pressDelta : 0
      radius: _root.down ? _root.radius + Styles.Hints.pressDelta / 2 : _root.radius
      checkable: true
      checked: _root.checked
      hovered: _root.hovered
      down: _root.down
      enabled: _root.enabled
      role: _root.checked ? Styles.Colors.Role.Primary : Styles.Colors.Role.Secondary

      Controls.SvgIcon {
        id: _icon
        opacity: _root.checked ? 1 : 0
        scale: _root.checked ? 1 : 0
        anchors.centerIn: parent
        iconId: Styles.Icons.check_16
        color: Styles.Colors.getControlForeground(_root.enabled)
        width: Styles.Hints.iconSize * 1.5
        height: Styles.Hints.iconSize * 1.5
        Layout.alignment: Qt.AlignCenter

        Behavior on scale {
          enabled: _root.enabled
          NumberAnimation {
            duration: Styles.Hints.animationDuration * 2
            easing.type: _root.checked ? Easing.OutQuart : Easing.OutBack
            easing.overshoot: 4
          }
        }
        Behavior on opacity {
          enabled: _root.enabled
          NumberAnimation {
            duration: Styles.Hints.animationDuration
            easing.type: Easing.OutQuart
          }
        }
      }
    }

    Controls.FocusBorder {
      id: _focusBorder
      anchors.fill: parent
      showFocus: _root.visualFocus
      animateBorder: !_root.down
      z: 1
    }
  }

  contentItem: Text {
    id: _text
    anchors.fill: parent
    color: Styles.Colors.getControlForeground(_root.enabled)
    text: _root.text
    font: _root.font
    elide: Text.ElideRight
    horizontalAlignment: Text.AlignLeft
    verticalAlignment: Text.AlignVCenter
    renderType: Styles.Fonts.getRenderType(font.pixelSize)
    leftPadding: _root.indicator.width + _root.spacing
    //width: _root.width - _indicator.width
    visible: _root.text.length > 0
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
