/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as Templates

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

Templates.ItemDelegate {
  id: _root
  implicitHeight: Styles.Hints.menuItemHeight
  width: ListView.view ? ListView.view.width : implicitWidth
  font: Styles.Fonts.primaryFont
  topPadding: 0
  bottomPadding: 0
  leftPadding: Styles.Hints.spacing / 2
  rightPadding: Styles.Hints.spacing / 2
  hoverEnabled: Styles.Hints.hoverEnabled
  spacing: Styles.Hints.spacing / 3
  highlighted: ListView.isCurrentItem

  onClicked: if (ListView.view) ListView.view.currentIndex = index

  indicator: Controls.SvgIcon {
    id: _icon
    x: _root.leftPadding
    iconId: Styles.Icons.check_16
    color: Styles.Colors.getControlForeground(_root.enabled)
    anchors.verticalCenter: _root.verticalCenter
    enabled: false
    visible: _root.checkable && _root.checked
    opacity: _root.checkable && _root.checked ? 1 : 0
  }

  contentItem: Text {
    id: _text
    text: _root.text
    font: _root.font
    renderType: Styles.Fonts.getRenderType(font.pixelSize)
    height: parent.height
    color: Styles.Colors.getControlForeground(_root.enabled)
    horizontalAlignment: Text.AlignLeft
    verticalAlignment: Text.AlignVCenter
    elide: Text.ElideRight
  }

  background: Rectangle {
    id: _background
    visible: _root.enabled && (_root.highlighted || _root.down || _root.hovered)
    color: Styles.Colors.getListItemBackground(_root.highlighted, _root.hovered, _root.down)
    radius: Styles.Hints.radius - 2

    Behavior on color {
      enabled: _root.enabled
      ColorAnimation {
        duration: Styles.Hints.animationDuration / 2
        easing.type: Easing.OutCubic
      }
    }
  }
}
