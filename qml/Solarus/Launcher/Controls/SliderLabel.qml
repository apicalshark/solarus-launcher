/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles

Text {
  property double value: 0.0
  property var valueToText: (v) => Number(v * 100).toFixed(0)
  property double from: 0.0
  property double to: 1.0
  readonly property int fixedWidth: _private.maxWidth(from, to)

  id: _root
  font: Styles.Fonts.primaryFont
  renderType: Styles.Fonts.getRenderType(font.pixelSize)
  text: valueToText && valueToText instanceof Function ? valueToText(value) : ""
  wrapMode: Text.NoWrap
  color: Styles.Colors.textColorBody
  horizontalAlignment: Text.AlignRight
  verticalAlignment: Text.AlignVCenter
  height: Styles.Hints.controlHeight
  width: fixedWidth

  QtObject {
    id: _private

    function maxWidth(from, to) {
      const fromText = _root.valueToText(from)
      const toText = _root.valueToText(to)
      const fromWidth = Math.ceil(_fontMetrics.boundingRect(fromText).width)
      const toWidth = Math.ceil(_fontMetrics.boundingRect(toText).width)
      return Math.max(fromWidth, toWidth)
    }
  }

  FontMetrics {
    id: _fontMetrics
    font: _root.font
  }
}
