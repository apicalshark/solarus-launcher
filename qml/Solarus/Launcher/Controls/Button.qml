/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

// @disable-check M129
T.Button {
  property alias iconId: _icon.iconId
  property alias iconColor: _icon.color
  property int radius: Styles.Hints.radius
  property int role: Styles.Colors.Role.Secondary
  property alias tooltip: _tooltip.text
  property alias shadowEnabled: _background.shadowEnabled
  property bool hidden: false
  property alias iconWidth: _icon.width
  property alias iconHeight: _icon.height
  property alias focusRadius: _focusBorder.radius
  property bool showBackgroundWhenIdle: true

  id: _root
  font: Styles.Fonts.buttonFont
  height: implicitHeight
  implicitHeight: Styles.Hints.controlHeight
  implicitWidth: _root.text ? Math.ceil(_contentLayout.implicitWidth + leftPadding + rightPadding)
                            : Math.round(Styles.Hints.controlHeight * 1.25)
  bottomPadding: 0
  topPadding: 0
  leftPadding: Styles.Hints.spacing
  rightPadding: Styles.Hints.spacing
  spacing: Styles.Hints.spacing / 2
  text: ""
  hoverEnabled: Qt.styleHints.useHoverEffects
  focusPolicy: Qt.StrongFocus
  autoRepeat: false
  scale: _root.hidden ? 0.0 : 1.0
  opacity: _root.hidden ? 0.0 : 1.0
  Keys.onPressed: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)
  Keys.onReleased: Utils.EventUtils.blockAutoRepeat(event, _root.autoRepeat)
  onPressed: Services.SoundManager.focus.play()
  onClicked: Services.SoundManager.click.play()

  background: Utils.ButtonShape {
    id: _background
    anchors.fill: _root
    anchors.margins: _root.down ? -Styles.Hints.pressDelta : 0
    radius: _root.down ? _root.radius + Styles.Hints.pressDelta / 2 : _root.radius
    role: _root.role
    down: _root.down
    hovered: _root.hovered
    enabled: _root.enabled
    checkable: _root.checkable
    checked: _root.checked
    showBackgroundWhenIdle: _root.showBackgroundWhenIdle
  }

  contentItem: Item {
    id: _contentItem
    anchors.fill: _root
    anchors.leftMargin: _root.text ? _root.leftPadding : 0
    anchors.rightMargin: _root.text ? _root.rightPadding : 0
    anchors.topMargin: _root.topPadding
    anchors.bottomMargin: _root.bottomPadding

    RowLayout {
      id: _contentLayout
      anchors.centerIn: parent
      spacing: _root.spacing

      Controls.SvgIcon {
        id: _icon
        color: Styles.Colors.getControlForeground(_root.enabled)
        width: Styles.Hints.iconSize
        height: Styles.Hints.iconSize
        Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
        visible: status != Image.Null
      }

      Text {
        id: _text
        color: Styles.Colors.getControlForeground(_root.enabled)
        elide: Text.ElideRight
        font: _root.font
        horizontalAlignment: Text.AlignHCenter
        renderType: Styles.Fonts.getRenderType(font.pixelSize)
        text: _root.text
        verticalAlignment: Text.AlignVCenter
        visible: _root.text.length > 0
        Layout.alignment: Qt.AlignVCenter
        Layout.fillWidth: true
        Layout.minimumWidth: Styles.Hints.controlHeight * 1.25
      }
    }
  }

  Behavior on scale {
    NumberAnimation {
      duration: Styles.Hints.animationDuration * 2
      easing.type: Easing.OutBack
    }
  }

  Behavior on opacity {
    NumberAnimation {
      duration: Styles.Hints.animationDuration * 2
      easing.type: Easing.OutCubic
    }
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: _background
    showFocus: _root.visualFocus || _root.activeFocus
    animateBorder: !_root.down
    z: 1
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  Controls.Tooltip {
    id: _tooltip
  }
}
