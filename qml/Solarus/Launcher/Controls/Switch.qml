/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles

// @disable-check M129
T.Switch {
  id: _root
  text: ""
  font: Styles.Fonts.primaryFont
  implicitWidth: Styles.Hints.sliderHandleSize * 2 + spacing + Math.ceil(contentItem.implicitWidth)
  implicitHeight: Styles.Hints.controlHeight
  spacing: Styles.Hints.spacing / 2
  autoRepeat: false
  hoverEnabled: Qt.styleHints.useHoverEffects

  indicator: Rectangle {
    id: _groove
    implicitWidth: Styles.Hints.sliderHandleSize * 2
    implicitHeight: Styles.Hints.sliderHandleSize - Styles.Hints.radius
    x: 0
    y: (parent.height - height) / 2
    radius: height / 2
    color: Styles.Colors.getSwitchGroove(_root.hovered, _root.down, _root.enabled)
    layer.enabled: visible && _root.enabled
    layer.effect: InnerShadow {
      horizontalOffset: 0
      verticalOffset: Styles.Hints.borderWidth
      radius: Styles.Hints.borderWidth
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColorLighter
    }

    Behavior on color {
      enabled: _root.enabled

      ColorAnimation {
        duration: Styles.Hints.animationDuration / 2
        easing.type: Easing.InOutQuad
      }
    }

    Rectangle {
      id: _rectValue
      width: checked ? parent.width : Styles.Hints.radius * 2
      height: parent.height
      color: Styles.Colors.getSwitchTrack(_root.hovered, _root.down, _root.enabled)
      radius: height / 2
      opacity: width <= Styles.Hints.radius * 2 ? 0 : 1

      Behavior on width {
        NumberAnimation {
          duration: Styles.Hints.animationDuration / 2
          easing.type: Easing.InOutQuad
        }
      }

      Behavior on color {
        enabled: _root.enabled

        ColorAnimation {
          duration: Styles.Hints.animationDuration / 2
          easing.type: Easing.InOutQuad
        }
      }
    }
  }

  contentItem: Item {
    implicitWidth: Math.max(_textOn.implicitWidth, _textOff.implicitWidth)
    anchors.top: _root.top
    anchors.bottom: _root.bottom
    anchors.left: _root.indicator.right
    anchors.leftMargin: _root.spacing

    Text {
      id: _textOn
      text: qsTr("On")
      font: _root.font
      anchors.fill: parent
      color: Styles.Colors.getControlForeground(_root.enabled)
      horizontalAlignment: Text.AlignLeft
      verticalAlignment: Text.AlignVCenter
      elide: Text.ElideRight
      visible: _root.checked
      renderType: Styles.Fonts.getRenderType(font.pixelSize)

    }

    Text {
      id: _textOff
      text: qsTr("Off")
      font: _root.font
      anchors.fill: parent
      color: Styles.Colors.getControlForeground(_root.enabled)
      horizontalAlignment: Text.AlignLeft
      verticalAlignment: Text.AlignVCenter
      elide: Text.ElideRight
      visible: !_root.checked
      renderType: Styles.Fonts.getRenderType(font.pixelSize)
    }
  }

  Keys.onPressed: {
    if (event.isAutoRepeat && event.key === Qt.Key_Space) {
      event.accepted = true
    } else if (event.key === Qt.Key_Down || event.key === Qt.Key_Up || event.key === Qt.Key_Left || event.key === Qt.Key_Right) {
      event.accepted = false
    }
  }

  Keys.onReleased: {
    if (event.isAutoRepeat && event.key === Qt.Key_Space) {
      event.accepted = true
    } else if (event.key === Qt.Key_Down || event.key === Qt.Key_Up || event.key === Qt.Key_Left || event.key === Qt.Key_Right) {
      event.accepted = false
    }
  }

  onDownChanged: {
    // Disable/enable animation first.
    _xBehavior.enabled = !down
  }

  onPressed: Services.SoundManager.focus.play()
  onClicked: Services.SoundManager.click.play()

  Rectangle {
    property int deltaPress: 2

    id: _handle
    x: _root.checked ? (_root.down ? parent.indicator.width - width + deltaPress / 2 : parent.indicator.width - width)
                     : (_root.down ? -deltaPress / 2 : 0)
    y: (parent.height - height) / 2
    width: _root.down ? Styles.Hints.sliderHandleSize + deltaPress : Styles.Hints.sliderHandleSize
    height: width
    radius: height / 2
    color: Styles.Colors.getSwitchHandle(_root.hovered, _root.down, _root.enabled, _root.checked)
    opacity: 1.0
    layer.enabled: _root.enabled
    layer.effect: DropShadow {
      visible: _root.enabled
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: Styles.Hints.borderWidth
      radius: Styles.Hints.borderWidth * 4
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColor
    }

    Behavior on x {
      id: _xBehavior
      NumberAnimation {
        duration: _root.Styles.Hints.animationDuration
        easing.type: Easing.InOutQuad
      }
    }

    Behavior on color {
      enabled: _root.enabled
      ColorAnimation {
        duration: _root.Styles.Hints.animationDuration
        easing.type: Easing.InOutQuad
      }
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  Controls.FocusBorder {
    id: _focusBorder
    showFocus: _root.visualFocus || _root.activeFocus
    animateBorder: !_root.down
    anchors.fill: parent
    anchors.leftMargin: -Styles.Hints.focusBorderWidth * 4
    anchors.rightMargin: -Styles.Hints.focusBorderWidth * 4
  }
}
