/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T

import Solarus.Launcher.Styles 1.0 as Styles

// @disable-check M129
T.BusyIndicator {
  property int thickness: Math.max(4, Math.round(height / 8))
  property color color: Styles.Colors.primaryColorLighter
  property int animationDuration: 5000

  id: _root
  implicitWidth: implicitContentWidth
  implicitHeight: implicitContentHeight

  contentItem: Item {
    id: _contentItem
    implicitWidth: Math.floor(Styles.Hints.controlHeight * 1.5)
    implicitHeight: implicitWidth
    opacity: _root.visible && _root.running ? 1.0 : 0.0

    Behavior on opacity {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.InOutQuad
      }
    }

    RotationAnimator {
      target: _contentItem
      running: _root.visible && _root.running
      from: 0
      to: 360
      loops: Animation.Infinite
      duration: _root.animationDuration
      easing.type: Easing.InExpo
    }

    Repeater {
      id: _repeater
      model: 3

      Item {
        id: _circleWrapper
        width: parent.width
        height: parent.height

        function getEasingType(index) {
          switch (index) {
            case 0: return Easing.OutExpo
            case 1: return Easing.OutCubic
            case 2: return Easing.OutSine
            default: return Easing.OutExpo
          }
        }

        OpacityAnimator {
          target: _circleWrapper
          running: index > 0 && _root.visible && _root.running
          from: 1.0
          to: 0.25
          loops: Animation.Infinite
          duration: _root.animationDuration
          easing.type: getEasingType(index)
        }

        RotationAnimator {
          target: _circleWrapper
          running: _root.visible && _root.running
          from: 0
          to: 360
          loops: Animation.Infinite
          duration: _root.animationDuration
          easing.type: getEasingType(index)
        }

        Rectangle {
          id: _circle
          x: _circleWrapper.width / 2 - width / 2
          y: _circleWrapper.height / 2 - height / 2
          implicitWidth: _root.thickness * 1.5
          implicitHeight: _root.thickness * 1.5
          radius: _root.thickness
          opacity: 1 - Math.pow(index / _repeater.count, 1 / 2)
          color: _root.color
          transform: [
            Translate {
              y: -Math.min(_circleWrapper.width, _circleWrapper.height) / 2 + _root.thickness
            }
          ]
        }
      }
    }
  }
}
