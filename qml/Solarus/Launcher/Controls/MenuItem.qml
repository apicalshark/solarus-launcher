/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Controls 1.0 as Controls

// @disable-check M129
T.MenuItem {
  property string shortcut: action && action.shortcut ? action.shortcut : ""
  property string iconId: ""

  id: _root
  implicitHeight: visible ? Styles.Hints.menuItemHeight : (-ListView.view ? ListView.view.spacing : 0)
  implicitWidth: Math.ceil(leftPadding + implicitContentWidth + rightPadding
                   + (indicator ? indicator.width + spacing : 0)
                   + (arrow && arrow.visible ? arrow.width + spacing : 0))
  font: Styles.Fonts.primaryFont
  topPadding: 0
  bottomPadding: 0
  leftPadding:  Math.round(Styles.Hints.spacing / 2)
  rightPadding:  Math.round(Styles.Hints.spacing / 2)
  hoverEnabled: true
  spacing:  Math.round(Styles.Hints.spacing / 3)
  onHighlightedChanged: {
    if (highlighted) {
      Services.SoundManager.focus.stop()
      Services.SoundManager.focus.play()
    }
  }
  onPressed: Services.SoundManager.focus.play()
  onClicked: Services.SoundManager.click.play()

  onSubMenuChanged: {
    if (subMenu) {
      subMenu.overlap = Math.round(Styles.Hints.spacing / 2)
    }
  }

  arrow: Controls.SvgIcon {
    id: _arrow
    x: _root.width - _root.rightPadding - width
    y: Math.round((_root.height - height) / 2)
    iconId: Styles.Icons.right_16
    color: Styles.Colors.getControlForeground(_root.enabled)
    //anchors.verticalCenter: _root.verticalCenter
    enabled: false
    visible: _root.subMenu
    opacity: _root.subMenu ? 1 : 0
  }

  indicator: Controls.SvgIcon {
    x: _root.leftPadding
    y: Math.round((_root.height - height) / 2)
    iconId: _root.checkable ? Styles.Icons.check_16 : _root.iconId
    color:  _root.checkable ? Styles.Colors.getControlForeground(_root.enabled)
                            : Styles.Colors.getMenuItemText(_root.enabled)
    enabled: _root.checkable ? false : _root.enabled
    visible: _root.checkable ? _root.checked : iconId
    opacity: _root.checkable ? (_root.checked ? 1 : 0) : 1

    Behavior on opacity {
      NumberAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  contentItem: RowLayout {
    id: _contentLayout
    anchors.left: undefined // _root.left
    anchors.right: undefined //_root.right
    anchors.margins: 0
    spacing: Styles.Hints.spacing

    Text {
      id: _text
      text: _root.text
      font: _root.font
      renderType: Styles.Fonts.getRenderType(font.pixelSize)
      height: parent.height
      color: Styles.Colors.getMenuItemText(enabled)
      horizontalAlignment: Text.AlignLeft
      verticalAlignment: Text.AlignVCenter
      elide: Text.ElideRight
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
      Layout.leftMargin: _root.indicator ? _root.indicator.width + _root.spacing : 0
    }

    Text {
      id: _textShortcut
      visible: _root.shortcut.length > 0
      text: _root.shortcut
      font: _root.font
      renderType: Styles.Fonts.getRenderType(font.pixelSize)
      height: parent.height
      elide: Text.ElideRight
      horizontalAlignment: Text.AlignRight
      verticalAlignment: Text.AlignVCenter
      color: Styles.Colors.getMenuItemShortcut(_root.hovered || _root.highlighted, _root.pressed, enabled)
      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
      Layout.minimumWidth: implicitWidth
    }
  }

  background: Rectangle {
    id: _background
    visible: _root.enabled && (_root.highlighted || _root.down)
    color: Styles.Colors.getMenuItemBackground(_root.highlighted, _root.down, _root.enabled)
    radius: Styles.Hints.radius - 2 * Styles.Hints.borderWidth

    Behavior on color {
      enabled: _root.enabled
      ColorAnimation {
        duration: Styles.Hints.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }
}
