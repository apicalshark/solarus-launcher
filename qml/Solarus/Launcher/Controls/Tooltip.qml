/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls
import QtGraphicalEffects 1.15

import Solarus.Launcher.Styles 1.0 as Styles

QtControls.ToolTip {
  enum DisplayMode {
    Auto,
    Manual
  }

  property int gap: Styles.Hints.spacing * 0.375
  property Item target: parent
  property bool hideOnPress: true
  property bool showTooltip: false
  property int displayMode: Tooltip.DisplayMode.Auto

  QtObject {
    id: _private
    property double deltaY: 0
    property bool shouldShowTooltip: false

    onShouldShowTooltipChanged: {
      if (shouldShowTooltip) {
        _root.open()
      } else if (!_root.showTooltip) {
        _root.close()
      }
    }
  }

  id: _root
  enabled: false
  font: Styles.Fonts.primaryFont
  margins: Styles.Hints.spacing
  delay: Styles.Hints.tooltipDelay
  timeout: -1
  x: target ? Math.round((target.width - implicitWidth) / 2) : 0
  y: target ? -implicitHeight - gap + _private.deltaY : 0
  z: 100

  onShowTooltipChanged: {
    if (showTooltip) {
      _root.open()
    } else {
      _root.close()
    }
  }

  contentItem: Text {
    id: _tooltipText
    text: _root.text
    font: _root.font
    color: Styles.Colors.tooltipTextColor
    renderType: Styles.Fonts.getRenderType(font.pixelSize)
    leftPadding: Styles.Hints.spacing / 2
    rightPadding: Styles.Hints.spacing / 2
    topPadding: 0
    bottomPadding: 0
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    width: _root.width
    wrapMode: Text.Wrap
  }

  background: Rectangle {
    color: Styles.Colors.tooltipColor
    radius: Styles.Hints.radius
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.tooltipBorderColor
    layer.enabled: true
    layer.effect: DropShadow {
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: Styles.Hints.borderWidth * 4
      radius: Styles.Hints.borderWidth * 12
      samples: (radius * 2) + 1
      color: Styles.Colors.tooltipShadowColor
    }
  }

  enter: Transition {
    ParallelAnimation {
      NumberAnimation {
        target: _private
        property: "deltaY"
        from: gap
        to: 0.0
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.InOutCubic
      }

      NumberAnimation {
        property: "opacity"
        from: 0.0
        to: 1.0
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.InOutCubic
      }
    }
  }

  exit: Transition {
    NumberAnimation {
      property: "opacity"
      to: 0.0
      duration: Styles.Hints.animationDuration
      easing.type: Easing.InOutCubic
    }
  }

  Timer {
    id: _timer
    interval: _root.delay
    repeat: false

    onTriggered: {
      if (_root.displayMode !== Tooltip.DisplayMode.Auto) return

      if (_root.target.hovered && !_root.target.down && _root.text) {
        _private.shouldShowTooltip = true
      }
    }
  }

  onClosed: {
    _timer.stop()
    if (_private.shouldShowTooltip) {
      _private.shouldShowTooltip = false
    }
  }

  onOpened: {
    if (!_private.shouldShowTooltip) {
      _private.shouldShowTooltip = true
    }
  }

  Connections {
    id: _targetConnections
    target: _root.displayMode === Tooltip.DisplayMode.Auto ? _root.target : null
    enabled: _root.displayMode === Tooltip.DisplayMode.Auto

    function onHoveredChanged() {
      if (_root.target.hovered && _root.text) {
        _timer.start()
      } else {
        _private.shouldShowTooltip = false
      }
    }

    function onDownChanged() {
      if (_root.hideOnPress && _root.target.down) {
        _private.shouldShowTooltip = false
        _root.close()
      }
    }
  }
}
