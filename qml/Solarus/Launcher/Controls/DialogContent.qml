/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

FocusScope {
  property string title: "Title"
  property string text: "Content"
  property string iconId: Styles.Icons.mouse_64
  property real progressValue: 0.5
  property bool progressVisible: false

  id: _root
  implicitWidth: _layout.width
  implicitHeight: _layout.height

  ColumnLayout {
    id: _layout
    width: 400
    height: _layout.implicitHeight
    spacing: Styles.Hints.spacing * 2

    Controls.SvgImage {
      id: _icon
      visible: _root.iconId.length > 0
      source: _root.iconId
      Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
      height: Styles.Hints.iconSize * 6
      width: height

      layer.enabled: true
      layer.effect: DropShadow {
        transparentBorder: true
        horizontalOffset: 0
        verticalOffset: 2
        radius: 16
        samples: (radius * 2) + 1
        color: Styles.Colors.shadowColor
      }
    }

    Controls.Text {
      id: _title
      role: Controls.Text.Role.H1
      text: _root.title
      horizontalAlignment: Text.AlignHCenter
      visible: text
      Layout.fillWidth: true
      //Layout.preferredHeight: implicitHeight
    }

    Controls.Text {
      id: _text
      role: Controls.Text.Role.Body
      text: _root.text
      horizontalAlignment: Text.AlignHCenter
      visible: text
      Layout.fillWidth: true
      //Layout.preferredHeight: implicitHeight
    }

    Controls.ProgressBar {
      id: _progressBar
      value: _root.progressValue
      Layout.fillWidth: true
      visible: _root.progressVisible
    }

    Controls.LayoutSpacer {
      Layout.minimumHeight: Styles.Hints.spacing
    }
  }
}
