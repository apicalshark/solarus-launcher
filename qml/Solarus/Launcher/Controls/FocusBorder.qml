/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Services 1.0 as Services

Item {
  property bool showFocus: false
  property int borderWidth: Styles.Hints.focusBorderWidth
  property bool animateBorder: true
  property alias radius: _background.radius

  id: _root
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: Styles.Hints.controlHeight
  visible: false
  opacity: 0

  QtObject {
    id: _private
    property real transitionMargin: 0
    property real animationMargin: -Math.floor(Styles.Hints.focusBorderWidth / 2)

    SequentialAnimation on animationMargin {
      id: _anchorsAnimation
      loops: Animation.Infinite
      running: _root.showFocus && _root.animateBorder

      NumberAnimation {
        to: 0
        duration: Styles.Hints.animationDuration * 4
        easing.type: Easing.OutQuart
      }

      NumberAnimation {
        to: -Math.floor(Styles.Hints.focusBorderWidth / 2)
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.OutQuart
      }
    }
  }

  Rectangle {
    id: _background
    anchors.fill: parent
    anchors.margins: -(_private.transitionMargin + _private.animationMargin + border.width)
    border.width: 0
    border.color: Styles.Colors.primaryColorLighter
    color: "transparent"
    radius: Styles.Hints.radius + border.width

    SequentialAnimation on border.color {
      id: _backgroundAnimation
      loops: Animation.Infinite
      running: _root.showFocus && _root.animateBorder

      ColorAnimation {
        to: Qt.lighter(Styles.Colors.primaryColorLighter, 1.5)
        duration: Styles.Hints.animationDuration * 2
        easing.type: Easing.OutCubic
      }

      ColorAnimation {
        to: Styles.Colors.primaryColorLighter
        duration: Styles.Hints.animationDuration * 4
        easing.type: Easing.OutCubic
      }
    }
  }

  states: [
    State {
      name: "stateVisible"
      when: _root.showFocus

      PropertyChanges {
        target: _root
        opacity: 1.0
        visible: true
      }

      PropertyChanges {
        target: _background
        border.width: _root.borderWidth
      }
    },
    State {
      name: "stateNotVisible"
      when: !_root.showFocus

      PropertyChanges {
        target: _root
        opacity: 0.0
        visible: false
      }

      PropertyChanges {
        target: _background
        border.width: 0
      }
    }
  ]

  transitions: [
    Transition {
      to: "stateVisible"

      SequentialAnimation {
        NumberAnimation {
          target: _root
          property: "visible"
          duration: 0
        }

        ParallelAnimation {
          ScriptAction {
            script: {
              Services.SoundManager.focus.stop()
              Services.SoundManager.focus.play()
            }
          }

          NumberAnimation {
            target: _root
            property: "opacity"
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.OutCubic
          }

          NumberAnimation {
            target: _background
            property: "border.width"
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.OutCubic
          }
        }
      }
    },
    Transition {
      to: "stateNotVisible"
      SequentialAnimation {
        ParallelAnimation {
          NumberAnimation {
            target: _root
            property: "opacity"
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.OutCubic
          }

          NumberAnimation {
            target: _background
            property: "border.width"
            duration: Styles.Hints.animationDuration * 2
            easing.type: Easing.OutCubic
          }
        }

        NumberAnimation {
          target: _root
          property: "visible"
          duration: 0
        }
      }
    }
  ]
}
