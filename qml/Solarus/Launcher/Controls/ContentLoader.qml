/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15

import Solarus.Launcher.Controls 1.0 as Controls

FocusScope {
  default property alias content: _loader.sourceComponent
  readonly property alias active: _loader.active
  readonly property alias contentItem: _loader.item

  id: _root

  onActiveFocusChanged: function () {
    if (activeFocus && _loader.item) {
      _loader.item.forceActiveFocus()
    }
  }

  Loader {
    id: _loader
    anchors.fill: parent
    asynchronous: true
    sourceComponent: null
    visible: _loader.status == Loader.Ready
  }

  Controls.BusyIndicator {
    id: _indicatorLoading
    anchors.centerIn: parent
    visible: _loader.status == Loader.Loading
  }

  Controls.Text {
    id: _textError
    anchors.fill: parent
    text: qsTr("Error: impossible to load the view")
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
    wrapMode: Text.Wrap
    visible: _loader.status === Loader.Error
  }
}
