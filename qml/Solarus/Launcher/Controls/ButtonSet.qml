/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

FocusScope {
  property alias model: _repeater.model
  property int currentIndex: -1
  readonly property alias count: _repeater.count

  signal leftRequested
  signal rightRequested
  signal downRequested
  signal buttonClicked(int index)

  id: _root
  implicitWidth: _layout.implicitWidth
  implicitHeight: _layout.implicitHeight
  height: implicitHeight
  KeyNavigation.right: null

  Rectangle {
    id: _background
    anchors.fill: _root
    color: Styles.Colors.secondaryColorDarker
    border.width: Styles.Hints.borderWidth
    border.color: Styles.Colors.secondaryColorPressed
    radius: Styles.Hints.radius
  }

  ButtonGroup {
    id: _buttonGroup
    exclusive: true
  }

  RowLayout {
    id: _layout
    anchors.horizontalCenter: _root.horizontalCenter
    anchors.verticalCenter: _root.verticalCenter
    spacing: 3

    Repeater {
      id: _repeater
      model: ListModel {
        id: _listModel
      }

      delegate: Controls.Button {
        id: _checkableButton
        tooltip: model.tooltip
        checkable: true
        shadowEnabled: checked
        showBackgroundWhenIdle: false
        role: checked ? Styles.Colors.Role.Primary : Styles.Colors.Role.Secondary
        checked: model.index === _root.currentIndex
        iconId: model.icon
        onClicked: function() {
          _root.buttonClicked(model.index)
          _root.currentIndex = model.index

          // Keep focus within ButtonSet if already there.
          if (_root.activeFocus) {
            _checkableButton.forceActiveFocus()
          }
        }
        Component.onCompleted: _buttonGroup.buttons.push(this)
        Keys.onRightPressed: focusNextItem(index, _repeater.count, true)
        Keys.onTabPressed: focusNextItem(index, _repeater.count, true, true)
        Keys.onLeftPressed: focusNextItem(index, _repeater.count, false)
        Keys.onBacktabPressed: focusNextItem(index, _repeater.count, false)

        function focusNextItem(index, count, forward, tab) {
          if (index === count - 1 && forward && !tab) {
            const nextOnRight = _root.KeyNavigation.right
            if (nextOnRight && nextOnRight.visible && nextOnRight.enabled) {
              nextOnRight.forceActiveFocus()
            }
            return
          }
          const next = nextItemInFocusChain(forward)
          if (next) {
            next.forceActiveFocus()
          }
        }

        Connections {
          target: _root

          function onCurrentIndexChanged() {
            _checkableButton.checked = model.index === _root.currentIndex
          }
        }
      }
    }
  }
}
