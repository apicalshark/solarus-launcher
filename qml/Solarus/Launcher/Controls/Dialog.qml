/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

// @disable-check M129
QtControls.Dialog {
  property string text: ""
  property string iconId: ""
  property real progressValue: 0.0
  property bool progressVisible: false
  property int animationDuration: Styles.Hints.animationDuration * 4
  property int defaultButton: -1

  QtObject {
    id: _private
    property real gap: Styles.Hints.spacing * 4

    function isAcceptRole(role) {
      return role === QtControls.DialogButtonBox.AcceptRole
          || role === QtControls.DialogButtonBox.YesRole
          || role === QtControls.DialogButtonBox.ApplyRole
    }

    function tryFindDefaultButton(dialogButtonBox, defaultButton) {
      var btn = null
      var stdBtn = null
      if (defaultButton >= 0) {
        stdBtn = dialogButtonBox.standardButton(defaultButton)
        if (stdBtn) {
          return stdBtn
        }
      }

      const buttonTypes = [QtControls.DialogButtonBox.Ok,
                           QtControls.DialogButtonBox.Yes,
                           QtControls.DialogButtonBox.Apply]
      for (let buttonType of buttonTypes) {
        stdBtn = dialogButtonBox.standardButton(buttonType)
        if (stdBtn) {
          btn = stdBtn
          break
        }
      }

      return btn
    }

    function focusButton() {
      Qt.callLater(function() {
        if (!_root.opened) {
          return
        }

        // Force focus within the popup.
        _root.forceActiveFocus(Qt.PopupFocusReason)

        // Force focus on the default button.
        const defaultButton = _private.tryFindDefaultButton(_dialogButtonBox, _root.defaultButton)
        if (defaultButton) {
          defaultButton.forceActiveFocus()
        }
      })
    }
  }

  id: _root
  modal: true
  parent: QtControls.Overlay.overlay
  standardButtons: QtControls.Dialog.Ok | QtControls.Dialog.Cancel
  closePolicy: QtControls.Popup.CloseOnEscape | QtControls.Popup.CloseOnPressOutside
  bottomPadding: Styles.Hints.spacing * 2
  topPadding: Styles.Hints.spacing * 2
  rightPadding: Styles.Hints.spacing * 2
  leftPadding: Styles.Hints.spacing * 2
  margins: Styles.Hints.spacing * 2
  width: QtControls.Overlay.overlay ? Math.min(QtControls.Overlay.overlay.width - margins * 2, implicitWidth) : implicitWidth
  implicitHeight: implicitContentHeight + topPadding + bottomPadding + spacing
  spacing: Styles.Hints.spacing * 2
  height: QtControls.Overlay.overlay ? Math.min(QtControls.Overlay.overlay.height - margins * 2, implicitHeight) : implicitHeight
  opacity: 0.0
  focus: true
  x: QtControls.Overlay.overlay ? Math.round((QtControls.Overlay.overlay.width - width) / 2) : 0
  y: QtControls.Overlay.overlay ? Math.round((QtControls.Overlay.overlay.height - height) / 2) + _private.gap : _private.gap

  Connections {
    target: _root

    function onAboutToShow() {
      Services.SoundManager.open.play()
    }

    function onAboutToHide() {
      Services.SoundManager.close.play()
    }

    function onOpenedChanged() {
      if(_root.opened) {
        _private.focusButton()
      }
    }
  }

  // By default, show DialogContent.
  contentItem: DialogContent {
    title: _root.title
    text: _root.text
    iconId: _root.iconId
    progressValue: _root.progressValue
    progressVisible: _root.progressVisible
  }

  header: null

  footer: QtControls.DialogButtonBox {
    id: _dialogButtonBox
    background: null
    spacing: Styles.Hints.spacing
    buttonLayout: QtControls.DialogButtonBox.GnomeLayout
    bottomPadding: Styles.Hints.spacing * 2
    leftPadding: Styles.Hints.spacing * 2
    rightPadding: Styles.Hints.spacing * 2
    topPadding: Styles.Hints.spacing * 2
    focus: true

    delegate: Controls.Button {
      focus: _private.isAcceptRole(QtControls.DialogButtonBox.buttonRole)
      role: _private.isAcceptRole(QtControls.DialogButtonBox.buttonRole) ? Styles.Colors.Role.Primary
                                                                         : Styles.Colors.Role.Secondary
      Keys.onRightPressed: Utils.EventUtils.tryFocusNextItem(this, true)
      Keys.onLeftPressed: Utils.EventUtils.tryFocusNextItem(this, true)
    }
  }

  background: Rectangle {
   color: Styles.Colors.popupColor
   border.width: 1
   radius: Styles.Hints.radius * 2
   border.color: Styles.Colors.getSecondaryBorder(false, false, true)

   layer.enabled: _root.enabled
   layer.effect: DropShadow {
      visible: _root.enabled
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: 4
      radius: 64
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColor
    }
  }

  QtControls.Overlay.modal: DialogOverlay {
    id: _overlay
    value: opacity
    blurSource: Window.contentItem

    Behavior on opacity {
      NumberAnimation {
        duration: opacity === 0 ? _root.animationDuration : _root.animationDuration / 2
        easing.type: Easing.OutCubic
      }
    }
  }

  enter: Transition {
    ParallelAnimation {
      NumberAnimation {
        target: _private
        property: "gap"
        from: -Styles.Hints.spacing * 4
        to: 0
        duration: _root.animationDuration
        easing.type: Easing.OutCubic
      }
      NumberAnimation {
        property: "opacity"
        from: 0.0
        to: 1.0
        duration: _root.animationDuration
        easing.type: Easing.OutCubic
      }
    }
  }

  exit: Transition {
    ParallelAnimation {
      NumberAnimation {
        target: _private
        property: "gap"
        to: -Styles.Hints.spacing * 4
        duration: _root.animationDuration
        easing.type: Easing.OutCubic
      }
      NumberAnimation {
        property: "opacity"
        to: 0.0
        duration: _root.animationDuration / 2
        easing.type: Easing.OutCubic
      }
    }
  }
}
