/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15 as QtQuick

import Solarus.Launcher.Styles 1.0 as Styles

QtQuick.Text {
  enum Role {
    Body,
    H1,
    H2,
    H3,
    H4,
    Caption,
    Console,
    UppercaseLabel
  }

  property int role: Text.Role.Body
  property bool bold: false
  readonly property alias hovered: _private.hovered
  property alias hoverEnabled: _mouseArea.hoverEnabled

  id: _root
  font.family: getFontFamily(role, bold)
  font.pixelSize: getFontPixelSize(role)
  color: getColor(role, enabled)
  wrapMode: QtQuick.Text.Wrap
  renderType: Styles.Fonts.getRenderType(font.pixelSize)
  linkColor: Styles.Colors.getLinkColor(hoveredLink, enabled)
  lineHeight: getLineHeight(role)
  lineHeightMode: QtQuick.Text.FixedHeight
  font.bold: bold
  font.capitalization: getCapitalization(role)

  function getColor(role, enabled) {
    switch (role) {
      case Text.Role.H1: return enabled ? Styles.Colors.textColorH1 : Styles.Colors.textColorDisabled
      case Text.Role.H2: return enabled ? Styles.Colors.textColorH2 : Styles.Colors.textColorDisabled
      case Text.Role.H3: return enabled ? Styles.Colors.textColorH3 : Styles.Colors.textColorDisabled
      case Text.Role.H4: return enabled ? Styles.Colors.textColorH4 : Styles.Colors.textColorDisabled
      case Text.Role.Caption: return enabled ? Styles.Colors.textColorCaption : Styles.Colors.textColorCaptionDisabled
      case Text.Role.Console: return enabled ? Styles.Colors.textColorConsole : Styles.Colors.textColorDisabled
      case Text.Role.UppercaseLabel: return enabled ? Styles.Colors.textColorUppercaseLabel : Styles.Colors.textColorDisabled
      default: return enabled ? Styles.Colors.textColorBody : Styles.Colors.textColorDisabled
    }
  }

  function getFontFamily(role, bold) {
    switch (role) {
    case Text.Role.H1: return Styles.Fonts.h1FontFamily
    case Text.Role.H2: return Styles.Fonts.h2FontFamily
    case Text.Role.H3: return Styles.Fonts.h3FontFamily
    case Text.Role.H4: return bold ? Styles.Fonts.h4FontFamilyBold : Styles.Fonts.h4FontFamily
    case Text.Role.Caption: return bold ? Styles.Fonts.captionFontFamilyBold : Styles.Fonts.captionFontFamily
    case Text.Role.Console: return bold ? Styles.Fonts.consoleFontFamilyBold : Styles.Fonts.consoleFontFamily
    case Text.Role.UppercaseLabel: return Styles.Fonts.uppercaseLabelFontFamily
    default:
      return bold ? Styles.Fonts.primaryFontFamilyBold : Styles.Fonts.primaryFontFamily
    }
  }

  function getFontPixelSize(role) {
    switch (role) {
      case Text.Role.H1: return Styles.Fonts.h1FontSize
      case Text.Role.H2: return Styles.Fonts.h2FontSize
      case Text.Role.H3: return Styles.Fonts.h3FontSize
      case Text.Role.H4: return Styles.Fonts.h4FontSize
      case Text.Role.Caption: return Styles.Fonts.captionFontSize
      case Text.Role.Console: return Styles.Fonts.consoleFontSize
      case Text.Role.UppercaseLabel: return Styles.Fonts.uppercaseLabelFontSize
      default: return Styles.Fonts.bodyFontSize
    }
  }

  function getLineHeight(role) {
    switch (role) {
      case Text.Role.Body: return Styles.Fonts.bodyLineHeight
      case Text.Role.H1: return Styles.Fonts.h1LineHeight
      case Text.Role.H2: return Styles.Fonts.h2LineHeight
      case Text.Role.H3: return Styles.Fonts.h3LineHeight
      case Text.Role.H4: return Styles.Fonts.h4LineHeight
      case Text.Role.Caption: return Styles.Fonts.captionLineHeight
      case Text.Role.Console: return Styles.Fonts.consoleLineHeight
      case Text.Role.UppercaseLabel: return Styles.Fonts.uppercaseLabelLineHeight
      default: return Styles.Fonts.bodyFontSize
    }
  }

  function getCapitalization(role) {
    switch (role) {
      case Text.Role.UppercaseLabel: return QtQuick.Font.AllUppercase
      default: return QtQuick.Font.MixedCase
    }
  }

  onLinkActivated: {
    Qt.openUrlExternally(link)
  }

  QtQuick.QtObject {
    property bool hovered: false

    id: _private
  }

  QtQuick.Behavior on linkColor {
    enabled: _root.enabled

    QtQuick.ColorAnimation {
      duration: Styles.Hints.animationDuration
      easing.type: QtQuick.Easing.OutCubic
    }
  }

  QtQuick.MouseArea {
    id: _mouseArea
    anchors.fill: parent
    hoverEnabled: true
    acceptedButtons: Qt.NoButton
    cursorShape: _root.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
    onEntered: _private.hovered = true
    onExited: _private.hovered = false
  }
}
