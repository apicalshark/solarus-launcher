/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Utils 1.0 as Utils

// @disable-check M129
T.TextField {
  property bool down: false
  property int radius: Styles.Hints.radius
  property alias iconId: _icon.iconId
  property bool iconAutoColor: true
  property color iconColor: "transparent"

  id: _root
  renderType: Styles.Fonts.getRenderType(font.pixelSize)
  color: Styles.Colors.getTextFieldText(_root.enabled)
  placeholderTextColor: Styles.Colors.getTextFieldPlaceholder(_root.enabled)
  hoverEnabled: true
  selectByMouse: true
  selectionColor: Styles.Colors.primaryColor
  selectedTextColor: Styles.Colors.textColorBody
  font: Styles.Fonts.primaryFont
  leftPadding: _icon.visible ? Styles.Hints.iconSize + Styles.Hints.spacing
                             : Styles.Hints.spacing / 2
  rightPadding: !_clearButton.hidden ? Styles.Hints.iconSize + Styles.Hints.spacing
                                     : Styles.Hints.spacing / 2
  activeFocusOnPress: true
  persistentSelection: _menu.visible
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: Styles.Hints.controlHeight
  verticalAlignment: TextInput.AlignVCenter

  onEnabledChanged: {
    if (!enabled) {
      deselect()
    }
  }

  onVisibleChanged: {
    deselect()
  }

  background: Utils.TextInputShape {
    id: _background
    down: _root.down
    hovered: _root.hovered
    enabled: _root.enabled
    hasFocus: _root.activeFocus
    radius: _root.radius

    Controls.SvgIcon {
      id: _icon
      anchors.verticalCenter: parent.verticalCenter
      x: Styles.Hints.spacing / 2
      color: _root.iconAutoColor ? Styles.Colors.getTextFieldText(_root.enabled) : _root.iconColor
      width: Styles.Hints.iconSize
      height: Styles.Hints.iconSize
      visible: status != Image.Null
    }

    Text {
      id: _placeholder
      text: _root.placeholderText
      color: _root.placeholderTextColor
      renderType: _root.renderType
      font: _root.font
      visible: _root.text.length <= 0
      anchors.fill: parent
      verticalAlignment: Text.AlignVCenter
      leftPadding: _root.leftPadding
      rightPadding: _root.rightPadding
      elide: Text.ElideRight
    }
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: background
    showFocus: _root.activeFocus || _menu.visible
    animateBorder: _root.activeFocus || !_menu.visible
  }

  Controls.CircleButton {
    id: _clearButton
    height: Styles.Hints.controlHeight - Styles.Hints.borderWidth * 8
    width: height
    anchors.right: _root.right
    anchors.rightMargin: Styles.Hints.borderWidth * 4
    anchors.verticalCenter: _root.verticalCenter
    iconId: Styles.Icons.close_16
    iconHeight: Styles.Hints.iconSize
    iconWidth: Styles.Hints.iconSize
    hidden: !(_root.text && _root.hovered)
    enabled: !hidden && _root.enabled
    focusPolicy: Qt.NoFocus
    visible: opacity != 0

    onClicked: {
      const hasFocus = activeFocus
      _root.clear()
      if (hasFocus) {
        _root.forceActiveFocus()
      }
    }

    onHiddenChanged: {
      Qt.callLater(() => _root.ensureVisible(_root.cursorPosition))
    }
  }

  onPressed: {
    if (event.button === Qt.LeftButton) {
      _root.down = true
    }
    if (!_root.activeFocus) {
      _root.forceActiveFocus()
    }
  }

  onPressAndHold: {
    if (event.button === Qt.LeftButton && !activeFocus) {
      _root.down = false
      _root.forceActiveFocus()
    }
  }

  onReleased: {
    _root.down = false
    if (event.button === Qt.RightButton) {
      _menu.popup(event.x, event.y)
    }
  }

  Controls.ContextMenu {
    id: _menu
    canCopy: _root.selectedText.length > 0
    canPaste: _root.canPaste
    canCut: _root.selectedText.length > 0
    canSelectAll: _root.text.length > 0
    onCopyTriggered: {
      _root.forceActiveFocus()
      _root.copy()
    }
    onPasteTriggered: {
      _root.forceActiveFocus()
      _root.paste()
    }
    onCutTriggered: {
      _root.forceActiveFocus()
      _root.cut()
    }
    onSelectAllTriggered: {
      _root.forceActiveFocus()
      _root.selectAll()
    }
    onClosed: _root.forceActiveFocus()
  }
}
