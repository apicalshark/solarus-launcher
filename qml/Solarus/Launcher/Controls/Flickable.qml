/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15 as QtQuick
import QtQuick.Controls 2.15

import Solarus.Launcher.Controls 1.0 as Controls

// @disable-check M129
QtQuick.Flickable {
  property int scrollSecurityMargin: 32
  property int scrollKeyDistance: 512
  property alias contentY: _root.contentY

  id: _root

  ScrollBar.vertical: Controls.ScrollBar {
    id: _scrollBarVertical
    orientation: Qt.Vertical
    needed: _root.contentHeight > _root.height
  }

  ScrollBar.horizontal: Controls.ScrollBar {
    id: _scrollBarHorizontal
    orientation: Qt.Horizontal
    needed: _root.contentWidth > _root.width
  }

  QtQuick.Behavior on contentY {
    QtQuick.NumberAnimation {
      target: _root
      property: "contentY"
      duration: 150
      easing.type: QtQuick.Easing.InOutQuad
    }
  }

  QtQuick.Keys.onPressed: {
    if (event.key === Qt.Key_Up) {
      if (event.isAutoRepeat && _root.contentY <= 0) {
        event.accepted = false
      } else {
        _root.flick(0, scrollKeyDistance)
        event.accepted = true
      }
    } else if (event.key === Qt.Key_Down) {
      if (event.isAutoRepeat
          && _root.contentY >= _root.contentHeight - _root.height) {
        event.accepted = false
      } else {
        _root.flick(0, -scrollKeyDistance)
        event.accepted = true
      }
    }
  }

  function scrollToTop() {
    _root.contentY = 0
  }

  function ensureVisible(item) {
    const itemTop = item.mapToItem(_root.contentItem, 0, 0).y - _root.scrollSecurityMargin
    const itemHeight = item.height + _root.scrollSecurityMargin * 2
    const itemBottom = itemTop + itemHeight
    if (itemTop < _root.contentY || itemBottom < _root.contentY) {
      _root.contentY = Math.round(Math.max(0, Math.min(itemTop, _root.contentHeight)))
    } else if (itemTop > _root.contentY + _root.height || itemBottom > _root.contentY + _root.height) {
      _root.contentY = Math.round(Math.max(0, Math.min(itemTop - _root.height + itemHeight, _root.contentHeight - _root.height)))
    }
  }
}
