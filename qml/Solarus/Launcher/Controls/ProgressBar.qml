/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15 as QtControls
import QtGraphicalEffects 1.15
import QtQuick.Templates 2.15 as T

import Solarus.Launcher.Styles 1.0 as Styles

T.ProgressBar {
  property bool alternateBackground: false

  id: _root
  value: 0.0
  implicitHeight: Styles.Hints.controlHeight / 2
  implicitWidth: Styles.Hints.sliderDefaultWidth

  background: Rectangle {
    id: _groove
    height: Styles.Hints.radius
    width: _root.width
    anchors.verticalCenter: _root.verticalCenter
    color: Styles.Colors.getSliderGroove(_root.enabled, _root.alternateBackground)
    radius: height / 2
    implicitHeight: Styles.Hints.radius
    layer.enabled: _root.enabled
    layer.effect: InnerShadow {
      horizontalOffset: 0
      verticalOffset: Styles.Hints.borderWidth
      radius: Styles.Hints.borderWidth
      samples: (radius * 2) + 1
      color: Styles.Colors.shadowColorLighter
    }
  }

  contentItem: Item {
    id: _contentItem

    Rectangle {
      id: _track
      width: getValueWidth(_root.width, _root.visualPosition)
      implicitHeight: Styles.Hints.radius
      height: Styles.Hints.radius
      anchors.verticalCenter: parent.verticalCenter
      radius: height / 2
      color: Styles.Colors.getSliderTrack(_root.enabled)
      visible: width > Styles.Hints.radius
      layer.enabled: _root.enabled
      layer.effect: DropShadow {
        transparentBorder: true
        horizontalOffset: 0
        verticalOffset: Styles.Hints.borderWidth
        radius: Styles.Hints.borderWidth
        samples: (radius * 2) + 1
        color: Styles.Colors.shadowColorLighter
      }

      Behavior on width {
        NumberAnimation {
          duration: Styles.Hints.animationDuration / 2
          easing.type: Easing.OutQuad
        }
      }

      function getValueWidth(totalWidth, ratio) {
        const v = totalWidth * ratio
        return v < Styles.Hints.radius ? 0 : v
      }
    }
  }
}
