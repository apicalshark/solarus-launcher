/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15 as QtQuick
import QtQuick.Controls 2.15 as QtControls
import QtQuick.Window 2.15

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Utils 1.0 as Utils

// This QML component is here to provide a better ScrollView than the built-in one.
// It fixes various behavior bugs and provides better scrollbars.
// @disable-check M129
QtControls.ScrollView {
  signal scrolled()
  readonly property bool userIsScrolling: _private.flickable.flicking || ScrollBar.vertical.pressed || ScrollBar.horizontal.pressed
  readonly property bool atYBeginning: _private.flickable.atYBeginning
  property real verticalScrollBarVMargin: 0
  property real verticalScrollBarHMargin: 0
  property real horizontalScrollBarHMargin: 0
  property real horizontalScrollBarVMargin: 0

  id: _root
  clip: true
  topPadding: 0
  bottomPadding: 0
  rightPadding: 0
  leftPadding: 0
  ScrollBar.horizontal.policy: ScrollBar.AsNeeded
  ScrollBar.vertical.policy: ScrollBar.AsNeeded
  ScrollBar.vertical.visible: false
  ScrollBar.horizontal.visible: false

  QtQuick.QtObject {
    id: _private
    property bool shouldShowScrollBar: false
    readonly property QtQuick.Flickable flickable: _root.contentItem
    readonly property QtQuick.Item focusItem: Utils.ScrollUtils.isParentOf(_root, _root.Window.activeFocusItem) ? _root.Window.activeFocusItem : null
    readonly property QtQuick.Component componentScrollBar: QtQuick.Component {
      Controls.ScrollBar {
        parent: _root
        anchors.rightMargin: orientation === Qt.Vertical ? verticalScrollBarHMargin - 1 : 0
        anchors.bottomMargin: orientation === Qt.Vertical ? _root.verticalScrollBarVMargin : -1
        anchors.topMargin: orientation === Qt.Vertical ? _root.verticalScrollBarVMargin : 0
        anchors.leftMargin: orientation === Qt.Vertical ? 0 : _root.horizontalScrollBarHMargin
        anchors.right: orientation === Qt.Vertical ? _root.right : undefined
        anchors.top: orientation === Qt.Vertical ? _root.top : undefined
        anchors.left: orientation === Qt.Vertical ? undefined : _root.left
        anchors.bottom: _root.bottom
        height: orientation === Qt.Vertical ? _root.availableHeight : implicitHeight
        width: orientation === Qt.Horizontal ? _root.availableWidth : implicitWidth
        policy: orientation === Qt.Vertical ? _root.ScrollBar.vertical.policy : _root.ScrollBar.horizontal.policy
        needed: policy !== ScrollBar.AlwaysOff
                && (_root.hovered || _private.flickable.moving || _private.shouldShowScrollBar)
                && (orientation === Qt.Vertical ? (_private.flickable.contentHeight > _root.availableHeight)
                                                : (_private.flickable.contentWidth > _root.availableWidth))
      }
    }

    onFocusItemChanged: {
      if (!focusItem) return
      const willShow = Utils.ScrollUtils.ensureVisibleScrollArea(_root, _private.focusItem, Styles.Hints.scrollMargin, Styles.Hints.scrollMargin, true)
       showTransientScrollBars(willShow)
    }

    function scrollWithKeyboardVertically(direction, autoRepeat, faster) {
      Utils.ScrollUtils.scrollWithKeyboard(_private.flickable, _root.ScrollBar.vertical, direction, true, autoRepeat, faster)
      showTransientScrollBars(true)
    }

    function scrollWithKeyboardHorizontally(direction, autoRepeat, faster) {
      Utils.ScrollUtils.scrollWithKeyboard(_private.flickable, _root.ScrollBar.horizontal, direction, true, autoRepeat, faster)
      showTransientScrollBars(true)
    }

    function showTransientScrollBars(show) {
      _timerHover.stop()
      shouldShowScrollBar = show
      if (show) {
        _timerHover.start()
      }
    }

    // I've tried to use QtQuick.Template's ScrollView but there is a crash
    // when clicking on a scrollbar, for an unknown reason. Hence the unusual
    // need for injecting new scrollbars. The previous ones are hidden then replaced.
    function injectScrollBars() {
      _root.ScrollBar.vertical = _private.componentScrollBar.createObject(_root, {
        "orientation": Qt.Vertical
      })
      _root.ScrollBar.horizontal = _private.componentScrollBar.createObject(_root, {
        "orientation": Qt.Horizontal
      })
    }
  }

  QtQuick.Timer {
    id: _timerHover
    interval: Styles.Hints.scrollBarDisappearingDelay
    repeat: false
    onTriggered: {
      _private.shouldShowScrollBar = false
    }
  }

  onHoveredChanged: {
    _private.showTransientScrollBars(true)
  }

  function scrollToTop() {
    _private.flickable.contentY = 0
  }

  function scrollToBottom() {
    ScrollBar.vertical.position = 1.0 - ScrollBar.vertical.size
  }

  QtQuick.Connections {
    target: _private.flickable

    function onFlickingChanged() {
      if (_private.flickable.flicking) {
        _root.scrolled()
      }
    }
  }

  QtQuick.Connections {
    target: _root.ScrollBar.vertical

    function onPressedChanged() {
      if (_root.ScrollBar.vertical.pressed) {
        _root.scrolled()
      }
    }
  }

  QtQuick.Connections {
    target: _root.ScrollBar.horizontal

    function onPressedChanged() {
      if (_root.ScrollBar.horizontal.pressed) {
        _root.scrolled()
      }
    }
  }

  function ensureVisible(item) {
    Utils.ScrollUtils.ensureVisibleScrollArea(this, item, Styles.Hints.scrollMargin, Styles.Hints.scrollMargin, true)
  }

  QtQuick.Component.onCompleted: {
    _private.injectScrollBars()
  }

  QtQuick.Keys.onUpPressed: {
    _private.scrollWithKeyboardVertically(Utils.ScrollUtils.ScrollDirection.Previous, event.isAutoRepeat)
  }

  QtQuick.Keys.onDownPressed: {
    _private.scrollWithKeyboardVertically(Utils.ScrollUtils.ScrollDirection.Next, event.isAutoRepeat)
  }

  QtQuick.Keys.onLeftPressed: {
    _private.scrollWithKeyboardHorizontally(Utils.ScrollUtils.ScrollDirection.Previous, event.isAutoRepeat)
  }

  QtQuick.Keys.onRightPressed: {
    _private.scrollWithKeyboardHorizontally(Utils.ScrollUtils.ScrollDirection.Next, event.isAutoRepeat)
  }

  QtQuick.Keys.onPressed: {
    switch (event.key) {
    case Qt.Key_PageUp:
      _private.scrollWithKeyboardVertically(Utils.ScrollUtils.ScrollDirection.Previous, event.isAutoRepeat, true)
      event.accepted = true
      break
    case Qt.Key_PageDown:
      _private.scrollWithKeyboardVertically(Utils.ScrollUtils.ScrollDirection.Next, event.isAutoRepeat, true)
      event.accepted = true
      break
    default:
      break
    }
  }
}
