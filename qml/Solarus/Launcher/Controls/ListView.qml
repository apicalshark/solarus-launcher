/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15 as QtQuick
import QtQuick.Templates 2.15 as Templates
import QtQuick.Controls 2.15 as QtControls

import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Controls 1.0 as Controls

// @disable-check M129
QtQuick.ListView {
  property bool showBackground: false

  id: _root
  clip: true
  implicitWidth: Styles.Hints.controlDefaultWidth
  implicitHeight: count * Styles.Hints.menuItemHeight

  delegate: Controls.ListViewItem {
    id: _delegate
    text: model.display ?? model.text
  }

  QtQuick.Rectangle {
    id: _background
    visible: _root.showBackground
    z: -1
    anchors.fill: _root
    color: Styles.Colors.windowColorDarker
  }
}
