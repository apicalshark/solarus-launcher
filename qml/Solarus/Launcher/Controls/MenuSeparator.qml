/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Templates 2.15 as T

import Solarus.Launcher.Styles 1.0 as Styles

// @disable-check M129
T.MenuSeparator {
  id: _root
  enabled: false
  implicitHeight: implicitContentHeight + Styles.Hints.spacing / 2

  contentItem: Item {
    implicitWidth: 200
    implicitHeight: Styles.Hints.borderWidth

    Rectangle {
      color: Styles.Colors.menuSeparatorColor
      height: Styles.Hints.borderWidth
      anchors.left: parent.left
      anchors.leftMargin: Styles.Hints.spacing / 3
      anchors.right: parent.right
      anchors.rightMargin: Styles.Hints.spacing / 3
      anchors.verticalCenter: parent.verticalCenter
    }
  }
}
