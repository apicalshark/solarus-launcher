/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

Controls.Button {
  id: _root
  text: ""
  implicitHeight: Styles.Hints.controlHeight *  2
  implicitWidth: Styles.Hints.controlHeight *  2
  padding: 0
  bottomPadding: 0
  topPadding: 0
  leftPadding: 0
  rightPadding: 0
  checkable: false
  checked: false
  autoRepeat: false
  spacing: 0
  radius: height / 2
  iconWidth: Styles.Hints.iconSize * 2
  iconHeight: Styles.Hints.iconSize * 2
  focusRadius: height / 2 + Styles.Hints.focusBorderWidth
}
