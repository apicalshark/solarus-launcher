/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Styles 1.0 as Styles

Controls.SvgImage {
  property color color: "transparent"
  property alias iconId: _root.source

  id: _root
  width: Styles.Hints.iconSize
  height: Styles.Hints.iconSize
  layer.enabled: _root.color != null && _root.color != "transparent"
  asynchronous: true
  layer.effect: ColorOverlay {
    id: _colorOverlay
    visible: _root.layer.enabled
    color: _root.color
    antialiasing: true
  }
}
