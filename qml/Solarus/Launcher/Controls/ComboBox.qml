/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQml 2.15
import QtQuick 2.15
import QtQuick.Templates 2.15 as T
import QtQuick.Controls 2.15 as QtControls
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15

import Solarus.Launcher.Controls 1.0 as Controls
import Solarus.Launcher.Services 1.0 as Services
import Solarus.Launcher.Styles 1.0 as Styles
import Solarus.Launcher.Utils 1.0 as Utils

// @disable-check M129
T.ComboBox {
  readonly property alias maxContentWidth: _private.maxContentWidth
  property int radius: Styles.Hints.radius
  signal upDownKeyPressed(bool down)
  signal upDownKeyReleased(bool down)

  id: _root
  font: Styles.Fonts.primaryFont
  implicitHeight: Styles.Hints.controlHeight
  implicitWidth:  Math.max(implicitBackgroundWidth, implicitContentWidth + implicitIndicatorWidth + leftPadding + rightPadding + spacing)
  leftPadding: Styles.Hints.spacing
  rightPadding: Styles.Hints.spacing / 2
  spacing: Styles.Hints.spacing / 2
  focusPolicy: Qt.StrongFocus
  KeyNavigation.priority: KeyNavigation.BeforeItem
  onActivated: Services.SoundManager.click.play()

  QtObject {
    property bool spaceDown: false
    property int maxContentWidth: 0

    function updateMaxContentWidth() {
      maxContentWidth = Utils.GeometryUtils.getMinimumWidth(_root.popup.contentItem, _root.count, 0)
    }

    id: _private
  }

  Component.onCompleted: _private.updateMaxContentWidth()

  onModelChanged: _private.updateMaxContentWidth()

  onCountChanged: _private.updateMaxContentWidth()

  background: Item {
    id: _background
    anchors.fill: _root
    anchors.margins: _root.down && !_root.editable && !popup.opened ? -Styles.Hints.pressDelta : 0

    layer.enabled: _root.visible && _root.enabled
    layer.effect: DropShadow {
      visible: _root.enabled
      transparentBorder: true
      horizontalOffset: 0
      verticalOffset: _root.down ? 0 : Styles.Hints.borderWidth
      radius: _root.down ? Styles.Hints.borderWidth : Styles.Hints.borderWidth * 2
      samples: (radius * 2) + 1
      color: (!_root.checkable || _root.checkable && _root.checked) ? Styles.Colors.shadowColor : Styles.Colors.shadowColorLighter

      Behavior on verticalOffset {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }

      Behavior on radius {
        enabled: _root.enabled
        NumberAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    Loader {
      anchors.fill: parent
      active: !_root.editable
      sourceComponent: Utils.ButtonShape {
        radius: _root.down ? _root.radius + Styles.Hints.pressDelta / 2 : _root.radius
        role: Styles.Colors.Role.Secondary
        hovered: _root.hovered
        down: _root.down
        enabled: _root.enabled
        shadowEnabled: false
      }
    }

    Loader {
      anchors.fill: parent
      active: _root.editable
      sourceComponent: Utils.TextInputShape {
        hovered: _root.hovered
        down: _root.down
        enabled: _root.enabled
        shadowEnabled: false
        hasFocus: _root.activeFocus
      }
    }
  }

  indicator: Item {
    id: _indicator
    anchors.right: parent.right
    height: parent.height
    implicitWidth: height
    width: height
    anchors.verticalCenter: parent.verticalCenter

    Rectangle {
      anchors.left: parent.left
      width: Styles.Hints.borderWidth
      anchors.top: parent.top
      anchors.topMargin: Styles.Hints.borderWidth * 3
      anchors.bottom: parent.bottom
      anchors.bottomMargin: Styles.Hints.borderWidth * 3
      color: Styles.Colors.getButtonBorder(Styles.Colors.Role.Secondary, _root.hovered, _root.down, _root.enabled)

      Behavior on color {
        enabled: _root.enabled
        ColorAnimation {
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    Controls.SvgIcon {
      id: _indicatorIcon
      iconId: Styles.Icons.combobox_16
      color: Styles.Colors.getControlForeground(_root.enabled)
      anchors.centerIn: parent
      width: Styles.Hints.iconSize
      height: Styles.Hints.iconSize
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      visible: status != Image.Null
      opacity: _root.enabled ? 1 : 0.5
    }
  }

  contentItem: Item {
    anchors.fill: _root
    implicitWidth: _private.maxContentWidth
    anchors.leftMargin: _root.leftPadding
    anchors.rightMargin: _root.rightPadding + _root.indicator.width
    anchors.topMargin: _root.topPadding
    anchors.bottomMargin: _root.bottomPadding

    Loader {
      anchors.fill: parent
      active: !_root.editable
      sourceComponent: Text {
        id: _contentItem
        anchors.fill: parent
        text: _root.displayText
        font: _root.font
        color: Styles.Colors.getControlForeground(_root.enabled)
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
        renderType: Styles.Fonts.getRenderType(font.pixelSize)
        visible: text.length > 0
      }
    }

    Loader {
      anchors.fill: parent
      active: _root.editable
      sourceComponent: TextInput {
        anchors.fill: parent
        text: _root.editText
        font: _root.font
        color: Styles.Colors.getControlForeground(_root.enabled)
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        renderType: Styles.Fonts.getRenderType(font.pixelSize)
        clip: true
        selectByMouse: true
        selectionColor: Styles.Colors.primaryColor
        selectedTextColor: Styles.Colors.bodyTextColor
        activeFocusOnPress: true
        persistentSelection: true
        onEnabledChanged: {
          if (!enabled) {
            deselect()
          }
        }
      }
    }
  }

  popup: T.Popup {
    x: 0
    y: 0
    closePolicy: QtControls.Popup.CloseOnPressOutside | QtControls.Popup.CloseOnEscape
    implicitWidth: Math.max(_root.width, contentWidth + leftPadding + rightPadding)
    implicitHeight: contentHeight + topPadding + bottomPadding
    leftPadding: Styles.Hints.spacing / 4
    rightPadding: Styles.Hints.spacing / 4
    topPadding: Styles.Hints.spacing / 4
    bottomPadding: Styles.Hints.spacing / 4
    leftMargin: Styles.Hints.spacing / 4
    rightMargin: Styles.Hints.spacing / 4
    transformOrigin: Item.Top
    focus: true

    onAboutToShow: {
      y = getYPosition()
      _listView.currentIndex = _root.currentIndex
      Services.SoundManager.slider.play()
    }

    onClosed: _root.forceActiveFocus()

    function getYPosition() {
      if (currentIndex > -1 && _listView.count > 0) {
        const currentItem = _listView.itemAtIndex(currentIndex)
        if (currentItem) {
          return (_root.height - currentItem.height) / 2 - currentItem.y - topPadding
        }
      }
      return 0
    }

    background: Rectangle {
      id: _menuBackground
      implicitWidth: 160
      radius: Styles.Hints.radius
      color: "transparent"
      clip: true

      layer.enabled: true
      layer.effect: DropShadow {
        id: _shadow
        transparentBorder: true
        horizontalOffset: 0
        verticalOffset: 4
        radius: 16
        samples: (radius * 2) + 1
        color: Styles.Colors.shadowColor
      }

      // Handle mouse over disabled items.
      MouseArea {
        anchors.fill: _menuBackground
        anchors.margins: Styles.Hints.spacing / 4
        hoverEnabled: true
        onPositionChanged: {
          if (_root.currentIndex != -1) {
            _root.currentIndex = -1
          }
        }
      }

      Rectangle {
        anchors.fill: parent
        color: Styles.Colors.menuColor
        border.color: Styles.Colors.menuBorderColor
        border.width: Styles.Hints.borderWidth
        radius: Styles.Hints.radius
      }
    }

    contentItem: ListView {
      id: _listView
      model: _root.delegateModel
      interactive: Window.window ? contentHeight > Window.window.height : false
      clip: true
      currentIndex: _root.highlightedIndex
      implicitHeight: contentHeight
      spacing: 0
      highlightMoveDuration: 0
      highlight: null
      focus: true
      keyNavigationEnabled: true
      highlightFollowsCurrentItem: true

      onImplicitWidthChanged: _private.maxContentWidth = implicitWidth
    }

    enter: Transition {
      ParallelAnimation {
        NumberAnimation {
          property: "scale"
          from: 0.75
          to: 1.0
          duration: Styles.Hints.animationDuration / 2
          easing.type: Easing.OutCubic
        }
        NumberAnimation {
          property: "opacity"
          from: 0.0
          to: 1.0
          duration: Styles.Hints.animationDuration
          easing.type: Easing.OutCubic
        }
      }
    }

    exit: Transition {
      NumberAnimation {
        property: "opacity"
        to: 0.0
        duration: Styles.Hints.animationDuration
        easing.type: Easing.InCubic
      }
    }
  }

  Keys.onPressed: function (event) {
    const key = event.key
    if ((key === Qt.Key_Space || key === Qt.Key_Return) && !event.isAutoRepeat) {
      // Fix non-forwarded press event.
      _private.spaceDown = true
      event.accepted = false
    } else if (key === Qt.Key_Down || key === Qt.Key_Up) {
      // Block up and down.
      event.accepted = true
      _root.upDownKeyPressed(key === Qt.Key_Down)
    }
  }

  Keys.onReleased: function (event) {
    const key = event.key
    if ((key === Qt.Key_Space || key === Qt.Key_Return) && !event.isAutoRepeat) {
      // Fix non-forwarded release event.
      _private.spaceDown = false
      event.accepted = false
    } else if (key === Qt.Key_Down || key === Qt.Key_Up) {
      // Block up and down.
      event.accepted = true
      _root.upDownKeyReleased(key === Qt.Key_Down)
    }
  }

  Keys.priority: Keys.BeforeItem

  delegate: Controls.MenuItem {
    id: _itemDelegate
    text: _root.textRole ? (Array.isArray(_root.model) ? modelData[_root.textRole] : model[_root.textRole]) : modelData
    highlighted: _listView.currentIndex === index
    indicator: null
    arrow: null
    width: parent.width

    // Fix non-forwarded press/release event.
    Connections {
      enabled: _root.highlightedIndex === index
      target: _private

      function onSpaceDownChanged() {
        down = _private.spaceDown || pressed
      }
    }

    onHoveredChanged: {
      if (hovered)
        _listView.currentIndex = index
    }
  }

  MouseArea {
    anchors.fill: parent
    acceptedButtons: Qt.NoButton
    cursorShape: Qt.PointingHandCursor
  }

  Controls.FocusBorder {
    id: _focusBorder
    anchors.fill: _background
    showFocus: (_root.visualFocus || _root.activeFocus) && !popup.opened
    animateBorder: !popup.opened && !_root.down
    z: 1
  }
}
