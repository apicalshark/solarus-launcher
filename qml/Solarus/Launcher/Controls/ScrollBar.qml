/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Templates 2.15 as T

import Solarus.Launcher.Styles 1.0 as Styles

// @disable-check M129
T.ScrollBar {
  property real thickness: Styles.Hints.scrollBarFullThickness
  property real thicknessSmall: Styles.Hints.scrollBarSmallThickness
  property bool needed: false

  id: _root
  padding: Styles.Hints.borderWidth
  implicitWidth: orientation === Qt.Vertical ? thickness : 100
  implicitHeight: orientation === Qt.Horizontal ? thickness : 100
  minimumSize: 0.2
  opacity: 0.0
  visible: false
  focusPolicy: Qt.NoFocus

  QtObject {
    property real actualThickness: _root.hovered || _root.pressed ? _root.thickness
                                                                  : _root.thicknessSmall
    id: _private
    Behavior on actualThickness { NumberAnimation { duration: Styles.Hints.animationDuration } }
  }

  contentItem: Item {
    id: _handle
    implicitHeight: _root.orientation === Qt.Horizontal ? _root.thickness : 100
    implicitWidth: _root.orientation === Qt.Vertical ? _root.thickness : 100

    Rectangle {
      x: _root.orientation === Qt.Vertical ? parent.width - width : 0
      y: _root.orientation === Qt.Horizontal ? parent.height - height : 0
      height: _root.orientation === Qt.Vertical ? parent.height : _private.actualThickness - _root.topPadding - _root.bottomPadding
      width: _root.orientation === Qt.Horizontal ?  parent.width : _private.actualThickness - _root.leftPadding - _root.rightPadding
      radius: _root.orientation === Qt.Vertical ? width / 2 : height / 2
      color: Styles.Colors.getScrollBarHandle(_hoverHandler.hovered, _root.pressed, enabled)

      Behavior on color { ColorAnimation { duration: Styles.Hints.animationDuration } }
    }

    HoverHandler { id: _hoverHandler }
  }

  background: Item {
    id: _groove
    anchors.fill: _root
    implicitHeight: _root.orientation === Qt.Horizontal ? _root.thickness : 100
    implicitWidth: _root.orientation === Qt.Vertical ? _root.thickness : 100

    Rectangle {
      x: _root.orientation === Qt.Vertical ? parent.width - width : 0
      y: _root.orientation === Qt.Horizontal ? parent.height - height : 0
      height: _root.orientation === Qt.Vertical ? parent.height : _private.actualThickness
      width:  _root.orientation === Qt.Horizontal ? parent.width : _private.actualThickness
      color: Styles.Colors.getScrollBarGroove(_root.hovered, _root.pressed, enabled)
      radius: _root.orientation === Qt.Vertical ? width / 2 : height / 2

      Behavior on color { ColorAnimation { duration: Styles.Hints.animationDuration } }
    }
  }

  states: [
    State {
      name: "stateNeeded"
      when: _root.needed
      PropertyChanges {
        target: _root
        opacity: 1.0
        visible: true
      }
    },
    State {
      name: "stateNotNeeded"
      when: !_root.needed
      PropertyChanges {
        target: _root
        opacity: 0.0
        visible: false
      }
    }
  ]

  transitions: [
    Transition {
      to: "stateNeeded"
      SequentialAnimation {
        NumberAnimation {
          target: _root
          property: "visible"
          duration: 0
        }
        NumberAnimation {
          target: _root
          property: "opacity"
          duration: Styles.Hints.animationDuration * 2
          easing.type: Easing.OutCubic
        }
      }
    },
    Transition {
      to: "stateNotNeeded"
      SequentialAnimation {
        NumberAnimation {
          target: _root
          property: "opacity"
          duration: Styles.Hints.animationDuration * 2
          easing.type: Easing.OutCubic
        }
        NumberAnimation {
          target: _root
          property: "visible"
          duration: 0
        }
      }
    }
  ]
}
