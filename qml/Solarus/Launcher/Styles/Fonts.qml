/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  readonly property FontLoader primaryFontLoader: FontLoader {
    source: "/solarus/launcher/resources/fonts/Inter-Regular.otf"
  }
  readonly property FontLoader primaryFontLoaderBold: FontLoader {
    source: "/solarus/launcher/resources/fonts/Inter-Bold.otf"
  }
  readonly property FontLoader secondaryFontLoader: FontLoader {
    source: "/solarus/launcher/resources/fonts/UbuntuTitling-Bold.ttf"
  }
  readonly property FontLoader monospaceFontLoader: FontLoader {
    source: "/solarus/launcher/resources/fonts/CascadiaMono-Regular.otf"
  }
  readonly property FontLoader monospaceFontLoaderBold: FontLoader {
    source: "/solarus/launcher/resources/fonts/CascadiaMono-Bold.otf"
  }

  readonly property font primaryFont: Qt.font({
    family: primaryFontLoader.name,
    pixelSize: bodyFontSize
  })

  readonly property font buttonFont: Qt.font({
    family: secondaryFontLoader.name,
    pixelSize: buttonFontSize,
    capitalization: Font.AllUppercase
  })

  readonly property font sideBarItemFont: Qt.font({
    family: secondaryFontLoader.name,
    pixelSize: sideBarItemFontSize,
    capitalization: Font.AllUppercase
  })

  // Base font properties

  readonly property string primaryFontFamily: primaryFontLoader.name
  readonly property string primaryFontFamilyBold: primaryFontLoaderBold.name

  readonly property string secondaryFontFamily: secondaryFontLoader.name

  readonly property string monospaceFontFamily: monospaceFontLoader.name
  readonly property string monospaceFontFamilyBold: monospaceFontLoaderBold.name

  // Roles
  readonly property real buttonFontSize: 16
  readonly property real sideBarItemFontSize: 18

  readonly property string bodyFontFamily: primaryFontFamily
  readonly property string bodyFontFamilyBold: primaryFontFamilyBold
  readonly property real bodyFontSize: 15
  readonly property real bodyLineHeight: 19
  readonly property real bodyLineSpacing: bodyLineHeight - bodyFontSize

  readonly property string captionFontFamily: primaryFontFamily
  readonly property string captionFontFamilyBold: primaryFontFamilyBold
  readonly property real captionFontSize: 15
  readonly property real captionLineHeight: 19

  readonly property string consoleFontFamily: monospaceFontFamily
  readonly property string consoleFontFamilyBold: monospaceFontFamilyBold
  readonly property real consoleFontSize: 13
  readonly property real consoleLineHeight: 17

  readonly property string h1FontFamily: secondaryFontFamily
  readonly property real h1FontSize: 30
  readonly property real h1LineHeight: 36

  readonly property string h2FontFamily: secondaryFontFamily
  readonly property real h2FontSize: 24
  readonly property real h2LineHeight: 26

  readonly property string h3FontFamily: secondaryFontFamily
  readonly property real h3FontSize: 20
  readonly property real h3LineHeight: 24

  readonly property string h4FontFamily: primaryFont
  readonly property string h4FontFamilyBold: primaryFontFamilyBold
  readonly property real h4FontSize: 16
  readonly property real h4LineHeight: 20

  readonly property string uppercaseLabelFontFamily: secondaryFontFamily
  readonly property real uppercaseLabelFontSize: 20
  readonly property real uppercaseLabelLineHeight: 24

  function getRenderType(pixelSize) {
    return pixelSize < 12 ? Text.NativeRendering : Text.QtRendering
  }
}
