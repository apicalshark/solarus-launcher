/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  readonly property string logo: "/solarus/launcher/resources/images/brand/solarus-logo-full.svg"
  readonly property string icon: "/solarus/launcher/resources/images/brand/solarus-logo-icon.svg"
  readonly property string logoText: "/solarus/launcher/resources/images/brand/solarus-logo-text.svg"
  readonly property string libraryIcon: "/solarus/launcher/resources/images/brand/solarus-library-icon.svg"
  readonly property string questIcon: "/solarus/launcher/resources/images/brand/solarus-quest-icon.svg"
}
