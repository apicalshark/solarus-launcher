/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  readonly property real bottomBarHeight: 64

  readonly property int animationDuration: 150
  readonly property int spacing: 16
  readonly property int radius: 6
  readonly property int iconSize: 16
  readonly property int borderWidth: 1
  readonly property int pressDelta: 1
  readonly property int focusBorderWidth: 2
  readonly property int controlDefaultWidth: 200
  readonly property int controlHeight: 32
  readonly property int controlWidthSmall: 20
  readonly property int sliderHandleSize: controlWidthSmall
  readonly property int tooltipDelay: 500
  readonly property int tooltipTimeout: 5000
  readonly property int sliderDefaultWidth: controlDefaultWidth
  readonly property int menuItemHeight: 28
  readonly property int scrollBarFullThickness: 10
  readonly property int scrollBarSmallThickness: 6
  readonly property int scrollBarMargin: 6
  readonly property int scrollStep: 100
  readonly property int scrollStepAutoRepeat: 600
  readonly property int scrollMargin: spacing * 2
  readonly property bool hoverEnabled: true
  readonly property int sideBarExpandedWidth: 270
  readonly property int sideBarContractedWidth: 56
  readonly property int sideBarBreakpoint: 832
  readonly property int windowDefaultWidth: 1280
  readonly property int windowDefaultHeight: 720
  readonly property int contentMaxWidth: 720
  readonly property int homeContentMaxWidth: questThumbnailWidth * 3 + spacing * 6
  readonly property int questThumbnailWidth: 280
  readonly property int questThumbnailHeight: Math.round(questThumbnailWidth / questThumbnailAspectRatio)
  readonly property real questThumbnailAspectRatio: 350 / 180
  readonly property int questDialogWidth: 680
  readonly property int newsThumbnailWidth: 280
  readonly property int newsThumbnailHeight: 240
  readonly property int searchBarHeight: 72
  readonly property int scrollBarDisappearingDelay: 2000
  readonly property int blurRadius: 32
  readonly property int questUpdateThumbnailWidth: controlHeight * 3
  readonly property int questUpdateThumbnailHeight: questUpdateThumbnailWidth / questThumbnailAspectRatio
  readonly property int questUpdateHeight: questUpdateThumbnailHeight + spacing * 2
}
