/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
pragma Singleton

import QtQuick 2.15

QtObject {
  enum Role {
    Primary,
    Secondary
  }

  readonly property color shadowColor: "#FF000000"
  readonly property color shadowColorLighter: "#10000000"

  readonly property color primaryColor: "#FF7B00"
  readonly property color primaryColorHover: "#FF9800"
  readonly property color primaryColorPressed: "#FFAB00"
  readonly property color primaryColorLighter: "#FFBC00"
  readonly property color primaryColorLightest: "#FFDC18"
  readonly property color primaryColorDisabled: secondaryColorDisabled

  readonly property color secondaryColor: "#2E1B66"
  readonly property color secondaryColorHover: "#382474"
  readonly property color secondaryColorPressed: "#44307F"
  readonly property color secondaryColorLighter: "#593BB0"
  readonly property color secondaryColorLightest: "#643FCF"
  readonly property color secondaryColorDisabled: "#291A58"
  readonly property color secondaryColorDarker: "#241550"
  readonly property color secondaryColorDarker2: "#29185B"

  readonly property color tertiaryColor: "#8A73CC"
  readonly property color tertiaryColorHover: "#A58DE8"
  readonly property color tertiaryColorPressed: "#B79FFF"
  readonly property color tertiaryColorLighter: "#D9CCFF"
  readonly property color tertiaryColorLightest: "#EAE3FF"
  readonly property color tertiaryColorDisabled: "#4F3991"
  readonly property color tertiaryColorDarker: "#291A58"

  readonly property color tooltipColor: "#0B0425"
  readonly property color tooltipBorderColor: secondaryColorHover
  readonly property color tooltipShadowColor: "#C0000000"
  readonly property color tooltipTextColor: tertiaryColor

  readonly property color menuColor: colorWithAlpha(tooltipColor, 0.95)
  readonly property color menuBorderColor: secondaryColor
  readonly property color menuSeparatorColor: colorWithAlpha(secondaryColor, 0.95)

  readonly property color sliderHandleColor: "#F2F0FF"
  readonly property color sliderHandleColorHover: foregroundColor
  readonly property color sliderHandleColorPressed: sliderHandleColorHover
  readonly property color sliderHandleColorDisabled: tertiaryColorDisabled

  readonly property color cardColor: "#150E33"
  readonly property color cardColorHover: "#18103A"
  readonly property color cardBorderColor: secondaryColorDarker
  readonly property color cardBorderColorHover: secondaryColorDarker2

  readonly property color windowColor: "#19103B"
  readonly property color popupColor: colorWithAlpha(windowColor, 0.8)
  readonly property color windowColorDarker: "#120C23"
  readonly property color overlayColor: colorWithAlpha(colorFromHex("#07001D"), 0.75)
  readonly property color imageOverlayColor: colorWithAlpha(sideBarColor, 0.85)

  readonly property color bottomBarColor: colorWithAlpha(overlayColor, 0.9)
  readonly property color bottomBarItemColor: "#10ffffff"
  readonly property color bottomBarItemForegroundColor: textColorBody

  readonly property color foregroundColor: "#FFFFFF"

  readonly property color textColorBody: foregroundColor
  readonly property color textColorConsole: foregroundColor
  readonly property color textColorCaption: tertiaryColor
  readonly property color textColorH1: tertiaryColor
  readonly property color textColorH2: tertiaryColorHover
  readonly property color textColorH3: tertiaryColorPressed
  readonly property color textColorH4: foregroundColor
  readonly property color textColorDisabled: tertiaryColorDisabled
  readonly property color textColorCaptionDisabled: tertiaryColorDarker
  readonly property color textColorUppercaseLabel: foregroundColor

  readonly property color textColorDebug: "#7FFFFFFF"
  readonly property color textColorInfo: "#7EA0FF"
  readonly property color textColorWarning: "#FFBC00"
  readonly property color textColorError: "#FC4850"
  readonly property color textColorSuccess: "#24C192"
  readonly property color textColorFatal: textColorError

  readonly property color scrollBarGroove: "#00000000"
  readonly property color scrollBarGroovePressed: "#55000000"
  readonly property color scrollBarGrooveHovered: "#44000000"
  readonly property color scrollBarGrooveDisabled: "#00000000"

  readonly property color sideBarColor: secondaryColorDarker
  readonly property color sideBarSeparatorColor: secondaryColorPressed

  readonly property color sideBarItemColor: colorWithAlpha(sideBarItemColorHover, 0)
  readonly property color sideBarItemColorHover: secondaryColor
  readonly property color sideBarItemColorPressed: secondaryColorHover
  readonly property color sideBarItemColorHoverChecked: secondaryColorPressed
  readonly property color sideBarItemColorPressedChecked: secondaryColorLighter
  readonly property color sideBarItemColorChecked: secondaryColorPressed

  readonly property color tableRowOdd: colorWithAlpha(tertiaryColorPressed, 0.1)
  readonly property color tableRowEven: colorWithAlpha(tertiaryColorPressed, 0)

  // Keeps the R, G and B channels but sets the A channel as alphaValue.
  // @param alphaValue Alpha between 0.0 and 1.0
  function colorWithAlpha(color, alphaValue) {
    return color ? Qt.rgba(color.r, color.g, color.b, alphaValue) : "black"
  }

  // Creates a QML color from its string hexadecimal representation #RRGGBB.
  function colorFromHex(colorHex) {
    const match = colorHex.match(/^#([0-9a-fA-F]{6})$/i)[1];
    if (match) {
      const r = parseInt(match.substr(0, 2), 16) / 255
      const g = parseInt(match.substr(2, 2), 16) / 255
      const b = parseInt(match.substr(4, 2), 16) / 255
      return Qt.rgba(r, g, b)
    }
    return Qt.rgba(0, 0, 0, 1)
  }

  // --- Base colors ---

  function getPrimary(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return primaryColorDisabled
    else if (isPressed) return primaryColorPressed
    else if (isHovered) return primaryColorHover
    else return primaryColor
  }

  function getSecondary(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return secondaryColorDisabled
    else if (isPressed) return secondaryColorPressed
    else if (isHovered) return secondaryColorHover
    else return secondaryColor
  }

  function getTertiary(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return tertiaryColorDisabled
    else if (isPressed) return tertiaryColorPressed
    else if (isHovered) return tertiaryColorHover
    else return tertiaryColor
  }

  function getPrimaryBorder(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return colorWithAlpha(primaryColor, 0)
    else if (isPressed) return primaryColorLightest
    else if (isHovered) return primaryColorLighter
    else return primaryColorPressed
  }

  function getSecondaryBorder(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return colorWithAlpha(secondaryColor, 0)
    else if (isPressed) return secondaryColorLightest
    else if (isHovered) return secondaryColorLighter
    else return secondaryColorPressed
  }

  function getTertiaryBorder(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return colorWithAlpha(tertiaryColor, 0)
    else if (isPressed) return tertiaryColorLightest
    else if (isHovered) return tertiaryColorLighter
    else return tertiaryColorPressed
  }

  // --- Button ---

  function getButtonBackground(role, isHovered, isPressed, isEnabled) {
    return role === Colors.Role.Primary ? getPrimary(isHovered, isPressed, isEnabled)
                                        : getSecondary(isHovered, isPressed, isEnabled)
  }

  function getButtonBorder(role, isHovered, isPressed, isEnabled) {
    return role === Colors.Role.Primary ? getPrimaryBorder(isHovered, isPressed, isEnabled)
                                        : getSecondaryBorder(isHovered, isPressed, isEnabled)
  }

  function getButtonForeground(isEnabled) {
    return isEnabled ? textColorBody : textColorDisabled
  }

  // --- TextField --

  function getTextFieldBackground(isEnabled) {
    return isEnabled ? windowColorDarker : windowColor
  }

  function getTextFieldBorder(hasFocus, isHovered, isPressed, isEnabled) {
    return hasFocus ? getPrimaryBorder(false, false, isEnabled)
                    : getSecondaryBorder(isHovered, isPressed || isHovered, isEnabled)
  }

  function getTextFieldPlaceholder(isEnabled) {
    return isEnabled ? textColorCaption : textColorDisabled
  }

  function getTextFieldText(isEnabled) {
    return isEnabled ? textColorBody : textColorDisabled
  }

  // --- MenuItem ---

  function getMenuItemBackground(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return "transparent"
    if (isPressed) return primaryColorPressed
    else if (isHovered) return primaryColor
    else return colorWithAlpha(primaryColor, 0)
  }

  function getMenuItemText(isEnabled) {
    return isEnabled ? textColorBody : textColorDisabled
  }

  function getMenuItemShortcut(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return colorWithAlpha(textColorDisabled, 0.3)
    if (isPressed || isHovered) return textColorBody
    else return textColorCaption
  }

  // --- Slider ---

  function getSliderGroove(isEnabled, alternateBackground) {
    return isEnabled ? alternateBackground ? colorWithAlpha(foregroundColor, 0.5) : secondaryColorHover
                     : secondaryColorDisabled
  }

  function getSliderTrack(isEnabled) {
    return isEnabled ? primaryColorHover : colorWithAlpha(tertiaryColorDisabled, 0.5)
  }

  function getSliderHandle(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return sliderHandleColorDisabled
    else if (isPressed) return sliderHandleColorPressed
    else if (isHovered) return sliderHandleColorHover
    else return sliderHandleColor
  }

  // --- Switch ---

  function getSwitchGroove(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return secondaryColorDisabled
    else if (isPressed) return secondaryColorPressed
    else if (isHovered) return secondaryColorHover
    else return secondaryColor
  }

  function getSwitchTrack(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return secondaryColorDisabled
    else if (isPressed) return primaryColorPressed
    else if (isHovered) return primaryColorHover
    else return primaryColor
  }

  function getSwitchHandle(isHovered, isPressed, isEnabled, isChecked) {
    if (isChecked) {
      if (!isEnabled) return secondaryColorHover
      else if (isPressed) return primaryColorLightest
      else if (isHovered) return primaryColorLightest
      else return primaryColorLighter
    } else {
      if (!isEnabled) return secondaryColorHover
      else if (isPressed) return tertiaryColorHover
      else if (isHovered) return tertiaryColorHover
      else return tertiaryColor
    }
  }

  // -- List ---

  function getListItemBackground(isCurrent, isHovered, isPressed) {
    if (isPressed) {
      return primaryColorPressed
    } else if (isCurrent) {
      return primaryColor
    } else if (isHovered) {
      return primaryColorHover
    } else {
      return colorWithAlpha(primaryColor, 0)
    }
  }

  // --- Text ---

  function getControlForeground(isEnabled) {
    return isEnabled ? textColorBody : textColorDisabled
  }

  function getCaptionForeground(isEnabled) {
    return isEnabled ? textColorCaption : textColorCaptionDisabled
  }

  function getLinkColor(isHovered, isEnabled) {
    if (!isEnabled) return textColorDisabled
    return isHovered ? primaryColorLighter : primaryColorHover
  }

  // --- ScrollView --

  function getScrollBarHandle(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return secondaryColorDisabled
    if (isPressed) return tertiaryColorHover
    if (isHovered) return tertiaryColor
    return secondaryColorPressed
  }

  function getScrollBarGroove(isHovered, isPressed, isEnabled) {
    if (!isEnabled) return scrollBarGrooveDisabled
    if (isPressed) return scrollBarGroovePressed
    if (isHovered) return scrollBarGrooveHovered
    return scrollBarGroove
  }

  // --- SideBar ---

  function getSideBarItemBackground(isHovered, isPressed, isEnabled, isChecked) {
    if (isChecked) {
      if (!isEnabled) return sideBarColor
      else if (isPressed) return sideBarItemColorPressedChecked
      else if (isHovered) return sideBarItemColorHoverChecked
      else return sideBarItemColorChecked
    } else {
      if (!isEnabled) return sideBarColor
      else if (isPressed) return sideBarItemColorPressed
      else if (isHovered) return sideBarItemColorHover
      else return sideBarItemColor
    }
  }
}
