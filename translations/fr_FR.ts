<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="93"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="101"/>
        <source>A game launcher and browser for Solarus, a free and open-source Action-RPG/Adventure 2D game engine.</source>
        <translation>Un lanceur et une ludothèque pour Solarus, un moteur de jeu 2D d&apos;Action-RPG/Aventure libre et open-source.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="127"/>
        <source>This program is licensed under the &lt;a href=&apos;%1&apos;&gt;GNU Public License, version 3&lt;/a&gt;.</source>
        <translation>Ce logiciel est sous licence &lt;a href=&apos;%1&apos;&gt;GNU Public License, version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="134"/>
        <source>Included assets are licensed under &lt;a href=&apos;%2&apos;&gt;CC-BY-SA 4.0&lt;/a&gt;.</source>
        <translation>Les ressources incluses sont sous licence &lt;a href=&apos;%2&apos;&gt;CC-BY-SA 4.0&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="177"/>
        <source>User Manual</source>
        <translation>Manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="178"/>
        <source>Show User Manual</source>
        <translation>Afficher le manuel utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="190"/>
        <source>Changelog</source>
        <translation>Journal des modifications</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="191"/>
        <source>Show Changelog</source>
        <translation>Afficher le journal des modifications</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="203"/>
        <source>Source code</source>
        <translation>Code source</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="216"/>
        <source>Report a bug</source>
        <translation>Remonter un bogue</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="229"/>
        <source>View website</source>
        <translation>Se rendre sur le site web</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="31"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="37"/>
        <source>Check For Updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="49"/>
        <source>Preferences…</source>
        <translation>Préférences…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="63"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="74"/>
        <source>Quest</source>
        <translation>Quête</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="80"/>
        <source>Play</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="92"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="103"/>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="109"/>
        <source>Full Screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="123"/>
        <source>Show Quest as Full Screen</source>
        <translation>Afficher les quêtes en plein écran</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="134"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="140"/>
        <source>Changelog…</source>
        <translation>Journal des modifications…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="152"/>
        <source>User Manual…</source>
        <translation>Manuel utilisateur…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="164"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
</context>
<context>
    <name>ContentLoader</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContentLoader.qml" line="53"/>
        <source>Error: impossible to load the view</source>
        <translation>Erreur : impossible de charger la vue</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="39"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="48"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="56"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="68"/>
        <source>Select All</source>
        <translation>Sélectionner tout</translation>
    </message>
</context>
<context>
    <name>ControlsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="43"/>
        <source>Controls</source>
        <translation>Contrôles</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="52"/>
        <source>You can control Solarus Launcher with these devices:</source>
        <translation>Vous pouvez contrôller Solarus Launcher avec ces équipements :</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="60"/>
        <source>Mouse</source>
        <translation>Souris</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="68"/>
        <source>Keyboard</source>
        <translation>Clavier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="76"/>
        <source>Gamepad</source>
        <translation>Manette</translation>
    </message>
</context>
<context>
    <name>DirectoryChooser</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="55"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Chemin vers le dossier des quêtes Solarus</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="83"/>
        <source>Open file dialog</source>
        <translation>Ouvrir la fenêtre de choix de dossier</translation>
    </message>
</context>
<context>
    <name>FinishPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="49"/>
        <source>Ready!</source>
        <translation>Prêt !</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="57"/>
        <source>Have fun!</source>
        <translation>Amusez-vous bien !</translation>
    </message>
</context>
<context>
    <name>FolderChoicePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="44"/>
        <source>Quests Folder</source>
        <translation>Dossier des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="53"/>
        <source>This will be the the folder where Solarus Launcher looks for existing Solarus Quests (i.e. games), and downloads them.</source>
        <translation>Ce sera dans ce dossier que Solarus Launcher va rechercher des quêtes Solarus (c&apos;est-à-dire des jeux), et les télécharger.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="67"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Chemin vers le dossier des quêtes Solarus</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="141"/>
        <source>If the folder doesn&apos;t exist yet, it will be automatically created.</source>
        <translation>Si le dossier n&apos;existe pas, il sera créé automatiquement.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="161"/>
        <source>It can still be changed later in the app preferences.</source>
        <translation>Il pourra toujours être modifié plus tard dans les préférénces de l&apos;application.</translation>
    </message>
</context>
<context>
    <name>FolderLoadingPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="50"/>
        <source>Directory creation failed</source>
        <translation>Échec de la création du dossier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="52"/>
        <source>Path is not a directory</source>
        <translation>Le chemin n&apos;est pas un chemin de dossier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="54"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="61"/>
        <source>It is impossible to create such a directory.
Check that you have correct access rights.</source>
        <translation>Impossible de créer un tel dossier.
Veuillez vérifiez que vous avez les droits d&apos;accès.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="63"/>
        <source>This path seems to already exists, but points to a file instead of a directory.</source>
        <translation>Ce chemin semble déjà exister, mais pointe vers un fichier au lieu d&apos;un dossier.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="65"/>
        <source>Error code: %1</source>
        <translation>Code d&apos;erreur : %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="72"/>
        <source>Error!</source>
        <translation>Erreur !</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="100"/>
        <source>Loading Quests</source>
        <translation>Chargement des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="109"/>
        <source>Solarus Launcher is loading your Solarus Quest library…</source>
        <translation>Solarus Launcher est en train de charger votre bibliothèque de quêtes Solarus…</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="112"/>
        <source>Welcome to Solarus Launcher</source>
        <translation>Bienvenue dans Solarus Launcher</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="120"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation>Découvrez, téléchargez et jouez à des quêtes Solarus sur votre ordinateur.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="130"/>
        <source>Recently played</source>
        <translation>Récemment jouées</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="175"/>
        <source>Once you&apos;ve played some Solarus quests, the most recent ones will appear here.</source>
        <translation>Une fois que vous aurez joué à des quêtes Solarus, les plus récemment lancées apparaîtront ici.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="180"/>
        <source>Go to Quest list</source>
        <translation>Aller à la liste des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="310"/>
        <source>Loading…</source>
        <translation>Chargement…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="209"/>
        <source>See all quests</source>
        <translation>Voir toutes les quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="225"/>
        <source>Latest News</source>
        <translation>Dernières actualités</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="287"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="235"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="288"/>
        <source>Refresh news articles</source>
        <translation>Rafraîchir les actualités</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="282"/>
        <source>Can&apos;t fetch latest news.</source>
        <translation>Impossible de télécharger les dernières actualités.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="318"/>
        <source>More news</source>
        <translation>Plus d&apos;articles</translation>
    </message>
</context>
<context>
    <name>LanguageUtils</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="29"/>
        <source>German</source>
        <translation>Allemand</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="30"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="31"/>
        <source>Spanish</source>
        <translation>Espagnol</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="32"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="33"/>
        <source>Italian</source>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="34"/>
        <source>Portuguese</source>
        <translation>Portugais</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="35"/>
        <source>Russian</source>
        <translation>Russe</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="36"/>
        <source>Chinese</source>
        <translation>Chinois</translation>
    </message>
</context>
<context>
    <name>LocalQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>Titre inconnu</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="130"/>
        <source>Unknown Author</source>
        <translation>Auteur inconnu</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="171"/>
        <source>Play</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="184"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="197"/>
        <source>Check for Updates</source>
        <translation>Vérifier mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="209"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="221"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="233"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="247"/>
        <source>Unknown description.</source>
        <translation>Description inconnue.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="262"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="263"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="269"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="276"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="281"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="287"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="292"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="298"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="303"/>
        <source>Unknown Value</source>
        <translation>Valeur inconnue</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="268"/>
        <source>Solarus Version</source>
        <translation>Version de Solarus</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="275"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="280"/>
        <source>Languages</source>
        <translation>Langues</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="286"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="291"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="297"/>
        <source>Release Date</source>
        <translation>Date de sortie</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="302"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
</context>
<context>
    <name>LocalQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="163"/>
        <source>Play</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="171"/>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="183"/>
        <source>Show information</source>
        <translation>Afficher les informations</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="191"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="199"/>
        <source>Check For Updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="211"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>LocalQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="54"/>
        <source>Looking for Solarus quests…</source>
        <translation>Recherche de quêtes Solarus…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="52"/>
        <source>No quests found</source>
        <translation>Aucune quête trouvée</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="64"/>
        <source>Please wait while the app is looking for Solarus quests.</source>
        <translation>Veuillez patienter pendant que l&apos;application recherche des quêtes Solarus.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="50"/>
        <source>Configuration error</source>
        <translation>Erreur de configuration</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="60"/>
        <source>You need to add a valid directory that contains Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>Vous devez sélectionner un dossier valide qui contienne des quêtes Solarus.
Veuillez changer ce réglage dans les paramètres de l&apos;application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="62"/>
        <source>The app cannot find any Solarus quest in the directory path you provided.
Have you tried downloading some quests?</source>
        <translation>L&apos;application n&apos;a pas trouvé de quêtes Solarus dans le dossier que vous avez spécifié.
Avez-vous essayé d&apos;y télécharger des quêtes ?</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="70"/>
        <source>Go to Settings page</source>
        <translation>Aller aux préférences</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="72"/>
        <source>Browse and download quests</source>
        <translation>Découvrez et téléchargez des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="276"/>
        <source>Sort by title</source>
        <translation>Trier par titre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="281"/>
        <source>Sort by release date</source>
        <translation>Trier par date de sortie</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="286"/>
        <source>Sort by last played time</source>
        <translation>Trier par date de dernier lancement</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="337"/>
        <source>Are you sure?</source>
        <translation>Êtes-vous sûr(e) ?</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="338"/>
        <source>Do you really want to remove &apos;&lt;b&gt;%1&lt;/b&gt;&apos; from disk?</source>
        <translation>Voulez-vous vraiment supprimer &apos;&lt;b&gt;%1&lt;/b&gt;&apos; du disque ?</translation>
    </message>
</context>
<context>
    <name>OnboardingDevice</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Onboarding/OnboardingDevice.qml" line="80"/>
        <source>Not connected</source>
        <translation>Non connecté</translation>
    </message>
</context>
<context>
    <name>OnlineQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>Titre inconnu</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="129"/>
        <source>Unknown Author</source>
        <translation>Auteur inconnu</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="170"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="183"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="197"/>
        <source>Unknown description.</source>
        <translation>Description inconnue.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="212"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="213"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="219"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="226"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="231"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="237"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="242"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="248"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="253"/>
        <source>Unknown Value</source>
        <translation>Valeur inconnue</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="218"/>
        <source>Solarus Version</source>
        <translation>Version de Solarus</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="225"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="230"/>
        <source>Languages</source>
        <translation>Langues</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="236"/>
        <source>Players</source>
        <translation>Joueurs</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="241"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="247"/>
        <source>Release Date</source>
        <translation>Date de sortie</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="252"/>
        <source>Website</source>
        <translation>Site web</translation>
    </message>
</context>
<context>
    <name>OnlineQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="163"/>
        <source>Show information</source>
        <translation>Afficher les informations</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="171"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
</context>
<context>
    <name>OnlineQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="55"/>
        <source>Configuration error</source>
        <translation>Erreur de configuration</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="57"/>
        <source>No quests found</source>
        <translation>Aucune quête trouvée</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="59"/>
        <source>Asking the server for Solarus quests…</source>
        <translation>Récupération des quêtes Solarus depuis le serveur…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="65"/>
        <source>You need to set a valid URL to retrieve Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>Vous devez utiliser une URL valide pour récupérer la liste des quêtes Solarus.
Veuillez changer ce réglage dans les paramètres de l&apos;application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="67"/>
        <source>There are no quests or the server isn&apos;t responding.</source>
        <translation>Aucune quête, ou bien le serveur ne répond pas.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="69"/>
        <source>Please wait while the app is downloading the Solarus quests list.</source>
        <translation>Veuillez patienter pendant que l&apos;application télécharge la liste des quêtes Solarus.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="75"/>
        <source>Go to Settings page</source>
        <translation>Aller aux préférences</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="77"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="272"/>
        <source>Sort by title</source>
        <translation>Trier par titre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="277"/>
        <source>Sort by release date</source>
        <translation>Trier par date de sortie</translation>
    </message>
</context>
<context>
    <name>QuestThumbnailOverlay</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="118"/>
        <source>Downloading</source>
        <translation>En cours de téléchargement</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="161"/>
        <source>Downloaded</source>
        <translation>Téléchargée</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="184"/>
        <source>Playing</source>
        <translation>En cours</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="125"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="155"/>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
</context>
<context>
    <name>SearchOptions</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="146"/>
        <source>Sort by title</source>
        <translation>Trier par titre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="151"/>
        <source>Sort by release date</source>
        <translation>Trier par date de sortie</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="156"/>
        <source>Sort by last played time</source>
        <translation>Trier par date de dernier lancement</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="78"/>
        <source>Application Settings</source>
        <translation>Réglages de l&apos;application</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="83"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="88"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="89"/>
        <source>Choose the language this application is displayed in.</source>
        <translation>Choisir la langue dans laquelle est affichée cette application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="104"/>
        <source>Quests directory</source>
        <translation>Dossier des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="105"/>
        <source>This application will look for Solarus quests in this directory.</source>
        <translation>Cette application va rechercher des quêtes Solarus dans ce dossier.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="125"/>
        <source>Force software rendering for app</source>
        <translation>Forcer le rendu logiciel pour cette application</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="126"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="256"/>
        <source>Enable this option if your GPU doesn&apos;t support OpenGL. Restart required.</source>
        <translation>Activez cette option si votre carte graphique ne supporte pas OpenGL. Redémarrage nécessaire.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="169"/>
        <source>Adjust audio volume for this application&apos;s sound effects.</source>
        <translation>Ajuster le volume des effets sonores de cette application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="189"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="302"/>
        <source>Enable auto-updates</source>
        <translation>Mises à jour automatiques</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="190"/>
        <source>Automatically check and download updates for this application.</source>
        <translation>Vérifier et télécharger automatiquement les mises à jour pour cette application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="313"/>
        <source>Checking frequency</source>
        <translation>Fréquence de vérification</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="202"/>
        <source>Define how frequently this application should look for updates.</source>
        <translation>Définir à quelle fréquence cette application devrait vérifier s&apos;il y a une mise à jour.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="228"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="340"/>
        <source>Check for updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="245"/>
        <source>Quest Settings</source>
        <translation>Réglages des quêtes</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="255"/>
        <source>Force software rendering for games</source>
        <translation>Forcer le rendu logiciel pour les jeux</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="266"/>
        <source>Full screen games</source>
        <translation>Jeux en plein écran</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="267"/>
        <source>Display games in full screen.</source>
        <translation>Afficher les jeux en plein écran.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="284"/>
        <source>Enable quests audio</source>
        <translation>Activer le son des jeux</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="285"/>
        <source>Play the games audio.</source>
        <translation>Jouer le son des jeux.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="303"/>
        <source>Automatically check and download updates for quests.</source>
        <translation>Vérifier et télécharger automatiquement les mises à jour pour les jeux.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="314"/>
        <source>Define how frequently this application should look for quest updates.</source>
        <translation>Définir à quelle fréquence  les jeux devraient vérifier s&apos;ils ont une mise à jour.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="363"/>
        <source>Reset settings</source>
        <translation>Remettre à zéro les préférences</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="364"/>
        <source>Reset the application settings to defaults. Doesn&apos;t remove quests and saves.</source>
        <translation>Réinitialise les paramètres à ceux par défaut. Ne supprime pas les jeux ni leurs sauvegardes.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="109"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Chemin vers le dossier des quêtes Solarus</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="120"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="250"/>
        <source>Video</source>
        <translation>Réglages vidéo</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="138"/>
        <source>Full screen app</source>
        <translation>Application en plein écran</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="139"/>
        <source>Display this application in full screen.</source>
        <translation>Afficher cette application en plein écran.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="279"/>
        <source>Audio</source>
        <translation>Réglages audio</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="156"/>
        <source>Enable app audio</source>
        <translation>Activer le son de l&apos;application</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="157"/>
        <source>Play sound effects in this application.</source>
        <translation>Jouer le son de cette application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="168"/>
        <source>App audio volume</source>
        <translation>Volume du son de l&apos;application</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="184"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="297"/>
        <source>Updates</source>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="229"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="341"/>
        <source>Last checked: %1</source>
        <translation>Dernière vérification : %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="345"/>
        <source>Check</source>
        <translation>Vérifier</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="358"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="367"/>
        <source>Reset</source>
        <translation>Remettre à zéro</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="378"/>
        <source>Reset Settings to defaults</source>
        <translation>Restaurer les préférences aux valeurs par défaut</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="380"/>
        <source>Do you really want to reset settings to defaults?
All your current settings will be erased.

Note: Your Solarus quests and their save files will be kept.</source>
        <translation>Voulez-vous vraiment remettre les préférences aux valeurs par défaut ?
Tous vous réglages actuels seront effacés.

Note : Vos quêtes Solarus et leurs sauvegardes seront conservées.</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="134"/>
        <source>Playing: %1</source>
        <translation>En cours : %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="175"/>
        <source>Home</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="176"/>
        <source>Local Quests</source>
        <translation>Quêtes locales</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="177"/>
        <source>Online Quests</source>
        <translation>Quêtes en ligne</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="178"/>
        <source>Playing</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="179"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="180"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
</context>
<context>
    <name>Switch</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="99"/>
        <source>On</source>
        <translation>On</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="113"/>
        <source>Off</source>
        <translation>Off</translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="50"/>
        <source>Welcome to Solarus Launcher</source>
        <translation>Bienvenue dans Solarus Launcher</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="59"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation>Découvrez, téléchargez et jouez à des quêtes Solarus sur votre ordinateur.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="67"/>
        <source>Skip tutorial</source>
        <translation>Passer le tutoriel</translation>
    </message>
</context>
<context>
    <name>solarus::launcher::controllers::CheckFrequencyListModel</name>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="75"/>
        <source>Every Start</source>
        <translation>À chaque démarrage</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="77"/>
        <source>Every Day</source>
        <translation>Tous les jours</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="79"/>
        <source>Every Week</source>
        <translation>Toutes les semaines</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="81"/>
        <source>Every Month</source>
        <translation>Tous les mois</translation>
    </message>
</context>
</TS>
