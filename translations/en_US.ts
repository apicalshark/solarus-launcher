<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="93"/>
        <source>Version %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="101"/>
        <source>A game launcher and browser for Solarus, a free and open-source Action-RPG/Adventure 2D game engine.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="127"/>
        <source>This program is licensed under the &lt;a href=&apos;%1&apos;&gt;GNU Public License, version 3&lt;/a&gt;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="134"/>
        <source>Included assets are licensed under &lt;a href=&apos;%2&apos;&gt;CC-BY-SA 4.0&lt;/a&gt;.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="177"/>
        <source>User Manual</source>
        <translation>User Manual</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="178"/>
        <source>Show User Manual</source>
        <translation>Show User Manual</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="190"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="191"/>
        <source>Show Changelog</source>
        <translation>Show Changelog</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="203"/>
        <source>Source code</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="216"/>
        <source>Report a bug</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="229"/>
        <source>View website</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="31"/>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="37"/>
        <source>Check For Updates</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="49"/>
        <source>Preferences…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="63"/>
        <source>Quit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="74"/>
        <source>Quest</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="80"/>
        <source>Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="92"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="103"/>
        <source>View</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="109"/>
        <source>Full Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="123"/>
        <source>Show Quest as Full Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="134"/>
        <source>Help</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="140"/>
        <source>Changelog…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="152"/>
        <source>User Manual…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="164"/>
        <source>About</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ContentLoader</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContentLoader.qml" line="53"/>
        <source>Error: impossible to load the view</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="39"/>
        <source>Copy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="48"/>
        <source>Paste</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="56"/>
        <source>Cut</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="68"/>
        <source>Select All</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ControlsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="43"/>
        <source>Controls</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="52"/>
        <source>You can control Solarus Launcher with these devices:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="60"/>
        <source>Mouse</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="68"/>
        <source>Keyboard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="76"/>
        <source>Gamepad</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DirectoryChooser</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="55"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Path to the Solarus Quests directory</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="83"/>
        <source>Open file dialog</source>
        <translation>Open the  window to choose a directory</translation>
    </message>
</context>
<context>
    <name>FinishPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="49"/>
        <source>Ready!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="57"/>
        <source>Have fun!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FolderChoicePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="44"/>
        <source>Quests Folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="53"/>
        <source>This will be the the folder where Solarus Launcher looks for existing Solarus Quests (i.e. games), and downloads them.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="67"/>
        <source>Path to the Solarus quests directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="141"/>
        <source>If the folder doesn&apos;t exist yet, it will be automatically created.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="161"/>
        <source>It can still be changed later in the app preferences.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FolderLoadingPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="50"/>
        <source>Directory creation failed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="52"/>
        <source>Path is not a directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="54"/>
        <source>Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="61"/>
        <source>It is impossible to create such a directory.
Check that you have correct access rights.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="63"/>
        <source>This path seems to already exists, but points to a file instead of a directory.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="65"/>
        <source>Error code: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="72"/>
        <source>Error!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="100"/>
        <source>Loading Quests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="109"/>
        <source>Solarus Launcher is loading your Solarus Quest library…</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="112"/>
        <source>Welcome to Solarus Launcher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="120"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="130"/>
        <source>Recently played</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="175"/>
        <source>Once you&apos;ve played some Solarus quests, the most recent ones will appear here.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="180"/>
        <source>Go to Quest list</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="310"/>
        <source>Loading…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="209"/>
        <source>See all quests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="225"/>
        <source>Latest News</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="287"/>
        <source>Refresh</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="235"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="288"/>
        <source>Refresh news articles</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="282"/>
        <source>Can&apos;t fetch latest news.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="318"/>
        <source>More news</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LanguageUtils</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="29"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="30"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="31"/>
        <source>Spanish</source>
        <translation>Spanish</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="32"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="33"/>
        <source>Italian</source>
        <translation>Italian</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="34"/>
        <source>Portuguese</source>
        <translation>Portuguese</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="35"/>
        <source>Russian</source>
        <translation>Russian</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="36"/>
        <source>Chinese</source>
        <translation>Chinese</translation>
    </message>
</context>
<context>
    <name>LocalQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>Unknown Title</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="130"/>
        <source>Unknown Author</source>
        <translation>Unknown Author</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="171"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="184"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="197"/>
        <source>Check for Updates</source>
        <translation>Check for Updates</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="209"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="221"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="233"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="247"/>
        <source>Unknown description.</source>
        <translation>Unknown Description.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="262"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="263"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="269"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="276"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="281"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="287"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="292"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="298"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="303"/>
        <source>Unknown Value</source>
        <translation>Unknown value</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="268"/>
        <source>Solarus Version</source>
        <translation>Solarus Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="275"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="280"/>
        <source>Languages</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="286"/>
        <source>Players</source>
        <translation>Players</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="291"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="297"/>
        <source>Release Date</source>
        <translation>Release Date</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="302"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>LocalQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="163"/>
        <source>Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="171"/>
        <source>Stop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="183"/>
        <source>Show information</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="191"/>
        <source>Update</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="199"/>
        <source>Check For Updates</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="211"/>
        <source>Remove</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LocalQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="54"/>
        <source>Looking for Solarus quests…</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="52"/>
        <source>No quests found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="64"/>
        <source>Please wait while the app is looking for Solarus quests.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="50"/>
        <source>Configuration error</source>
        <translation>Configuration Error</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="60"/>
        <source>You need to add a valid directory that contains Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>You need to add a valid directory that contains Solarus quests.
Please change this configuration in the app settings.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="62"/>
        <source>The app cannot find any Solarus quest in the directory path you provided.
Have you tried downloading some quests?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="70"/>
        <source>Go to Settings page</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="72"/>
        <source>Browse and download quests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="276"/>
        <source>Sort by title</source>
        <translation>Sort by title</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="281"/>
        <source>Sort by release date</source>
        <translation>Sort by release date</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="286"/>
        <source>Sort by last played time</source>
        <translation>Sort by last played time</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="337"/>
        <source>Are you sure?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="338"/>
        <source>Do you really want to remove &apos;&lt;b&gt;%1&lt;/b&gt;&apos; from disk?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OnboardingDevice</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Onboarding/OnboardingDevice.qml" line="80"/>
        <source>Not connected</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OnlineQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>Unknown Title</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="129"/>
        <source>Unknown Author</source>
        <translation>Unknown Author</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="170"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="183"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="197"/>
        <source>Unknown description.</source>
        <translation>Unknown Description.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="212"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="213"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="219"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="226"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="231"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="237"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="242"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="248"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="253"/>
        <source>Unknown Value</source>
        <translation>Unknown value</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="218"/>
        <source>Solarus Version</source>
        <translation>Solarus Version</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="225"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="230"/>
        <source>Languages</source>
        <translation>Languages</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="236"/>
        <source>Players</source>
        <translation>Players</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="241"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="247"/>
        <source>Release Date</source>
        <translation>Release Date</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="252"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
</context>
<context>
    <name>OnlineQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="163"/>
        <source>Show information</source>
        <translation>Show information</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="171"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
</context>
<context>
    <name>OnlineQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="55"/>
        <source>Configuration error</source>
        <translation>Configuration Error</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="57"/>
        <source>No quests found</source>
        <translation>No Quests Found</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="59"/>
        <source>Asking the server for Solarus quests…</source>
        <translation>Asking the server for Solarus quests…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="65"/>
        <source>You need to set a valid URL to retrieve Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>You need to set a valid URL to retrieve Solarus quests.
Please change this configuration in the app settings.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="67"/>
        <source>There are no quests or the server isn&apos;t responding.</source>
        <translation>There are no quests or the server isn&apos;t responding.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="69"/>
        <source>Please wait while the app is downloading the Solarus quests list.</source>
        <translation>Please wait while the app is downloading the Solarus Quests list.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="75"/>
        <source>Go to Settings page</source>
        <translation>Go to Settings page</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="77"/>
        <source>Refresh</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="272"/>
        <source>Sort by title</source>
        <translation>Sort by title</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="277"/>
        <source>Sort by release date</source>
        <translation>Sort by release date</translation>
    </message>
</context>
<context>
    <name>QuestThumbnailOverlay</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="118"/>
        <source>Downloading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="161"/>
        <source>Downloaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="184"/>
        <source>Playing</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="125"/>
        <source>Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="155"/>
        <source>Refresh</source>
        <translation>Refresh</translation>
    </message>
</context>
<context>
    <name>SearchOptions</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="146"/>
        <source>Sort by title</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="151"/>
        <source>Sort by release date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="156"/>
        <source>Sort by last played time</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="78"/>
        <source>Application Settings</source>
        <translation>Application Settings</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="83"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="88"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="89"/>
        <source>Choose the language this application is displayed in.</source>
        <translation>Choose the language this application is displayed in.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="104"/>
        <source>Quests directory</source>
        <translation>Quests directory</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="105"/>
        <source>This application will look for Solarus quests in this directory.</source>
        <translation>This application will look for Solarus Quests in this directory.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="125"/>
        <source>Force software rendering for app</source>
        <translation>Force software rendering for app</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="126"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="256"/>
        <source>Enable this option if your GPU doesn&apos;t support OpenGL. Restart required.</source>
        <translation>Enable this option if your GPU doesn&apos;t support OpenGL. Restart required.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="169"/>
        <source>Adjust audio volume for this application&apos;s sound effects.</source>
        <translation>Adjust audio volume for this application&apos;s sound effects.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="189"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="302"/>
        <source>Enable auto-updates</source>
        <translation>Enable auto-updates</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="190"/>
        <source>Automatically check and download updates for this application.</source>
        <translation>Automatically check and download updates for this application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="313"/>
        <source>Checking frequency</source>
        <translation>Checking frequency</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="202"/>
        <source>Define how frequently this application should look for updates.</source>
        <translation>Define how frequently this application should look for updates.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="228"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="340"/>
        <source>Check for updates</source>
        <translation>Check for Updates</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="245"/>
        <source>Quest Settings</source>
        <translation>Quest Settings</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="255"/>
        <source>Force software rendering for games</source>
        <translation>Force software rendering for games</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="266"/>
        <source>Full screen games</source>
        <translation>Full screen games</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="267"/>
        <source>Display games in full screen.</source>
        <translation>Display games in full screen.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="284"/>
        <source>Enable quests audio</source>
        <translation>Enable game audio</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="285"/>
        <source>Play the games audio.</source>
        <translation>Play the games audio.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="303"/>
        <source>Automatically check and download updates for quests.</source>
        <translation>Automatically check and download updates for quests.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="314"/>
        <source>Define how frequently this application should look for quest updates.</source>
        <translation>Define how frequently this application should look for quest updates.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="363"/>
        <source>Reset settings</source>
        <translation>Reset settings</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="364"/>
        <source>Reset the application settings to defaults. Doesn&apos;t remove quests and saves.</source>
        <translation>Reset the application settings to defaults. Doesn&apos;t remove games and their saves.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="109"/>
        <source>Path to the Solarus quests directory</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="120"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="250"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="138"/>
        <source>Full screen app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="139"/>
        <source>Display this application in full screen.</source>
        <translation>Display this application in full screen.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="279"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="156"/>
        <source>Enable app audio</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="157"/>
        <source>Play sound effects in this application.</source>
        <translation>Play sound effects in this application.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="168"/>
        <source>App audio volume</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="184"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="297"/>
        <source>Updates</source>
        <translation>Updates</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="229"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="341"/>
        <source>Last checked: %1</source>
        <translation>Last checked: %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="345"/>
        <source>Check</source>
        <translation>Check</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="358"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="367"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="378"/>
        <source>Reset Settings to defaults</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="380"/>
        <source>Do you really want to reset settings to defaults?
All your current settings will be erased.

Note: Your Solarus quests and their save files will be kept.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="134"/>
        <source>Playing: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="175"/>
        <source>Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="176"/>
        <source>Local Quests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="177"/>
        <source>Online Quests</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="178"/>
        <source>Playing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="179"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="180"/>
        <source>About</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Switch</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="99"/>
        <source>On</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="113"/>
        <source>Off</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="50"/>
        <source>Welcome to Solarus Launcher</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="59"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="67"/>
        <source>Skip tutorial</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>solarus::launcher::controllers::CheckFrequencyListModel</name>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="75"/>
        <source>Every Start</source>
        <translation>Every Start</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="77"/>
        <source>Every Day</source>
        <translation>Every Day</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="79"/>
        <source>Every Week</source>
        <translation>Every Week</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="81"/>
        <source>Every Month</source>
        <translation>Every Month</translation>
    </message>
</context>
</TS>
