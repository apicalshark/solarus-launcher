<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW" sourcelanguage="en_US">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="93"/>
        <source>Version %1</source>
        <translation>版本 %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="101"/>
        <source>A game launcher and browser for Solarus, a free and open-source Action-RPG/Adventure 2D game engine.</source>
        <translation>Solarus 遊戲啟動器與瀏覽器，免費與開源的動作角色扮演/冒險 2D 遊戲引擎。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="127"/>
        <source>This program is licensed under the &lt;a href=&apos;%1&apos;&gt;GNU Public License, version 3&lt;/a&gt;.</source>
        <translation>This program is licensed under the &lt;a href=&apos;%1&apos;&gt;GNU Public License, version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="134"/>
        <source>Included assets are licensed under &lt;a href=&apos;%2&apos;&gt;CC-BY-SA 4.0&lt;/a&gt;.</source>
        <translation>Included assets are licensed under &lt;a href=&apos;%2&apos;&gt;CC-BY-SA 4.0&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="177"/>
        <source>User Manual</source>
        <translation>使用者手冊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="178"/>
        <source>Show User Manual</source>
        <translation>顯示使用者手冊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="190"/>
        <source>Changelog</source>
        <translation>變更日誌</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="191"/>
        <source>Show Changelog</source>
        <translation>顯示變更日誌</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="203"/>
        <source>Source code</source>
        <translation>原始碼</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="216"/>
        <source>Report a bug</source>
        <translation>報告錯誤</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/AboutPage.qml" line="229"/>
        <source>View website</source>
        <translation>查看網站</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="31"/>
        <source>File</source>
        <translation>檔案</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="37"/>
        <source>Check For Updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="49"/>
        <source>Preferences…</source>
        <translation>喜好設定...</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="63"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="74"/>
        <source>Quest</source>
        <translation>任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="80"/>
        <source>Play</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="92"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="103"/>
        <source>View</source>
        <translation>檢視</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="109"/>
        <source>Full Screen</source>
        <translation>全螢幕</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="123"/>
        <source>Show Quest as Full Screen</source>
        <translation>全螢幕顯示遊戲</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="134"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="140"/>
        <source>Changelog…</source>
        <translation>日誌...</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="152"/>
        <source>User Manual…</source>
        <translation>使用者手冊...</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/App/AppMenu.qml" line="164"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
</context>
<context>
    <name>ContentLoader</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContentLoader.qml" line="53"/>
        <source>Error: impossible to load the view</source>
        <translation>錯誤: 無法載入視窗</translation>
    </message>
</context>
<context>
    <name>ContextMenu</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="39"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="48"/>
        <source>Paste</source>
        <translation>貼上</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="56"/>
        <source>Cut</source>
        <translation>剪下</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/ContextMenu.qml" line="68"/>
        <source>Select All</source>
        <translation>全選</translation>
    </message>
</context>
<context>
    <name>ControlsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="43"/>
        <source>Controls</source>
        <translation>控制</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="52"/>
        <source>You can control Solarus Launcher with these devices:</source>
        <translation>您可以使用以下設備控制 Solarus 啟動器:</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="60"/>
        <source>Mouse</source>
        <translation>滑鼠</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="68"/>
        <source>Keyboard</source>
        <translation>鍵盤</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/ControlsPage.qml" line="76"/>
        <source>Gamepad</source>
        <translation>控制器</translation>
    </message>
</context>
<context>
    <name>DirectoryChooser</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="55"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Solarus 任務資料夾的路徑</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Settings/DirectoryChooser.qml" line="83"/>
        <source>Open file dialog</source>
        <translation>開啟檔案</translation>
    </message>
</context>
<context>
    <name>FinishPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="49"/>
        <source>Ready!</source>
        <translation>準備完成！</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FinishPage.qml" line="57"/>
        <source>Have fun!</source>
        <translation>祝你玩得愉快！</translation>
    </message>
</context>
<context>
    <name>FolderChoicePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="44"/>
        <source>Quests Folder</source>
        <translation>任務目錄</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="53"/>
        <source>This will be the the folder where Solarus Launcher looks for existing Solarus Quests (i.e. games), and downloads them.</source>
        <translation>這裡將會是 Solarus 啟動器尋找現有 Solarus 任務 (即遊戲) 並下載它們的資料夾。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="67"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Solarus 任務資料夾的路徑</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="141"/>
        <source>If the folder doesn&apos;t exist yet, it will be automatically created.</source>
        <translation>如果該資料夾還不存在，則會自動建立。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderChoicePage.qml" line="161"/>
        <source>It can still be changed later in the app preferences.</source>
        <translation>稍後仍可以在應用程式設定中更改它。</translation>
    </message>
</context>
<context>
    <name>FolderLoadingPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="50"/>
        <source>Directory creation failed</source>
        <translation>目錄建立失敗</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="52"/>
        <source>Path is not a directory</source>
        <translation>路徑不是資料夾</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="54"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="61"/>
        <source>It is impossible to create such a directory.
Check that you have correct access rights.</source>
        <translation>無法建立目錄。
請檢查您是否具有存取權限。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="63"/>
        <source>This path seems to already exists, but points to a file instead of a directory.</source>
        <translation>該路徑似乎已經存在，但指向一個檔案而不是目錄。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="65"/>
        <source>Error code: %1</source>
        <translation>錯誤碼: %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="72"/>
        <source>Error!</source>
        <translation>錯誤!</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="100"/>
        <source>Loading Quests</source>
        <translation>載入任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/FolderLoadingPage.qml" line="109"/>
        <source>Solarus Launcher is loading your Solarus Quest library…</source>
        <translation>Solarus 啟動器正在載入您的 Solarus 任務庫...</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="112"/>
        <source>Welcome to Solarus Launcher</source>
        <translation>歡迎使用 Solarus 啟動器</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="120"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation>在電腦上瀏覽、下載和遊玩 Solarus 任務。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="130"/>
        <source>Recently played</source>
        <translation>近期遊玩</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="175"/>
        <source>Once you&apos;ve played some Solarus quests, the most recent ones will appear here.</source>
        <translation>當您遊玩了一些 Solarus 任務，最近遊玩的任務就會出現在這裡。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="180"/>
        <source>Go to Quest list</source>
        <translation>前往任務清單</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="310"/>
        <source>Loading…</source>
        <translation>載入中…</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="209"/>
        <source>See all quests</source>
        <translation>查看所有任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="225"/>
        <source>Latest News</source>
        <translation>最新消息</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="287"/>
        <source>Refresh</source>
        <translation>重新整理</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="235"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="288"/>
        <source>Refresh news articles</source>
        <translation>重新整理新聞文章</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="282"/>
        <source>Can&apos;t fetch latest news.</source>
        <translation>無法取得最新消息。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/HomePage.qml" line="318"/>
        <source>More news</source>
        <translation>更多新聞</translation>
    </message>
</context>
<context>
    <name>LanguageUtils</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="29"/>
        <source>German</source>
        <translation>德語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="30"/>
        <source>English</source>
        <translation>英語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="31"/>
        <source>Spanish</source>
        <translation>西班牙語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="32"/>
        <source>French</source>
        <translation>法語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="33"/>
        <source>Italian</source>
        <translation>義大利語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="34"/>
        <source>Portuguese</source>
        <translation>葡萄牙語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="35"/>
        <source>Russian</source>
        <translation>俄語</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Utils/LanguageUtils.qml" line="36"/>
        <source>Chinese</source>
        <translation>中文</translation>
    </message>
</context>
<context>
    <name>LocalQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>未知標題</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="130"/>
        <source>Unknown Author</source>
        <translation>未知作者</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="171"/>
        <source>Play</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="184"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="197"/>
        <source>Check for Updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="209"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="221"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="233"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="247"/>
        <source>Unknown description.</source>
        <translation>未知描述。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="262"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="263"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="269"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="276"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="281"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="287"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="292"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="298"/>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="303"/>
        <source>Unknown Value</source>
        <translation>未知的值</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="268"/>
        <source>Solarus Version</source>
        <translation>Solarus 版本</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="275"/>
        <source>License</source>
        <translation>許可證</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="280"/>
        <source>Languages</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="286"/>
        <source>Players</source>
        <translation>玩家</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="291"/>
        <source>Genre</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="297"/>
        <source>Release Date</source>
        <translation>發布日期</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestDialog.qml" line="302"/>
        <source>Website</source>
        <translation>網站</translation>
    </message>
</context>
<context>
    <name>LocalQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="163"/>
        <source>Play</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="171"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="183"/>
        <source>Show information</source>
        <translation>顯示資訊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="191"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="199"/>
        <source>Check For Updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/LocalQuests/LocalQuestListItem.qml" line="211"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
</context>
<context>
    <name>LocalQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="54"/>
        <source>Looking for Solarus quests…</source>
        <translation>正在尋找 Solarus 任務...</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="52"/>
        <source>No quests found</source>
        <translation>沒有找到任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="64"/>
        <source>Please wait while the app is looking for Solarus quests.</source>
        <translation>應用程式正在尋找 Solarus 任務，請稍候。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="50"/>
        <source>Configuration error</source>
        <translation>設定值錯誤</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="60"/>
        <source>You need to add a valid directory that contains Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>您需要新增包含 Solarus 任務的有效目錄。
請在應用程式設定中變更此配置。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="62"/>
        <source>The app cannot find any Solarus quest in the directory path you provided.
Have you tried downloading some quests?</source>
        <translation>應用程式無法在您提供的目錄路徑中找到任何 Solarus 任務。
您要嘗試下載一些任務嗎？</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="70"/>
        <source>Go to Settings page</source>
        <translation>前往設定頁面</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="72"/>
        <source>Browse and download quests</source>
        <translation>瀏覽並下載任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="276"/>
        <source>Sort by title</source>
        <translation>按標題排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="281"/>
        <source>Sort by release date</source>
        <translation>按發布日期排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="286"/>
        <source>Sort by last played time</source>
        <translation>按上次遊玩時間排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="337"/>
        <source>Are you sure?</source>
        <translation>你確定嗎？</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/LocalQuestsPage.qml" line="338"/>
        <source>Do you really want to remove &apos;&lt;b&gt;%1&lt;/b&gt;&apos; from disk?</source>
        <translation>您真的要從硬碟中刪除 &apos;&lt;b&gt;%1&lt;/b&gt; &apos;嗎？</translation>
    </message>
</context>
<context>
    <name>OnboardingDevice</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Onboarding/OnboardingDevice.qml" line="80"/>
        <source>Not connected</source>
        <translation>沒有連線</translation>
    </message>
</context>
<context>
    <name>OnlineQuestDialog</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="121"/>
        <source>Unknown Title</source>
        <translation>未知標題</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="129"/>
        <source>Unknown Author</source>
        <translation>未知作者</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="170"/>
        <source>Download</source>
        <translation>下載</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="183"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="197"/>
        <source>Unknown description.</source>
        <translation>未知描述。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="212"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="213"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="219"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="226"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="231"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="237"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="242"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="248"/>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="253"/>
        <source>Unknown Value</source>
        <translation>未知的值</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="218"/>
        <source>Solarus Version</source>
        <translation>Solarus 版本</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="225"/>
        <source>License</source>
        <translation>許可證</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="230"/>
        <source>Languages</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="236"/>
        <source>Players</source>
        <translation>玩家</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="241"/>
        <source>Genre</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="247"/>
        <source>Release Date</source>
        <translation>發布日期</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestDialog.qml" line="252"/>
        <source>Website</source>
        <translation>網站</translation>
    </message>
</context>
<context>
    <name>OnlineQuestListItem</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="163"/>
        <source>Show information</source>
        <translation>顯示資訊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/OnlineQuests/OnlineQuestListItem.qml" line="171"/>
        <source>Download</source>
        <translation>下載</translation>
    </message>
</context>
<context>
    <name>OnlineQuestsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="55"/>
        <source>Configuration error</source>
        <translation>設定值錯誤</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="57"/>
        <source>No quests found</source>
        <translation>沒有找到任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="59"/>
        <source>Asking the server for Solarus quests…</source>
        <translation>向伺服器尋找 Solarus 任務...</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="65"/>
        <source>You need to set a valid URL to retrieve Solarus quests.
Please change this configuration in the app settings.</source>
        <translation>您需要設定一個有效的 URL 來尋找 Solarus 任務。
請在應用程式設定中變更此設定。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="67"/>
        <source>There are no quests or the server isn&apos;t responding.</source>
        <translation>任務或伺服器沒有回應。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="69"/>
        <source>Please wait while the app is downloading the Solarus quests list.</source>
        <translation>應用程式正在下載 Solarus 任務列表，請稍候。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="75"/>
        <source>Go to Settings page</source>
        <translation>前往設定頁面</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="77"/>
        <source>Refresh</source>
        <translation>重新整理</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="272"/>
        <source>Sort by title</source>
        <translation>按標題排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/OnlineQuestsPage.qml" line="277"/>
        <source>Sort by release date</source>
        <translation>按發布日期排序</translation>
    </message>
</context>
<context>
    <name>QuestThumbnailOverlay</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="118"/>
        <source>Downloading</source>
        <translation>下載中</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="161"/>
        <source>Downloaded</source>
        <translation>已下載</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/Quests/QuestThumbnailOverlay.qml" line="184"/>
        <source>Playing</source>
        <translation>正在遊玩</translation>
    </message>
</context>
<context>
    <name>SearchBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="125"/>
        <source>Search</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchBar.qml" line="155"/>
        <source>Refresh</source>
        <translation>重新整理</translation>
    </message>
</context>
<context>
    <name>SearchOptions</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="146"/>
        <source>Sort by title</source>
        <translation>按標題排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="151"/>
        <source>Sort by release date</source>
        <translation>按發布日期排序</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SearchBar/SearchOptions.qml" line="156"/>
        <source>Sort by last played time</source>
        <translation>按上次遊玩時間排序</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="78"/>
        <source>Application Settings</source>
        <translation>應用程式設定</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="83"/>
        <source>General</source>
        <translation>一般</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="88"/>
        <source>Language</source>
        <translation>語言</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="89"/>
        <source>Choose the language this application is displayed in.</source>
        <translation>選擇此應用程式顯示的語言。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="104"/>
        <source>Quests directory</source>
        <translation>任務目錄</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="105"/>
        <source>This application will look for Solarus quests in this directory.</source>
        <translation>此應用程式將在此目錄中尋找 Solarus 任務。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="125"/>
        <source>Force software rendering for app</source>
        <translation>強制使用軟體渲染</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="126"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="256"/>
        <source>Enable this option if your GPU doesn&apos;t support OpenGL. Restart required.</source>
        <translation>如果您的 GPU 不支援 OpenGL，請啟用此選項。 需要重新啟動。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="169"/>
        <source>Adjust audio volume for this application&apos;s sound effects.</source>
        <translation>調整應用程式音效的音量。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="189"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="302"/>
        <source>Enable auto-updates</source>
        <translation>啟用自動更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="190"/>
        <source>Automatically check and download updates for this application.</source>
        <translation>自動檢查並下載此應用程式的更新。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="201"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="313"/>
        <source>Checking frequency</source>
        <translation>檢查頻率</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="202"/>
        <source>Define how frequently this application should look for updates.</source>
        <translation>設定該應用程式檢查更新的頻率。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="228"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="340"/>
        <source>Check for updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="245"/>
        <source>Quest Settings</source>
        <translation>任務設定</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="255"/>
        <source>Force software rendering for games</source>
        <translation>對遊戲強制使用軟體渲染</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="266"/>
        <source>Full screen games</source>
        <translation>全螢幕遊戲</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="267"/>
        <source>Display games in full screen.</source>
        <translation>全螢幕顯示遊戲。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="284"/>
        <source>Enable quests audio</source>
        <translation>啟用任務的音訊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="285"/>
        <source>Play the games audio.</source>
        <translation>播放遊戲音訊。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="303"/>
        <source>Automatically check and download updates for quests.</source>
        <translation>自動檢查並下載任務更新。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="314"/>
        <source>Define how frequently this application should look for quest updates.</source>
        <translation>設定該任務檢查更新的頻率。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="363"/>
        <source>Reset settings</source>
        <translation>重設</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="364"/>
        <source>Reset the application settings to defaults. Doesn&apos;t remove quests and saves.</source>
        <translation>將應用程式設定重設為預設值。不會刪除任務和存檔。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="109"/>
        <source>Path to the Solarus quests directory</source>
        <translation>Solarus 任務資料夾的路徑</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="120"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="250"/>
        <source>Video</source>
        <translation>影像</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="138"/>
        <source>Full screen app</source>
        <translation>全螢幕</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="139"/>
        <source>Display this application in full screen.</source>
        <translation>全螢幕顯示此應用程式。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="151"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="279"/>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="156"/>
        <source>Enable app audio</source>
        <translation>啟用應用程式的音訊</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="157"/>
        <source>Play sound effects in this application.</source>
        <translation>在此應用程式中播放音效。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="168"/>
        <source>App audio volume</source>
        <translation>應用程式音量</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="184"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="297"/>
        <source>Updates</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="229"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="341"/>
        <source>Last checked: %1</source>
        <translation>上次檢查: %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="234"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="345"/>
        <source>Check</source>
        <translation>檢查</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="358"/>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="367"/>
        <source>Reset</source>
        <translation>重設</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="378"/>
        <source>Reset Settings to defaults</source>
        <translation>重設至預設值</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/MainPages/SettingsPage.qml" line="380"/>
        <source>Do you really want to reset settings to defaults?
All your current settings will be erased.

Note: Your Solarus quests and their save files will be kept.</source>
        <translation>您真的想將設定重設為預設值嗎？
您目前的所有設定都將被刪除。

注意：您的 Solarus 任務及其儲存的檔案將被保留。</translation>
    </message>
</context>
<context>
    <name>SideBar</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="134"/>
        <source>Playing: %1</source>
        <translation>遊玩: %1</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="175"/>
        <source>Home</source>
        <translation>首頁</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="176"/>
        <source>Local Quests</source>
        <translation>本地任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="177"/>
        <source>Online Quests</source>
        <translation>線上任務</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="178"/>
        <source>Playing</source>
        <translation>遊玩</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="179"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Components/SideBar/SideBar.qml" line="180"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
</context>
<context>
    <name>Switch</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="99"/>
        <source>On</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Controls/Switch.qml" line="113"/>
        <source>Off</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>WelcomePage</name>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="50"/>
        <source>Welcome to Solarus Launcher</source>
        <translation>歡迎使用 Solarus 啟動器</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="59"/>
        <source>Browse, download and play Solarus Quests on desktop.</source>
        <translation>在電腦上瀏覽、下載和遊玩 Solarus 任務。</translation>
    </message>
    <message>
        <location filename="../qml/Solarus/Launcher/Pages/OnboardingPages/WelcomePage.qml" line="67"/>
        <source>Skip tutorial</source>
        <translation>跳過新手教學</translation>
    </message>
</context>
<context>
    <name>solarus::launcher::controllers::CheckFrequencyListModel</name>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="75"/>
        <source>Every Start</source>
        <translation>每次啟動</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="77"/>
        <source>Every Day</source>
        <translation>每天</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="79"/>
        <source>Every Week</source>
        <translation>每星期</translation>
    </message>
    <message>
        <location filename="../src/solarus/launcher/controllers/SettingsPageController.cpp" line="81"/>
        <source>Every Month</source>
        <translation>每月</translation>
    </message>
</context>
</TS>
