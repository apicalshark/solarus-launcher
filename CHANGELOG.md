# Changelog

## Solarus Launcher 1.0.0 (in progress)

### New Features

* QML components library.
* NSIS installer for Windows.
* Quest list within a user-defined directory.
* Latest news from website.
* Application settings.
* Application automatic updates.
* Application single instance.
