# Quests

## Quest List

The only C++ classes that are exposed to QML are the controllers for each page/panel of the UI. **The QML files only use their associated controller**, to prevent any spaghetti code and respect the MVC pattern.

The C++ classes exposed to QML are prefixed with a `$`. For example, `$localQuestsPageController` is the variable to reference the unique exposed instance of `LocalQuestsPageController`.

`Quest` is the class exposed to QML, and inherits from `QObject`. It can't be copied, and exposes `QuestData`, a copyable POD built from a `.solarus` file (in the case of a local database) or a `.json` file (in the case of an online database that retrieves data from the Solarus website API).

The selected `Quest` is set by the views on the controllers. The unique source of truth for the selected `Quest` should be the `Navigation` class, and all controllers should bind to it. The `QuestDialog` always opens the selected quest.

### Quest images in QML

To display pixmaps loaded from disk or downloded in QML, there is `LocalQuestImageProvider`. It provides an API to get associated images for a quest, at a given size. Images URLs to use in QML follow this scheme: `image://quest/<questId>/<imageType>`.

- `<imageType>` only supports `thumbnail` as for now.

Example: `"image://quest/oceans-heart/thumbnail"`

## Quest Playing

The class `QuestRunner` runs the Solarus quest given as parameter. Only one quest can be run at the same time.

An attached process is started. It is given parameters, including the quest path, retreived from the `app::Settings` class, such as full-screen mode. **This attached process is the launcher itself**, set to run without GUI. When given a quest path as parameter from command-line, the launcher doesn't start the GUI and instead starts the engine.

When some console output is available from the process, `QuestOutputHandler` parses it and prepares it for display, with nice colors. These colors can be set from QML thanks to the class `QuestOutputColors`.

`PlayingQuestPageController` exposes the console output and commands to QML.

The page `PlayingQuestPage` is only shown in the sidebar when a quest is actually running.
