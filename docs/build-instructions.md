# Build instructions

1. First you need to pull the git submodules, which are:

    - `solarus`
    - `QtUpdater`
    - `QtAppInstanceManager`

    Solarus has been added as a submodule so you don't need to install it with `make install` to build this project.

    ```bash
    git submodule update --init --recursive
    ```

2. Then you need to install the other dependencies and `solarus` dependencies. On Ubuntu, for instance:

      ```bash
      # Common dependencies.
      sudo apt install -y \
        build-essential \
        cmake \
        pkg-config \

      # Solarus dependencies.
      sudo apt install -y \
        libsdl2-dev \
        libsdl2-image-dev \
        libsdl2-ttf-dev \
        libluajit-5.1-dev \
        libphysfs-dev \
        libopenal-dev \
        libvorbis-dev \
        libmodplug-dev \
        libglm-dev \

      # Solarus Launcher dependencies.
      sudo apt install -y \
        qtbase5-dev \
        qttools5-dev \
        qttools5-dev-tools \
        qtdeclarative5-dev \
        qtquickcontrols2-5-dev \
        qtmultimedia5-dev \
        qml-module-qtmultimedia \
        qml-module-qtquick2 \
        qml-module-qtquick-controls2 \
        qml-module-qtquick-templates2 \
        qml-module-qtquick-shapes \
        qml-module-qtquick-layouts \
        qml-module-qtquick-window2 \
        qml-module-qtgraphicaleffects \
        qml-module-qt-labs-settings \
        qml-module-qt-labs-platform \
        libqt5gamepad5 \
        libqt5gamepad5-dev \
        libqt5concurrent5 \
      ```

3. Configure the project with CMake:

    ```bash
    cmake -B ./build
    ```

    You might need to explicitly tell CMake the location of your Qt installation, like this:

    ```bash
    cmake \
      -B ./build \
      -DCMAKE_PREFIX_PATH="<path_to_Qt>/Qt/<version>/clang_64/lib/cmake" \
    ```

4. Build it:

    ```bash
    cmake --build ./build
    ```

5. The build result is in `./build/bin/`.
