# News

The architecture for displaying the last news from the website is straightforward.

The url of the website API is in the root `CMakeLists.txt.`, named `PROJECT_NEWS_API_URL`.

1. `NewsListModel` just fetches the news from the website API, gets a JSON file, and keeps only the _n_ first ones (_n_ being `3` by default).
2. The model then creates and stores a `NewsData` list. The news articles data are accessed with item data roles.
3. The component `NewsListView` displays that list.
