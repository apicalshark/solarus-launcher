# Code Documentation

- [Build instructions](build-instructions.md)
- [Project structure](project-structure.md)
- [Quests](quests.md)
- [News](news.md)
- [Auto-updates](auto-updates.md)
