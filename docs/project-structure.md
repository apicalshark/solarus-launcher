# Project Structure

- 📂 `/cmake/`: CMake files and related resources.
- 📂 `/docs/`: Documentation.
- 📂 `/include/`: Public headers (`.h` files).
- 📂 `/qml/`: QML files  (`.qml` files).
- 📂 `/resources/`: Application resources (images, sounds, etc.).
- 📂 `/scripts/`: Utilities and CI scripts.
- 📂 `/src/`: Source files (`.cpp` files).
- 📂 `/translations/`: Translations for the application (`.ts` files).
- 📂 `/user-manual/`: Files to be used with MkDocs to generate a book (`.md`).
