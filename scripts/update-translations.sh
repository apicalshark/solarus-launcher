#!/bin/bash

declare tsDir="translations/"
declare extensions="h,cpp,qml"
lupdate -no-ui-lines -no-obsolete "./include" "./src" "./qml" -ts "$tsDir/en_US.ts" "$tsDir/fr_FR.ts" -source-language en_US -extensions $extensions
