# Reverses a DNS. Example: from "product.company.com", gets "com.company.product".
# Usage: reverse_dns("product.company.com" RESULT)
function(reverse_dns DNS OUTPUT)
  string(REPLACE "." ";" OUTPUT_LOCAL ${DNS})
  list(REVERSE OUTPUT_LOCAL)
  list(JOIN OUTPUT_LOCAL "." OUTPUT_LOCAL)
  set(${OUTPUT} ${OUTPUT_LOCAL} PARENT_SCOPE)
endfunction()

# Formats a string for URL usage (no spaces, lowercase).
function(format_string_for_url OUTPUT INPUT)
  string(REPLACE " " "" NOSPACE_INPUT ${INPUT})
  string(TOLOWER ${NOSPACE_INPUT} LOWER_INPUT)
  set(${OUTPUT} ${LOWER_INPUT} PARENT_SCOPE)
endfunction()

# Get user-friendly system name.
# Usage: get_user_friendly_system_name(RESULT)
function(get_user_friendly_system_name OUTPUT)
  if(${CMAKE_SYSTEM_PROCESSOR} MATCHES ".*64.*")
    set(USER_FRIENDLY_ARCHITECTURE "64bit")
  else()
    set(USER_FRIENDLY_ARCHITECTURE "32bit")
  endif()

  if(${CMAKE_SYSTEM_NAME} MATCHES Windows)
    set(USER_FRIENDLY_SYSTEM_NAME "windows")
  elseif(${CMAKE_SYSTEM_NAME} MATCHES Darwin)
    set(USER_FRIENDLY_SYSTEM_NAME "macos")
  else()
    message(FATAL_ERROR "System ${CMAKE_SYSTEM_NAME} not supported yet.") # TODO
  endif()
  set(${OUTPUT} ${USER_FRIENDLY_SYSTEM_NAME}-${USER_FRIENDLY_ARCHITECTURE} PARENT_SCOPE)
endfunction()

# Configures NSIS installer generation for Windows.
function(configure_nsis)
  # Set common package variables.
  set(INSTALLER_PACKAGE_NAME ${PROJECT_NAME})
  set(INSTALLER_PACKAGE_VERSION ${PROJECT_DISPLAY_VERSION})
  get_user_friendly_system_name(INSTALLER_SYSTEM_NAME)
  set(INSTALLER_PACKAGE_FILE_NAME "${INSTALLER_PACKAGE_NAME}-${INSTALLER_PACKAGE_VERSION}-${INSTALLER_SYSTEM_NAME}")
  set(INSTALLER_OUTPUT_DIR ${CMAKE_BINARY_DIR}/package/${INSTALLER_SYSTEM_NAME})

  # NSIS generation is managed manually, without CPack variables.
  set(NSIS_OUTPUT_DIR ${INSTALLER_OUTPUT_DIR})
  set(NSIS_OUTPUT_FILENAME ${INSTALLER_PACKAGE_FILE_NAME}.exe)

  # CPack copies installed files to this directory.
  set(NSIS_INPUT_DIR ${NSIS_OUTPUT_DIR}/${INSTALLER_PACKAGE_FILE_NAME})

  # Installer information.
  set(NSIS_INSTALLER_NAME "${PROJECT_DISPLAY_NAME} Installer")
  set(NSIS_INSTALLER_VERSION ${PROJECT_VERSION})

  # App information.
  set(NSIS_APP_NAME ${PROJECT_DISPLAY_NAME})
  set(NSIS_APP_VERSION ${PROJECT_VERSION})
  set(NSIS_APP_DISPLAY_VERSION ${PROJECT_DISPLAY_VERSION})
  set(NSIS_APP_EXECUTABLE ${PROJECT_DISPLAY_NAME}.exe)
  set(NSIS_APP_SHORTCUT ${PROJECT_DISPLAY_NAME}.lnk)

  # Mutual information for App and Installer.
  set(NSIS_ORGANIZATION_NAME ${PROJECT_ORGANIZATION_NAME})
  set(NSIS_COPYRIGHT ${PROJECT_COPYRIGHT})
  set(NSIS_TRADEMARKS ${PROJECT_TRADEMARKS})
  set(NSIS_URL_INFOABOUT ${PROJECT_HOMEPAGE_URL})
  set(NSIS_URL_UPDATEINFO ${PROJECT_HOMEPAGE_URL})

  # File type information.
  set(NSIS_FILE_EXTENSION .${PROJECT_FILE_EXTENSION})
  set(NSIS_FILE_TYPE_NAME ${PROJECT_FILE_TYPE_NAME})
  set(NSIS_FILE_TYPE_ID ${PROJECT_FILE_TYPE_ID})

  # Installer resources files.
  set(NSIS_INSTALLER_RESOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/cmake/win32/installer)

  # Assets dir (images, license, etc.).
  set(NSIS_INSTALLER_ASSETS_PATH ${NSIS_INSTALLER_RESOURCES_DIR}/assets)

  # Configure custom definitions file for NSIS, using all the variables defined above.
  configure_file(
    ${NSIS_INSTALLER_RESOURCES_DIR}/NSISDefinitions.nsh.in
    ${NSIS_OUTPUT_DIR}/NSISDefinitions.nsh
    @ONLY)

  # Prepare for CPack usage.
  configure_file(
    ${NSIS_INSTALLER_RESOURCES_DIR}/NSISInstaller.nsi.in
    ${NSIS_OUTPUT_DIR}/NSISInstaller.nsi
    COPYONLY)

endfunction()

# Creates an imported target for the executable '[win|mac]deployqt'.
function(FindDeployQt)
  set(DEPLOYQT_TARGET_NAME Qt5::DeployQt)

  if(NOT TARGET ${DEPLOYQT_TARGET_NAME})
    # Find Qt binary dir.
    find_package(Qt5 COMPONENTS Core CONFIG REQUIRED)
    get_target_property(QMAKE_LOCATION Qt5::qmake IMPORTED_LOCATION)
    get_filename_component(QT_BINARY_DIR ${QMAKE_LOCATION} DIRECTORY)

    if(WIN32)
      find_program(DEPLOYQT_EXE "windeployqt" HINTS "${QT_BINARY_DIR}" REQUIRED)
      set(${DEPLOYQT_EXE} ${DEPLOYQT_EXE} PARENT_SCOPE)
    elseif(APPLE)
      find_program(DEPLOYQT_EXE "macdeployqt" HINTS "${QT_BINARY_DIR}" REQUIRED)
      set(${DEPLOYQT_EXE} ${DEPLOYQT_EXE} PARENT_SCOPE)
    else()
      message(FATAL_ERROR "Unsupported platform")
    endif()

    # Create an imported target.
    add_executable(${DEPLOYQT_TARGET_NAME} IMPORTED)
    set_property(TARGET ${DEPLOYQT_TARGET_NAME} PROPERTY IMPORTED_LOCATION ${DEPLOYQT_EXE})
  endif()
endfunction()

# Builds the target's translations to the OUTPUT directory.
function(target_deploy_qt_translations TARGET_NAME)
  cmake_parse_arguments(LOCAL "" "OUTPUT" "TS_FILES" ${ARGN})

  get_target_property(LRELEASE_EXE ${Qt5_LRELEASE_EXECUTABLE} IMPORTED_LOCATION)

  foreach(TS_FILE ${LOCAL_TS_FILES})
    get_filename_component(NAME_NO_EXTENSION ${TS_FILE} NAME_WLE)
    set(OUTPUT_QM_fILE ${LOCAL_OUTPUT}/${NAME_NO_EXTENSION}.qm)

    add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E echo "Building ${TS_FILE} to ${OUTPUT_QM_fILE}"
      COMMAND ${CMAKE_COMMAND} -E make_directory ${LOCAL_OUTPUT}
      COMMAND ${LRELEASE_EXE} ${TS_FILE} -qm ${OUTPUT_QM_fILE} -compress
      DEPENDS ${TS_FILE}
    )
  endforeach()
endfunction()

# Creates a post-build step that deploys the Qt dependencies.
function(configure_qt_dependencies_deployment TARGET_NAME)
  set(QML_DIR "${CMAKE_CURRENT_SOURCE_DIR}/qml")

  if(WIN32)
    FindDeployQt()
    if(NOT TARGET Qt5::DeployQt)
      message(FATAL_ERROR "Executable for deploying Qt not found.")
      return()
    endif()

    set(OUTPUT "$<TARGET_FILE_DIR:${TARGET_NAME}>")
    add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E echo "Deploying Qt..."
      COMMAND Qt5::DeployQt --verbose 0 --qmldir ${QML_DIR} --no-patchqt --no-compiler-runtime --no-webkit2 --no-system-d3d-compiler --no-translations --no-angle --no-opengl-sw --dir "${OUTPUT}" "$<TARGET_FILE:${TARGET_NAME}>"
    )
  elseif(APPLE)
    FindDeployQt()
    if(NOT TARGET Qt5::DeployQt)
      message(FATAL_ERROR "Executable for deploying Qt not found.")
      return()
    endif()
    message("TODO: deploy dependencies for MacOS build.")
  else()
    message("TODO: deploy dependencies for Linux build.")
  endif()
endfunction()

function(configure_qt_translations_deployment TARGET_NAME)
  set(TS_DIR "${CMAKE_CURRENT_SOURCE_DIR}/translations")
  set(OUTPUT "$<TARGET_FILE_DIR:${TARGET_NAME}>")

  # Build .qm files from .ts files and copy them along the target binary.
  file(GLOB TS_FILES CONFIGURE_DEPENDS ${TS_DIR}/*.ts)
  target_deploy_qt_translations(${TARGET_NAME}
    OUTPUT ${OUTPUT}/translations
    TS_FILES ${TS_FILES}
  )
endfunction()

# Gets the bundle/app identifier ("com.company.product").
function(get_bundle_uid OUTPUT APP_NAME COMPANY_DOMAIN)
  format_string_for_url(NOSPACE_COMPANY_DOMAIN ${COMPANY_DOMAIN})
  format_string_for_url(NOSPACE_APP_NAME ${APP_NAME})
  set(APP_UID_REVERSED ${NOSPACE_APP_NAME}.${NOSPACE_COMPANY_DOMAIN})
  reverse_dns(${APP_UID_REVERSED} APP_UID)
  set(${OUTPUT} ${APP_UID} PARENT_SCOPE)
endfunction()

function(configure_mingw_deployment TARGET_NAME)
  if(WIN32 AND MINGW)
    SET(CMAKE_FIND_LIBRARY_SUFFIXES ".lib" ".dll")
    # TODO replace with more robust dependencies look-up.
    # objdump.exe -p ./build/Solarus\ Launcher.exe | grep "DLL Name:"
    set(MINGW_DEPENDENCIES
      "libwinpthread-1"
      "libgcc_s_seh-1"
      "libstdc++-6"
      "zlib1"
      "libpng16-16"
      "libharfbuzz-0"
      "libmd4c"
      "libicuin69"
      "libicuuc69"
      "libdouble-conversion"
      "libpcre2-16-0"
      "libzstd"
      "libfreetype-6"
      "libglib-2.0-0"
      "libgraphite2"
      "libicudt69"
      "libintl-8"
      "libbz2-1"
      "libbrotlidec"
      "libpcre-1"
      "libiconv-2"
      "libbrotlicommon"
    )
    set(DEPENDENCY_FILES )
    foreach(DEPENDENCY ${MINGW_DEPENDENCIES})
      find_library(LIB_${DEPENDENCY} NAMES "${DEPENDENCY}" REQUIRED CACHE)
      set(LIB_FILE ${LIB_${DEPENDENCY}})
      list(APPEND DEPENDENCY_FILES ${LIB_FILE})
    endforeach()

    add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E echo "Deploying MinGW dependencies..."
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${DEPENDENCY_FILES} "$<TARGET_FILE_DIR:${TARGET_NAME}>"
    )
  endif()
endfunction()
