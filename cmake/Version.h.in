/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

namespace solarus::launcher::app {
constexpr auto APPLICATION_VERSION = "${PROJECT_VERSION}";
constexpr auto APPLICATION_DISPLAY_VERSION = "${PROJECT_DISPLAY_VERSION}";
constexpr auto APPLICATION_NAME = "${PROJECT_NAME}";
constexpr auto APPLICATION_DISPLAY_NAME = "${PROJECT_DISPLAY_NAME}";
constexpr auto APPLICATION_DESCRIPTION = "${PROJECT_DESCRIPTION}";

constexpr auto ORGANIZATION_NAME = "${PROJECT_ORGANIZATION_NAME}";
constexpr auto ORGANIZATION_DOMAIN = "${PROJECT_ORGANIZATION_DOMAIN}";

constexpr auto LEGAL_COPYRIGHT = "${PROJECT_COPYRIGHT}";
constexpr auto LEGAL_TRADEMARKS = "${PROJECT_TRADEMARKS}";
} // namespace solarus::launcher::app
