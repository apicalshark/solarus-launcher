<div align="center" style="margin-bottom: 3em;">
  <img src="logo.svg" width="500px"/>
</div>

# Solarus Launcher

[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://www.gnu.org/copyleft/gpl.html)
[![Qt version](https://img.shields.io/badge/Qt-5.15.2+-41CD52?logo=qt)](https://www.qt.io)

<div align="center" style="padding: 1em;">
  <a href="screenshot.png">
    <img alt="Solarus Launcher logo" src="screenshot.png" width="600px"/>
  </a>
</div>

## About

**Solarus Launcher** is a game launcher and browser for [Solarus](https://gitlab.com/solarus-games/solarus). It is written in C++/QML. Its goals are:

- Providing more **user-friendly interface** to Solarus.
- Providing an installer and better **OS integration**, such as double-click on `.solarus` files to open them.
- Allowing people to **browse and download games** directly from the launcher thanks to the website API.
- Being designed to both **desktop and TV usage** with clear and big interface.
- Being usable with **mouse**, **keyboard** or **gamepad**.
- Being **multiplatform** (Windows, MacOS, Linux).

Current status: **WORK IN PROGRESS**.

## Contribution

- [Build instructions](docs/build-instructions.md) for this project.
- [Contributing](CONTRIBUTING.md) to this project.
- [Code documentation](docs/DOCUMENTATION.md).

## License

The source code of Solarus Launcher is licensed under the terms of the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.html) (GPL v3).

Images (by **Olivier Cléro**) and sounds (by **Louis Couka**) used in the editor are licensed under [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).
