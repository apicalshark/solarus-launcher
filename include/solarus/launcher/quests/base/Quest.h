/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDate>
#include <QVersionNumber>
#include <QUrl>

namespace solarus::launcher::quests {
/**
 * @brief Minimal information to display a read-only Solarus quest for QML.
 */
class Quest : public QObject {
  Q_OBJECT

  // Properties read from .solarus file.
  Q_PROPERTY(QString title READ title NOTIFY titleChanged)
  Q_PROPERTY(QStringList authors READ authors NOTIFY authorsChanged)
  Q_PROPERTY(QDate initialReleaseDate READ initialReleaseDate NOTIFY initialReleaseDateChanged)
  Q_PROPERTY(QDate latestReleaseDate READ latestReleaseDate NOTIFY latestReleaseDateChanged)
  Q_PROPERTY(QString description READ description NOTIFY descriptionChanged)
  Q_PROPERTY(QString version READ version NOTIFY versionChanged)
  Q_PROPERTY(QString engineVersion READ engineVersion NOTIFY engineVersionChanged)
  Q_PROPERTY(QStringList licenses READ licenses NOTIFY licensesChanged)
  Q_PROPERTY(QStringList languages READ languages NOTIFY languagesChanged)
  Q_PROPERTY(uint minPlayers READ minPlayers NOTIFY minPlayersChanged)
  Q_PROPERTY(uint maxPlayers READ maxPlayers NOTIFY maxPlayersChanged)
  Q_PROPERTY(QStringList genre READ genre NOTIFY genreChanged)
  Q_PROPERTY(QUrl website READ website NOTIFY websiteChanged)
  Q_PROPERTY(QString id READ id NOTIFY idChanged)

  // Additionnal properties.
  Q_PROPERTY(bool isValid READ isValid NOTIFY isValidChanged)

public:
  explicit Quest(QObject* parent = nullptr);
  virtual ~Quest();

public:
  const QString& title() const;
  const QStringList& authors() const;
  const QDate& initialReleaseDate() const;
  const QDate& latestReleaseDate() const;
  const QString& description() const;
  QString version() const;
  QString engineVersion() const;
  const QStringList& licenses() const;
  const QStringList& languages() const;
  uint maxPlayers() const;
  uint minPlayers() const;
  const QStringList& genre() const;
  const QUrl& website() const;
  const QString& id() const;
  bool isValid() const;

signals:
  void titleChanged();
  void authorsChanged();
  void initialReleaseDateChanged();
  void latestReleaseDateChanged();
  void descriptionChanged();
  void versionChanged();
  void engineVersionChanged();
  void licensesChanged();
  void languagesChanged();
  void minPlayersChanged();
  void maxPlayersChanged();
  void genreChanged();
  void websiteChanged();
  void idChanged();
  void isValidChanged();

protected:
  virtual void notifyAllChanged();
  virtual void clear();

  QString _title;
  QStringList _authors;
  QDate _initialReleaseDate;
  QDate _latestReleaseDate;
  QString _description;
  QVersionNumber _version;
  QVersionNumber _engineVersion;
  QStringList _licenses;
  QStringList _languages;
  uint _minPlayers{ 0 };
  uint _maxPlayers{ 0 };
  QStringList _genre;
  QUrl _website;
  QString _id;
  bool _isValid{ false };
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::Quest*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::Quest*);
