/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/base/Quest.h>

namespace solarus::launcher::quests {
/**
 * @brief Necessary information to display a Solarus quest.
 * This class exposes as read-only to QML the meta-data of a quest, along with additionnal information.
 */
class OnlineQuest : public Quest {
  Q_OBJECT

  Q_PROPERTY(QUrl url READ url NOTIFY urlChanged)
  Q_PROPERTY(QString thumbnail READ thumbnail NOTIFY thumbnailChanged)
  Q_PROPERTY(QStringList screenshots READ screenshots NOTIFY screenshotsChanged)
  Q_PROPERTY(solarus::launcher::quests::OnlineQuest::State state READ state NOTIFY stateChanged)
  Q_PROPERTY(int downloadProgress READ downloadProgress NOTIFY downloadProgressChanged)

public:
  enum class State {
    Idle,
    Downloading,
    Downloaded,
  };
  Q_ENUM(State)

public:
  explicit OnlineQuest(QObject* parent = nullptr);
  ~OnlineQuest();

public:
  void initFromJson(const QJsonObject& json, const QString& urlDomain = {});
  void clear() override;

  const QUrl& url() const;
  const QString& thumbnail() const;
  const QStringList& screenshots() const;
  State state() const;
  bool hasUpdate() const;
  int downloadProgress() const;

signals:
  void urlChanged();
  void thumbnailChanged();
  void screenshotsChanged();
  void stateChanged();
  void hasUpdateChanged();
  void downloadProgressChanged();

private:
  friend class QuestUpdate;
  void setHasUpdate(bool hasUpdate);
  void setDownloadProgress(int progress);
  void setState(State state);

private:
  QUrl _url;
  QString _thumbnail;
  QStringList _screenshots;
  State _state{ State::Idle };
  bool _hasUpdate{ false };
  int _downloadProgress{ 0 };
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::OnlineQuest*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::OnlineQuest*);
