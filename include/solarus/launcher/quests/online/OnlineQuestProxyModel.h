/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/online/OnlineQuestListModel.h>

#include <QSortFilterProxyModel>

namespace solarus::launcher::quests {
/**
 * @brief Model that provides a filtered and sorted Quest list based on OnlineQuestListModel.
 */
class OnlineQuestProxyModel : public QSortFilterProxyModel {
  Q_OBJECT

  Q_PROPERTY(QString searchText READ searchText WRITE setSearchText NOTIFY searchTextChanged)
  Q_PROPERTY(QuestSortMode questSortMode READ questSortMode WRITE setQuestSortMode NOTIFY questSortModeChanged)
  Q_PROPERTY(QuestSortOrder questSortOrder READ questSortOrder WRITE setQuestSortOrder NOTIFY questSortOrderChanged)
  Q_PROPERTY(solarus::launcher::quests::OnlineQuestListModel::State state READ state NOTIFY stateChanged)
  Q_PROPERTY(int questCount READ questCount NOTIFY questCountChanged)

public:
  /**
   * @brief Sort criteria.
   */
  enum class QuestSortMode {
    Title,
    ReleaseDate,
  };
  Q_ENUM(QuestSortMode)

  /**
   * @brief Sort order.
   */
  enum class QuestSortOrder {
    Ascending,
    Descending,
  };
  Q_ENUM(QuestSortOrder)

  OnlineQuestProxyModel(OnlineQuestListModel& parentModel, QObject* parent = nullptr);
  ~OnlineQuestProxyModel();

public:
  const QString& searchText() const;
  void setSearchText(const QString& text);

  QuestSortMode questSortMode() const;
  void setQuestSortMode(QuestSortMode questSortMode);

  QuestSortOrder questSortOrder() const;
  void setQuestSortOrder(QuestSortOrder questSortOrder);

  int questCount() const;

  /**
   * @brief Current state.
   * @return The current model state.
   */
  OnlineQuestListModel::State state() const;

protected:
  bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;

signals:
  void searchTextChanged();
  void questSortModeChanged();
  void questSortOrderChanged();
  void stateChanged();
  void questCountChanged();

private:
  OnlineQuestListModel& _parentModel;
  QString _searchText;
  QStringList _searchTags;
  QuestSortMode _questSortMode{ QuestSortMode::Title };
  QuestSortOrder _questSortOrder{ QuestSortOrder::Ascending };
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::OnlineQuestProxyModel*);
