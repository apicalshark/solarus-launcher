/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QString>
#include <QStringList>
#include <QDate>
#include <QVector>
#include <QVersionNumber>
#include <QPixmap>
#include <QUrl>

namespace solarus::launcher::quests {
/**
 * @brief P.O.D. structure to be passed around safely between threads.
 */
struct LocalQuestData {
  bool isValid{ false };
  QString path;
  QString title;
  QStringList authors;
  QDate initialReleaseDate;
  QDate latestReleaseDate;
  QString description;
  QVersionNumber version;
  QVersionNumber engineVersion;
  QStringList licenses;
  QStringList languages;
  uint minPlayers{ 0 };
  uint maxPlayers{ 0 };
  QStringList genre;
  QUrl website;
  QString id;
  QPixmap thumbnail;
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::LocalQuestData)
Q_DECLARE_METATYPE(QVector<solarus::launcher::quests::LocalQuestData>)
