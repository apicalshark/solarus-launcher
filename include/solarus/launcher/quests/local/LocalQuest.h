/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/base/Quest.h>
#include <oclero/QtDownloader.hpp>
#include <solarus/launcher/quests/local/LocalQuestData.h>

#include <QDateTime>
#include <QScopedPointer>
#include <QPixmap>

namespace solarus::launcher::quests {
/**
 * @brief Necessary information to display a Solarus quest.
 * This class exposes as read-only to QML the meta-data of a quest, along with additionnal information.
 */
class LocalQuest : public Quest {
  Q_OBJECT

  Q_PROPERTY(QString path READ path NOTIFY pathChanged)
  Q_PROPERTY(QPixmap thumbnail READ thumbnail NOTIFY thumbnailChanged)
  Q_PROPERTY(QDateTime lastPlayedTime READ lastPlayedTime WRITE setLastPlayedTime NOTIFY lastPlayedTimeChanged)
  Q_PROPERTY(solarus::launcher::quests::LocalQuest::State state READ state NOTIFY stateChanged)
  Q_PROPERTY(bool hasUpdate READ hasUpdate NOTIFY hasUpdateChanged)
  Q_PROPERTY(int downloadProgress READ downloadProgress NOTIFY downloadProgressChanged)

public:
  enum class State {
    Idle,
    Playing,
    Downloading,
  };
  Q_ENUM(State)

public:
  explicit LocalQuest(QObject* parent = nullptr);
  ~LocalQuest();

public:
  void initFromSolarusFile(const LocalQuestData& data);
  void clear() override;

  const QString& path() const;
  const QPixmap& thumbnail() const;
  const QDateTime& lastPlayedTime() const;
  State state() const;
  bool hasUpdate() const;
  int downloadProgress() const;

public slots:
  void setLastPlayedTime(const QDateTime& lastPlayedTime);
  void setPlaying(bool playing);

signals:
  void pathChanged();
  void thumbnailChanged();
  void lastPlayedTimeChanged();
  void stateChanged();
  void hasUpdateChanged();
  void downloadProgressChanged();

private:
  friend class QuestUpdate;
  void setState(State state);
  void setHasUpdate(bool hasUpdate);
  void setDownloadProgress(int progress);

private:
  QString _path;
  QPixmap _thumbnail;
  QDateTime _lastPlayedTime;
  State _state{ State::Idle };
  bool _hasUpdate{ false };
  int _downloadProgress{ 0 };
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::LocalQuest*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::LocalQuest*);
