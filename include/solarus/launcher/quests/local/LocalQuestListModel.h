/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QAbstractListModel>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/local/LocalQuestDataBase.h>
#include <solarus/launcher/quests/local/LocalQuest.h>
#include <solarus/launcher/quests/local/LocalQuestData.h>

namespace solarus::launcher::quests {
/**
 * @brief Model that provides the list of Solarus quests found in a given directory path.
 */
class LocalQuestListModel : public QAbstractListModel {
  Q_OBJECT

  Q_PROPERTY(int questCount READ questCount NOTIFY questCountChanged)
  Q_PROPERTY(bool validConfig READ validConfig NOTIFY validConfigChanged)
  Q_PROPERTY(State state READ state NOTIFY stateChanged)
  Q_PROPERTY(QString directory READ directory WRITE setDirectory NOTIFY directoryChanged)

public:
  /**
   * @brief Custom roles that are accessible to QML.
   */
  enum class QuestInfoRoles : int {
    Quest = Qt::UserRole + 1,
    Title,
    InitialReleaseDate,
    LatestUpdateDate,
    LastPlayedTime,
  };
  Q_ENUM(QuestInfoRoles)

  /**
   * @brief Model state (useful for async feedback in QML).
   */
  enum class State {
    Uninitialized,
    Loading,
    Idle,
  };
  Q_ENUM(State)

public:
  /**
   * @brief Constructs a LocalQuestListModel.
   * @param settings The settings to connect to.
   * @param parent The parent object.
   */
  LocalQuestListModel(app::Settings& settings, QObject* parent = nullptr);

  /**
   * Destructs a LocalQuestListModel.
   */
  ~LocalQuestListModel() = default;

public: // Properties exposed to QML.
  /**
   * @brief Number of Solarus quests.
   * @return The number of Solarus quests found.
   */
  int questCount() const;

  /**
   * @brief Current state of the model.
   * @return The current state of the model.
   */
  State state() const;

  /**
   * @brief Tells if the configuration (e.g. local directory path, server connection...) is valid.
   * @return true if the configuration is valid, false otherwise.
   */
  bool validConfig() const;

  /**
   * @brief Gets the directory to look for quests.
   * @return The directory to look for quests.
   */
  const QString& directory() const;

public: // QAbstractListModel overrides.
  /**
   * @brief Returns the number of rows.
   * @return The number of rows.
   */
  int rowCount(const QModelIndex& index = {}) const override;

  /**
   * @brief Returns the data stored under the given role for the item referred to by the index.
   * @param index The quest index.
   * @param role The QuestInfoRole for the given index.
   * @return The data stored under the given role for the item referred to by the index.
   */
  QVariant data(const QModelIndex& index, int role) const override;

  /**
   * @brief Returns the available roles in QML.
   * @return The available roles in QML.
   */
  QHash<int, QByteArray> roleNames() const override;

public slots: // Exposed to QML.
  /**
   * @brief Set the directory to search for Solarus quests.
   * @param directory The directory path to search.
   */
  void setDirectory(const QString& directory);

  /**
   * @brief Removes the quest from the database and from disk.
   * @param quest The quest to remove.
   */
  void removeQuest(LocalQuest* quest);

  /**
   * @brief Starts the search for quests.
   */
  void startSearch();

  /**
   * @brief Stops the search for quests.
   */
  void stopSearch();

  /**
   * @brief Returns the quest row index in the list, or -1 if the quest is not found.
   * @param quest The quest to get the row index.
   * @return The quest's row index.
   */
  int questRow(const LocalQuest* quest) const;

  /**
   * @brief Returns the quest with the specified id or nullptr if not found.
   * @param id The quest id to look for.
   * @return The quest with the specified id or nullptr if not found.
   */
  LocalQuest* questOfId(const QString& id) const;

public: // Not exposed to QML.
  /**
   * @brief Adds the quest to the database.
   * @param quest The quest to add.
   * @return The LocalQuest object created.
   */
  LocalQuest* addQuest(LocalQuestData& questData);

  /**
   * @brief Gets the list of quests.
   * @return The list of quests.
   */
  const QVector<LocalQuest*>& quests() const;

signals:
  /**
   * @brief Called when the quest count has changed.
   */
  void questCountChanged();

  /**
   * @brief Called when the state has changed.
   */
  void stateChanged();

  /**
   * @brief Called when the search has started.
   */
  void searchStarted();

  /**
   * @brief Called when the search has finished.
   */
  void searchFinished();

  /**
   * @brief Called when the configuration validity has changed.
   */
  void validConfigChanged();

  /**
   * @brief Called when the directory has changed.
   */
  void directoryChanged();

  /**
   * @brief Called when the search progress has changed.
   * @param Progress, normalized from 0 to 100.
   */
  void loadingProgressChanged(int progress);

private:
  quests::LocalQuestDataBase _db;
  app::Settings& _settings;
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::LocalQuestListModel*);
