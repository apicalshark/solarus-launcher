/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QVector>
#include <QString>
#include <QMutex>

#include <solarus/launcher/quests/local/LocalQuestData.h>

namespace solarus::launcher::quests {
/**
 * @brief Basic worker that just look for Solarus quests in the given directory list.
 */
class LocalQuestSearcher : public QObject {
  Q_OBJECT

public:
  LocalQuestSearcher(QObject* parent = nullptr);
  ~LocalQuestSearcher();

public slots:
  /**
   * @brief Search for Solarus quests. Will emit a signal every time when a Quest is found.
   * @param directories Directory to look for quests in.
   */
  void start(const QString& directory);

  /**
   * @brief Stop the search. Won't emit the finished() signal.
   */
  void stop();

  /**
   * @brief Create, if valid, a QuestInfo object from the path. NB: non-const because emits a signal.
   * @param path Path to the .solarus file.
   * @return A QuestInfo object to represent the Solarus quest.
   */
  void readQuest(const QString& path) const;

signals:
  /**
   * @brief Signal emitted when searching has progressed.
   * @param progress
   * @param total
   */
  void progressChanged(int progress, int total) const;

  /**
   * @brief Signal emitted when a Solarus quest is found.
   * @param questData The quest information.
   */
  void questFound(const LocalQuestData& questData) const;

  /**
   * @brief Signal emitted when a bunch of Solarus quests are found.
   * @param questDatas
   */
  void questsFound(const QVector<LocalQuestData>& questDatas) const;

  /**
   * @brief Signal emitted when the worker begins to search into the directory.
   */
  void started();

  /**
   * @brief Signal emitted when the directory has been fully searched.
   */
  void finished();

  /**
   * @brief Signal emitted when the search has been stopped by someone else.
   */
  void stopped();

private:
  bool _stopped = false;
  QMutex _mutex;
};
} //namespace solarus::launcher::quests
