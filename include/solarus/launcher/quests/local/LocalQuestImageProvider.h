/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QQuickImageProvider>
#include <QPointer>

#include <solarus/launcher/quests/local/LocalQuestListModel.h>

namespace solarus::launcher::quests {
/**
* @brief Allows to get QPixmaps from Quests in QML components.
*/
class LocalQuestImageProvider : public QQuickImageProvider {
public:
  LocalQuestImageProvider();

  void setModel(LocalQuestListModel* model);

public:
  /**
   * @brief Creates a QPixmap for the id in parameter.
   * The id must be formatted like this URL: <questId>/<imageType>[/<index>]
   * The <imageType> can be either "thumbnail" or "screenshots".
   * The <index> is only for screenshots.
   * @param id The URL to get a QPixmap from.
   * @param size The size of the returned QPixmap.
   * @param requestedSize The actual size the pixmap will be displayed at (ignore).
   * @return
   */
  QPixmap requestPixmap(const QString& id, QSize* size, const QSize& requestedSize) override;

private:
  QPixmap defaultThumbnail(QSize* size) const;
  QPixmap thumbnail(const QString& questId, QSize* size) const;

private:
  QPointer<LocalQuestListModel> _model;
};
} // namespace solarus::launcher::quests
