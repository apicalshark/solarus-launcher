/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/local/LocalQuestSearcher.h>
#include <solarus/launcher/quests/local/LocalQuest.h>

#include <QObject>
#include <QFileSystemWatcher>
#include <QPointer>
#include <QStringList>
#include <QTimer>
#include <QThread>

namespace solarus::launcher::quests {
/**
 * @brief Exposes a list of Solarus Quests in a directory on a local drive.
 */
class LocalQuestDataBase : public QObject {
  Q_OBJECT

public:
  enum class State {
    Uninitialized,
    NeedsRefresh,
    Searching,
    UpToDate,
  };
  Q_ENUM(State)

public:
  explicit LocalQuestDataBase(QObject* parent = nullptr);
  ~LocalQuestDataBase();

public:
  const QString& directory() const;
  void setDirectory(const QString& path);

  bool validPath() const;
  State state() const;

  void startSearch();
  void stopSearch();
  void reset();

  const QVector<LocalQuest*>& quests() const;
  int questCount() const;
  LocalQuest* questAt(int index) const;
  LocalQuest* questOfId(const QString& id) const;
  LocalQuest* questOfPath(const QString& path) const;
  int questIndex(const LocalQuest* quest) const;

  void addOrUpdateQuest(const LocalQuestData& data);
  void addOrUpdateQuests(const QVector<LocalQuestData>& datas);
  void removeQuest(LocalQuest* quest);

signals:
  void searchStarted();
  void searchFinished();

  void questCountChanged(uint);
  void questAdded(LocalQuest* quest);
  void questRemoved(LocalQuest* quest);
  void questUpdated(LocalQuest* quest);
  void cleared();

  void directoryChanged();
  void validPathChanged();
  void progressChanged(int progress);
  void stateChanged(State state);

  // Used to communicate between threads.
  void searchRequested(const QString& director);
  void questRequested(const QString& path);

private:
  void setupQuestSearcher();
  void clear();

private slots:
  void setState(State state);

  void onSearchStarted();
  void onSearchProgressChanged(int progress, int total);
  void onSearchFinished();

private:
  /**
  * @brief Basic information about a directory.
  */
  struct DirectoryInfo {
    /**
     * @brief Is the path an existing directory path?
     */
    bool isValid{ false };
    /**
     * @brief Directory absolute path.
     */
    QString path;
    /**
     * @brief Directory content (only .solarus files), used as cache.
     */
    QStringList entries;
  };

  State _state{ State::Uninitialized };
  QTimer _timer;
  DirectoryInfo _directoryInfo;
  QVector<LocalQuest*> _quests;
  QPointer<QFileSystemWatcher> _filesystemWatcher{ nullptr };
  QPointer<LocalQuestSearcher> _questSearcher{ nullptr };
  QPointer<QThread> _questSearcherThread{ nullptr };
};
} // namespace solarus::launcher::quests
