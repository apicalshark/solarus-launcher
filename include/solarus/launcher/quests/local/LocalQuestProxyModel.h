/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/local/LocalQuestListModel.h>

#include <QSortFilterProxyModel>

namespace solarus::launcher::quests {
/**
 * @brief Model that provides a filtered and sorted Quest list based on LocalQuestListModel.
 */
class LocalQuestProxyModel : public QSortFilterProxyModel {
  Q_OBJECT

  Q_PROPERTY(QString searchText READ searchText WRITE setSearchText NOTIFY searchTextChanged)
  Q_PROPERTY(QuestSortMode questSortMode READ questSortMode WRITE setQuestSortMode NOTIFY questSortModeChanged)
  Q_PROPERTY(QuestSortOrder questSortOrder READ questSortOrder WRITE setQuestSortOrder NOTIFY questSortOrderChanged)
  Q_PROPERTY(solarus::launcher::quests::LocalQuestListModel::State state READ state NOTIFY stateChanged)
  Q_PROPERTY(int questCount READ questCount NOTIFY questCountChanged)

public:
  /**
   * @brief Sort criteria.
   */
  enum class QuestSortMode {
    Title,
    ReleaseDate,
    LastPlayedTime,
  };
  Q_ENUM(QuestSortMode)

  /**
   * @brief Sort order.
   */
  enum class QuestSortOrder {
    Ascending,
    Descending,
  };
  Q_ENUM(QuestSortOrder)

  LocalQuestProxyModel(LocalQuestListModel& parentModel, QObject* parent = nullptr);
  ~LocalQuestProxyModel();

public:
  const QString& searchText() const;
  void setSearchText(const QString& text);

  QuestSortMode questSortMode() const;
  void setQuestSortMode(QuestSortMode questSortMode);

  QuestSortOrder questSortOrder() const;
  void setQuestSortOrder(QuestSortOrder questSortOrder);

  int questCount() const;

  /**
   * @brief Maximum displayed number of quests, or -1 if no maximum.
   * @return Maximum displayed number of quests.
   */
  int maxCount() const;

  /**
   * @brief Defines the maximum displayed number of quests, or -1 if no maximum.
   * @param The maximum displayed number of quests.
   */
  void setMaxCount(int maxCount);

  /**
   * @brief Current state.
   * @return The current model state.
   */
  LocalQuestListModel::State state() const;

protected:
  bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;

signals:
  void searchTextChanged();
  void questSortModeChanged();
  void questSortOrderChanged();
  void maxCountChanged();
  void stateChanged();
  void questCountChanged();

private:
  LocalQuestListModel& _parentModel;
  QString _searchText;
  QStringList _searchTags;
  QuestSortMode _questSortMode{ QuestSortMode::Title };
  QuestSortOrder _questSortOrder{ QuestSortOrder::Ascending };
  int _maxCount{ -1 };
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::LocalQuestProxyModel*);
