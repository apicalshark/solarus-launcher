/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QPointer>

#include <solarus/launcher/quests/local/LocalQuest.h>
#include <solarus/launcher/quests/online/OnlineQuest.h>

namespace solarus::launcher::quests {
/**
 * @brief
 */
class QuestUpdate : public QObject {
  Q_OBJECT

  Q_PROPERTY(State state READ state NOTIFY stateChanged)
  Q_PROPERTY(int progress READ progress NOTIFY progressChanged)
  Q_PROPERTY(QString id READ id NOTIFY idChanged)
  Q_PROPERTY(QString title READ title NOTIFY titleChanged)
  Q_PROPERTY(QString thumbnailPath READ thumbnailPath NOTIFY thumbnailPathChanged)
  Q_PROPERTY(QString currentVersion READ currentVersion NOTIFY currentVersionChanged)
  Q_PROPERTY(QString latestVersion READ latestVersion NOTIFY latestVersionChanged)
  Q_PROPERTY(QDate latestReleaseDate READ latestReleaseDate NOTIFY latestReleaseDateChanged)
  Q_PROPERTY(int errorCode READ errorCode NOTIFY errorCodeChanged)

public:
  enum class State {
    Uninitialized,
    Available,
    Pending,
    Downloading,
    Installing,
    Updated,
    Error,
  };
  Q_ENUM(State)

  //  enum class ErrorCode {
  //    NoError,
  //    UrlIsInvalid,
  //    LocalDirIsInvalid,
  //    CannotCreateLocalDir,
  //    CannotRemoveFile,
  //    NotAllowedToWriteFile,
  //    NetworkError,
  //    FileDoesNotExistOrIsCorrupted,
  //    FileDoesNotEndWithSuffix,
  //    CannotRenameFile,
  //    Cancelled,
  //    CannotOverwriteFile,
  //  };
  //  Q_ENUM(ErrorCode)

public:
  QuestUpdate(LocalQuest* localQuest, OnlineQuest* onlineQuest, QObject* object = nullptr);
  ~QuestUpdate();

  State state() const;
  int progress() const;
  QString id() const;
  QString title() const;
  QString thumbnailPath() const;
  QString currentVersion() const;
  QString latestVersion() const;
  QDate latestReleaseDate() const;
  QString localPath() const;
  QUrl downloadUrl() const;
  int errorCode() const;

private:
  friend class QuestUpdater;
  void setState(State state);
  void setProgress(int progress);
  void setErrorCode(int code);

signals:
  void stateChanged();
  void progressChanged();
  void idChanged();
  void titleChanged();
  void thumbnailPathChanged();
  void currentVersionChanged();
  void latestVersionChanged();
  void latestReleaseDateChanged();
  void errorCodeChanged();

private:
  State _state{ State::Uninitialized };
  int _progress{ 0 };
  int _errorCode{ 0 };
  QPointer<OnlineQuest> _onlineQuest;
  QPointer<LocalQuest> _localQuest;
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::QuestUpdate*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::QuestUpdate*);
