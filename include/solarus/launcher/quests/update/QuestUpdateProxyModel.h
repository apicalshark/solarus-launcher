/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/quests/update/QuestUpdateListModel.h>

#include <QSortFilterProxyModel>

namespace solarus::launcher::quests {
/**
 * @brief Model that provides a filtered and sorted QuestUpdate list based on QuestUpdateListModel.
 */
class QuestUpdateProxyModel : public QSortFilterProxyModel {
  Q_OBJECT

  Q_PROPERTY(QuestUpdateListModel::UpdateSection section READ section CONSTANT)

public:
  QuestUpdateProxyModel(
    QuestUpdateListModel& parentModel, QuestUpdateListModel::UpdateSection section, QObject* parent = nullptr);
  ~QuestUpdateProxyModel();

public:
  QuestUpdateListModel::UpdateSection section() const;

protected:
  bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
  bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const override;

private:
  QuestUpdateListModel& _parentModel;
  QuestUpdateListModel::UpdateSection _section;
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::QuestUpdateProxyModel*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::QuestUpdateProxyModel*);
