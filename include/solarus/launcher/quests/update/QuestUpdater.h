/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>
#include <solarus/launcher/quests/online/OnlineQuestListModel.h>
#include <solarus/launcher/quests/update/QuestUpdate.h>

#include <oclero/QtDownloader.hpp>

#include <QStringList>
#include <QPointer>
#include <QQueue>
#include <QHash>
#include <QFutureWatcher>
#include <QTemporaryDir>

namespace solarus::launcher::quests {
/**
 * @brief Handles the auto-updates for local quests.
 */
class QuestUpdater : public QObject {
  Q_OBJECT

public:
  /**
   * @brief Updater state (useful for async feedback in QML).
   */
  enum class State {
    Idle,
    CheckingForUpdates,
    WaitingForNextUpdate,
    Updating,
  };
  Q_ENUM(State)

public:
  /**
   * @brief Constructs a QuestUpdater
   * @param parent The parent object.
   */
  explicit QuestUpdater(app::Settings& settings, LocalQuestListModel& localQuestListModel,
    OnlineQuestListModel& onlineQuestListModel, QObject* parent = nullptr);

  /**
   * @brief Destructs a QuestUpdater.
   */
  ~QuestUpdater();

public:
  /**
   * @brief Gets the possible updates.
   * @return The possible updates.
   */
  const QVector<QuestUpdate*>& updates() const;

  /**
   * @brief Gets the total count of possible updates.
   * @return The total count of possible updates.
   */
  int updateCount() const;

  /**
   * @brief Gets the current state of the model.
   * @return The current state of the model.
   */
  State state() const;

  /**
   * @brief Gets the last time quest updates have been checked.
   * @return The last time quest updates have been checked.
   */
  QDateTime lastCheckTime() const;

  /**
   * @brief Checks for updates even if it has already checked recently.
   */
  void forceCheckForUpdates();

  /**
   * @brief Checks for updates asynchronously.
   */
  void checkForUpdates();

  /**
   * @brief Starts the quest update.
   * @param update The QuestUpdate to handle.
   */
  void updateQuest(QuestUpdate* update);

  /**
   * @brief Starts the quest update.
   * @param update The Quest id to handle.
   */
  void updateQuest(const QString& questId);

  /**
   * @brief Cancels the quest update.
   * @param update The QuestUpdate to cancel.
   */
  void cancelQuestUpdate(QuestUpdate* update);

  /**
   * @brief Gets the quest update index in the list.
   * @param update The QuestUpdate to get the index.
   * @return The index of the quest update, or -1 if not found.
   */
  int questUpdateIndex(const QuestUpdate* update) const;

  /**
   * @brief Gets the quest update index in the pending list.
   * @param update The QuestUpdate to get the index.
   * @return The index of the quest update, or -1 if not found/pending.
   */
  int questUpdatePendingIndex(const QuestUpdate* update) const;

private:
  /**
   * @brief Gets the update with the corresponding quest id, or null if not found.
   * @param questId The quest id to find.
   * @return The update with the corresponding quest id, or null if not found.
   */
  QuestUpdate* updateOfId(const QString& questId) const;

  /**
   * @brief Defines the current state of the updater.
   * @param state The current state.
   */
  void setState(State state);

  /**
   * @brief Remove first element of the queue and calls handleNextPendingUpdate().
   */
  void dequeueAndHandleNext();

  /**
   * @brief Starts the next update in the queue.
   */
  void handleNextPendingUpdate();

  /**
   * @brief Removes the update (NB: different from cancelling an update).
   * @param update The update to remove.
   */
  void removeQuestUpdate(QuestUpdate* update);

  bool shouldCheckForUpdates() const;

private slots:
  /**
   * @brief Refreshes the update list by comparing local and online data.
   */
  void refreshUpdates();

signals:
  /**
   * @brief Called when the state has changed.
   */
  void stateChanged();

  /**
   * @brief Called when pending/available updates have changed.
   */
  void updatesChanged();

  /**
   * @brief Called when a quest update has changed.
   * @param questUpdate The quest update.
   */
  void updateChanged(const QuestUpdate* questUpdate);

  void updateRemoved(const QuestUpdate* questUpdate, int index);

private:
  State _state{ State::Idle };
  app::Settings& _settings;
  LocalQuestListModel& _localQuestListModel;
  OnlineQuestListModel& _onlineQuestListModel;
  oclero::QtDownloader _downloader;
  QVector<QuestUpdate*> _updates;
  QQueue<QString> _pendingUpdates;
  QFutureWatcher<bool> _ioFutureWatcher;
  QPointer<QuestUpdate> _updatingQuest;
  QTemporaryDir _tempDir;
};
} // namespace solarus::launcher::quests
