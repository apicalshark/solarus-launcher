/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QAbstractListModel>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/update/QuestUpdate.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>

namespace solarus::launcher::quests {
/**
 * @brief Model that provides the list of Solarus quests updates.
 */
class QuestUpdateListModel : public QAbstractListModel {
  Q_OBJECT

  Q_PROPERTY(int updateCount READ updateCount NOTIFY updateCountChanged)
  Q_PROPERTY(State state READ state NOTIFY stateChanged)

public:
  /**
   * @brief Custom roles that are accessible to QML.
   */
  enum class QuestUpdateRoles : int {
    QuestUpdate = Qt::UserRole + 1,
    QuestUpdateState,
    Title,
    PendingOrder,
    Section,
    Id,
  };
  Q_ENUM(QuestUpdateRoles)

  /**
   * @brief Model state (useful for async feedback in QML).
   */
  enum class State {
    Uninitialized,
    Loading,
    Idle,
    UpdatingQuests,
  };
  Q_ENUM(State)

  /**
   * @brief The section the item is classified under.
   */
  enum class UpdateSection {
    NoSection,
    Error,
    Available,
    Pending,
    Updating,
  };
  Q_ENUM(UpdateSection)

public:
  /**
   * @brief Constructs a QuestUpdateListModel.
   * @param questUpdater The QuestUpdater to expose to QML.
   * @param settings The settings to connect to.
   * @param parent The parent object.
   */
  QuestUpdateListModel(quests::QuestUpdater& questUpdater, QObject* parent = nullptr);

  /**
   * Destructs a QuestUpdateListModel.
   */
  ~QuestUpdateListModel() = default;

public: // Properties exposed to QML.
  /**
   * @brief Number of Solarus quest updates.
   * @return The number of Solarus quests updates.
   */
  int updateCount() const;

  /**
   * @brief Current state of the model.
   * @return The current state of the model.
   */
  State state() const;

  //UpdateSection updateSection() const;

public: // QAbstractListModel overrides.
  /**
   * @brief Returns the number of rows.
   * @return The number of rows.
   */
  int rowCount(const QModelIndex& index = {}) const override;

  /**
   * @brief Returns the data stored under the given role for the item referred to by the index.
   * @param index The quest index.
   * @param role The QuestInfoRole for the given index.
   * @return The data stored under the given role for the item referred to by the index.
   */
  QVariant data(const QModelIndex& index, int role) const override;

  /**
   * @brief Returns the available roles in QML.
   * @return The available roles in QML.
   */
  QHash<int, QByteArray> roleNames() const override;

signals:
  /**
   * @brief Called when the quest count has changed.
   */
  void updateCountChanged();

  /**
   * @brief Called when the state has changed.
   */
  void stateChanged();

private:
  quests::QuestUpdater& _questUpdater;
};
} // namespace solarus::launcher::quests

Q_DECLARE_METATYPE(solarus::launcher::quests::QuestUpdateListModel*);
Q_DECLARE_METATYPE(const solarus::launcher::quests::QuestUpdateListModel*);
