/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QApplication>
#include <QPointer>
#include <QQmlApplicationEngine>
#include <QQuickWindow>

#include <oclero/QtUpdater.hpp>
#include <oclero/QtAppInstanceManager.hpp>
#include <solarus/launcher/app/LanguageManager.h>
#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/controllers/AboutPageController.h>
#include <solarus/launcher/controllers/AppMenuController.h>
#include <solarus/launcher/controllers/BottomBarController.h>
#include <solarus/launcher/controllers/HomePageController.h>
#include <solarus/launcher/controllers/LocalQuestsPageController.h>
#include <solarus/launcher/controllers/OnboardingPagesController.h>
#include <solarus/launcher/controllers/OnlineQuestsPageController.h>
#include <solarus/launcher/controllers/MainPagesController.h>
#include <solarus/launcher/controllers/PlayingQuestPageController.h>
#include <solarus/launcher/controllers/QuestUpdatePageController.h>
#include <solarus/launcher/controllers/SettingsPageController.h>
#include <solarus/launcher/controllers/UpdateDialogController.h>
#include <solarus/launcher/gamepad/GamepadManager.h>
#include <solarus/launcher/news/NewsListModel.h>
#include <solarus/launcher/quests/local/LocalQuestImageProvider.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>
#include <solarus/launcher/quests/local/LocalQuestProxyModel.h>
#include <solarus/launcher/quests/online/OnlineQuestListModel.h>
#include <solarus/launcher/quests/online/OnlineQuestProxyModel.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>
#include <solarus/launcher/runner/QuestOutputHandler.h>
#include <solarus/launcher/runner/QuestRunner.h>

namespace solarus::launcher::app {
class Application : public QApplication {
  Q_OBJECT

public:
  explicit Application(int& argc, char* argv[]);
  ~Application();

  static int start(int& argc, char* argv[]);

private:
  void setupSingleInstance();
  void setupLanguages();
  void setupQuests();
  void setupUpdater();
  void setupNews();
  void setupQmlEngine();
  bool loadView();
  QQuickWindow* getWindow() const;

private:
  Settings _settings;
  LanguageManager _languageManager;
  oclero::QtAppInstanceManager _instanceManager;
  oclero::QtUpdater _updater;
  Navigation _navigation;
  quests::LocalQuestListModel _localQuestListModel;
  quests::LocalQuestImageProvider* _localQuestImageProvider{ nullptr };
  quests::OnlineQuestListModel _onlineQuestListModel;
  quests::QuestUpdater _questUpdater;
  runner::QuestRunner _questRunner;
  news::NewsListModel _newsListModel;
  gamepad::GamepadManager _gamepadManager;
  controllers::AboutPageController _aboutPageController;
  controllers::AppMenuController _appMenuController;
  controllers::BottomBarController _bottomBarController;
  controllers::MainPagesController _mainPagesController;
  controllers::HomePageController _homePageController;
  controllers::LocalQuestsPageController _localQuestsPageController;
  controllers::OnlineQuestsPageController _onlineQuestsPageController;
  controllers::OnboardingPagesController _onboardingPagesController;
  controllers::PlayingQuestPageController _playingQuestPageController;
  controllers::QuestUpdatePageController _questUpdatePageController;
  controllers::SettingsPageController _settingsPageController;
  controllers::UpdateDialogController _updateDialogController;
  QPointer<QQuickWindow> _window{ nullptr };
  QQmlApplicationEngine _qmlEngine;
};
} // namespace solarus::launcher::app
