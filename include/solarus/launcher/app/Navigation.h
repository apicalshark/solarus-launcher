/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QPointer>

#include <solarus/launcher/quests/local/LocalQuest.h>
#include <oclero/QtScopedConnection.hpp>

namespace solarus::launcher::app {
/**
 * @brief Handles navigation within the application's pages and panels.
 */
class Navigation : public QObject {
  Q_OBJECT

  Q_PROPERTY(WindowId currentWindow READ currentWindow WRITE setCurrentWindow NOTIFY currentWindowChanged)
  Q_PROPERTY(PageId currentPage READ currentPage WRITE setCurrentPage NOTIFY currentPageChanged)
  Q_PROPERTY(solarus::launcher::quests::LocalQuest* selectedQuest READ selectedQuest WRITE setSelectedQuest NOTIFY
      selectedQuestChanged)

public:
  enum class WindowId {
    OnboardingWindow,
    MainWindow,
  };
  Q_ENUM(WindowId)

  enum class PageId {
    Home,
    LocalQuests,
    OnlineQuests,
    PlayingQuest,
    QuestUpdates,
    Settings,
    About,
  };
  Q_ENUM(PageId)

public:
  explicit Navigation(QObject* parent = nullptr);
  ~Navigation();

public:
  WindowId currentWindow() const;
  PageId currentPage() const;
  quests::LocalQuest* selectedQuest() const;

public slots:
  void setCurrentWindow(WindowId pageId);
  void setCurrentPage(PageId pageId);
  void setSelectedQuest(quests::LocalQuest* quest);
  void focusSideBar();
  void focusPageContent();

signals:
  void currentWindowChanged();
  void currentPageChanged();
  void sideBarFocusRequested();
  void pageContentFocusRequested();
  void selectedQuestStateChanged();
  void selectedQuestChanged();

private:
  WindowId _currentWindow{ WindowId::OnboardingWindow };
  PageId _currentPage{ PageId::Home };
  QPointer<quests::LocalQuest> _selectedQuest{ nullptr };
  oclero::QtScopedConnection _selectedQuestConnection;
};
} // namespace solarus::launcher::app
