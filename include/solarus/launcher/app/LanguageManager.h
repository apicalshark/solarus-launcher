/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QPointer>
#include <QDir>
#include <QLocale>
#include <QTranslator>
#include <QQmlEngine>
#include <QVector>

#include <memory>

namespace solarus::launcher::app {
/**
 * @brief Manages the language set in the app.
 */
class LanguageManager : public QObject {
  Q_OBJECT

  Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)
  Q_PROPERTY(QString sourceDirectory READ sourceDirectory WRITE setSourceDirectory NOTIFY sourceDirectoryChanged)

public:
  LanguageManager(QObject* parent = nullptr);
  ~LanguageManager();

public:
  void setQmlEngine(QQmlEngine* engine);
  const QVector<QString>& availableLanguages() const;

  QString sourceDirectory() const;
  void setSourceDirectory(QString const&);

  const QString& language() const;
  void setLanguage(const QString& value);

  static QString defaultTranslationsDirectory();

signals:
  void languageChanged();
  void availableLanguagesChanged();
  void sourceDirectoryChanged();

private:
  void updateLanguages();
  void synchronize();
  QString findBestLanguage(const QString& desiredLocale) const;

private:
  QPointer<QQmlEngine> _qmlEngine{ nullptr };
  QString _filePrefix;
  QString _desiredLanguage;
  QString _currentLanguage;
  QVector<QString> _languages;
  QDir _sourceDirectory;
  QPointer<QTranslator> _translator{ nullptr };
  QPointer<QTranslator> _qtTranslator{ nullptr };
};
} // namespace solarus::launcher::app
