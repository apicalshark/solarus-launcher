/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QAbstractListModel>

#include <solarus/launcher/app/LanguageManager.h>

namespace solarus::launcher::app {
/**
 * @brief Manages the language set in the app.
 */
class LanguageListModel : public QAbstractListModel {
  Q_OBJECT

  Q_PROPERTY(QString currentLanguage READ currentLanguage WRITE setCurrentLanguage NOTIFY currentLanguageChanged)

public:
  enum ItemDataRoles {
    LanguageRole = Qt::UserRole + 1,
  };

public:
  LanguageListModel(LanguageManager& languageManager, QObject* parent = nullptr);
  ~LanguageListModel() = default;

public:
  QString currentLanguage() const;
  void setCurrentLanguage(const QString& value);

public: // Model implementation
  int rowCount(const QModelIndex& index = QModelIndex()) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  QVariant data(const QModelIndex& index, int role) const override;
  QHash<int, QByteArray> roleNames() const override;

signals:
  void currentLanguageChanged();

private:
  LanguageManager& _languageManager;
};
} // namespace solarus::launcher::app

Q_DECLARE_METATYPE(solarus::launcher::app::LanguageListModel*);
