/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QSettings>
#include <QString>

#include <oclero/QtUpdater.hpp>

namespace solarus::launcher::app {
/**
 * @brief Wrapper around QSettings, to centralize access/writing of Solarus
 * settings.
 */
class Settings : public QObject {
  Q_OBJECT

  Q_PROPERTY(int appAudioVolume READ appAudioVolume WRITE setAppAudioVolume NOTIFY appAudioVolumeChanged)
  Q_PROPERTY(bool appEnableAudio READ appEnableAudio WRITE setAppEnableAudio NOTIFY appEnableAudioChanged)
  Q_PROPERTY(bool appFullScreen READ appFullScreen WRITE setAppFullScreen NOTIFY appFullScreenChanged)
  Q_PROPERTY(bool appEnableUpdates READ appEnableUpdates WRITE setAppEnableUpdates NOTIFY appEnableUpdatesChanged)
  Q_PROPERTY(oclero::QtUpdater::Frequency appUpdateCheckFrequency READ appUpdateCheckFrequency WRITE
      setAppUpdateCheckFrequency NOTIFY appUpdateCheckFrequencyChanged)
  Q_PROPERTY(
    QDateTime appLastUpdateCheck READ appLastUpdateCheck WRITE setAppLastUpdateCheck NOTIFY appLastUpdateCheckChanged)
  Q_PROPERTY(bool appOnboardingDone READ appOnboardingDone WRITE setAppOnboardingDone NOTIFY appOnboardingDoneChanged)
  Q_PROPERTY(bool appForceSoftwareRendering READ appForceSoftwareRendering WRITE setAppForceSoftwareRendering NOTIFY
      appForceSoftwareRenderingChanged)
  Q_PROPERTY(QString appLanguage READ appLanguage WRITE setAppLanguage NOTIFY appLanguageChanged)

  Q_PROPERTY(int windowX READ windowX WRITE setWindowX NOTIFY windowXChanged)
  Q_PROPERTY(int windowY READ windowY WRITE setWindowY NOTIFY windowYChanged)
  Q_PROPERTY(int windowWidth READ windowWidth WRITE setWindowWidth NOTIFY windowWidthChanged)
  Q_PROPERTY(int windowHeight READ windowHeight WRITE setWindowHeight NOTIFY windowHeightChanged)

  Q_PROPERTY(bool questEnableAudio READ questEnableAudio WRITE setQuestEnableAudio NOTIFY questEnableAudioChanged)
  Q_PROPERTY(bool questForceSoftwareRendering READ questForceSoftwareRendering WRITE setQuestForceSoftwareRendering
      NOTIFY questForceSoftwareRenderingChanged)
  Q_PROPERTY(bool questFullScreen READ questFullScreen WRITE setQuestFullScreen NOTIFY questFullScreenChanged)
  Q_PROPERTY(
    bool questEnableUpdates READ questEnableUpdates WRITE setQuestEnableUpdates NOTIFY questEnableUpdatesChanged)
  Q_PROPERTY(QDateTime questLastUpdateCheck READ questLastUpdateCheck WRITE setQuestLastUpdateCheck NOTIFY
      questLastUpdateCheckChanged)
  Q_PROPERTY(oclero::QtUpdater::Frequency questUpdateCheckFrequency READ questUpdateCheckFrequency WRITE
      setQuestUpdateCheckFrequency NOTIFY questUpdateCheckFrequencyChanged)
  Q_PROPERTY(QString questDirectory READ questDirectory WRITE setQuestDirectory NOTIFY questDirectoryChanged)
  Q_PROPERTY(QString questLibraryEndpointUrl READ questLibraryEndpointUrl WRITE setQuestLibraryEndpointUrl NOTIFY
      questLibraryEndpointUrlChanged)
  Q_PROPERTY(bool questSuspendWhenUnfocused READ questSuspendWhenUnfocused WRITE setQuestSuspendWhenUnfocused NOTIFY
      questSuspendWhenUnfocusedChanged)

public:
  explicit Settings(QObject* parent = nullptr);
  ~Settings();

  static QString getDefaultQuestDirectory();

public:
  static QString applicationName();
  static QString organizationName();

  int appAudioVolume() const;
  void setAppAudioVolume(int value);
  Q_SIGNAL void appAudioVolumeChanged();

  bool appEnableAudio() const;
  void setAppEnableAudio(bool value);
  Q_SIGNAL void appEnableAudioChanged();

  bool appFullScreen() const;
  void setAppFullScreen(bool value);
  Q_SIGNAL void appFullScreenChanged();

  bool appEnableUpdates() const;
  void setAppEnableUpdates(bool value);
  Q_SIGNAL void appEnableUpdatesChanged();

  oclero::QtUpdater::Frequency appUpdateCheckFrequency() const;
  void setAppUpdateCheckFrequency(oclero::QtUpdater::Frequency value);
  Q_SIGNAL void appUpdateCheckFrequencyChanged();

  QDateTime appLastUpdateCheck() const;
  void setAppLastUpdateCheck(const QDateTime& value);
  Q_SIGNAL void appLastUpdateCheckChanged();

  bool appOnboardingDone() const;
  void setAppOnboardingDone(bool value);
  Q_SIGNAL void appOnboardingDoneChanged();

  QString appLanguage() const;
  void setAppLanguage(const QString& value);
  Q_SIGNAL void appLanguageChanged();

  bool appForceSoftwareRendering() const;
  void setAppForceSoftwareRendering(bool value);
  Q_SIGNAL void appForceSoftwareRenderingChanged();

  int windowX() const;
  void setWindowX(int value);
  Q_SIGNAL void windowXChanged();

  int windowY() const;
  void setWindowY(int value);
  Q_SIGNAL void windowYChanged();

  int windowWidth() const;
  void setWindowWidth(int value);
  Q_SIGNAL void windowWidthChanged();

  int windowHeight() const;
  void setWindowHeight(int value);
  Q_SIGNAL void windowHeightChanged();

  bool questEnableAudio() const;
  void setQuestEnableAudio(bool value);
  Q_SIGNAL void questEnableAudioChanged();

  bool questForceSoftwareRendering() const;
  void setQuestForceSoftwareRendering(bool value);
  Q_SIGNAL void questForceSoftwareRenderingChanged();

  bool questFullScreen() const;
  void setQuestFullScreen(bool value);
  Q_SIGNAL void questFullScreenChanged();

  bool questEnableUpdates() const;
  void setQuestEnableUpdates(bool value);
  Q_SIGNAL void questEnableUpdatesChanged();

  QString questDirectory() const;
  void setQuestDirectory(const QString& value);
  Q_SIGNAL void questDirectoryChanged();

  QString questLibraryEndpointUrl() const;
  void setQuestLibraryEndpointUrl(const QString& value);
  Q_SIGNAL void questLibraryEndpointUrlChanged();

  oclero::QtUpdater::Frequency questUpdateCheckFrequency() const;
  void setQuestUpdateCheckFrequency(oclero::QtUpdater::Frequency value);
  Q_SIGNAL void questUpdateCheckFrequencyChanged();

  QDateTime questLastUpdateCheck() const;
  void setQuestLastUpdateCheck(const QDateTime& value);
  Q_SIGNAL void questLastUpdateCheckChanged();

  bool questSuspendWhenUnfocused() const;
  void setQuestSuspendWhenUnfocused(bool value);
  Q_SIGNAL void questSuspendWhenUnfocusedChanged();

public slots:
  void resetToDefaults();

private:
  QSettings _qSettings;
};
} // namespace solarus::launcher::app
