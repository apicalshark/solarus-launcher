/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>
#include <solarus/launcher/quests/local/LocalQuestProxyModel.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>
#include <solarus/launcher/runner/QuestRunner.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the LocalQuests page.
 */
class LocalQuestsPageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::quests::LocalQuestListModel* questListModel READ questListModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::LocalQuestProxyModel* questListProxyModel READ questListProxyModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::LocalQuest* selectedQuest READ selectedQuest WRITE setSelectedQuest NOTIFY
      selectedQuestChanged)

public:
  LocalQuestsPageController(app::Navigation& navigation, runner::QuestRunner& questRunner,
    quests::QuestUpdater& questUpdater, quests::LocalQuestListModel& questListModel, QObject* parent = nullptr);
  ~LocalQuestsPageController() = default;

  quests::LocalQuestListModel* questListModel() const;
  quests::LocalQuestProxyModel* questListProxyModel() const;

  quests::LocalQuest* selectedQuest() const;

public slots:
  void goToSettings();
  void goToOnlineQuestList();
  void focusSideBar();

  void setSelectedQuest(solarus::launcher::quests::LocalQuest* quest);
  void playQuest(solarus::launcher::quests::LocalQuest* quest);
  void stopQuest(solarus::launcher::quests::LocalQuest* quest);
  void updateQuest(solarus::launcher::quests::LocalQuest* quest);
  void removeQuest(solarus::launcher::quests::LocalQuest* quest);

signals:
  void selectedQuestChanged();

private:
  app::Navigation& _navigation;
  runner::QuestRunner& _questRunner;
  quests::QuestUpdater& _questUpdater;
  quests::LocalQuestListModel& _questListModel;
  quests::LocalQuestProxyModel* _questListProxyModel;
};
} // namespace solarus::launcher::controllers
