/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

namespace oclero {
class QtUpdater;
}

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the Update dialog.
 */
class UpdateDialogController : public QObject {
  Q_OBJECT

  Q_PROPERTY(State state READ state NOTIFY stateChanged)
  Q_PROPERTY(QString currentVersion READ currentVersion NOTIFY currentVersionChanged)
  Q_PROPERTY(QDateTime currentVersionDate READ currentVersionDate NOTIFY currentVersionDateChanged)
  Q_PROPERTY(QString latestVersion READ latestVersion NOTIFY latestVersionChanged)
  Q_PROPERTY(QDateTime latestVersionDate READ latestVersionDate NOTIFY latestVersionDateChanged)
  Q_PROPERTY(int downloadProgress READ downloadProgress NOTIFY downloadProgressChanged)

public:
  enum class State {
    None,
    Checking,
    CheckingFail,
    CheckingSuccess,
    CheckingUpToDate,
    Downloading,
    DownloadingFail,
    DownloadingSuccess,
    Installing,
    InstallingFail,
    InstallingSuccess,
  };
  Q_ENUM(State)

public:
  explicit UpdateDialogController(oclero::QtUpdater& updater, QObject* parent = nullptr);
  ~UpdateDialogController() = default;

  State state() const;
  QString currentVersion() const;
  QDateTime currentVersionDate() const;
  QString latestVersion() const;
  QDateTime latestVersionDate() const;
  int downloadProgress() const;

private:
  void setState(State state);
  void setDownloadProgress(int);

public slots:
  void cancel();
  void checkForUpdates();
  void downloadUpdate();
  void installUpdate();

signals:
  void stateChanged();
  void currentVersionChanged();
  void currentVersionDateChanged();
  void latestVersionChanged();
  void latestVersionDateChanged();
  void downloadProgressChanged(int);
  void manualCheckingRequested();
  void closeDialogRequested();

private:
  oclero::QtUpdater& _updater;
  State _state{ State::None };
  int _downloadProgress{ 0 };
};
} // namespace solarus::launcher::controllers
