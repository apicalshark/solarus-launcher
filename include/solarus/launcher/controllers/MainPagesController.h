/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/runner/QuestRunner.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the application's main pages.
 */
class MainPagesController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::app::Navigation* navigation READ navigation CONSTANT)

public:
  MainPagesController(runner::QuestRunner& questRunner, app::Navigation& navigation, QObject* parent = nullptr);
  ~MainPagesController() = default;

public:
  app::Navigation* navigation() const;

signals:
  void questProcessErrorRaised(solarus::launcher::runner::QuestRunner::ErrorCode error);

private:
  app::Navigation& _navigation;
  runner::QuestRunner& _questRunner;
};
} // namespace solarus::launcher::controllers

Q_DECLARE_METATYPE(solarus::launcher::runner::QuestRunner::ErrorCode);
