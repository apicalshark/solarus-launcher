/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <memory>

#include <solarus/launcher/utils/QmlAction.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>

namespace solarus::launcher::app {
class Navigation;
class Settings;
} // namespace solarus::launcher::app

namespace oclero {
class QtUpdater;
}

namespace solarus::launcher::runner {
class QuestRunner;
}

namespace solarus::launcher::quests {
class LocalQuestListModel;
} // namespace solarus::launcher::quests

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the application's system menu.
 */
class AppMenuController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::utils::QmlAction* preferences READ preferences CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* checkForUpdates READ checkForUpdates CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* quit READ quit CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* playQuest READ playQuest CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* stopQuest READ stopQuest CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* checkForQuestUpdates READ checkForQuestUpdates CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* fullScreen READ fullScreen CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* fullScreenQuest READ fullScreenQuest CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* changelog READ changelog CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* userManual READ userManual CONSTANT)
  Q_PROPERTY(solarus::launcher::utils::QmlAction* about READ about CONSTANT)

public:
  AppMenuController(app::Navigation& navigation, app::Settings& settings, oclero::QtUpdater& updater,
    runner::QuestRunner& questRunner, quests::QuestUpdater& questUpdater, QObject* parent = nullptr);
  ~AppMenuController() = default;

  utils::QmlAction* preferences() const;
  utils::QmlAction* checkForUpdates() const;
  utils::QmlAction* quit() const;
  utils::QmlAction* playQuest() const;
  utils::QmlAction* stopQuest() const;
  utils::QmlAction* checkForQuestUpdates() const;
  utils::QmlAction* fullScreen() const;
  utils::QmlAction* fullScreenQuest() const;
  utils::QmlAction* changelog() const;
  utils::QmlAction* userManual() const;
  utils::QmlAction* about() const;

private:
  void initPreferences();
  void initCheckForUpdate();
  void initQuit();
  void initPlayQuest();
  void initStopQuest();
  void initCheckForQuestUpdates();
  void initFullScreen();
  void initFullScreenQuest();
  void initChangelog();
  void initUserManual();
  void initAbout();

private:
  app::Navigation& _navigation;
  app::Settings& _settings;
  oclero::QtUpdater& _updater;
  runner::QuestRunner& _questRunner;
  quests::QuestUpdater& _questUpdater;
  std::unique_ptr<utils::QmlAction> _preferences;
  std::unique_ptr<utils::QmlAction> _checkForUpdates;
  std::unique_ptr<utils::QmlAction> _quit;
  std::unique_ptr<utils::QmlAction> _playQuest;
  std::unique_ptr<utils::QmlAction> _stopQuest;
  std::unique_ptr<utils::QmlAction> _checkForQuestUpdates;
  std::unique_ptr<utils::QmlAction> _fullScreen;
  std::unique_ptr<utils::QmlAction> _fullScreenQuest;
  std::unique_ptr<utils::QmlAction> _changelog;
  std::unique_ptr<utils::QmlAction> _userManual;
  std::unique_ptr<utils::QmlAction> _about;
};
} // namespace solarus::launcher::controllers
