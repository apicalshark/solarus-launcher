/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QPointer>

#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/quests/base/Quest.h>
#include <solarus/launcher/quests/online/OnlineQuestListModel.h>
#include <solarus/launcher/quests/online/OnlineQuestProxyModel.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the OnlineQuests page.
 */
class OnlineQuestsPageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::quests::OnlineQuestListModel* questListModel READ questListModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::OnlineQuestProxyModel* questListProxyModel READ questListProxyModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::Quest* selectedQuest READ selectedQuest WRITE setSelectedQuest NOTIFY
      selectedQuestChanged)

public:
  explicit OnlineQuestsPageController(app::Navigation& navigation, quests::QuestUpdater& questUpdater,
    quests::OnlineQuestListModel& questListModel, QObject* parent = nullptr);
  ~OnlineQuestsPageController() = default;

  quests::OnlineQuestListModel* questListModel() const;
  quests::OnlineQuestProxyModel* questListProxyModel() const;

  quests::Quest* selectedQuest() const;
  void setSelectedQuest(quests::Quest* quest);

public slots:
  void refresh();
  void focusSideBar();
  void openGamesOnWebsite() const;
  /**
   * @brief Downloads the quest in the quests directory, if not already downloaded.
   * @param quest The quest to download.
   */
  void downloadQuest(solarus::launcher::quests::Quest* quest);

  /**
   * @brief Updates the quest (if already installed locally).
   * @param quest The quest to update.
   */
  void updateQuest(solarus::launcher::quests::Quest* quest);

signals:
  void selectedQuestChanged();

private:
  app::Navigation& _navigation;
  QPointer<quests::Quest> _selectedQuest{ nullptr };
  quests::OnlineQuestListModel& _questListModel;
  quests::OnlineQuestProxyModel* _questListProxyModel;
  quests::QuestUpdater& _questUpdater;
};
} // namespace solarus::launcher::controllers
