/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QTimer>

#include <solarus/launcher/gamepad/GamepadManager.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the application's bottom bar.
 */
class BottomBarController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::gamepad::GamepadManager* gamepadManager READ gamepadManager CONSTANT)
  Q_PROPERTY(bool visible READ visible NOTIFY visibleChanged)

public:
  BottomBarController(gamepad::GamepadManager& gamepadManager, QObject* parent = nullptr);
  ~BottomBarController() = default;

  gamepad::GamepadManager* gamepadManager() const;

  bool visible() const;
  Q_SIGNAL void visibleChanged();

private:
  gamepad::GamepadManager& _gamepadManager;
  QTimer _timer;
  bool _tmp{ true };
};
} // namespace solarus::launcher::controllers
