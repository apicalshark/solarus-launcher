/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/app/LanguageListModel.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>

namespace oclero {
class QtUpdater;
}

namespace solarus::launcher::quests {
class LocalQuestListModel;
class QuestUpdater;
} // namespace solarus::launcher::quests

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the user settings page.
 */
class SettingsPageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::app::Settings* settings READ settings CONSTANT)
  Q_PROPERTY(bool validPath READ validPath NOTIFY validPathChanged)
  Q_PROPERTY(solarus::launcher::app::LanguageListModel* languageListModel READ languageListModel CONSTANT)
  Q_PROPERTY(QAbstractListModel* checkFrequencyListModel READ checkFrequencyListModel CONSTANT)
  Q_PROPERTY(QDateTime lastAppUpdateCheckTime READ lastAppUpdateCheckTime NOTIFY lastAppUpdateCheckTimeChanged)
  Q_PROPERTY(bool checkingforAppUpdates READ checkingforAppUpdates NOTIFY checkingForAppUpdatesChanged)
  Q_PROPERTY(bool checkingForQuestUpdates READ checkingForQuestUpdates NOTIFY checkingForQuestUpdatesChanged)

public:
  SettingsPageController(app::Settings& settings, oclero::QtUpdater& updater, quests::QuestUpdater& questUpdater,
    quests::LocalQuestListModel& questListModel, app::LanguageManager& languageManager, QObject* parent = nullptr);
  ~SettingsPageController() = default;

  app::Settings* settings() const;
  bool validPath() const;
  app::LanguageListModel* languageListModel() const;
  QAbstractListModel* checkFrequencyListModel() const;
  QDateTime lastAppUpdateCheckTime() const;
  bool checkingforAppUpdates() const;
  bool checkingForQuestUpdates() const;

public slots:
  void checkForAppUpdates();
  void checkForQuestUpdates();

signals:
  void validPathChanged();
  void lastAppUpdateCheckTimeChanged();
  void checkingForAppUpdatesChanged();
  void checkingForQuestUpdatesChanged();

private:
  app::Settings& _settings;
  oclero::QtUpdater& _updater;
  quests::QuestUpdater& _questUpdater;
  quests::LocalQuestListModel& _questListModel;
  app::LanguageListModel _languageListModel;
  QAbstractListModel* _checkFrequencyListModel;
};
} // namespace solarus::launcher::controllers
