/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the About page.
 */
class AboutPageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(QString applicationVersion READ applicationVersion CONSTANT)
  Q_PROPERTY(QString applicationName READ applicationName CONSTANT)
  Q_PROPERTY(QString applicationCopyright READ applicationCopyright CONSTANT)
  Q_PROPERTY(QString gplLicenseURL READ gplLicenseURL CONSTANT)
  Q_PROPERTY(QString ccLicenseURL READ ccLicenseURL CONSTANT)

  Q_PROPERTY(QString sourceCodeURL READ sourceCodeURL CONSTANT)
  Q_PROPERTY(QString bugReportURL READ bugReportURL CONSTANT)
  Q_PROPERTY(QString websiteURL READ websiteURL CONSTANT)

public:
  explicit AboutPageController(QObject* parent = nullptr);
  ~AboutPageController() = default;

  QString applicationVersion() const;
  QString applicationName() const;
  QString applicationCopyright() const;
  QString gplLicenseURL() const;
  QString ccLicenseURL() const;

  QString sourceCodeURL() const;
  QString bugReportURL() const;
  QString websiteURL() const;

  Q_SLOT void showUserManual() const;
  Q_SLOT void showChangelog() const;
  Q_SLOT void openSourceCodeURL() const;
  Q_SLOT void openBugReportURL() const;
  Q_SLOT void openWebsiteURL() const;
};
} // namespace solarus::launcher::controllers
