/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/runner/QuestOutputColors.h>
#include <solarus/launcher/runner/QuestRunner.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the PlayingQuest page.
 * Exposes to QML:
 * - QuestOutputHandler* questOutputHandler (get): the console output for the running quest.
 * - Quest* currentQuest (get): the quest that is running.
 * - void startQuest(Quest*): starts a quest
 * - void stop(): stops a quest
 */
class PlayingQuestPageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::runner::QuestOutputColors* outputColors READ outputColors WRITE setOutputColors NOTIFY
      outputColorsChanged)
  Q_PROPERTY(solarus::launcher::quests::LocalQuest* playingQuest READ playingQuest NOTIFY playingQuestChanged)
  Q_PROPERTY(QString fullOutput READ fullOutput NOTIFY fullOutputChanged)

public:
  PlayingQuestPageController(runner::QuestRunner& questRunner, QObject* parent = nullptr);
  ~PlayingQuestPageController() = default;

public:
  runner::QuestOutputColors* outputColors() const;
  void setOutputColors(runner::QuestOutputColors* outputColors);

  const QString& fullOutput() const;

  quests::LocalQuest* playingQuest() const;

public slots:
  void stopQuest();

signals:
  void outputAdded(const QString& output);
  void errorRaised(solarus::launcher::runner::QuestRunner::ErrorCode error);
  void outputCleared();
  void outputColorsChanged();
  void playingQuestChanged();
  void fullOutputChanged();

private:
  runner::QuestRunner& _questRunner;
};
} // namespace solarus::launcher::controllers
