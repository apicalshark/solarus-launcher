/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/app/Navigation.h>
#include <solarus/launcher/news/NewsListModel.h>
#include <solarus/launcher/quests/local/LocalQuestProxyModel.h>
#include <solarus/launcher/quests/local/LocalQuestListModel.h>
#include <solarus/launcher/runner/QuestRunner.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the Home page.
 */
class HomePageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::news::NewsListModel* newsListModel READ newsListModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::LocalQuestProxyModel* questListModel READ questListModel CONSTANT)

public:
  explicit HomePageController(app::Navigation& navigation, quests::LocalQuestListModel& questListModel,
    runner::QuestRunner& questRunner, news::NewsListModel& newsListModel, QObject* parent = nullptr);
  ~HomePageController() = default;

  quests::LocalQuestProxyModel* questListModel() const;
  news::NewsListModel* newsListModel() const;

public slots:
  void ensureSearchStarted();
  void goToQuestsPage();
  void openNewsWebsiteURL() const;
  void startQuest(solarus::launcher::quests::LocalQuest* quest);

private:
  app::Navigation& _navigation;
  runner::QuestRunner& _questRunner;
  news::NewsListModel& _newsListModel;
  quests::LocalQuestListModel& _questListModel;
  quests::LocalQuestProxyModel* _questListProxyModel;
};
} // namespace solarus::launcher::controllers
