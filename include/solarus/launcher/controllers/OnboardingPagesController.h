/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QGamepad>
#include <QObject>
#include <QString>

namespace solarus::launcher::app {
class Navigation;
class Settings;
} // namespace solarus::launcher::app
namespace solarus::launcher::quests {
class LocalQuestListModel;
}

namespace solarus::launcher::controllers {
/**
 * @brief Controller for all the onboarding steps.
 */
class OnboardingPagesController : public QObject {
  Q_OBJECT

  Q_PROPERTY(QString questDirectory READ questDirectory WRITE setQuestDirectory NOTIFY questDirectoryChanged)
  Q_PROPERTY(bool onboardingDone READ onboardingDone WRITE setOnboardingDone NOTIFY onboardingDoneChanged)
  Q_PROPERTY(bool gamepadConnected READ gamepadConnected NOTIFY gamepadConnectedChanged)

public:
  enum class ScanError {
    DirectoryCreationFailed,
    PathIsAlreadyAFile,
  };
  Q_ENUM(ScanError)

public:
  explicit OnboardingPagesController(app::Settings& settings, app::Navigation& navigation,
    quests::LocalQuestListModel& questListModel, QObject* parent = nullptr);
  ~OnboardingPagesController() = default;

public:
  const QString& questDirectory() const;
  bool onboardingDone() const;
  bool gamepadConnected() const;

public slots:
  void setQuestDirectory(const QString& value);
  void setOnboardingDone(bool value);

  QString getDefaultQuestDirectory() const;

  void skipOnboarding();
  void startScan();
  void stopScan();

signals:
  void scanFailed(const ScanError error);
  void questDirectoryChanged();
  void onboardingDoneChanged();
  void gamepadConnectedChanged();

  void scanStarted();
  void scanProgressChanged(int progress);
  void scanFinished();

private:
  QString _questDirectory;
  app::Settings& _settings;
  app::Navigation& _navigation;
  quests::LocalQuestListModel& _questListModel;
  QGamepad _gamepad;
};
} // namespace solarus::launcher::controllers
