/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>

#include <solarus/launcher/quests/update/QuestUpdate.h>
#include <solarus/launcher/quests/update/QuestUpdater.h>
#include <solarus/launcher/quests/update/QuestUpdateListModel.h>
#include <solarus/launcher/quests/update/QuestUpdateProxyModel.h>

namespace solarus::launcher::controllers {
/**
 * @brief Controller for the QuestUpdate page.
 */
class QuestUpdatePageController : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::quests::QuestUpdateListModel* questUpdateListModel READ questUpdateListModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::QuestUpdateProxyModel* availableQuestUpdateProxyModel READ
      availableQuestUpdateProxyModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::QuestUpdateProxyModel* pendingQuestUpdateProxyModel READ
      pendingQuestUpdateProxyModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::QuestUpdateProxyModel* updatingQuestUpdateProxyModel READ
      updatingQuestUpdateProxyModel CONSTANT)
  Q_PROPERTY(solarus::launcher::quests::QuestUpdateProxyModel* errorQuestUpdateProxyModel READ
      errorQuestUpdateProxyModel CONSTANT)
  Q_PROPERTY(QDateTime lastQuestUpdateCheckTime READ lastQuestUpdateCheckTime NOTIFY lastQuestUpdateCheckTimeChanged)

public:
  QuestUpdatePageController(quests::QuestUpdater& questUpdater, QObject* parent = nullptr);
  ~QuestUpdatePageController() = default;

public:
  quests::QuestUpdateListModel* questUpdateListModel() const;

  quests::QuestUpdateProxyModel* availableQuestUpdateProxyModel() const;
  quests::QuestUpdateProxyModel* pendingQuestUpdateProxyModel() const;
  quests::QuestUpdateProxyModel* updatingQuestUpdateProxyModel() const;
  quests::QuestUpdateProxyModel* errorQuestUpdateProxyModel() const;

  QDateTime lastQuestUpdateCheckTime() const;
  bool hasUpdates() const;

public slots:
  void checkForUpdates();
  void updateAllAvailable();
  void cancelAllPending();
  void cancelAllUpdating();
  void retryAllFailed();
  void updateQuest(solarus::launcher::quests::QuestUpdate* update);
  void cancelQuestUpdate(solarus::launcher::quests::QuestUpdate* update);

signals:
  void lastQuestUpdateCheckTimeChanged();
  void hasUpdatesChanged();

private:
  quests::QuestUpdater& _questUpdater;
  quests::QuestUpdateListModel* _questUpdateListModel;
  quests::QuestUpdateProxyModel* _availableQuestUpdateProxyModel;
  quests::QuestUpdateProxyModel* _pendingQuestUpdateProxyModel;
  quests::QuestUpdateProxyModel* _updatingQuestUpdateProxyModel;
  quests::QuestUpdateProxyModel* _errorQuestUpdateProxyModel;
};
} // namespace solarus::launcher::controllers
