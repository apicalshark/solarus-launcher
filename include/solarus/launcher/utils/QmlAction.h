/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QAction>
#include <functional>

namespace solarus::launcher::utils {
/**
 * @brief Since QAction isn't properly exposed to QML, this class is used as a
 * basic object to replace QAction and expose its most useful features to
 * Qt.Labs.Platform.MenuItem.
 * To be completed as needed.
 */
class QmlAction : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
  Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
  Q_PROPERTY(bool visible READ visible WRITE setVisible NOTIFY visibleChanged)
  Q_PROPERTY(bool checkable READ checkable WRITE setCheckable NOTIFY checkableChanged)
  Q_PROPERTY(bool checked READ checked WRITE setChecked NOTIFY checkedChanged)
  Q_PROPERTY(QString shortcut READ shortcut WRITE setShortcut NOTIFY shortcutChanged)
  Q_PROPERTY(QAction::MenuRole role READ role WRITE setRole NOTIFY roleChanged);

public:
  QmlAction(QObject* parent = nullptr);
  QmlAction(const QString& text, const QString& shortcut, const QAction::MenuRole role, QObject* parent = nullptr);

public:
  const QString& text() const;
  bool enabled() const;
  bool visible() const;
  bool checkable() const;
  bool checked() const;
  const QString& shortcut() const;
  QAction::MenuRole role() const;

  void setCallback(const std::function<void()>& callback);
  void setCheckedPredicate(const std::function<bool()>& predicate);
  void setEnabledPredicate(const std::function<bool()>& predicate);
  void setVisiblePredicate(const std::function<bool()>& predicate);

public slots:
  void setText(const QString& text);
  void setEnabled(bool enabled);
  void setVisible(bool visible);
  void setCheckable(bool checkable);
  void setChecked(bool checked);
  void setShortcut(const QString& shortcut);
  void setShortcutFromKey(const QKeySequence& keySeq);
  void setRole(QAction::MenuRole role);

  void trigger();
  void updateChecked();
  void updateEnabled();
  void updateVisible();

signals:
  void textChanged();
  void enabledChanged();
  void checkableChanged();
  void checkedChanged();
  void visibleChanged();
  void shortcutChanged();
  void roleChanged();

  void triggered();

private:
  QString _text;
  bool _enabled{ true };
  bool _visible{ true };
  bool _checkable{ false };
  bool _checked{ false };
  QString _shortcut;
  QAction::MenuRole _role{ QAction::MenuRole::NoRole };
  std::function<void()> _callback;
  std::function<bool()> _checkedPredicate;
  std::function<bool()> _enabledPredicate;
  std::function<bool()> _visiblePredicate;
};
} // namespace solarus::launcher::utils
