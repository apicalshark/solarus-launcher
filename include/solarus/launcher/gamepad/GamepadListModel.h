/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QAbstractListModel>
#include <QMetaObject>
#include <QVector>

namespace solarus::launcher::gamepad {
class GamepadListModel : public QAbstractListModel {
  Q_OBJECT

  Q_PROPERTY(int count READ count NOTIFY countChanged)

public:
  enum NewsInfoRoles : int {
    DeviceIdRole = Qt::UserRole + 1,
    DeviceNameRole,
  };

  explicit GamepadListModel(QObject* parent = nullptr);
  ~GamepadListModel();

public:
  virtual int rowCount(const QModelIndex& index = QModelIndex()) const override;
  virtual QVariant data(const QModelIndex& index, int role) const override;
  virtual QHash<int, QByteArray> roleNames() const override;

public:
  int count() const;
  Q_SIGNAL void countChanged();

public slots:
  void refresh();

private:
  struct GamepadInfo {
    int deviceId;
    QString deviceName;
  };
  QVector<GamepadInfo> _gamepads;
};
} // namespace solarus::launcher::gamepad

Q_DECLARE_METATYPE(solarus::launcher::gamepad::GamepadListModel*);
Q_DECLARE_METATYPE(const solarus::launcher::gamepad::GamepadListModel*);
