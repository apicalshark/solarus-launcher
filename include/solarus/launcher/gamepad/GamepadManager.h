/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include <QGamepadManager>
#include <QGamepad>
#include <QGamepadKeyNavigation>

#include <solarus/launcher/gamepad/Gamepad.h>
#include <solarus/launcher/gamepad/GamepadListModel.h>

namespace solarus::launcher::gamepad {
/**
* @brief Handles all gamepad-related stuff.
*/
class GamepadManager : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::gamepad::Gamepad* gamepad READ gamepad WRITE setGamepad NOTIFY gamepadChanged)
  Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
  Q_PROPERTY(int currentDeviceId READ currentDeviceId WRITE setCurrentDeviceId NOTIFY currentDeviceIdChanged)
  Q_PROPERTY(QString currentDeviceName READ currentDeviceName NOTIFY currentDeviceNameChanged)
  Q_PROPERTY(solarus::launcher::gamepad::GamepadListModel* deviceListModel READ deviceListModel CONSTANT)

public:
  explicit GamepadManager(QObject* parent = nullptr);
  ~GamepadManager();

public:
  Gamepad* gamepad() const;
  void setGamepad(Gamepad* gamepad);
  Q_SIGNAL void gamepadChanged();

  bool connected() const;
  Q_SIGNAL void connectedChanged();

  int currentDeviceId() const;
  void setCurrentDeviceId(int id);
  Q_SIGNAL void currentDeviceIdChanged();

  QString currentDeviceName() const;
  Q_SIGNAL void currentDeviceNameChanged();

  GamepadListModel* deviceListModel() const;

private:
  QVector<GamepadAction*> _actions;
  QGamepad _qGamepad;
  QGamepadKeyNavigation _qGamepadKeyNavigation;
  GamepadListModel _deviceListModel;
  QPointer<Gamepad> _gamepad{ nullptr };
};
} // namespace solarus::launcher::gamepad
