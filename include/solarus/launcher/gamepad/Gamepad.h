/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include <QGamepadManager>
#include <QGamepad>
#include <QtQml/qqml.h>

#include <solarus/launcher/gamepad/GamepadAction.h>

namespace solarus::launcher::gamepad {
/**
* @brief Information about the gamepad buttons actions.
*/
class Gamepad : public QObject {
  Q_OBJECT

  Q_PROPERTY(QQmlListProperty<solarus::launcher::gamepad::GamepadAction> actions READ actions CONSTANT)
  QML_ELEMENT

public:
  explicit Gamepad(QObject* parent = nullptr);
  ~Gamepad();

public:
  QQmlListProperty<GamepadAction> actions();

  void appendItem(GamepadAction* item);
  int itemCount() const;
  GamepadAction* itemAt(int index) const;
  void clearItems();
  void removeLastItem();
  void replaceItem(int index, GamepadAction* item);

private:
  static void appendItem(QQmlListProperty<GamepadAction>* list, GamepadAction* item);
  static int itemCount(QQmlListProperty<GamepadAction>* list);
  static GamepadAction* itemAt(QQmlListProperty<GamepadAction>* list, int index);
  static void clearItems(QQmlListProperty<GamepadAction>* list);
  static void removeLastItem(QQmlListProperty<GamepadAction>* list);
  static void replaceItem(QQmlListProperty<GamepadAction>* list, int index, GamepadAction* item);

private:
  QVector<GamepadAction*> _actions;
};
} // namespace solarus::launcher::gamepad

Q_DECLARE_METATYPE(solarus::launcher::gamepad::Gamepad*);
Q_DECLARE_METATYPE(const solarus::launcher::gamepad::Gamepad*);
