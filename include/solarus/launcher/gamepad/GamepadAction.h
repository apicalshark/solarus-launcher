/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QString>
#include <QtQml/qqml.h>

namespace solarus::launcher::gamepad {
/**
 * @brief Information about a gamepad button action.
 */
class GamepadAction : public QObject {
  Q_OBJECT

  Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
  Q_PROPERTY(solarus::launcher::gamepad::GamepadAction::Button button READ button WRITE setButton NOTIFY buttonChanged)
  QML_ELEMENT

public:
  enum class Button {
    None,
    A,
    B,
    X,
    Y,
    R1,
    L1,
    Left,
    Top,
    Right,
    Bottom,
    Start,
  };
  Q_ENUM(Button)

public:
  explicit GamepadAction(QObject* parent = nullptr);
  GamepadAction(const QString& text, const Button button, QObject* parent = nullptr);
  ~GamepadAction();

public:
  const QString& text() const;
  Q_SLOT void setText(const QString& text);
  Q_SIGNAL void textChanged();

  Button button() const;
  Q_SLOT void setButton(Button button);
  Q_SIGNAL void buttonChanged();

private:
  QString _text;
  Button _button{ Button::A };
};
} // namespace solarus::launcher::gamepad

Q_DECLARE_METATYPE(solarus::launcher::gamepad::GamepadAction*);
Q_DECLARE_METATYPE(const solarus::launcher::gamepad::GamepadAction*);
Q_DECLARE_METATYPE(solarus::launcher::gamepad::GamepadAction::Button);
