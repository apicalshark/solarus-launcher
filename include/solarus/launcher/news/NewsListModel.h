/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <oclero/QtDownloader.hpp>
#include <solarus/launcher/news/News.h>

#include <QAbstractListModel>
#include <QMetaObject>
#include <QVector>

namespace solarus::launcher::news {
class NewsListModel : public QAbstractListModel {
  Q_OBJECT

  Q_PROPERTY(int newsCount READ newsCount NOTIFY newsCountChanged)
  Q_PROPERTY(int maxNewsCount READ maxNewsCount WRITE setMaxNewsCount NOTIFY maxNewsCountChanged)
  Q_PROPERTY(QString endpointUrl READ endpointUrl WRITE setEndpointUrl NOTIFY endpointUrlChanged)
  Q_PROPERTY(solarus::launcher::news::NewsListModel::State state READ state NOTIFY stateChanged)

public:
  enum NewsInfoRoles : int {
    IsValidRole = Qt::UserRole + 1,
    UrlRole,
    TitleRole,
    DateRole,
    ExcerptRole,
    CoverUrlRole,
  };

  enum class State : int {
    Idle,
    Loading,
    Error,
  };
  Q_ENUM(State)

  explicit NewsListModel(QObject* parent = nullptr);
  ~NewsListModel();

public:
  virtual int rowCount(const QModelIndex& index = QModelIndex()) const override;
  virtual QVariant data(const QModelIndex& index, int role) const override;
  virtual QHash<int, QByteArray> roleNames() const override;

  const QString& endpointUrl() const;
  void setEndpointUrl(const QString& url);

  State state() const;

public slots:
  int newsCount() const;

  int maxNewsCount() const;
  void setMaxNewsCount(int maxNewsCount);

  void reset();
  void refresh();

signals:
  void newsCountChanged();
  void maxNewsCountChanged();
  void endpointUrlChanged();
  void stateChanged();
  void progressChanged(int progress);

private:
  bool updateNewsList(const QByteArray& data);
  void setState(State const state);

private:
  State _state{ State::Idle };
  QString _endpointUrl;
  QVector<News> _news;
  int _maxNewsCount{ 3 };
  oclero::QtDownloader _downloader;
};
} // namespace solarus::launcher::news
