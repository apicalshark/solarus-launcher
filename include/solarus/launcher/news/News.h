/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QDate>
#include <QString>
#include <QUrl>
#include <QPixmap>

class QJsonObject;

namespace solarus::launcher::news {
/**
 * @brief This struct represents the data read in the JSON file.
 */
struct News {
  /**
   * @brief True if the data is valid, false otherwise.
   **/
  bool isValid{ false };
  /**
   * @brief URL to the news article online.
   */
  QUrl url;
  /**
   * @brief Title of the news article.
   */
  QString title;
  /**
   * @brief Excerpt of the news article.
   */
  QString excerpt;
  /**
   * @brief Publish date of the news article.
   */
  QDate date;
  /**
   * @brief URL to the thumbnail image online.
   */
  QUrl thumbnailUrl;

  /**
   * @brief Creates a News object from a JSON object.
   * @return A News.
   */
  static News fromJson(const QJsonObject& jsonObject, const QString& urlDomain = {});
};
} // namespace solarus::launcher::news
