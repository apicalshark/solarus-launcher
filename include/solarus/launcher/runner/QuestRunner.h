/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QPointer>
#include <QProcess>
#include <QTimer>
#include <QUrl>

#include <solarus/launcher/app/Settings.h>
#include <solarus/launcher/quests/local/LocalQuest.h>
#include <solarus/launcher/runner/QuestOutputHandler.h>

namespace solarus::launcher::runner {
/**
 * @brief Run a quest in a dedicated process.
 * Basically a wrapper around QProcess.
 */
class QuestRunner : public QObject {
  Q_OBJECT

public:
  /**
   * @brief Possible states of the runner.
   */
  enum class State {
    Stopped,
    Starting,
    Running,
  };
  Q_ENUM(State)

  /**
   * @brief Types of error the runner can raise.
   */
  enum class ErrorCode {
    NoError,
    UnknownError,
    ProcessFailedToStart,
    ProcessCrashed,
    ProcessWriteError,
    ProcessReadError,
    ProcessTimedOut,
  };
  Q_ENUM(ErrorCode)

  explicit QuestRunner(app::Settings& settings, QObject* parent = nullptr);
  ~QuestRunner();

public:
  /**
   * @brief Current state of the runner.
   * @return true if a quest is running, false otherwise.
   */
  State state() const;

  /**
   * @brief Path to the currently running quest.
   * @return The path to the currently running quest.
   */
  quests::LocalQuest* quest() const;

  /**
   * @brief Returns the object that handles the IO for the quest.
   * @return The QuestOutputHandler.
   */
  QuestOutputHandler& outputHandler() const;

  /**
   * @brief Returns the full console output of the currently playing quest,
   * or an empty string if no quest is playing.
   * @return The full console output of the currently playing quest.
   */
  const QString& fullOutput() const;

public slots:
  /**
   * @brief Runs the Solarus quest that has the path in parameter.
   * @param questPath The path to the Solarus quest to run.
   */
  void start(quests::LocalQuest* quest);

  /**
   * @brief Stops the Solarus quest that is running, if any.
   */
  void stop();

  /**
   * @brief Executes a Lua command.
   * @param command The Lua command to execute
   * @return The unique id of the command
   */
  int executeCommand(const QString& command);

private slots:
  void onProcessError(QProcess::ProcessError error);
  void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void onProcessStandardOutputAvailable();
  void onProcessStateChanged(QProcess::ProcessState state);
  void onTimerTimeout();

signals:
  void aboutToStop();

  /**
   * @brief Signal emitted when the runner's state has changed.
   */
  void stateChanged();

  /**
   * @brief Signal emitted when the runner has encountered an error.
   * @param error The type of error.
   */
  void errorRaised(ErrorCode error);

  /**
   * @brief Signal emitted when the currently running quest has changed.
   */
  void questChanged();

private:
  void setQuest(quests::LocalQuest* quest);
  std::pair<QString, QStringList> createArguments(const QString& questPath) const;

private:
  QProcess _process;
  QuestOutputHandler _questOutputHandler;
  QString _questFullOutput;
  app::Settings& _settings;
  QTimer _timer;
  int _lastCommandId{ -1 };
  QPointer<quests::LocalQuest> _quest{ nullptr };
};
} // namespace solarus::launcher::runner
