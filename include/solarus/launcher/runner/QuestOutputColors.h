/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QObject>
#include <QColor>
#include <qqml.h>

namespace solarus::launcher::runner {
class QuestOutputColors : public QObject {
  Q_OBJECT

  Q_PROPERTY(QColor debugColor READ debugColor WRITE setDebugColor NOTIFY debugColorChanged)
  Q_PROPERTY(QColor infoColor READ infoColor WRITE setInfoColor NOTIFY infoColorChanged)
  Q_PROPERTY(QColor warningColor READ warningColor WRITE setWarningColor NOTIFY warningColorChanged)
  Q_PROPERTY(QColor errorColor READ errorColor WRITE setErrorColor NOTIFY errorColorChanged)
  Q_PROPERTY(QColor fatalColor READ fatalColor WRITE setFatalColor NOTIFY fatalColorChanged)
  QML_ELEMENT

public:
  explicit QuestOutputColors(QObject* parent = nullptr);
  ~QuestOutputColors() = default;

public:
  const QColor& debugColor() const;
  void setDebugColor(const QColor& color);

  const QColor& infoColor() const;
  void setInfoColor(const QColor& color);

  const QColor& warningColor() const;
  void setWarningColor(const QColor& color);

  const QColor& errorColor() const;
  void setErrorColor(const QColor& color);

  const QColor& fatalColor() const;
  void setFatalColor(const QColor& color);

signals:
  void debugColorChanged();
  void infoColorChanged();
  void warningColorChanged();
  void errorColorChanged();
  void fatalColorChanged();

private:
  QColor _debugColor{ 255, 255, 255, 127 };
  QColor _infoColor{ 126, 160, 255 };
  QColor _warningColor{ 255, 188, 0 };
  QColor _errorColor{ 252, 72, 80 };
  QColor _fatalColor{ 252, 72, 80 };
};
} // namespace solarus::launcher::runner

Q_DECLARE_METATYPE(solarus::launcher::runner::QuestOutputColors*);
