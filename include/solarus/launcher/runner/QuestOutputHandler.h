/*
 * This file is part of Solarus Launcher.
 * <https://www.solarus-games.org>
 *
 * Copyright (c) 2021 Solarus.
 *
 * Solarus Launcher is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Solarus Launcher is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <QMap>
#include <QObject>
#include <QPointer>
#include <QRegularExpression>

#include <solarus/launcher/runner/QuestOutputColors.h>

namespace solarus::launcher::runner {
/**
 * @brief Analyzes and colorizes the quest console logs,
 * and produces html that any html-compatible UI can display.
 */
class QuestOutputHandler : public QObject {
  Q_OBJECT

  Q_PROPERTY(solarus::launcher::runner::QuestOutputColors* outputColors READ outputColors WRITE setOutputColors NOTIFY
      outputColorsChanged)

public:
  QuestOutputHandler(QObject* parent = nullptr);
  ~QuestOutputHandler();

public:
  QuestOutputColors* outputColors() const;
  Q_SLOT void setOutputColors(QuestOutputColors* outputColors);

  void onOutputProduced(const QStringList& lines);
  void onStateChanged(bool playing);

private:
  /**
   * @brief Parses a Solarus output line and handles it.
   * @param line The output line.
   */
  void parseAndPrintLine(const QString& line);

  /**
   * @brief Detects output messages that are the result of a command that was
   * send from the console.
   * @param log_level The Solarus log level of the line.
   * @param message The rest of the message.
   * @return @c true if the message is a command result delimiter and was consumed.
   */
  bool detectCommandResult(const QString& logLevel, const QString& message);

  /**
   * @brief Emits the signal questSettingChanged if an output message
   * indicates that a setting has just changed.
   * @param log_level The Solarus log level of the line.
   * @param message The rest of the message.
   */
  void detectSettingChange(const QString& logLevel, const QString& message);

  /**
   * @brief Returns a colorized version of a Solarus output line.
   * Colors may be added.
   * @param log_level The Solarus log level of the line.
   * @param message The rest of the message.
   * @return The HTML decorated line.
   */
  QString colorizeOutput(const QString& logLevel, const QString& message) const;

  /**
   * @brief Wraps a line of plain text in html color tags.
   * @param line A plain text line.
   * @param color The color to set, with "#rrggbb" syntax.
   * @return The HTML colorized line.
   */
  QString colorize(const QString& line, const QString& color) const;

signals:
  void htmlProduced(const QString& html);
  void commandResultReceived(int id, const QString& command, bool success, const QString& result);
  void questSettingChanged(const QString& key, const QVariant& value);
  void outputCleared();
  void outputColorsChanged();

private:
  /** Commands for which we are waiting a result. */
  QMap<int, QString> _pendingCommands;
  /** Id of the command we are reading the result of (-1 if none). */
  int _outputCommandId = -1;
  /** Partial result of the command. */
  QString _outputCommandResult;
  /** Whether the user can type Lua commands. */
  bool _commandEnabled{ false };
  /** Colors used as HTML style */
  QPointer<QuestOutputColors> _outputColors;
};
} // namespace solarus::launcher::runner
